//
//  AppDelegate+Home.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/9.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "AppDelegate+home.h"

#import "LoadViewController.h"

#import "HomeViewController.h"
#import "HomeEventViewController.h"
#import "HomeLeftViewController.h"
#import "HomeEventRightViewController.h"
#import "ScrollViewController.h"

#define kFirstLoading @"k_first_loading"

@implementation AppDelegate(home)


- (void)homeApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GB_goMain)
                                                 name:@"GB_GoMain"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GB_reStart)
                                                 name:@"GB_ReStart"
                                               object:nil];
    LoadViewController *load = [[LoadViewController alloc]init];
    load.view.tag = -1;
    [self.window addSubview:load.view];
}


-(void)GB_goMain{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDidStopSelector:@selector(animation_stop)];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:.1f];
    [[self.window viewWithTag:-1] setAlpha:0];
    [UIView commitAnimations];
}

-(void)GB_reStart
{
    HomeLeftViewController *left = [[HomeLeftViewController alloc] init];
    HomeEventRightViewController *right = [[HomeEventRightViewController alloc]init];
    left.eventRightViewController = right;
    HomeEventViewController *con = [[HomeEventViewController alloc] init];
    left.eventListViewController = con;
    HomeViewController *rootViewController = [[HomeViewController alloc] initWithLeftViewController:left centerViewController:con rightViewController:right];
    [PublicDao shareInstance].rootViewController = rootViewController;
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:rootViewController];
    navController.navigationBar.hidden = YES;
    self.navController = navController;
    self.window.rootViewController = navController;
    [GB_NetWorkUtils cancelRequest];
    [navController release];
    if([GB_SharedPreferenceUtils hasObject:kFirstLoading]){
        [rootViewController checkLogin];
    }
    else{
        [GB_NetWorkUtils cancelRequest];
        [GB_SharedPreferenceUtils setObject:@"ok" forKey:kFirstLoading];
        ScrollViewController *scrollView = [[ScrollViewController alloc]init];
        scrollView.root = rootViewController;
        scrollView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [navController presentViewController:scrollView animated:NO completion:nil];
        [scrollView release];
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings
                                                                             settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                                                             categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        UIRemoteNotificationType types =
        (UIRemoteNotificationTypeBadge
         |UIRemoteNotificationTypeSound
         |UIRemoteNotificationTypeAlert);
        //注册消息推送
        [[UIApplication sharedApplication]registerForRemoteNotificationTypes:types];
    }
    
    
    [rootViewController release];
}

-(void)animation_stop{
    [[self.window viewWithTag:-1]removeFromSuperview];
    [self GB_reStart];
}



@end

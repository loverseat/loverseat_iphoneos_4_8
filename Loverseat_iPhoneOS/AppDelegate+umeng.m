//
//  AppDelegate+UMeng.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/9.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "AppDelegate+umeng.h"
#import "UMSocialSinaHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialWechatHandler.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import "MobClick.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"

@implementation AppDelegate (umeng)


- (void)umengApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UMSocialData setAppKey:@"53cf1e1156240b9e03075285"];
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    [UMSocialQQHandler setQQWithAppId:@"1103704469" appKey:@"xdY3t9jJ2O2Pij3u" url:@"http://www.7dianban.cn/"];
#pragma mark - 友盟
    [MobClick startWithAppkey:@"53cf1e1156240b9e03075285" reportPolicy:BATCH   channelId:@"App Store"];
    
    [UMSocialWechatHandler setWXAppId:@"wx91bdfcaa294386d7" appSecret:@"8af770db2cfe11a25f5cc67f1f6484f7" url:@"http://www.7dianban.cn/"];

}

- (BOOL)umengApplication:(UIApplication *)application handleOpenURLU:(NSURL *)url {
    return [UMSocialSnsService handleOpenURL:url];
}

- (BOOL)umengApplication:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [UMSocialSnsService handleOpenURL:url];
}

@end

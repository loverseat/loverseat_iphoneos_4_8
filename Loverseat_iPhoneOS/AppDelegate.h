//
//  AppDelegate.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeEventViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) NSDictionary *userInfo;


@property (nonatomic, strong) HomeEventViewController *eventListVC;

@end

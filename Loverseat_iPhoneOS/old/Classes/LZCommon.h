#import"EMSDKFull.h" //  包含所有功能。如果你的app中不需要实时语音功能
#import "WCAlertView.h"
#import "TTGlobalUICommon.h"
#import "UIViewController+HUD.h"
#import "UIViewController+DismissKeyboard.h"
#import "NSString+Valid.h"
#import "UIView+LZ.h"
#import "MBProgressHUD+MJ.h"
#import "MJRefresh.h"
#import "ToolClass.h"
#import "UIImageView+WebCache.h"
#import "LZHttpTool.h"
#import "UIImage+LZ.h"

// 1.颜色
#define LZColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

// 2.随机色
#define LZRandomColor LZColor(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256))

// 3.是否为iOS7
#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)
#define iOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0

// 4.自定义Log
#ifdef DEBUG // 调试状态, 打开LOG功能
#define LZLog(...) NSLog(__VA_ARGS__)
#else // 发布状态, 关闭LOG功能
#define LZLog(...)
#endif


// 5.屏幕尺寸(宽高度)
#define LZScreenW [UIScreen mainScreen].bounds.size.width
#define LZScreenH [UIScreen mainScreen].bounds.size.height

// 通知中心
#define LZNotificationCenter [NSNotificationCenter defaultCenter]



#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define KNOTIFICATION_LOGINCHANGE @"loginStateChange"

#define CHATVIEWBACKGROUNDCOLOR [UIColor colorWithRed:0.936 green:0.932 blue:0.907 alpha:1]


// 提醒数字
#define badgeValueNumber @"badgeValueNumber"


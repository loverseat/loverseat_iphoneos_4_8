//
//  LZFinishViewController.m
//  Loverseat_iPhoneOS
//
//  Created by nacker on 15/3/17.
//  Copyright (c) 2015年 LEIZHEN. All rights reserved.
//

#import "LZFinishViewController.h"
#import "User.h"
#import "EventBean.h"


@interface LZFinishViewController ()

@property (nonatomic, strong) EventBean *eventBean;
@property (nonatomic, assign) float eventContentY;
@property (nonatomic, weak) UILabel *descLabel;
@property (nonatomic, weak) UIView *contentView;
@property (nonatomic, weak) UIView *commentView;
@property (nonatomic, weak) UIButton *openBtn;
@property (nonatomic, weak) UIView *commentTool;
@property (nonatomic, weak) UILabel *commentLabel;

@property (nonatomic, assign) int zanIndex;
@property (nonatomic, assign) int caiIndex;
@property (nonatomic, weak) UIImageView *image;

@property (nonatomic, strong) NSMutableArray *statuses;

@end

@implementation LZFinishViewController

- (NSMutableArray *)statuses
{
    if (_statuses == nil)
    {
        self.statuses = [NSMutableArray array];
    }
    return _statuses;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [NavUtils addNavBgView:self];
    [NavUtils addBackButton:self];
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = LZRandomColor;
    scrollView.frame = CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]);
    [self.view addSubview:scrollView];


    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenWidth]*(330.0f/300.0f))];
    image.backgroundColor = [UIColor blackColor];
    
//    [image la];
    //[Url getImageUrl:<#(NSString *)#>];
    
    [scrollView addSubview:image];
    
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 40.0f, [NavUtils getNavHeight]) image:nil imageH:nil id:self sel:@selector(cb_back)];
    [navBackBtn setImage:[UIImage imageNamed:@"btn_back_gray"] forState:UIControlStateNormal];
    [scrollView addSubview:navBackBtn];
    self.leftBtn = navBackBtn;
    
    
    [self setupData];
}

- (void)setupData
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"event_id"] = self.event_id;
    params[@"uid"]= [User getUserInfo].uid;
    
    [LZHttpTool post:@"http://www.7dianban.cn/showapi_v3/get_event_detail" params:params success:^(id json) {
        
        //LZLog(@"%@",json);
        
        NSArray *statusDictArray = json[@"msg"];
        
        [self.statuses addObjectsFromArray:statusDictArray];
        
        LZLog(@"%@",self.statuses);
        
    } failure:^(NSError *error) {
        
        LZLog(@"%@",error);
        
    }];
}

@end

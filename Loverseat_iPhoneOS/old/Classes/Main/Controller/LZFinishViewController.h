//
//  LZFinishViewController.h
//  Loverseat_iPhoneOS
//
//  Created by nacker on 15/3/17.
//  Copyright (c) 2015年 LEIZHEN. All rights reserved.
//

#import "BaseViewController.h"

@interface LZFinishViewController : BaseViewController

@property (nonatomic, strong) NSString *event_id;

@end

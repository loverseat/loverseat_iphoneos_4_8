/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ChatViewController : BaseViewController

- (instancetype)initWithChatter:(NSString *)chatter isGroup:(BOOL)isGroup;
- (void)reloadData;

@property (nonatomic, strong) NSString *fromId;
@property (strong, nonatomic) NSString *name;

@property (nonatomic, strong) NSString *tu_portrait;

@property (nonatomic, strong) NSMutableDictionary *iconDictionary;

@property (nonatomic, strong) NSString *OtherIconStr;

@property (nonatomic, strong) NSString *chatters;
@end

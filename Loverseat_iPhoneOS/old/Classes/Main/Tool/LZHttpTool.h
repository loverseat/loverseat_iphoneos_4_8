//
//  LZHttpTool.h
//  黑马二期
//
//  Created by nacker on 15/2/21.
//  Copyright (c) 2015年 帶頭二哥. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LZHttpTool : NSObject

/** GET请求 */
+ (void)get:(NSString *)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSError *error))failure;

/** POST请求 */
+ (void)post:(NSString *)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSError *error))failure;
@end

//
//  ToolClass.m
//  leizhen
//
//  Created by Nathan-he on 14-9-26.
//  Copyright (c) 2014年 leizhen. All rights reserved.
//

#import "ToolClass.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@implementation ToolClass

/**
 * 把用户Dictionary存入NSUserDefaults
 * 标示 @“userInfo”
 **/
+ (void)saveUserInfo:(NSDictionary *)user
{
    //将上述数据全部存储到NSUserDefaults中
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setPersistentDomain:user forName:@"userInfo"];
    [userDefaults synchronize];
}

/**
 * 根据标示获取用户的Dictionary
 *
 **/
+ (NSDictionary *) userInfo
{
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    return [userDefaultes persistentDomainForName:@"userInfo"];
}

//记住账号
+ (void)saveUserAccount:(NSString *)account
{
    //将上述数据全部存储到NSUserDefaults中
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:account forKey:@"Account"];
    [userDefaults synchronize];
}

//获取账号
+(NSString *) userAccount
{
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    return [userDefaultes stringForKey:@"Account"];
}
@end

//
//  ToolClass.h
//  leizhen
//
//  Created by Nathan-he on 14-9-26.
//  Copyright (c) 2014年 leizhen. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ToolClass : NSObject
//保存用户信息
+ (void)saveUserInfo:(NSDictionary *)user;
//获取用户信息
+(NSDictionary *) userInfo;
//记住账号
+ (void)saveUserAccount:(NSString *)account;
//获取账号
+(NSString *) userAccount;

@end

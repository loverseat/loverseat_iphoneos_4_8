//
//  DataDefine.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/5.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//  模型

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface DataDefine : NSObject

@end
//今日达人
@interface ExpertInfo: JSONModel
@property (nonatomic,strong)NSString *tu_id;
@property (nonatomic,strong)NSString<Optional> *tu_portrait;
@end
//最近邀约
@interface InviteInfo : JSONModel
@property (nonatomic,strong)NSString *tbi_event_id;
@property (nonatomic,strong)NSString *tbi_portrait;
@property (nonatomic,strong)NSString *tbi_uid;
@end
//全部
@interface TreadsInfo : JSONModel
@property (nonatomic,strong)NSString<Optional> *tut_content;
/** 时间 */
@property (nonatomic,strong)NSString *tut_created;

@property (nonatomic,strong)NSString *tut_tread_id;
/** 动态的类型 */
@property (nonatomic,strong)NSString *tut_type;
/** 别人的用户ID */
@property (nonatomic,strong)NSString *tut_uid;
/** 用户ID */
@property (nonatomic,strong)NSString *tut_id;
/** 生日 */
@property (nonatomic,strong)NSString *tu_birthday;
/** 城市 */
@property (nonatomic,strong)NSString *tu_city;
/** 性别 0 男 1 女 */
@property (nonatomic,strong)NSString *tu_gender;
/** 昵称 */
@property (nonatomic,strong)NSString *tu_nickname;
/** 头像地址 */
@property (nonatomic,strong)NSString *tu_portrait;
@end
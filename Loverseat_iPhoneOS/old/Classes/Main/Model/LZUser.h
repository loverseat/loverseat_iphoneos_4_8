//
//  LZUser.h
//  Loverseat_iPhoneOS
//
//  Created by nacker on 15/3/26.
//  Copyright (c) 2015年 LEIZHEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LZUser : NSObject
@property (nonatomic, copy) NSString *tu_portrait;
@property (nonatomic, copy) NSString *tu_nickname;
- (instancetype)initWithDict:(NSDictionary *)dict;
@end

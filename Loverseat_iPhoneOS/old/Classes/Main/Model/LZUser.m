//
//  LZUser.m
//  Loverseat_iPhoneOS
//
//  Created by nacker on 15/3/26.
//  Copyright (c) 2015年 LEIZHEN. All rights reserved.
//

#import "LZUser.h"

@implementation LZUser
- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.tu_nickname = dict[@"tu_nickname"];
        self.tu_portrait = dict[@"tu_portrait"];
    }
    return self;
}
@end

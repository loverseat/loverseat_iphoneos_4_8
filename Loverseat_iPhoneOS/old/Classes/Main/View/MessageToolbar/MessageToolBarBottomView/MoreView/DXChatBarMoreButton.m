//
//  DXChatBarMoreButton.m
//  环信测试
//
//  Created by nacker on 15/3/20.
//  Copyright (c) 2015年 nacker. All rights reserved.
//

#define DXChatBarMoreButtonImageRatio 0.6

#import "DXChatBarMoreButton.h"


@implementation DXChatBarMoreButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        [self setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
//        self.imageView.layer.cornerRadius = 6;
//        self.imageView.layer.masksToBounds = YES;
//        self.imageView.backgroundColor = [UIColor redColor];

    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted {}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height * DXChatBarMoreButtonImageRatio;
    return CGRectMake(0, 0, imageW, imageH);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = contentRect.size.height * DXChatBarMoreButtonImageRatio;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    return CGRectMake(0, titleY, titleW, titleH);
}

@end

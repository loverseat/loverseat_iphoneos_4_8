//
//  LZBadgeView.h
//  
//
//  Created by nacker on 15/1/10.
//  Copyright (c) 2015年 nacker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LZBadgeView : UIButton
@property (nonatomic, copy) NSString *badgeValue;
@end

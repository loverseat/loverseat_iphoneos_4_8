/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "EMChatTimeCell.h"
#import "MessageModel.h"
#import "EMMessage.h"

@interface EMChatTimeCell()

//@property (nonatomic, weak) UILabel *timeLabel;

@end

@implementation EMChatTimeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    
//    UILabel *timeLabel = [[UILabel alloc] init];
//    //timeLabel.text = @"123123";
//    timeLabel.backgroundColor = [UIColor clearColor];
//    timeLabel.textAlignment = NSTextAlignmentCenter;
//    timeLabel.font = [UIFont systemFontOfSize:14];
//    timeLabel.textColor = [UIColor grayColor];
//    
//    //timeLabel.backgroundColor = [UIColor redColor];
//    [self.contentView addSubview:timeLabel];
//    self.timeLabel = timeLabel;
    
    self.backgroundColor = [UIColor clearColor];
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.font = [UIFont systemFontOfSize:14];
    self.textLabel.textColor = [UIColor grayColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    //self.timeLabel.frame = self.bounds;
    
}
@end

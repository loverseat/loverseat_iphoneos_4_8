//
//  UIImage+LZ.h
//  
//
//  Created by nacker on 14-7-18.
//  Copyright (c) 2014年 nacker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (LZ)

/**
 *  图片名
 */
+ (UIImage *)imageWithName:(NSString *)name;

/**
 *  返回一张自由拉伸的图片
 */
+ (UIImage *)resizedImage:(NSString *)name;


@end

//
//  UIView+LZ.h
//
//
//  Created by teacher on 14-6-6.
//  Copyright (c) 2014年 nacker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LZ)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;
@end

//
//  RecordRightCell.m
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/1.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "RecordRightCell.h"
#import "Static.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "User.h"
#import "DateRecordBean.h"
@interface RecordRightCell()
<GB_LoadImageDelegate>

@end
@implementation RecordRightCell

- (void)awakeFromNib {
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        UIImageView *top = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 10)];
        top.backgroundColor = GB_UIColorFromRGB(245, 245, 245);
        [self addSubview:top];
        
        UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, [GB_DeviceUtils getScreenWidth], 110)];
        bg.backgroundColor = [UIColor whiteColor];
        [self addSubview:bg];
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 64, 94)];
        [bg addSubview:_icon];
        
        self.status = [GB_WidgetUtils getLabel:CGRectMake(0, 74, 64, 20) title:nil font:[Static getFont:24 isBold:NO] color:[UIColor whiteColor]];
        _status.textAlignment = NSTextAlignmentCenter;
        [_icon addSubview:_status];

        
        self.name = [GB_WidgetUtils getLabel:CGRectMake(15+64+15, 8, 100, 20) title:nil font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(80, 80, 80)];
        [bg addSubview:_name];
        
        self.sexImg = [[UIImageView alloc]initWithFrame:CGRectZero];
        [bg addSubview:_sexImg];
        
        UIImageView *iv1 = [[UIImageView alloc]initWithFrame:CGRectMake(15+64+15, self.name.frame.origin.y + self.name.frame.size.height +8, 4, 4)];
        iv1.layer.cornerRadius = iv1.frame.size.width/2;
        iv1.backgroundColor = [UIColor lightGrayColor];
        [bg addSubview:iv1];
        self.showName = [GB_WidgetUtils getLabel:CGRectMake(iv1.frame.origin.x +iv1.frame.size.width + 2, self.name.frame.origin.y +self.name.frame.size.height +3, 200, 14) title:nil font:[Static getFont:25 isBold:NO] color:GB_UIColorFromRGB(140, 140, 140)];
        [bg addSubview:_showName];

        self.venueName = [GB_WidgetUtils getLabel:CGRectMake(self.showName.frame.origin.x, self.showName.frame.origin.y +self.showName.frame.size.height +1, 200, 14) title:nil font:[Static getFont:25 isBold:NO] color:GB_UIColorFromRGB(140, 140, 140)];
        [bg addSubview:_venueName];
        UIImageView *iv2 = [[UIImageView alloc]initWithFrame:CGRectMake(15+64+15, self.venueName.frame.origin.y+5, 4, 4)];
        iv2.layer.cornerRadius = iv1.frame.size.width/2;
        iv2.backgroundColor = [UIColor lightGrayColor];
        [bg addSubview:iv2];
        
        self.time = [GB_WidgetUtils getLabel:CGRectMake(self.venueName.frame.origin.x, self.venueName.frame.origin.y +self.venueName.frame.size.height +1, 200, 14) title:nil font:[Static getFont:25 isBold:NO] color:GB_UIColorFromRGB(140, 140, 140)];
        [bg addSubview:_time];
        
        UIImageView *iv3 = [[UIImageView alloc]initWithFrame:CGRectMake(15+64+15, self.time.frame.origin.y+5, 4, 4)];
        iv3.layer.cornerRadius = iv1.frame.size.width/2;
        iv3.backgroundColor = [UIColor lightGrayColor];
        [bg addSubview:iv3];
        
        UIImageView *money = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_date_record_payment"]];
        money.center = CGPointMake(_name.frame.origin.x+11, 101);
        [self addSubview:money];
        
        
        UIImageView *date = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_date_record_date"]];
        date.center = CGPointMake(money.frame.origin.x+98, money.frame.origin.y+11);
        [self addSubview:date];
        
        
        self.money = [GB_WidgetUtils getLabel:CGRectMake(money.frame.origin.x+28, money.frame.origin.y, 70, 22) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(78, 78, 78)];
        [self addSubview:_money];
        
        
        self.date = [GB_WidgetUtils getLabel:CGRectMake(date.frame.origin.x+28, date.frame.origin.y, 70, 22) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(78, 78, 78)];
        [self addSubview:_date];

    }
    return self;
}
-(void)setDateRecordBean:(DateRecordBean *)bean{
    
    self.name.font = [Static getFont:32 isBold:NO];
    CGSize size = [bean.user_name sizeWithFont:self.name.font constrainedToSize:CGSizeMake(MAXFLOAT, 14)];
    [self.name setFrame:CGRectMake(self.name.frame.origin.x, self.name.frame.origin.y, size.width, self.name.frame.size.height)];
    [self.sexImg setFrame:CGRectMake(self.name.frame.origin.x + self.name.frame.size.width +3, 8, 14, 14)];
    _name.text = bean.user_name;
    switch ([bean.tbi_payment intValue]) {
        case 1:
            _money.text = @"我请客";
            break;
        case 2:
            _money.text = @"AA";
            break;
        case 3:
            _money.text = @"TA请客";
            break;
        case 4:
            _money.text = @"都可以";
            break;
        default:
            _money.text = @"－－";
            break;
    }
    if(bean.gender == 0){
        _sexImg.image = [UIImage imageNamed:@"icon_male"];
    }else if (bean.gender == 1){
        _sexImg.image = [UIImage imageNamed:@"icon_female"];
    }
   
    _showName.text = bean.tad_show_name;
    _venueName.text = bean.venue_name;
    _time.text = [NSString stringWithFormat:@"%@ - %@",[[bean.tad_begin_time substringToIndex:10] stringByReplacingOccurrencesOfString:@"-" withString:@"."],[[bean.tad_end_time substringToIndex:10] stringByReplacingOccurrencesOfString:@"-" withString:@"."]];
    
    if([GB_StringUtils isBlank:bean.tbi_free_time]||[bean.tbi_free_time isEqualToString:@"all"]){
        _date.text = @"都可以";
    }
    else{
        _date.text = bean.tbi_free_time;
    }
    
    [GB_NetWorkUtils loadImage:bean.portrait container:_icon type:GB_ImageCacheTypeAll delegate:self tag:[bean.tbi_id intValue]];
    
    NSString *status;
    UIColor *color;
    if([bean.valid_flag intValue]==0){
        status = @"已结束";
        color = GB_UIColorFromRGB(80, 80, 80);
   }
    else{
        if([bean.tbi_status intValue] == 1){
            status = @"邀约成功";
            color = GB_UIColorFromRGB(251, 80, 87);
        }
        else if([bean.tbi_status intValue] == 0){
            status = @"邀约中";
            color = GB_UIColorFromRGB(80, 177, 251);
        }else if ([bean.tbi_status intValue] == 3){
            status = @"已结束";
            color = GB_UIColorFromRGB(80, 80, 80);
        }
    }
    _status.text = status;
    _status.backgroundColor = color;


}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    [GB_NetWorkUtils useImage:[Static getFImage:image w:_icon.frame.size.width h:_icon.frame.size.height] container:container];
}
@end

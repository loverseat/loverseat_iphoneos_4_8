//
//  ExpertView.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/6.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpertView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *bigImg;
@property (strong, nonatomic) IBOutlet UIImageView *smallImg;

@end

//
//  PersonhallCell.m
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/5.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "PersonhallCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Url.h"
#import "User.h"

@implementation PersonhallCell

- (void)awakeFromNib
{
    self.chatBtn.clipsToBounds = YES;
    self.chatBtn.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 网络请求
- (void)makeCell:(TreadsInfo *)model
{

//    self.nickNameLable.text = model.tu_nickname;
//    
//    if ([self.nickNameLable.text isEqualToString:[User getUserInfo].tu_nickname] ) {
//        self.chatBtn.hidden = YES;
//    }
//    
//    self.gender.image = [UIImage imageNamed:[model.tu_gender isEqualToString:@"1"]?@"icon_index_woman":@"icon_index_man"];
//    if([model.tut_type isEqualToString:@"invite_success"]||[model.tut_type isEqualToString:@"fuyue_success"]||[model.tut_type isEqualToString:@"create_invite"]||[model.tut_type isEqualToString:@"cancel_fuyue"]||[model.tut_type isEqualToString:@"create_fuyue"]){
//        self.typeImg.image = [UIImage imageNamed:@"icon_small_invite"];
//    }else if ([model.tut_type isEqualToString:@"event_like"]){
//        self.typeImg.image = [UIImage imageNamed:@"icon_small_like"];
//    }else if ([model.tut_type isEqualToString:@"update_portrait" ]||[model.tut_type isEqualToString:@"update_profile"]){
//        self.typeImg.image = [UIImage imageNamed:@"icon_small_update"];
//    }else if ([model.tut_type isEqualToString:@"event_add_comment"]){
//        self.typeImg.image = [UIImage imageNamed:@"icon_small_comment"];
//    }
//    [GB_NetWorkUtils loadImage:[Url getImageUrl:model.tu_portrait] container:self.portrait type:GB_ImageCacheTypeAll];
//
//    self.portrait.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(portraitClick:)];
//    [self.portrait addGestureRecognizer:tap];
//    self.createdTimeLable.text = model.tut_created;
//    self.contentLable.text = model.tut_content;
}

#pragma mark - 聊一聊
- (IBAction)chatBtn:(id)sender {
    if(self.delegate && [self.delegate respondsToSelector:@selector(chatButtonClick:)]){
        [self.delegate chatButtonClick:self.tag];
    }
}
- (void)portraitClick:(UITapGestureRecognizer *)tap{
    if(self.delegate && [self.delegate respondsToSelector:@selector(portraitClick:)]){
        [self.delegate portraitClick:self.tag];
    }
}
@end

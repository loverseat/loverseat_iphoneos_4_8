//
//  TestViewController.m
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/1/25.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "TestViewController.h"
#import "InviterCell.h"
#import "FuyueBean.h"
#import "DateAllLeftBean.h"
#import "UserInfoViewController.h"
@interface TestViewController (){
    IBOutlet UITableView *myTableView;
    NSMutableArray *dataArr1;
    NSMutableArray *dataArr2;
    NSString *tbi_id;
    NSString *tad_id;
    int selectIndex;
    BOOL isCancel;
}

@end

@implementation TestViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if(self.bean.mine_flag.intValue == 1){
        if(self.bean.tbi_status.intValue == 0){
            
        }else if (self.bean.tbi_status.intValue == 1){
            
            
        }
        self.inviteBtn.alpha = 0.4f;
    }else{
        if(self.bean.tbi_status.intValue == 1){
            //邀约成功
            self.inviteEnd.hidden = NO;
            self.inviteBtn.hidden = YES;
        }else{
            if(self.bean.invite_flag.intValue == 0){
                //我没约他显示约他
                isCancel = self.bean.invite_flag.intValue;
                [self.inviteBtn setTitle:@"参与邀约" forState:UIControlStateNormal];
                
            }else if (self.bean.invite_flag.intValue == 1){
                //我约了等待回音
                isCancel = self.bean.invite_flag.intValue;
                [self.inviteBtn setTitle:@"取消邀约" forState:UIControlStateNormal];
            }else if (self.bean.invite_flag.intValue == 3){
                //我约了被拒绝
                self.inviteBtn.hidden = YES;
                self.inviteEnd.hidden = NO;
            }
        }
    }
    [MobClick beginLogPageView:@"参与邀约"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"参与邀约"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initFrame];
}
- (void)initFrame{
    [NavUtils addBackWhiteButton:self];
    [NavUtils addNavTitleView:self text:@"邀约详情" color:[UIColor whiteColor]];
    [NavUtils addNavTitleLine:self];
    myTableView.dataSource = self;
    myTableView.delegate = self;
    myTableView.tableHeaderView = self.headView;
    
    [myTableView registerNib:[UINib nibWithNibName:@"InviterCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    dataArr1 = [NSMutableArray array];
    dataArr2 = [NSMutableArray array];
    
    self.portrait.layer.cornerRadius = self.portrait.frame.size.width/2;
    self.portrait.clipsToBounds = YES;
    self.portrait.layer.borderWidth = 2.0f;
    self.portrait.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.midLine.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2f];
    self.smallImg1.layer.cornerRadius = self.smallImg1.frame.size.width/2;
    self.smallImg1.clipsToBounds = YES;
    self.smallImg2.layer.cornerRadius = self.smallImg2.frame.size.width/2;
    self.smallImg2.clipsToBounds = YES;
    self.smallImg3.layer.cornerRadius = self.smallImg3.frame.size.width/2;
    self.smallImg3.clipsToBounds = YES;
    
    
    
    [self.inviteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.inviteBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.inviteBtn.layer.masksToBounds = YES;
    self.inviteBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    self.inviteBtn.layer.cornerRadius = 5;
    self.inviteBtn.layer.borderWidth = 0.5f;

    
    NSDictionary *info = [self.dic objectForKey:@"msg"];
    NSDictionary *userInfo = [info objectForKey:@"current_user"];
    
    
    self.nicknameLable.font = [UIFont systemFontOfSize:14];
    CGSize size = [[userInfo objectForKey:@"tu_nickname"] sizeWithFont:self.nicknameLable.font constrainedToSize:CGSizeMake(MAXFLOAT, self.nicknameLable.frame.size.height)];
    [self.nicknameLable setFrame:CGRectMake(self.nicknameLable.frame.origin.x, self.nicknameLable.frame.origin.y, size.width, self.nicknameLable.frame.size.height)];
    self.nicknameLable.text= [userInfo objectForKey:@"tu_nickname"];
    
    self.gender.frame = CGRectMake(self.nicknameLable.frame.origin.x + self.nicknameLable.frame.size.width +5 , self.gender.frame.origin.y, self.gender.frame.size.width, self.gender.frame.size.height);
    self.gender.image = [UIImage imageNamed:[[userInfo objectForKey:@"tu_gender"]intValue] == 1?@"icon_female":@"icon_male"];
    
//    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
//    [dateFormat setDateFormat:@"yyyy-MM-dd"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
//    NSDate *date =[dateFormat dateFromString:self.bean.tu_birthday];
//    NSString *str =  GB_NSStringFromInt([[GB_DateUtils getFormatStringByNow:@"yyyy"] intValue]-[[GB_DateUtils getFormatStringBy10Median:@"yyyy" timeMillis:[date timeIntervalSince1970]] intValue]);
//    self.starLable.text = [GB_StringUtils isBlank:[userInfo objectForKey:@"tu_star_sign"]]?[NSString stringWithFormat:@"%@岁",str]:[NSString stringWithFormat:@"%@岁 · %@",str,[userInfo objectForKey:@"tu_star_sign"]];
    self.starLable.text = [GB_StringUtils isBlank:[userInfo objectForKey:@"tu_star_sign"]]?[NSString stringWithFormat:@"%d岁",[[userInfo objectForKey:@"tu_birthday"]intValue]]:[NSString stringWithFormat:@"%d岁 · %@",[[userInfo objectForKey:@"tu_birthday"]intValue],[userInfo objectForKey:@"tu_star_sign"]];
    
    [GB_NetWorkUtils loadImage:[Url getImageUrl:[userInfo objectForKey:@"tu_portrait"]] container:self.portrait type:GB_ImageCacheTypeAll];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(portraitClick)];
    self.portrait.userInteractionEnabled = YES;
    [self.portrait addGestureRecognizer:tap];
    
    NSDictionary *inivteInfo = [info objectForKey:@"inivte_info"];
    NSString *payment = [inivteInfo objectForKey:@"tbi_payment"];
    NSString *freeTime = [inivteInfo objectForKey:@"tbi_free_time"];
    tbi_id = [inivteInfo objectForKey:@"tbi_id"];
    
    if([payment isEqualToString:@"1"]){
        self.paymentLable.text = @"我请客";
    }else if ([payment isEqualToString:@"2"]){
        self.paymentLable.text = @"AA";
    }else if ([payment isEqualToString:@"3"]){
        self.paymentLable.text = @"TA请客";
    }else {
        self.paymentLable.text = @"都可以";
    }
    
    if([freeTime isEqualToString:@"all"]||[GB_StringUtils isBlank:freeTime])
        self.freetimeLable.text = @"都可以";
    else{
        self.freetimeLable.text = freeTime;
    }
    
    NSDictionary *eventInfo = [info objectForKey:@"event_info"];
    tad_id = [eventInfo objectForKey:@"tad_id"];
    self.showNameLable.text = [eventInfo objectForKey:@"tad_show_name"];
    self.venueNameLable.text = [eventInfo objectForKey:@"tad_venue_name"];
    self.timeLable.text = [NSString stringWithFormat:@"%@ - %@",[eventInfo objectForKey:@"tad_begin_time"],[eventInfo objectForKey:@"tad_end_time"]];
    
    NSArray *arr = [info objectForKey:@"fuyue_info"];
    for(NSDictionary *dic in arr){
        FuyueBean *bean = [FuyueBean getBean:dic];
        [dataArr1 addObject:bean];
    }
}
#pragma mark--tableView的代理方法
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InviterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    FuyueBean *bean = [dataArr1 objectAtIndex:indexPath.row];
    [cell makeCell:bean];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr1.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

//头像点击事件
- (void)portraitClick{
    UserInfoViewController *user = [[UserInfoViewController alloc]init];
    user.uid = self.bean.tbi_uid;
    [self.navigationController pushViewController:user animated:YES];
}
- (IBAction)inviteClick:(UIButton *)sender {
    if(isCancel){
        UIAlertView *alertView1 = [[UIAlertView alloc]initWithTitle:@"您要取消对TA的邀约" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView1.tag = 2;
        [alertView1 show];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"给他发送邀请语吧" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        UITextField *userTextField = [alertView textFieldAtIndex:0];
        userTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        userTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        userTextField.textAlignment = NSTextAlignmentCenter;
        userTextField.placeholder = @"请输入邀请语";
        alertView.tag = 1;
        [alertView show];

    }

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        if(alertView.tag == 1){
        NSString *str = [[alertView textFieldAtIndex:0] text];
        if([GB_StringUtils isBlank:str]){
            [Static alert:self.navigationController.view msg:@"邀请语不能为空"];
            return;
        }
        if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
            [Static add:self.navigationController.view msg:@"正在加载"];
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbi_uid" value:self.bean.tbi_uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbi_id" value:tbi_id]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:tad_id]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"invite_message" value:str]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getInviteTargetPersonUrl] list:arr delegate:self tag:1];
        }
    }else if (alertView.tag == 2){
        if(buttonIndex != alertView.cancelButtonIndex){
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        for(FuyueBean *bean in dataArr1){
            selectIndex ++;
            if([bean.tbia_uid isEqualToString:[User getUserInfo].uid]){
                if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
                    [Static add:self.navigationController.view msg:@"正在取消"];
                    [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbia_id" value:bean.tbia_id]];
                    [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
                    [GB_NetWorkUtils startPostAsyncRequest:[Url getCancelInviteHistory] list:arr delegate:self tag:3];
                    }
                break;
                    }
                }
            }
        }
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            NSString *str = [NSString stringWithFormat:@"%@%@/%@",[Url getInviteDetail],self.bean.tbi_uid,self.event_id];
            self.bean.invite_flag = @"1";
            [GB_NetWorkUtils startGetAsyncRequest:str delegate:self tag:2];
        }else if (tag == 2){
            NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
            self.dic = dic;
            isCancel = YES;
            [self.inviteBtn setTitle:[NSString stringWithFormat:@"取消邀约"] forState:UIControlStateNormal];
            [self initFrame];
        }else if (tag == 3){
            [dataArr1 removeObjectAtIndex:selectIndex-1];
            selectIndex = 0;
            isCancel = NO;
            self.bean.invite_flag = @"0";
            [self.inviteBtn setTitle:@"参与邀约" forState:UIControlStateNormal];
        }
    }
    [myTableView reloadData];
}
#pragma mark --协议方法
- (void)pushUserInfoViewController:(NSString *)uid{
    UserInfoViewController *user= [[UserInfoViewController alloc]init];
    user.uid = uid;
    [self.navigationController pushViewController:user animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

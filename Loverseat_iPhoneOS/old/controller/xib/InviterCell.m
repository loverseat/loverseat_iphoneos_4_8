//
//  InviterCell.m
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/1/25.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "InviterCell.h"
#import "FuyueBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Url.h"
#import "UserInfoViewController.h"
#import "FuyueBean.h"

@implementation InviterCell{
    FuyueBean *cellBean;
}

- (void)awakeFromNib {
    // Initialization code
}
- (void)makeCell:(FuyueBean*)bean{
    cellBean = bean;
    self.portrait.layer.cornerRadius = self.portrait.frame.size.width/2;
    self.portrait.clipsToBounds = YES;
    self.portrait.layer.borderWidth = 2.0f;
    self.portrait.layer.borderColor = [UIColor grayColor].CGColor;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick:)];
    self.portrait.userInteractionEnabled = YES;
    [self.portrait addGestureRecognizer:tap];
    self.nicknameLable.font = [UIFont systemFontOfSize:14];
    CGSize size = [bean.tu_nickname sizeWithFont:self.nicknameLable.font constrainedToSize:CGSizeMake(MAXFLOAT, self.nicknameLable.frame.size.height)];
    [self.nicknameLable setFrame:CGRectMake(self.nicknameLable.frame.origin.x, self.nicknameLable.frame.origin.y, size.width, self.nicknameLable.frame.size.height)];
    self.nicknameLable.text = bean.tu_nickname;
    
    [GB_NetWorkUtils loadImage:[Url getImageUrl:bean.tu_portrait] container:self.portrait type:GB_ImageCacheTypeAll];
    self.gender.image = [UIImage imageNamed:[bean.tu_gender isEqualToString:@"1"]?@"icon_female":@"icon_male"];
    self.messageLable.text = bean.tbia_invite_message;
    self.cellBottomLine.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2f];
    
//    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
//    [dateFormat setDateFormat:@"yyyy-MM-dd"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
//    NSDate *date =[dateFormat dateFromString:bean.tu_birthday];
//    NSString *str =  GB_NSStringFromInt([[GB_DateUtils getFormatStringByNow:@"yyyy"] intValue]-[[GB_DateUtils getFormatStringBy10Median:@"yyyy" timeMillis:[date timeIntervalSince1970]] intValue]);
//    self.birthdayLable.text = [GB_StringUtils isBlank:bean.tu_star_sign]?[NSString stringWithFormat:@"%@岁",str]:[NSString stringWithFormat:@"%@岁 · %@",str,bean.tu_star_sign];
        self.birthdayLable.text = [GB_StringUtils isBlank:bean.tu_star_sign]?[NSString stringWithFormat:@"%@岁",bean.tu_birthday]:[NSString stringWithFormat:@"%@岁 · %@",bean.tu_birthday,bean.tu_star_sign];
}
//头像点击
-(void)tapClick:(UITapGestureRecognizer*)tap{
    if([self.delegate respondsToSelector:@selector(pushUserInfoViewController:)]){
        [self.delegate pushUserInfoViewController:cellBean.tbia_uid];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  TestViewController.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/1/25.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//  邀约详情

#import "BaseViewController.h"
#import "InviterCell.h"
@class DateAllLeftBean;
@class DateRecordBean;
@interface TestViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,cellPortraitClick>
@property (strong, nonatomic) IBOutlet UIImageView *smallImg1;
@property (strong, nonatomic) IBOutlet UIImageView *smallImg2;
@property (strong, nonatomic) IBOutlet UIImageView *smallImg3;
@property (strong, nonatomic) IBOutlet UIImageView *midLine;
@property (strong, nonatomic) IBOutlet UIButton *inviteBtn;
@property (strong, nonatomic) IBOutlet UILabel *showNameLable;
@property (strong, nonatomic) IBOutlet UILabel *venueNameLable;
@property (strong, nonatomic) IBOutlet UILabel *timeLable;
@property (strong, nonatomic) IBOutlet UILabel *paymentLable;
@property (strong, nonatomic) IBOutlet UILabel *freetimeLable;
@property (strong, nonatomic) IBOutlet UIImageView *portrait;
@property (strong, nonatomic) IBOutlet UILabel *nicknameLable;
@property (strong, nonatomic) IBOutlet UIImageView *gender;
@property (strong, nonatomic) IBOutlet UILabel *starLable;
@property (strong, nonatomic) IBOutlet UIView *headView;
@property (strong, nonatomic) IBOutlet UIView *midView;
@property (strong, nonatomic) IBOutlet UIImageView *inviteEnd;
- (IBAction)inviteClick:(UIButton *)sender;

@property (strong, nonatomic) NSDictionary *dic;
@property (strong, nonatomic) DateAllLeftBean *bean;
@property (strong, nonatomic) NSString *event_id;

@end

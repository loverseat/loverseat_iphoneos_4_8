//
//  InviterCell.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/1/25.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//
@protocol cellPortraitClick <NSObject>

- (void)pushUserInfoViewController:(NSString*)uid;

@end
#import <UIKit/UIKit.h>
@class FuyueBean;
@interface InviterCell : UITableViewCell<cellPortraitClick>
@property (strong, nonatomic) IBOutlet UIImageView *portrait;
@property (strong, nonatomic) IBOutlet UILabel *nicknameLable;
@property (strong, nonatomic) IBOutlet UIImageView *gender;
@property (strong, nonatomic) IBOutlet UILabel *birthdayLable;
@property (strong, nonatomic) IBOutlet UILabel *messageLable;
@property (strong, nonatomic) IBOutlet UIImageView *cellBottomLine;
- (void)makeCell:(FuyueBean*)bean;
@property (assign, nonatomic) id<cellPortraitClick>delegate;
@end

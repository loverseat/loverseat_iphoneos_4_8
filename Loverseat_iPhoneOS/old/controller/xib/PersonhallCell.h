//
//  PersonhallCell.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/5.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//
@protocol chatButtonClickDelegate <NSObject>

- (void)chatButtonClick:(NSInteger)row;
- (void)portraitClick:(NSInteger)row;

@end
#import <UIKit/UIKit.h>
@class TreadsInfo;
@interface PersonhallCell : UITableViewCell
/** 头像 */
@property (strong, nonatomic) IBOutlet UIImageView *portrait;
/** 用户名 */
@property (strong, nonatomic) IBOutlet UILabel *nickNameLable;
/** 男女 */
@property (strong, nonatomic) IBOutlet UIImageView *gender;
/** 最新资讯(图片) */
@property (strong, nonatomic) IBOutlet UIImageView *typeImg;
/** 最新资讯 */
@property (strong, nonatomic) IBOutlet UILabel *contentLable;
/** 时间 */
@property (strong, nonatomic) IBOutlet UILabel *createdTimeLable;
/** 分割线 */
@property (strong, nonatomic) IBOutlet UIImageView *middleLine;
/** 点击聊聊 */
- (IBAction)chatBtn:(id)sender;
/** 聊一聊按钮 */
@property (strong, nonatomic) IBOutlet UIButton *chatBtn;
@property (assign, nonatomic)id<chatButtonClickDelegate>delegate;
- (void)makeCell:(TreadsInfo *)model;
@end

//
//  PersonhallHeadView.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/5.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonhallHeadView : UIView
@property (strong, nonatomic) IBOutlet UIScrollView *expertScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *inviteScrollView;

@end

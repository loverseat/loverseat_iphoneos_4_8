//
//  RecordRightCell.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/2/1.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DateRecordBean;
@interface RecordRightCell : UITableViewCell
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *status;
@property (nonatomic, strong) UILabel *money;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UIImageView *sexImg;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *showName;
@property (nonatomic, strong) UILabel *venueName;

-(void)setDateRecordBean:(DateRecordBean *)bean;
@end

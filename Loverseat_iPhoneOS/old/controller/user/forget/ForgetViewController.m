//
//  ForgetViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/25.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "ForgetViewController.h"
#import "LoginViewController.h"


#import "HomeViewController.h"
#import "HomeLeftViewController.h"
#import "HomeCenterViewController.h"
@interface ForgetViewController()
<UIGestureRecognizerDelegate, GB_KeyboardDelegate>

@property (nonatomic, strong) UIImageView *placeholder;

@property (nonatomic, strong) UITextField *userTf;
@property (nonatomic, strong) UITextField *codeTf;
@property (nonatomic, strong) UITextField *pwdTf;
@property (nonatomic, strong) UIButton *codeBtn;

@property (nonatomic, strong) UIScrollView *scrollView;


@property (strong, nonatomic) UIButton *regBtn;

@end

@implementation ForgetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initFrame];
    [GB_ToolUtils addKeyboardNotification:self];
}

#pragma mark - 添加控件
-(void)initFrame
{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 568)];
    bg.image = [UIImage imageNamed:@"bg_reg&forget"];
    [self.view addSubview:bg];
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
    _scrollView.bounces = NO;
    
    [self.view addSubview:_scrollView];
    
    self.placeholder = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_reg&forget_placeholder"]];
    _placeholder.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5, 200);
    [_scrollView addSubview:_placeholder];
    
    [NavUtils addNavTitleView:self text:@"忘记密码" color:[UIColor whiteColor]];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(g_tap:)];
    singleFingerOne.numberOfTouchesRequired = 1; //手指数
    singleFingerOne.numberOfTapsRequired = 1; //tap次数
    singleFingerOne.delegate = self;
    [_scrollView addGestureRecognizer:singleFingerOne];
    
    [NavUtils addBackWhiteButton:self];
    
    self.userTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 113, 120, 26)];
    _userTf.textColor = [UIColor whiteColor];
    _userTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _userTf.keyboardType = UIKeyboardTypeNumberPad;
    _userTf.placeholder = @"请输入手机号码";
    _userTf.font = [UIFont systemFontOfSize:14];
    [_scrollView addSubview:_userTf];
    
    self.codeBtn = [GB_WidgetUtils getButton:CGRectMake(208, 113, 70, 23) image:nil imageH:nil id:self sel:@selector(c_code)];
    _codeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_codeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_scrollView addSubview:_codeBtn];
    
    
    self.codeTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 174, 220, 26)];
    _codeTf.textColor = [UIColor whiteColor];
    _codeTf.placeholder = @"请输入验证码";
    _codeTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _codeTf.keyboardType = UIKeyboardTypeNumberPad;
    _codeTf.font = [UIFont systemFontOfSize:14];
    [_scrollView addSubview:_codeTf];
    
    self.pwdTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 261, 220, 26)];
    _pwdTf.textColor = [UIColor whiteColor];
    _pwdTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _pwdTf.secureTextEntry = YES;
    _pwdTf.placeholder = @"请输入要设定的密码";
    _pwdTf.font = [UIFont systemFontOfSize:14];
    [_scrollView addSubview:_pwdTf];
    
    _scrollView.contentSize = CGSizeZero;
    
    self.regBtn = [GB_WidgetUtils getButton:CGRectMake(17, 360, 286, 40) image:[GB_ToolUtils getPureImg:GB_UIColorFromRGB(51, 136, 233) size:CGSizeMake(286, 40)] imageH:nil id:self sel:@selector(c_reg)];
    [_regBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_regBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _regBtn.layer.masksToBounds = YES;
    _regBtn.layer.cornerRadius = 2;
    [_scrollView addSubview:_regBtn];
    
}

-(void)code{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t tt = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(tt,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(tt, ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            int diff = [[NSDate new] timeIntervalSince1970]-[[[PublicDao shareInstance].codeTimeData objectForKey:kCode] timeIntervalSince1970];
            if(diff >= 60){
                [[PublicDao shareInstance].codeTimeData removeObjectForKey:kCode];
                [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                dispatch_source_cancel(tt);
            }
            else{
                [_codeBtn setTitle:[NSString stringWithFormat:@"%d",60-diff] forState:UIControlStateNormal];
            }
        });
    });
    dispatch_resume(tt);
}

-(void)res{
    [_pwdTf resignFirstResponder];
    [_userTf resignFirstResponder];
    [_codeTf resignFirstResponder];
}

-(void)g_tap:(UITapGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateEnded){
        [self res];
    }
}

-(void)c_reg{
    [self res];
    if([GB_StringUtils isBlank:self.userTf.text]){
        [Static alert:self.navigationController.view msg:@"手机号不能为空"];
        return;
    }
    if([GB_StringUtils isBlank:_codeTf.text]){
        [Static alert:self.navigationController.view msg:@"验证码错误"];
        return;
    }
    if([GB_StringUtils isBlank:_pwdTf.text]||_pwdTf.text.length < 6 || _pwdTf.text.length> 16){
        [Static alert:self.navigationController.view msg:@"密码错误"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在重置"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"mobile" value:_userTf.text]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"password" value:_pwdTf.text]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"captcha" value:_codeTf.text]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getforgetPwdUrl] list:arr delegate:self tag:2];
    }
}

#pragma mark - 获取验证码
-(void)c_code{
    int diff = [[NSDate new] timeIntervalSince1970]-[[[PublicDao shareInstance].codeTimeData objectForKey:kCode] timeIntervalSince1970];
    if(diff <= 60)return;
    [self res];
    if([GB_StringUtils isBlank:_userTf.text]){
        [Static alert:self.navigationController.view msg:@"手机号不能为空"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在发送"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"mobile" value:self.userTf.text]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"1"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getCheckUserMobileUrl] list:arr delegate:self tag:1];
    }
}

#pragma mark GB_KeyboardDelegate

-(void)GB_keyboardWillHide:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    _placeholder.center = CGPointMake(_placeholder.center.x, 200);
    _userTf.center = CGPointMake(_userTf.center.x, 126);
    _codeTf.center = CGPointMake(_codeTf.center.x, 187);
    _pwdTf.center = CGPointMake(_pwdTf.center.x, 274);
    self.leftBtn.hidden = NO;
    _regBtn.center = CGPointMake(_regBtn.center.x, 380);
    _codeBtn.center = CGPointMake(_codeBtn.center.x, _userTf.center.y);
    _scrollView.frame = CGRectMake(0, 0, _scrollView.frame.size.width, [GB_DeviceUtils getScreenHeight]);
    _scrollView.contentSize = CGSizeZero;
    self.titleL.hidden = NO;
    [UIView commitAnimations];
}

-(void)GB_keyboardWillShow:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    int keyBoardHeight = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.leftBtn.hidden = YES;
    _scrollView.frame = CGRectMake(0, 0, _scrollView.frame.size.width, [GB_DeviceUtils getScreenHeight]-keyBoardHeight);
    _scrollView.contentSize =CGSizeMake([GB_DeviceUtils getScreenWidth], 352);
    _placeholder.center = CGPointMake(_placeholder.center.x, 120);
    _userTf.center = CGPointMake(_userTf.center.x, 46);
    _pwdTf.center = CGPointMake(_pwdTf.center.x, 194);
    _codeTf.center = CGPointMake(_codeTf.center.x, 107);
    _regBtn.center = CGPointMake(_regBtn.center.x, 300);
    _codeBtn.center = CGPointMake(_codeBtn.center.x, _userTf.center.y);
    self.titleL.hidden = YES;
    [UIView commitAnimations];
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [super GB_requestDidFailed:tag];
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [super GB_requestDidSuccess:str tag:tag];
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            [[PublicDao shareInstance].codeTimeData setObject:[NSDate new] forKey:kCode];
            [self code];
        }
        if(tag == 2){
            [User login:[Static getRequestData:str]];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [Static add:self.navigationController.view msg:@"正在加载"];
            [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
            [[PublicDao shareInstance].rootViewController.centerViewController initData];
        }
    }
}


@end

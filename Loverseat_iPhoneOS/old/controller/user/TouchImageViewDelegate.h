//
//  TouchImageViewDelegate.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TouchImageViewDelegate <NSObject>

-(void)begin:(CGPoint)point view:(UIImageView *)imageView;
-(void)move:(CGPoint)point view:(UIImageView *)imageView;
-(void)end:(CGPoint)point view:(UIImageView *)imageView;
-(void)change:(CGPoint)point newPoint:(CGPoint)newPoint view:(UIImageView *)imageView;
@end

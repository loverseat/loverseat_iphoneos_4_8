//
//  PickerSelectViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "EditDelegate.h"
@interface PickerSelectViewController : BaseViewController
<UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *defaultValue;
@property id<EditDelegate> delegate;
@property (nonatomic, strong) NSArray *pickerData;
@end

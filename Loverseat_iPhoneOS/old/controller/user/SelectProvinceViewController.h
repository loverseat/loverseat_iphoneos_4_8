//
//  SelectProvinceViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "EditDelegate.h"

@interface SelectProvinceViewController : BaseViewController
<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) int type;
@property (nonatomic, strong) UIViewController<EditDelegate> *delegate;
@end

//
//  EditDelegate.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EditDelegate <NSObject>

-(void)changeValue:(NSString *)value valueId:(NSString *)valueId type:(int)type;

@end
//
//  UserTagsViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
@class EditViewController;
@interface UserTagsViewController : BaseViewController
<UITableViewDelegate, UITableViewDataSource, GB_KeyboardDelegate>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *defaultValue;
@property (nonatomic, strong) EditViewController *editViewController;
@end

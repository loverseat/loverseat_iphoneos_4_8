//
//  EditPwdViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EditPwdViewController.h"
#import "InputCell.h"

@interface EditPwdViewController ()
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UITextField *pwdTf;
@property (strong, nonatomic) UITextField *oPwdTf;
@property (strong, nonatomic) UITextField *rPwdTf;
@property (strong, nonatomic) UIButton *submitBtn;
@end

@implementation EditPwdViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
    [GB_ToolUtils addKeyboardNotification:self];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"修改密码"];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60)];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    UIButton *submitBtn = [GB_WidgetUtils getButton:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-60, [GB_DeviceUtils getScreenWidth], 60) image:[GB_ToolUtils getPureImg:GB_UIColorFromRGB(251, 80, 87) size:CGSizeMake([GB_DeviceUtils getScreenWidth], 60)] imageH:nil id:self sel:@selector(c_submit)];
    [submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [submitBtn setTitle:@"提交" forState:UIControlStateHighlighted];
    [self.view addSubview:submitBtn];
    self.submitBtn = submitBtn;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"InputCellIdentifier";
    InputCell *cell = [tableView dequeueReusableCellWithIdentifier:
                       identifier];
    if (cell == nil) {
        cell = [[InputCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    if(indexPath.row == 0){
        cell.title.text = @"旧密码";
        cell.tf.keyboardType = UIKeyboardTypeEmailAddress;
        cell.tf.secureTextEntry = YES;
        cell.tf.placeholder = @"请输入旧密码";
        self.oPwdTf = cell.tf;
    }
    if(indexPath.row == 1){
        cell.title.text = @"新密码";
        cell.tf.keyboardType = UIKeyboardTypeEmailAddress;
        cell.tf.secureTextEntry = YES;
        cell.tf.placeholder = @"请输入新密码";
        self.pwdTf = cell.tf;
    }
    
    if(indexPath.row == 2){
        cell.title.text = @"确认密码";
        self.rPwdTf = cell.tf;
        cell.tf.secureTextEntry = YES;
        cell.tf.placeholder = @"请再次输入新密码";
        cell.tf.keyboardType = UIKeyboardTypeEmailAddress;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self res];
}

-(void)GB_keyboardWillHide:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    self.tableView.frame = CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60);
    self.submitBtn.frame = CGRectMake(0, [GB_DeviceUtils getScreenHeight]-60, [GB_DeviceUtils getScreenWidth], 60);
    [UIView commitAnimations];
}

-(void)GB_keyboardWillShow:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    int keyBoardHeight = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.tableView.frame = CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60-keyBoardHeight);
    self.submitBtn.frame = CGRectMake(0, [GB_DeviceUtils getScreenHeight]-60-keyBoardHeight, [GB_DeviceUtils getScreenWidth], 60);
    [UIView commitAnimations];
    
}

-(void)c_submit{
    [self res];
    if([GB_StringUtils isBlank:_oPwdTf.text]){
        [Static alert:self.navigationController.view msg:@"旧密码不能为空"];
        return;
    }
    if([GB_StringUtils isBlank:_pwdTf.text]){
        [Static alert:self.navigationController.view msg:@"新密码不能为空"];
        return;
    }
    if(![_rPwdTf.text isEqualToString:_pwdTf.text]){
        [Static alert:self.navigationController.view msg:@"两次输入的新密码不一致"];
        return;
    }
    
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"mobile" value:[User getUserInfo].tu_mobile]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"password" value:_pwdTf.text]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"original_password" value:_oPwdTf.text]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getRestPwdUrl] list:arr delegate:self tag:1];
    }
}


-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.view]){
        [Static alert:self.navigationController.view msg:@"修改成功"];
        [self cb_back];
    }
}

-(void)res{
    [self.pwdTf resignFirstResponder];
    [self.rPwdTf resignFirstResponder];
    [self.oPwdTf resignFirstResponder];
}

@end

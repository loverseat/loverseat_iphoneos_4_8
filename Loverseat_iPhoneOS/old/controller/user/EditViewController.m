//
//  EditViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EditViewController.h"
#import "TextFieldViewController.h"
#import "SelectCityViewController.h"
#import "UserDatePickerViewController.h"
#import "EditDelegate.h"
#import "PickerSelectViewController.h"
#import "UserTagsViewController.h"
#import "SelectCityViewController.h"
#import "UserInfoViewController.h"
#import "SelectProvinceViewController.h"
#import "MultiSelectViewController.h"
#import "InputCell.h"
#import "HomeViewController.h"
#import "HomeLeftViewController.h"

@interface EditViewController();

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UserBean *oBean;
@end

@implementation EditViewController

#define genderArr @[[[GB_KeyValue alloc] initWithKeyValue:@"0" value:@"男"],[[GB_KeyValue alloc] initWithKeyValue:@"1" value:@"女"]]

#define incomeArr @[[[GB_KeyValue alloc] initWithKeyValue:@"3000" value:@"3000元以下／月"],[[GB_KeyValue alloc] initWithKeyValue:@"5000" value:@"3000 - 5000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"8000" value:@"5000 - 8000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"12000" value:@"8000 - 12000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"18000" value:@"12000 - 18000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"30000" value:@"18000 - 30000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"50000" value:@"30000 - 50000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"10000" value:@"50000 - 10000元／月"],[[GB_KeyValue alloc] initWithKeyValue:@"100000" value:@"100000元以上／月"]]

#define occupationArr @[[[GB_KeyValue alloc] initWithKeyValue:@"高新科技" value:@"高新科技"],[[GB_KeyValue alloc] initWithKeyValue:@"信息传媒" value:@"信息传媒"],[[GB_KeyValue alloc] initWithKeyValue:@"金融" value:@"金融"],[[GB_KeyValue alloc] initWithKeyValue:@"服务业" value:@"服务业"],[[GB_KeyValue alloc] initWithKeyValue:@"教育" value:@"教育"],[[GB_KeyValue alloc] initWithKeyValue:@"医疗服务" value:@"医疗服务"],[[GB_KeyValue alloc] initWithKeyValue:@"艺术娱乐" value:@"艺术娱乐"],[[GB_KeyValue alloc] initWithKeyValue:@"制造加工" value:@"制造加工"],[[GB_KeyValue alloc] initWithKeyValue:@"地产建筑" value:@"地产建筑"],[[GB_KeyValue alloc] initWithKeyValue:@"贸易零售" value:@"贸易零售"],[[GB_KeyValue alloc] initWithKeyValue:@"公共服务" value:@"公共服务"],[[GB_KeyValue alloc] initWithKeyValue:@"开采冶金" value:@"开采冶金"],[[GB_KeyValue alloc] initWithKeyValue:@"交通物流" value:@"交通物流"],[[GB_KeyValue alloc] initWithKeyValue:@"农林牧渔" value:@"农林牧渔"],[[GB_KeyValue alloc] initWithKeyValue:@"自由职业" value:@"自由职业"]]

#define purposeArr @[[[GB_KeyValue alloc] initWithKeyValue:@"找个知音一同分享" value:@"找个知音一同分享"],[[GB_KeyValue alloc] initWithKeyValue:@"结交更多的朋友" value:@"结交更多的朋友"],[[GB_KeyValue alloc] initWithKeyValue:@"不想一个人看演出" value:@"不想一个人看演出"],[[GB_KeyValue alloc] initWithKeyValue:@"寻觅另一半" value:@"寻觅另一半"]]


#define tagArr @[@"音乐",@"舞蹈",@"宠物",@"摄影",@"自驾",@"游戏控",@"cosplay",@"小说",@"美剧",@"屌丝",@"吃货",@"二次元",@"购物",@"展览控",@"旅游",@"海归",@"多金",@"健身",@"胖纸",@"美妆",@"极限运动",@"公益",@"技术宅",@"星座控",@"文青",@"摇滚",@"事业型"]

#define favourArr @[@"话剧",@"歌剧",@"管弦乐",@"独奏",@"交响乐",@"演唱会",@"舞剧",@"芭蕾",@"Livehouse",@"音乐节",@"摇滚",@"相声",@"戏曲",@"魔术",@"体育赛事",@"F1"]


-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.oBean = [UserBean copyBean:self.bean];
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"编辑资料"];
    [NavUtils addRightButton:self title:@"保存" sel:@selector(c_save)];
    self.rightBtn.hidden = ![self isEdit];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]) style:UITableViewStyleGrouped];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if(section == 0)return 6;
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"InputCellIdentifier";
    InputCell *cell = [tableView dequeueReusableCellWithIdentifier:
                       identifier];
    if (cell == nil) {
        cell = [[InputCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    cell.tf.enabled = NO;
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            cell.title.text = @"昵称";
            cell.tf.text = self.bean.tu_nickname;
        }
        if(indexPath.row == 1){
            cell.title.text = @"性别";
            cell.tf.text = self.bean.tu_gender.intValue == 0?@"男":@"女";
        }
        if(indexPath.row == 2){
            cell.title.text = @"年龄";
            NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
            [dateFormat setDateFormat:@"yyyy-MM-dd"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
            NSDate *date =[dateFormat dateFromString:self.bean.tu_birthday];
            
            cell.tf.text = GB_NSStringFromInt([[GB_DateUtils getFormatStringByNow:@"yyyy"] intValue]-[[GB_DateUtils getFormatStringBy10Median:@"yyyy" timeMillis:[date timeIntervalSince1970]] intValue]);
        }
        
        if(indexPath.row == 3){
            cell.title.text = @"地点";
            cell.tf.text = [NSString stringWithFormat:@"%@%@",self.bean.tu_province,self.bean.tu_city];
        }
        
        if(indexPath.row == 4){
            cell.title.text = @"签名";
            cell.tf.text = self.bean.tu_signature;
        }
        if(indexPath.row == 5){
            cell.title.text = @"标签";
            cell.tf.text = self.bean.tu_tabs;
        }
    }
    else if(indexPath.section == 1){
        if(indexPath.row == 0){
            cell.title.text = @"职业";
            cell.tf.text = self.bean.tu_occupation;
        }
        if(indexPath.row == 1){
            cell.title.text = @"收入";
            for (GB_KeyValue *kv in incomeArr) {
                if([kv.key intValue] == [self.bean.tu_income intValue]){
                    cell.tf.text = kv.value;
                    break;
                }
            }
        }
        
        if(indexPath.row == 2){
            NSString *str = nil;
            if([self.bean.tu_marital_status intValue]==0){
                str = @"未婚";
            }
            else{
                 str = @"已婚";
            }
            cell.title.text = @"婚姻状况";
            cell.tf.text = str;
        }
        
        if(indexPath.row == 3){
            cell.title.text = @"喜欢的演出类型";
            cell.tf.text = self.bean.tu_favour;
        }
        if(indexPath.row == 4){
            cell.title.text = @"看演出的目的";
            cell.tf.text = self.bean.tu_purpose;
        }

    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            TextFieldViewController *edit = [[TextFieldViewController alloc]init];
            edit.title = @"昵称";
            edit.type = 1;
            edit.defaultValue = self.bean.tu_nickname;
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES];
        }
        if(indexPath.row == 1){
            PickerSelectViewController *edit = [[PickerSelectViewController alloc]init];
            edit.title = @"性别";
            edit.type = 11;
            edit.defaultValue = self.bean.tu_gender;
            edit.pickerData = genderArr;
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES];
        }
        else if(indexPath.row == 2){
            UserDatePickerViewController *picker = [[UserDatePickerViewController alloc]init];
            picker.title = @"生日";
            picker.type = 2;
            picker.defaultValue = self.bean.tu_birthday;
            picker.editViewController = self;
            [self.navigationController pushViewController:picker animated:YES];
            
        }
        else if(indexPath.row == 3){
            SelectProvinceViewController *select = [[SelectProvinceViewController alloc]init];
            select.type = 3;
            select.delegate = self;
            [self.navigationController pushViewController:select animated:YES];
        }
        else if(indexPath.row == 4){
            TextFieldViewController *edit = [[TextFieldViewController alloc]init];
            edit.title = @"签名";
            edit.type = 4;
            edit.canNull = YES;
            edit.defaultValue = self.bean.tu_signature;
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES];
        }
        else if(indexPath.row == 5){
            MultiSelectViewController *edit = [[MultiSelectViewController alloc]init];
            edit.title = @"标签";
            edit.type = 5;
            edit.defaultValue = self.bean.tu_tabs;
            edit.delegate = self;
            edit.contentData = tagArr;
            [self.navigationController pushViewController:edit animated:YES];
        }
    }
    if(indexPath.section == 1){
        if(indexPath.row == 0){
            PickerSelectViewController *edit = [[PickerSelectViewController alloc]init];
            edit.title = @"职业";
            edit.type = 6;
            edit.defaultValue = self.bean.tu_occupation;
            edit.pickerData = occupationArr;
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES];
        }
        else if(indexPath.row == 1){
            PickerSelectViewController *edit = [[PickerSelectViewController alloc]init];
            edit.title = @"收入";
            edit.type = 7;
            edit.defaultValue = self.bean.tu_income;
            edit.pickerData = incomeArr;
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES];
        }
        else if(indexPath.row == 2){
            PickerSelectViewController *edit = [[PickerSelectViewController alloc]init];
            edit.title = @"婚姻状况";
            edit.type = 8;
            edit.defaultValue = self.bean.tu_marital_status;
            edit.pickerData = @[[[GB_KeyValue alloc] initWithKeyValue:@"0" value:@"未婚"],[[GB_KeyValue alloc] initWithKeyValue:@"1" value:@"已婚"]];
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES];
        }
        else if(indexPath.row == 3){
            MultiSelectViewController *edit = [[MultiSelectViewController alloc]init];
            edit.title = @"喜欢的演出类型";
            edit.type = 9;
            edit.defaultValue = self.bean.tu_favour;
            edit.delegate = self;
            edit.contentData = favourArr;
            [self.navigationController pushViewController:edit animated:YES];
        }
        else if(indexPath.row == 4){
            PickerSelectViewController *edit = [[PickerSelectViewController alloc]init];
            edit.title = @"看演出的目的";
            edit.type = 10;
            edit.defaultValue = self.bean.tu_purpose;
            edit.pickerData = purposeArr;
            edit.delegate = self;
            [self.navigationController pushViewController:edit animated:YES]; 
        }
    }
}

-(void)changeValue:(NSString *)value valueId:(NSString *)valueId type:(int)type{
    if(type == 1){
        self.bean.tu_nickname = value;
    }
    if(type == 2){
        self.bean.tu_birthday = value;
    }
    if(type == 3){
        self.bean.tu_province = [[GB_StringUtils split:value separator:@":"] objectAtIndex:0];
        self.bean.tu_city = [[GB_StringUtils split:value separator:@":"] lastObject];
        self.bean.tu_city_id = [[GB_StringUtils split:valueId separator:@":"] lastObject];
        self.bean.tu_province_id = [[GB_StringUtils split:valueId separator:@":"] objectAtIndex:0];
    }
    if(type == 4){
        self.bean.tu_signature = value;
    }
    if(type == 5){
        self.bean.tu_tabs = value;
    }
    if(type == 6){
        self.bean.tu_occupation = value;
    }
    if(type == 7){
        self.bean.tu_income = valueId;
    }
    if(type == 8){
        self.bean.tu_marital_status = valueId;
    }
    if(type == 9){
        self.bean.tu_favour = value;
    }
    if(type == 10){
        self.bean.tu_purpose = value;
    }
    if(type == 11){
        self.bean.tu_gender = valueId;
    }
    self.rightBtn.hidden = ![self isEdit];
    [_tableView reloadData];
}

-(BOOL)isEdit{
    if(![_oBean.tu_nickname isEqualToString:_bean.tu_nickname])
        return YES;
    if(![_oBean.tu_birthday isEqualToString:_bean.tu_birthday])
        return YES;
    if(![_oBean.tu_signature isEqualToString:_bean.tu_signature])
        return YES;
    if(![_oBean.tu_tabs isEqualToString:_bean.tu_tabs])
        return YES;
    if(![_oBean.tu_occupation isEqualToString:_bean.tu_occupation])
        return YES;
    if(![_oBean.tu_income isEqualToString:_bean.tu_income])
        return YES;
    if(![_oBean.tu_marital_status isEqualToString:_bean.tu_marital_status])
        return YES;
    if(![_oBean.tu_favour isEqualToString:_bean.tu_favour])
        return YES;
    if(![_oBean.tu_purpose isEqualToString:_bean.tu_purpose])
        return YES;
    if(![_oBean.tu_city_id isEqualToString:_bean.tu_city_id])
        return YES;
    if(![_oBean.tu_gender isEqualToString:_bean.tu_gender])
        return YES;
    return NO;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [GB_NetWorkUtils cancelRequest];
        [Static remove:self.navigationController.view];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self c_save];
    }
}

-(void)cb_back{
    if([self isEdit]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否保存更改？" message:nil delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是",nil];
        [alert show];
    }
    else{
        [GB_NetWorkUtils cancelRequest];
        [Static remove:self.navigationController.view];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)c_save{
    if([GB_StringUtils isBlank:self.oBean.tu_nickname]){
        [Static alert:self.navigationController.view msg:@"昵称不能为空"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.view]){
        [Static add:self.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"nickname" value:self.bean.tu_nickname]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"gender" value:self.bean.tu_gender]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"birthday" value:self.bean.tu_birthday]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"signature" value:self.bean.tu_signature]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tabs" value:self.bean.tu_tabs]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"city" value:self.bean.tu_city_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"province" value:self.bean.tu_province_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"occupation" value:self.bean.tu_occupation]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"income" value:self.bean.tu_income]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"marital_status" value:self.bean.tu_marital_status]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"favour" value:self.bean.tu_favour]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"purpose" value:self.bean.tu_purpose]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUserInfoUpdateUrl] list:arr delegate:self tag:1];
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view :self.view]){
        [User login:[Static getRequestData:str]];
        _bean.tu_star_sign = [User getUserInfo].tu_star_sign;
        _userInfoViewController.bean = _bean;
        [_userInfoViewController initBeanFrame];
        [GB_NetWorkUtils cancelRequest];
        [Static remove:self.navigationController.view];
        _userInfoViewController.didEdit = YES;
        [self.navigationController popViewControllerAnimated:YES];
        [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
      
    }
}

@end

//
//  CompleteInfoViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/26.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface CompleteInfoViewController : BaseViewController

@property (nonatomic, strong) NSString *uid;

@end

//
//  LoginViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-25.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "LoginViewController.h"
#import "RegViewController.h"
#import "ForgetViewController.h"
#import "CompleteInfoViewController.h"
#import "HomeViewController.h"
#import "HomeCenterViewController.h"
#import "HomeLeftViewController.h"
#import "UMSocial.h"
#import "WXApi.h"

#define TAG_WITH_IMGEVIEW 100
#define WIDTH_WITH_IMAGEVIEW 60
@interface LoginViewController ()<UIGestureRecognizerDelegate, GB_KeyboardDelegate>

@property (nonatomic, strong) UIImageView *placeholder;

@property (nonatomic, strong) UITextField *userTf;
@property (nonatomic, strong) UITextField *pwdTf;


@property (strong, nonatomic) UIButton *loginBtn;
@property (strong, nonatomic) UIButton *forgetBtn;
@property (strong, nonatomic) UIButton *regBtn;
@end

@implementation LoginViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFrame];
    [GB_ToolUtils addKeyboardNotification:self];

}


#pragma mark - 添加控件
-(void)initFrame
{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 568)];
    bg.image = [UIImage imageNamed:@"bg_login"];
    [self.view addSubview:bg];
    
    self.placeholder = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_login_placeholder"]];
    _placeholder.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5, 200);
    [self.view addSubview:_placeholder];
    
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(g_tap:)];
    singleFingerOne.numberOfTouchesRequired = 1; //手指数
    singleFingerOne.numberOfTapsRequired = 1; //tap次数
    singleFingerOne.delegate = self;
    [self.view addGestureRecognizer:singleFingerOne];

    UITextField *userTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 242, 240, 26)];
    userTf.textColor = [UIColor whiteColor];
    userTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userTf.textAlignment = NSTextAlignmentCenter;
    userTf.keyboardType = UIKeyboardTypeNumberPad;
    userTf.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:userTf];
    self.userTf = userTf;

    UITextField *pwdTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 284, 240, 26)];
    pwdTf.textColor = [UIColor whiteColor];
    pwdTf.textAlignment = NSTextAlignmentCenter;
    pwdTf.font = [UIFont systemFontOfSize:14];
    pwdTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    pwdTf.secureTextEntry = YES;
    [self.view addSubview:pwdTf];
    self.pwdTf = pwdTf;
    

    UIFont *forgetBtnFont = [UIFont systemFontOfSize:14];
    NSString *forgetBtnStr = @"忘记密码";
    float forgetBtnWidth = [GB_ToolUtils getTextWidth:forgetBtnStr font:forgetBtnFont size:CGSizeMake(100, 20)];
    self.forgetBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-forgetBtnWidth-17, 318, forgetBtnWidth, 20) image:nil imageH:nil id:self sel:@selector(c_forget)];
    [_forgetBtn setTitle:forgetBtnStr forState:UIControlStateNormal];
    _forgetBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_forgetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_forgetBtn];

    NSString *regBtnStr = @"注册 |";
    float regBtnWidth = [GB_ToolUtils getTextWidth:regBtnStr font:forgetBtnFont size:CGSizeMake(100, 20)];
    self.regBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth] - forgetBtnWidth -17-regBtnWidth - 4, 318, regBtnWidth, 20) image:nil imageH:nil id:self sel:@selector(c_reg)];
    [_regBtn setTitle:regBtnStr forState:UIControlStateNormal];
    _regBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_regBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_regBtn];
    
    
    
    self.loginBtn = [GB_WidgetUtils getButton:CGRectMake(17, 360, 286, 32) image:[UIImage imageNamed:@"alpha_white_0.08"] imageH:nil id:self sel:@selector(c_login)];
    [_loginBtn setTitle:@"登录账户" forState:UIControlStateNormal];
    [_loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _loginBtn.layer.masksToBounds = YES;
    _loginBtn.layer.cornerRadius = 2;
    _loginBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    _loginBtn.layer.borderWidth = 0.5f;
    [self.view addSubview:_loginBtn];
    

    
    NSArray *imgArr = @[@"icon_QQ",@"icon_bg_weixin",@"icon_bg_weibo"];
    float width = [GB_DeviceUtils getScreenWidth];
    for(int i = 0;i<imgArr.count;i++){
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake((width - 250)/2 +(60+35)*i, 410, 60, 60)];
        image.userInteractionEnabled = YES;
        image.tag = i + TAG_WITH_IMGEVIEW;
        image.image = [UIImage imageNamed:imgArr[i]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick:)];
        [image addGestureRecognizer:tap];
        [self.view addSubview:image];
    }
    if(![WXApi isWXAppInstalled]){
        UIImageView *weixinIV = (UIImageView *)[self.view viewWithTag:TAG_WITH_IMGEVIEW + 1];
        weixinIV.hidden = YES;
        UIImageView *qqIV = (UIImageView *)[self.view viewWithTag:TAG_WITH_IMGEVIEW];
        UIImageView *weiboIV = (UIImageView *)[self.view viewWithTag:TAG_WITH_IMGEVIEW + 2];
        qqIV.frame = CGRectMake((width - WIDTH_WITH_IMAGEVIEW*2)/3, 410, WIDTH_WITH_IMAGEVIEW, WIDTH_WITH_IMAGEVIEW);
        weiboIV.frame = CGRectMake(qqIV.frame.origin.x*2+WIDTH_WITH_IMAGEVIEW, 410, WIDTH_WITH_IMAGEVIEW, WIDTH_WITH_IMAGEVIEW);
    }
}


-(void)tapClick:(UITapGestureRecognizer *)tap
{
    if(tap.view.tag == 100){
        //QQ登陆
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
            [self reg:@"QQ" userName:snsAccount.userName icon:snsAccount.iconURL usid:snsAccount.usid];
            }});
        
    }else if (tap.view.tag == 101){
        //微信登陆
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
            [self reg:@"wechat" userName:snsAccount.userName icon:snsAccount.iconURL usid:snsAccount.usid];
                }
            });
    }else if (tap.view.tag == 102){
        //微博登陆
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
                [self reg:@"weibo" userName:snsAccount.userName icon:snsAccount.iconURL usid:snsAccount.usid];
            }});
        
    }
}
- (void)reg:(NSString *)str userName:(NSString *)name icon:(NSString *)icon usid:(NSString *)usid{
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"username" value:name]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"portrait" value:icon]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"user_source" value:str]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"source_id" value:usid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"source_type" value:@"appstore"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUserOtherLoginUrl] list:arr delegate:self tag:2];
}

#pragma mark - 注册
-(void)c_reg
{
    RegViewController *con = [[RegViewController alloc]init];
    [self.navigationController pushViewController:con animated:YES];
}

#pragma mark - 忘记密码
-(void)c_forget
{
    ForgetViewController *con = [[ForgetViewController alloc]init];
    [self.navigationController pushViewController:con animated:YES];
}

-(void)res{
    [_pwdTf resignFirstResponder];
    [_userTf resignFirstResponder];
}

#pragma mark - 登录账号
-(void)c_login
{

    [self res];
    if([GB_StringUtils isBlank:self.userTf.text]){
        [Static alert:self.view msg:@"手机号不能为空"];
        return;
    }
    if(![GB_VerificationUtils isMobilePhoneNumber:self.userTf.text]){
        [Static alert:self.navigationController.view msg:@"手机号错误"];
        return;
    }
    if([GB_StringUtils isBlank:self.pwdTf.text]){
        [Static alert:self.navigationController.view msg:@"密码不能为空"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在登录"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc] initWithKeyValue:@"mobile" value:self.userTf.text]];
        [arr addObject:[[GB_KeyValue alloc] initWithKeyValue:@"password" value:self.pwdTf.text]];
        if([GB_StringUtils isNotBlank:[User getUserToken]]){
            [arr addObject:[[GB_KeyValue alloc] initWithKeyValue:@"token" value:[User getUserToken]]];
        }
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUserLoginUrl] list:arr delegate:self tag:1];
    }
}

-(void)g_tap:(UITapGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateEnded){
        [self res];
    }
}

#pragma mark GB_KeyboardDelegate

-(void)GB_keyboardWillHide:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    
    _regBtn.hidden = NO;
    _forgetBtn.hidden = NO;
    
    _placeholder.center = CGPointMake(_placeholder.center.x, 200);
    _userTf.center = CGPointMake(_userTf.center.x, 255);
    _pwdTf.center = CGPointMake(_pwdTf.center.x, 297);
    _loginBtn.center = CGPointMake(_loginBtn.center.x, 376);
    [UIView commitAnimations];
}

-(void)GB_keyboardWillShow:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    _regBtn.hidden = YES;
    _forgetBtn.hidden = YES;
    
    if([GB_DeviceUtils getScreenHeight] < 568){
        _placeholder.center = CGPointMake(_placeholder.center.x, 0);
        _userTf.center = CGPointMake(_userTf.center.x, 55);
        _pwdTf.center = CGPointMake(_pwdTf.center.x, 97);
        _loginBtn.center = CGPointMake(_loginBtn.center.x, 206);
    }
    else{
        _placeholder.center = CGPointMake(_placeholder.center.x, 150);
        _userTf.center = CGPointMake(_userTf.center.x, 205);
        _pwdTf.center = CGPointMake(_pwdTf.center.x, 247);
        _loginBtn.center = CGPointMake(_loginBtn.center.x, 306);
    }
    [UIView commitAnimations];
}
#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [super GB_requestDidFailed:tag];
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [super GB_requestDidSuccess:str tag:tag];
    [Static remove:self.navigationController.view];
    //if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            UserBean *bean = [UserBean getBean:[Static getRequestData:str]];
            if([GB_StringUtils isBlank:bean.tu_birthday]||[GB_StringUtils isBlank:bean.tu_nickname]||[GB_StringUtils isBlank:bean.tu_gender]||[GB_StringUtils isBlank:bean.tu_portrait]){
                CompleteInfoViewController *con = [[CompleteInfoViewController alloc]init];
                con.uid = bean.uid;
                [self.navigationController pushViewController:con animated:YES];
            }
            else{
                [User login:[Static getRequestData:str]];
                [MobClick event:@"2"];
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
                [[PublicDao shareInstance].rootViewController.centerViewController initData];
            }
        }else if (tag == 2){
                [User login:[Static getRequestData:str]];
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
                [[PublicDao shareInstance].rootViewController.centerViewController initData];
        }
    }
//}
@end

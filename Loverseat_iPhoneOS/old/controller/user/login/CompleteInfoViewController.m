//
//  CompleteInfoViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/26.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "CompleteInfoViewController.h"
#import "HomeViewController.h"
#import "HomeCenterViewController.h"
#import "HomeLeftViewController.h"

@interface CompleteInfoViewController ()
<UIGestureRecognizerDelegate, GB_KeyboardDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,UIActionSheetDelegate,GB_NetWorkDelegate,GB_ImagePickerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) NSString *birthday;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *nickname;

@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) UIPickerView *pickerView;

@property (nonatomic, strong) UIImageView *placeholder;

@property (nonatomic, strong) UITextField *nicknameTf;
@property (nonatomic, strong) UITextField *genderTf;
@property (nonatomic, strong) UITextField *birthdayTf;

@property (strong, nonatomic) UIButton *doneBtn;
@property (strong, nonatomic) UIButton *iconBtn;

@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) NSString *iconPath;

@property (nonatomic, strong) UIScrollView *scrollView;
@end

@implementation CompleteInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([GB_StringUtils isBlank:_birthday])
        self.birthday = @"1990-01-01";
    if([GB_StringUtils isBlank:_gender])
        self.gender = @"0";
    [self initFrame];
    [GB_ToolUtils addKeyboardNotification:self];
    
}

-(void)initFrame{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 568)];
    bg.image = [UIImage imageNamed:@"bg_complete_info"];
    [self.view addSubview:bg];
    
    
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
    _scrollView.bounces = NO;
    [self.view addSubview:_scrollView];
    
    self.placeholder = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_complete_info_placeholder"]];
    _placeholder.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5, 230);
    [_scrollView addSubview:_placeholder];
    
    [NavUtils addNavTitleView:self text:@"完善资料" color:[UIColor whiteColor]];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(g_tap:)];
    singleFingerOne.numberOfTouchesRequired = 1; //手指数
    singleFingerOne.numberOfTapsRequired = 1; //tap次数
    singleFingerOne.delegate = self;
    [_scrollView addGestureRecognizer:singleFingerOne];
    
    [NavUtils addBackWhiteButton:self];
    
    self.iconBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5-61, 60, 122, 122) image:[UIImage imageNamed:@"btn_complete_info_icon_placeholder"] imageH:nil id:self sel:@selector(c_icon)];
    _iconBtn.layer.masksToBounds = YES;
    _iconBtn.layer.cornerRadius = 61;
    [_scrollView addSubview:_iconBtn];
    
    self.nicknameTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 230, 220, 26)];
    _nicknameTf.textColor = [UIColor whiteColor];
    _nicknameTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _nicknameTf.keyboardType = UIKeyboardTypeNamePhonePad;
    _nicknameTf.placeholder = @"请输入昵称";
    _nicknameTf.delegate = self;
    _nicknameTf.font = [UIFont systemFontOfSize:14];
    [_scrollView addSubview:_nicknameTf];
    
    self.genderTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 291, 220, 26)];
    _genderTf.textColor = [UIColor whiteColor];
    _genderTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _genderTf.keyboardType = UIKeyboardTypeNamePhonePad;
    _genderTf.placeholder = @"请选择性别";
    _genderTf.enabled = NO;
    _genderTf.font = [UIFont systemFontOfSize:14];
    _genderTf.text = [_gender isEqualToString:@"0"]?@"男":@"女";
    [_scrollView addSubview:_genderTf];
    [self addBtn:@selector(c_gender) tf:_genderTf];
    
    self.birthdayTf = [[UITextField alloc]initWithFrame:CGRectMake(58, 352, 220, 26)];
    _birthdayTf.textColor = [UIColor whiteColor];
    _birthdayTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _birthdayTf.keyboardType = UIKeyboardTypeNamePhonePad;
    _birthdayTf.enabled = NO;
    _birthdayTf.placeholder = @"请选择生日";
    _birthdayTf.font = [UIFont systemFontOfSize:14];
    [_scrollView addSubview:_birthdayTf];
    [self addBtn:@selector(c_birthday) tf:_birthdayTf];

    
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, [GB_DeviceUtils getScreenHeight], [GB_DeviceUtils getScreenWidth], 216)];
    _pickerView.backgroundColor = [UIColor whiteColor];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    [self.view addSubview:_pickerView];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date=[formatter dateFromString:_birthday];
    _birthdayTf.text = [GB_DateUtils getFormatStringBy10Median:@"yyyy年MM月dd日" timeMillis:[date timeIntervalSince1970]];

    self.datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, [GB_DeviceUtils getScreenHeight], [GB_DeviceUtils getScreenWidth], 216)];
    _datePicker.maximumDate = [NSDate new];
    _datePicker.date = date;
    _datePicker.backgroundColor = [UIColor whiteColor];
    _datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
    [_datePicker addTarget:self action:@selector(c_date:) forControlEvents:UIControlEventValueChanged];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [self.view addSubview:_datePicker];
    
    self.doneBtn = [GB_WidgetUtils getButton:CGRectMake(17, 430, 286, 40) image:[GB_ToolUtils getPureImg:GB_UIColorFromRGB(51, 136, 233) size:CGSizeMake(286, 40)] imageH:nil id:self sel:@selector(c_done)];
    [_doneBtn setTitle:@"完成" forState:UIControlStateNormal];
    [_doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _doneBtn.layer.masksToBounds = YES;
    _doneBtn.layer.cornerRadius = 2;
    [_scrollView addSubview:_doneBtn];
    
    _scrollView.contentSize = CGSizeMake([GB_DeviceUtils getScreenWidth], 490);
}

-(void)addBtn:(SEL)sel tf:(UITextField *)tf{
    [_scrollView addSubview:[GB_WidgetUtils getButton:tf.frame image:nil imageH:nil id:self sel:sel]];
}

-(void)res{
    self.titleL.hidden = NO;
    [_nicknameTf resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25f];
    self.datePicker.center = CGPointMake(self.datePicker.center.x, [GB_DeviceUtils getScreenHeight]+self.datePicker.frame.size.height*0.5);
    self.pickerView.center = self.datePicker.center;
    _scrollView.frame = CGRectMake(0,0,[GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]);
    [UIView commitAnimations];
}

-(void)g_tap:(UITapGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateEnded){
        [self res];
    }
}

-(void)c_done{
    if([GB_ToolUtils isNull:_icon]){
        [Static alert:self.navigationController.view msg:@"请上传照片"];
        return;
    }
    if([GB_ToolUtils isBlank:_nicknameTf.text]){
        [Static alert:self.navigationController.view msg:@"请输入昵称"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.view]){
        [Static add:self.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"nickname" value:_nicknameTf.text]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"gender" value:_gender]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"birthday" value:_birthday]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"portrait" value:_iconPath]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:_uid]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUserInfoUpdateUrl] list:arr delegate:self tag:2];
    }
}

-(void)c_icon{
    [self res];
    UIActionSheet *ac = nil;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ac = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:@"取消"
                           destructiveButtonTitle:nil
                                otherButtonTitles:@"相册",@"拍照",nil];
    }
    else
    {
        ac = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:@"取消"
                           destructiveButtonTitle:nil
                                otherButtonTitles:@"相册",nil];
    }
    ac.tag = 1;
    ac.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [ac showInView:self.view];
}

-(void)c_gender{
    self.titleL.hidden = YES;
    float y = [GB_DeviceUtils getScreenHeight]+self.datePicker.frame.size.height*0.5;
    [_nicknameTf resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25f];
    if(self.datePicker.center.y != y){
        self.datePicker.center = CGPointMake(self.datePicker.center.x, y);
    }
    self.pickerView.center = CGPointMake(self.pickerView.center.x, [GB_DeviceUtils getScreenHeight]-self.pickerView.frame.size.height*0.5);
    _scrollView.frame = CGRectMake(0, 0, _scrollView.frame.size.width, [GB_DeviceUtils getScreenHeight]-_pickerView.frame.size.height);
    [UIView commitAnimations];
}

-(void)c_birthday{
    self.titleL.hidden = YES;
    float y = [GB_DeviceUtils getScreenHeight]+self.pickerView.frame.size.height*0.5;
     [_nicknameTf resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25f];
    if(self.pickerView.center.y != y){
        self.pickerView.center = CGPointMake(self.pickerView.center.x, y);
    }
    self.datePicker.center = CGPointMake(self.datePicker.center.x, [GB_DeviceUtils getScreenHeight]-self.datePicker.frame.size.height*0.5);
    _scrollView.frame = CGRectMake(0, 0, _scrollView.frame.size.width, [GB_DeviceUtils getScreenHeight]-_datePicker.frame.size.height);
    [UIView commitAnimations];
}

-(void)c_date:(id)sender{
    NSDate *curDate =[self.datePicker date];
    self.birthday = [GB_DateUtils getFormatStringBy10Median:@"yyyy-MM-dd" timeMillis:[curDate timeIntervalSince1970]];
    self.birthdayTf.text = [GB_DateUtils getFormatStringBy10Median:@"yyyy年MM月dd日" timeMillis:[curDate timeIntervalSince1970]];
}

#pragma mark UIActionSheetDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"])
    {
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        if([GB_ToolUtils isBlank:image]){
            image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        [self GB_imagePickerDidFinish:image];
        __block UIViewController<GB_ImagePickerDelegate> *safeViewController = self;
        [picker dismissViewControllerAnimated:YES completion:^{
            if([safeViewController respondsToSelector:@selector(GB_imagePickerDismissFinish:)])
                [safeViewController GB_imagePickerDismissFinish:image];
        }];
        return;
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 1){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        if(buttonIndex == 0){
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        if(buttonIndex == 1){
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
}

#pragma mark UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.titleL.hidden = YES;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25f];
    self.datePicker.center = CGPointMake(self.datePicker.center.x, [GB_DeviceUtils getScreenHeight]+self.datePicker.frame.size.height*0.5);
    self.pickerView.center = self.datePicker.center;
    [UIView commitAnimations];
    return YES;
}

#pragma mark UIPickerViewDelegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 2;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return row==0?@"男":@"女";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.genderTf.text = row==0?@"男":@"女";
    self.gender = row==0?@"0":@"1";
}

-(void)GB_keyboardDidHide:(NSNotification *)note{

}


#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [super GB_requestDidFailed:tag];
    self.view.userInteractionEnabled = YES;
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [super GB_requestDidSuccess:str tag:tag];
    [Static remove:self.navigationController.view];
    self.view.userInteractionEnabled = YES;
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:[Error getCurrentStr:str]];
            self.iconPath = [dic objectForKey:@"path"];
            [self.iconBtn setBackgroundImage:_icon forState:UIControlStateNormal];
        }
        else if(tag == 2){
            [User login:[Static getRequestData:str]];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [Static add:self.navigationController.view msg:@"正在加载"];
            [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
            [[PublicDao shareInstance].rootViewController.centerViewController initData];
        }
    }
    else{
        [Static alert:self.navigationController.view msg:@"上传错误"];
    }
}

#pragma mark GB_KeyboardDelegate

-(void)GB_keyboardWillHide:(NSNotification *)note{
   
}

-(void)GB_keyboardWillShow:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    int keyBoardHeight = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    _scrollView.frame = CGRectMake(0, 0, _scrollView.frame.size.width, [GB_DeviceUtils getScreenHeight]-keyBoardHeight);
    [UIView commitAnimations];
}

#pragma mark GB_ImagePickerDelegate

-(void)GB_imagePickerDidFinish:(UIImage *)image{
    NSMutableArray *params = [NSMutableArray array];
    NSMutableArray *imageParams = [NSMutableArray array];
    UIImage *resultImg = nil;
    if(image.size.width>680||image.size.height>680){
        int w,h;
        if(image.size.width>image.size.height){
            w = 680;
            h = image.size.height*680/image.size.width;
        }
        else{
            h = 680;
            w = image.size.width*680/image.size.height;
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), NO, 0.0);
        [image drawInRect:CGRectIntegral(CGRectMake(0, 0, w, h))];
        UIImage *ALayoutImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        resultImg = ALayoutImage;
    }
    else{
        resultImg = image;
    }
    self.icon = resultImg;
    NSData* data = UIImagePNGRepresentation(image);
    [imageParams addObject:[[GB_KeyValue alloc]initWithKeyValue:@"userfile" value:[[GB_Data alloc]initWithData:data]]];
    [imageParams addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"0"]];
    [Static add:self.navigationController.view msg:@"正在上传"];
    [GB_NetWorkUtils startMultipartPostAsyncRequest:[Url getFileUploadUrl] params:params imageParams:imageParams delegate:self tag:1];
}

-(void)GB_imagePickerDismissFinish:(UIImage *)image{
}

-(void)GB_imagePickerDidCancel{
    
}
    
@end

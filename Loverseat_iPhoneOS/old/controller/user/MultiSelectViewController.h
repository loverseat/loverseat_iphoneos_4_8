//
//  MultiSelectViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "EditDelegate.h"

@interface MultiSelectViewController : BaseViewController
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *defaultValue;
@property (nonatomic, strong) NSArray *contentData;
@property (nonatomic, strong) id<EditDelegate> delegate;
@end

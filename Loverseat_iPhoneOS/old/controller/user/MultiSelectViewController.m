//
//  MultiSelectViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "MultiSelectViewController.h"

@interface MultiSelectViewController()
<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *resultData;
@end

@implementation MultiSelectViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.resultData = [NSMutableArray arrayWithArray:[GB_StringUtils split:self.defaultValue separator:@","]];
    NSMutableArray *arr = [NSMutableArray arrayWithArray:_contentData];
    for (NSString *str in _resultData) {
        if(![_contentData containsObject:str]){
            [arr addObject:str];
        }
    }
    self.contentData = arr;
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:self.title];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60) style:UITableViewStyleGrouped];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.bounces = NO;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int height = 10;
    float maxWidth = [GB_DeviceUtils getScreenWidth]-40;
    float x = 0;
    float fontHeight = [GB_ToolUtils getFontHeight:[UIFont systemFontOfSize:14]];
    for (NSString *str  in _contentData) {
        float width = [GB_ToolUtils getTextWidth:str font:[UIFont systemFontOfSize:14] size:CGSizeMake(maxWidth, fontHeight)];
        width+=30;
        if(x + width > maxWidth){
            x = 0;
            height += fontHeight;
            height += 10;
            height += 10;
        }
        x+=width;
        x+=5;
    }
    height += fontHeight;
    height += 10;
    height += 10;
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    
    float maxWidth = [GB_DeviceUtils getScreenWidth]-40;
    float x = 0;
    float fontHeight = [GB_ToolUtils getFontHeight:[UIFont systemFontOfSize:14]];
    int height = 10;
    int index = 0;
    for (NSString *str  in _contentData) {
        float width = [GB_ToolUtils getTextWidth:str font:[UIFont systemFontOfSize:14] size:CGSizeMake(maxWidth, fontHeight)];
        width+=30;
        if(x + width > maxWidth){
            x = 0;
            height += fontHeight;
            height += 10;
            height += 10;
        }
        
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(20+x, height, width, fontHeight+10) image:nil imageH:nil id:self sel:@selector(c_tag:)];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = fontHeight*0.5+5;
        btn.backgroundColor = [_resultData containsObject:str]?GB_UIColorFromRGB(251, 80, 87):GB_UIColorFromRGB(150, 150, 150);
        btn.tag = index +1;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitle:str forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.contentView addSubview:btn];
        
        x+=width;
        x+=5;
        index ++;
    }
    
    
    
    height += fontHeight;
    height += 10;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)c_tag:(UIButton *)btn{
    NSString *str = [_contentData objectAtIndex:btn.tag-1];
    if([_resultData containsObject:str]){
        [_resultData removeObject:str];
        [btn setBackgroundColor:GB_UIColorFromRGB(150, 150, 150)];
    }
    else{
        [_resultData addObject:str];
        [btn setBackgroundColor:GB_UIColorFromRGB(251, 80, 87)];
    }
}

-(void)cb_back{
    int i = 0;
    NSMutableString *rStr = [NSMutableString string];
    for (NSString *str in _resultData) {
        if(i>0){
            [rStr appendString:@","];
        }
        [rStr appendString:str];
        i++;
    }
    [self.delegate changeValue:rStr valueId:nil type:self.type];
    [super cb_back];
}

@end

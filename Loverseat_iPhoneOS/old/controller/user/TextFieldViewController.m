//
//  TextFieldViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TextFieldViewController.h"
#import "UserEditCell.h"

@interface TextFieldViewController ()
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, assign) UserEditCell *cell;
@property (nonatomic, strong) UITextField *textField;
@end

@implementation TextFieldViewController

-(id)init{
    self = [super init];
    if(self){
        self.keyboardType = UIKeyboardAppearanceDefault;
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:self.title];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60) style:UITableViewStyleGrouped];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.bounces = NO;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"UserEditCellIdentifier";
    UserEditCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          identifier];
    if (cell == nil) {
        cell = [[UserEditCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
        self.cell = cell;
        self.textField = cell.tf;
        cell.tf.keyboardType = _keyboardType;
        [cell setDefaultValue:self.defaultValue];
    }
    return cell;
}

-(void)cb_back{
    if([GB_StringUtils isBlank:_textField.text]&&!_canNull){
        [GB_AlertUtils alert:self.navigationController.view msg:@"不能为空"];
        return;
    }
    [self.delegate changeValue:self.textField.text valueId:nil type:self.type];
    [super cb_back];
}

@end

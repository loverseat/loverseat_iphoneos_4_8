//
//  UserInfoViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//


#import "HomeViewController.h"
#import "HomeLeftViewController.h"

#import "UserInfoViewController.h"
#import "PhotoBean.h"
#import "PictureViewController.h"
#import "EditViewController.h"
#import <Accelerate/Accelerate.h>
#import "DateAllCenterViewController.h"
#import "PictureViewController.h"
#import "DateRecordViewController.h"



@interface UserInfoViewController ()<UIActionSheetDelegate>
@property (nonatomic, strong) UIScrollView *contentView;
@property (nonatomic, strong) UIImageView *iconBgView;
@property (nonatomic, strong) UIView *iconModelView;
@property (nonatomic, strong) NSMutableArray *photoArr;


@property (nonatomic, weak) UIButton *reportBtn;

@property (nonatomic, assign) BOOL isSecondActionSheet;
@property (nonatomic,strong) UIActionSheet *actionSheet;
@property (nonatomic,strong) UIActionSheet *secondActionSheet;
@property (nonatomic, assign) BOOL isOne;
@property (nonatomic, assign) BOOL isTwo;
@end

@implementation UserInfoViewController

-(void)cb_back{
    [super cb_back];
    if(_didEdit){
        if(_centerViewController){
            [_centerViewController clearData];
            [_centerViewController initData:YES];
        }
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"personalCenter"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"personalCenter"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isSecondActionSheet = NO;
    
    self.photoArr = [NSMutableArray array];
    [self initFrame];
    [self initData];
}

-(void)initFrame{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIScrollView *contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
    
    [self.view addSubview:contentView];
    self.contentView = contentView;
}

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:self.uid]];
        if([User isLogin]){
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"positive_uid" value:[User getUserInfo].uid]];
        }
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUserInfoUrl] list:arr delegate:self tag:1];
    }
}

-(void)GB_requestDidFailed:(int)tag
{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag
{
    str = [Error getCurrentStr:str];
    if([Error verify:str view :self.view]){
        if(tag == 1){
            [Static remove:self.navigationController.view];
            self.isLoad = YES;
            self.bean = [UserBean getBean:[Static getRequestData:str]];
            if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
                //[Static add:self.navigationController.view msg:@"正在加载"];
                NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:self.uid]];
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"3"]];
                [GB_NetWorkUtils startPostAsyncRequest:[Url getDealAlbumUrl] list:arr delegate:self tag:2];
            }
        }
        else if(tag == 2){
            [Static remove:self.navigationController.view];
            if([[Static getRequestData:str] isKindOfClass:[NSArray class]]){
                [self.photoArr setArray:[PhotoBean getBeanList:[Static getRequestData:str]]];
            }
            [self initBeanFrame];
        }
        else if(tag == 3){
            NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
            if([GB_StringUtils isBlank:[dic objectForKey:@"path"]])return;
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:self.uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"photo_url" value:[dic objectForKey:@"path"]]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"1"]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getDealAlbumUrl] list:arr delegate:self tag:4];
        }
        else if(tag == 4){
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:self.uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"3"]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getDealAlbumUrl] list:arr delegate:self tag:2];
        }
        else if (tag == 5){
            [Static remove:self.navigationController.view];
            [self initData];
            [User login:[Static getRequestData:str]];
            [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
        }
        else if (tag == 6){
            
        }
    }
}

-(void)initBeanFrame{
    for (UIView *v in self.contentView.subviews) {
        [v removeFromSuperview];
    }
    self.iconBgView = [[UIImageView alloc]init];
    [self.contentView addSubview:_iconBgView];
    
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60.0f, [NavUtils getNavHeight]) image:[UIImage imageNamed:@"btn_back_white"] imageH:nil id:self sel:@selector(cb_back)];
    [self.contentView addSubview:navBackBtn];

    if([self.uid intValue] == [[User getUserInfo].uid intValue]){
        UIButton *rightBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-60-[GB_ToolUtils getTextWidth:@"编辑" font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]+28, 2, 60+[GB_ToolUtils getTextWidth:@"编辑" font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]-28, [NavUtils getNavHeight]) image:nil imageH:nil id:self sel:@selector(c_edit)];
        rightBtn.titleLabel.font = [Static getFont:32 isBold:NO];
        [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [rightBtn setTitle:@"编辑" forState:UIControlStateHighlighted];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.contentView addSubview:rightBtn];

    }
    UILabel *l = [GB_WidgetUtils getLabel:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [NavUtils getNavHeight]) title:_bean.tu_nickname font:[Static getFont:36 isBold:NO] color:[UIColor whiteColor]];
    l.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:l];

    float height = 10+[NavUtils getNavHeight];

#pragma mark - 个人中心按钮
    self.iconBtn = [[UIButton alloc]initWithFrame:CGRectMake(([GB_DeviceUtils getScreenWidth]-120)*0.5, height, 120, 120)];
    _iconBtn.layer.masksToBounds = YES;
    _iconBtn.layer.cornerRadius = 60.0f;
    [_iconBtn setBackgroundImage:[UIImage imageNamed:_bean.tu_gender.intValue == 0?@"default_male":@"default_female"] forState:UIControlStateNormal];
    [_iconBtn addTarget:self action:@selector(c_user) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_iconBtn];
    
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:_bean.tu_gender.intValue == 0?@"icon_male_large":@"icon_female_large"]];
    icon.frame = CGRectMake(187, 152.25, 30, 30);
    [_contentView addSubview:icon];


    if([self.uid intValue] != [[User getUserInfo].uid intValue]){
#pragma mark - 给对方发消息按钮
        UIButton *msgBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-60, height+40.5f, 39, 39) image:[UIImage imageNamed:@"btn_user_info_msg"] imageH:nil id:self sel:@selector(c_msg)];
        [_contentView addSubview:msgBtn];
        
#pragma mark - 举报按钮
        UIButton *reportBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth] - CGRectGetMaxX(msgBtn.frame), height+40.5f, 39, 39) image:[UIImage imageNamed:@"user_info_reportBtn"] imageH:nil id:self sel:@selector(reportBtnClick)];
        [self.contentView addSubview:reportBtn];
        self.reportBtn = reportBtn;
    }
    

    
    
    [GB_NetWorkUtils loadImage:[Url getImageUrl:self.bean.tu_portrait] container:_iconBtn type:GB_ImageCacheTypeAll delegate:self tag:-2];


    height += 140;
    
    NSString *info = [GB_StringUtils isBlank:self.bean.tu_star_sign]?[NSString stringWithFormat:@"%d岁",[Static getAgeByBirthday:self.bean.tu_birthday]]:[NSString stringWithFormat:@"%d岁，%@",[Static getAgeByBirthday:self.bean.tu_birthday],self.bean.tu_star_sign];
    
    
    
    UILabel *infoL = [GB_WidgetUtils getLabel:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], [GB_ToolUtils getFontHeight:[Static getFont:28 isBold:YES]]) title:info font:[Static getFont:28 isBold:YES] color:[UIColor whiteColor]];
    infoL.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:infoL];
    height+=[GB_ToolUtils getFontHeight:[Static getFont:28 isBold:YES]];
    height+=5;
    
    
    NSString *sign = self.bean.tu_signature;
    float signHeight = [GB_ToolUtils getTextHeight:sign font:[Static getFont:28 isBold:NO] size:CGSizeMake([GB_DeviceUtils getScreenWidth]-100, INT_MAX)];

    
    UILabel *signL = [GB_WidgetUtils getLabel:CGRectMake(50, height, [GB_DeviceUtils getScreenWidth]-100, signHeight) title:sign font:[Static getFont:28 isBold:NO] color:[UIColor whiteColor]];
    signL.numberOfLines = 0;
    signL.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:signL];
    
    height += signHeight;
  
    height += 15;
    
#pragma mark - 邀约记录按钮
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]/2-60, height, 130, 30)];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"邀约记录" forState:UIControlStateNormal];
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    btn.layer.cornerRadius = 5;
    btn.layer.borderWidth = 0.5f;
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:btn];
    height += btn.frame.size.height;
    height += 30;
    _iconModelView.frame = CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], height);
    _iconBgView.frame = CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], height);

    
    UIImageView *iconBgViewModel = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], height)];
    iconBgViewModel.image = [UIImage imageNamed:@"bg_profile_shadow"];
    [_iconBgView addSubview:iconBgViewModel];

    _iconBgView.image = [self blurryImage:[Static getFImage:[UIImage imageNamed:@"bg_user_info"] w:[GB_DeviceUtils getScreenWidth] h:_iconBgView.frame.size.width] withBlurLevel:8];
    
    UIScrollView *photoView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 184)];
    int i = 0;
   
    if([GB_ToolUtils isNotBlank:_photoArr]){
        for (PhotoBean *bean in _photoArr) {
            if(i == 8)break;
            int row = floor(i/4);
            int count = i%4;
            UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(count*74+15, 20+row*74, 68, 68) image:nil imageH:nil id:self sel:@selector(c_photo:)];
            [GB_NetWorkUtils loadImage:bean.ta_photo_url container:btn type:GB_ImageCacheTypeAll delegate:self tag:i+1];
            btn.tag = i+1;
            btn.layer.masksToBounds = YES;
            btn.layer.cornerRadius = 5;
            [photoView addSubview:btn];
            i++;
        }
    }
    
    if([self.uid intValue] == [[User getUserInfo].uid intValue] && i<7){
        int row = floor(i/4);
        int count = i%4;
        i++;
        UIButton *addBtn = [GB_WidgetUtils getButton:CGRectMake(count*74+15, 20+row*74, 68, 68) image:[UIImage imageNamed:@"btn_add_photo"] imageH:nil id:self sel:@selector(c_addphoto)];
        addBtn.layer.masksToBounds = YES;
        addBtn.layer.cornerRadius = 5;
        [photoView addSubview:addBtn];
    }
    if(i>0){
        [self.contentView addSubview:photoView];
        height += 20;
        height += 74;
        height+=14;
        if(i>4){
            height += 74;
        }
        else{
            photoView.frame = CGRectMake(photoView.frame.origin.x, photoView.frame.origin.y
                                         , photoView.frame.size.width, 110);
        }
    }

    
    
    
    [self.contentView addSubview:[View getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0.5f) color:GB_UIColorFromRGB(200, 200, 200)]];
    height += 20;
    
    if([GB_StringUtils isNotBlank:self.bean.tu_tabs]){

#pragma mark - 个性标签
//        UILabel *tagsAlertL = [GB_WidgetUtils getLabel:CGRectMake(15, height, [GB_DeviceUtils getScreenWidth]-30, [GB_ToolUtils getFontHeight:[Static getFont:28 isBold:YES]]) title:@"个性标签" font:[Static getFont:28 isBold:YES] color:GB_UIColorFromRGB(80, 80, 80)];
//        [self.contentView addSubview:tagsAlertL];
//        
//        
//        height+=[GB_ToolUtils getFontHeight:[Static getFont:28 isBold:YES]];
//        height+=20;
//        
//        NSArray *strArr = [GB_StringUtils split:self.bean.tu_tabs separator:@","];
//        
//        float maxWidth = [GB_DeviceUtils getScreenWidth]-40;
//        float x = 0;
//        float fontHeight = [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]];
//        for (NSString *str  in strArr) {
//            float width = [GB_ToolUtils getTextWidth:str font:[Static getFont:24 isBold:NO] size:CGSizeMake(maxWidth, fontHeight)];
//            width+=20;
//            if(x + width > maxWidth){
//                x = 0;
//                height += fontHeight;
//                height += 10;
//                height += 10;
//            }
//            
//            UILabel *label = [GB_WidgetUtils getLabel:CGRectMake(20+x, height, width, fontHeight+10)  title:str font:[Static getFont:24 isBold:NO] color:[UIColor whiteColor]];
//            label.layer.masksToBounds = YES;
//            label.layer.cornerRadius = fontHeight*0.5+5;
//            label.backgroundColor = GB_UIColorFromRGB(251, 80, 87);
//            label.textAlignment = NSTextAlignmentCenter;
//            [self.contentView addSubview:label];
//            
//            x+=width;
//            x+=5;
//        }
//        
//        
//        
//        height += fontHeight;
//        height += 40;
        
        
        
        
//        [self.contentView addSubview:[View getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0.5f) color:GB_UIColorFromRGB(200, 200, 200)]];
//        height -= 18;
//        
//        UILabel *detialAlertL = [GB_WidgetUtils getLabel:CGRectMake(15, height, [GB_DeviceUtils getScreenWidth]-15, [GB_ToolUtils getFontHeight:[Static getFont:28 isBold:YES]]) title:@"详细信息" font:[Static getFont:28 isBold:YES] color:GB_UIColorFromRGB(80, 80, 80)];
//        [self.contentView addSubview:detialAlertL];
//        
//        
//        height+=[GB_ToolUtils getFontHeight:[Static getFont:28 isBold:YES]];
//        height+=20;
    }
    
    
    for (int i = 0 ; i<6; i++) {
        NSString *str = nil;
        NSString *currentStr;
        if(i == 0){
            str = self.bean.tu_city;
            currentStr = str;
        }
        if(i == 1){
            str = self.bean.tu_occupation;
            currentStr = str;
        }
        else if(i == 2){
            str = [NSString stringWithFormat:@"%@元／月",self.bean.tu_income];
            currentStr = self.bean.tu_income;
        }
        else if(i == 3){
            if([self.bean.tu_marital_status intValue]==0){
                str = @"未婚";
            }
            else{
                str = @"已婚";
            }
            currentStr = str;
        }
        else if(i == 4){
            str = self.bean.tu_favour;
            currentStr = str;
        }
        else if(i == 5){
            str = self.bean.tu_purpose;
            currentStr = str;
        }
        
        if([GB_StringUtils isBlank:currentStr])continue;
        UIFont *rowFont = [Static getFont:32 isBold:NO];
        CGSize textSize = [GB_ToolUtils getTextSize:str font:rowFont size:CGSizeMake([GB_DeviceUtils getScreenWidth]-50-40, INT_MAX)];
        float rowHeight = 55-[GB_ToolUtils getFontHeight:rowFont]+(textSize.height==0?[GB_ToolUtils getFontHeight:rowFont]:textSize.height);
        
        UIView *bg = [[UIView alloc]initWithFrame:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], rowHeight)];
        bg.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:bg];
        
        UIImageView *rowIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"user_info_icon_%d",i+1]]];
        rowIcon.center = CGPointMake(37.5f, 27.5);
        [bg addSubview:rowIcon];
        
        height+=rowHeight;
        
        UILabel *label = [GB_WidgetUtils getLabel:CGRectMake(20+50, 0, textSize.width, rowHeight) title:str font:rowFont color:GB_UIColorFromRGB(80, 80, 80)];
        label.numberOfLines = 0;
        [bg addSubview:label];
    }
    self.contentView.contentSize = CGSizeMake([GB_DeviceUtils getScreenWidth], height);
}
//上传图像的点击事件
-(void)c_addphoto{
    UIActionSheet *ac = nil;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ac = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:@"取消"
                           destructiveButtonTitle:nil
                                otherButtonTitles:@"相册",@"拍照",nil];
    }
    else
    {
        ac = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:@"取消"
                           destructiveButtonTitle:nil
                                otherButtonTitles:@"相册",nil];
    }
    ac.tag = -1;
    ac.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [ac showInView:self.view];
}

-(void)c_edit{
    if([GB_ToolUtils isBlank:self.bean])return;
    EditViewController *edit = [[EditViewController alloc]init];
    edit.bean = self.bean;
    edit.userInfoViewController = self;
    [self.navigationController pushViewController:edit animated:YES];
}

#pragma mark - 个人中心的头像点击事件
//个人中心的头像点击事件
-(void)c_user{
    NSMutableArray *arr = [NSMutableArray array];
    [arr addObject:[Url getImageUrl:self.bean.tu_portrait]];

    PictureViewController *pic = [[PictureViewController alloc]initWithSourceArr:arr index:0 scrollViewFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    pic.uid = self.bean.uid;
    [self.navigationController pushViewController:pic animated:YES];
    
}

#pragma mark - 图片的点击事件
//图片的点击事件
-(void)c_photo:(UIButton *)btn{
    if([self.uid intValue] == [[User getUserInfo].uid intValue]){
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"预览",@"上传头像",@"删除", nil];
        sheet.tag = btn.tag-1;
        [sheet showInView:self.view];
    }
    else{
        NSMutableArray *arr = [NSMutableArray array];
        for (PhotoBean *bean in _photoArr) {
            [arr addObject:[Url getImageUrl:bean.ta_photo_url]];
        }
        PictureViewController *pic = [[PictureViewController alloc]initWithSourceArr:arr index:btn.tag-1 scrollViewFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
        [self.navigationController pushViewController:pic animated:YES];
    }
}

#pragma mark - 在好友的信息界面给他发消息
-(void)c_msg{
    
//    MsgDetailViewController *msgViewController = [[MsgDetailViewController alloc]init];
//    msgViewController.name = _bean.tu_nickname;
//    msgViewController.fromId = _bean.uid;
//    [self.navigationController pushViewController:msgViewController animated:YES];
    
    
//    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:_bean.tu_nickname isGroup:NO];
//#warning 传递参数有问题所以聊天记录不会被存储
//    chatVC.title = _bean.tu_nickname;
//    chatVC.fromId = _bean.uid;
//    [self.navigationController pushViewController:chatVC animated:YES];
    
    
    //LZLog(@"%@",_bean.uid);
    
}


#warning ---- 这里还没搞定
#pragma mark - actionSheet的代理方法
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (self.isOne) {
        
        
        if (!self.isTwo) {
            self.isTwo = YES;
            
            
            
            if (buttonIndex == 0) { //
                
                self.isSecondActionSheet = YES;
                
                _secondActionSheet = [[UIActionSheet alloc]
                                      initWithTitle:@"title,nil时不显示"
                                      delegate:self
                                      cancelButtonTitle:@"取消"
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:@"第一项", @"第二项",nil];
                _secondActionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
                [_secondActionSheet showInView:self.view];
                
            }
            
        }else{
            
            self.isTwo = NO;
            self.isOne = NO;
            if (buttonIndex == 0) {
                NSLog(@"111");
            }
            
            if (buttonIndex == 1) {
                NSLog(@"222");
            }
            
            if (buttonIndex == 2) {
                NSLog(@"取消");
            }
        }
        
        
        
        
        
                
                
                    
//                }
//                else{  // 点击了第二项 第一项界面
//                    
//                    
//                    self.isSecondActionSheet = NO;
//                    
//                    if(buttonIndex == 2){
//                        
//                    }else if(buttonIndex == 0){
//                        NSLog(@"第一项");
//                    }else{
//                        NSLog(@"第二项");
//                    }
//                    
//                    
//                    
//                }
        
//        }
        
        
//        [MBProgressHUD showMessage:@"赶快升级到IOS8吧,不然不让你做操作!!!"];
        
        
    }else{
        if(actionSheet.tag == -1){
            if(buttonIndex == 0){
                [GB_ImagePickerUtils pickerImageWithType:GB_ImagePickerTypePhotoLibrary allowsEditing:NO viewController:self];
            }
            if(buttonIndex == 1){
                [GB_ImagePickerUtils pickerImageWithType:GB_ImagePickerTypeCamera  allowsEditing:NO viewController:self];
            }
        }
        else{
            if(buttonIndex == 0){
                NSMutableArray *arr = [NSMutableArray array];
                for (PhotoBean *bean in _photoArr) {
                    [arr addObject:[Url getImageUrl:bean.ta_photo_url]];
                }
                PictureViewController *pic = [[PictureViewController alloc]initWithSourceArr:arr index:actionSheet.tag scrollViewFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
                [self.navigationController pushViewController:pic animated:YES];
            }
            else if (buttonIndex == 1){
                NSMutableArray *arr =[NSMutableArray array];
                //[Static add:self.navigationController.view msg:@"正在上传"];
                for(PhotoBean *bean in _photoArr){
                    [arr addObject:[Url getImageUrl:bean.ta_photo_url]];
                }
                NSInteger i = actionSheet.tag;
                NSString *img = [arr objectAtIndex:i];
                NSMutableArray *arr1 = [NSMutableArray arrayWithArray:[Url getParmsArr]];
                [arr1 addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value: [_photoArr[i] ta_uid]]];
                [arr1 addObject:[[GB_KeyValue alloc]initWithKeyValue:@"portrait" value:img]];
                [GB_NetWorkUtils startPostAsyncRequest:[Url getUserInfoUpdateUrl] list:arr1 delegate:self tag:5];
            }
            else if(buttonIndex == 2){
                if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
                    [GB_NetWorkUtils cancelRequest];
                    PhotoBean *bean = [_photoArr objectAtIndex:actionSheet.tag];
                    [Static add:self.navigationController.view msg:@"正在删除"];
                    NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
                    [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
                    [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"photo_id" value:bean.ta_id]];
                    [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"2"]];
                    [GB_NetWorkUtils startPostAsyncRequest:[Url getDealAlbumUrl] list:arr delegate:self tag:2];
                }
            }
            
        }
        
    }
}

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    if(tag > 0){
        [GB_NetWorkUtils useImage:[Static getFImage:image w:106 h:106] container:container];
    }
    else{
        if(tag == -2){
            if([GB_ToolUtils isNotBlank:image])
                [GB_NetWorkUtils useImage:[Static getFImage:image w:120 h:120] container:container];
        }
    }
}

- (UIImage *)blurryImage:(UIImage *)image withBlurLevel:(CGFloat)blur {
    if (blur < 0.f || blur > 1.f) {
        blur = 0.5f;
    }
    int boxSize = (int)(blur * 100);
    boxSize = boxSize - (boxSize % 2) + 1;
    
    CGImageRef img = image.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    
    void *pixelBuffer;
    
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) *
                         CGImageGetHeight(img));
    
    if(pixelBuffer == NULL)
        NSLog(@"No pixelbuffer");
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    error = vImageBoxConvolve_ARGB8888(&inBuffer,
                                       &outBuffer,
                                       NULL,
                                       0,
                                       0,
                                       boxSize,
                                       boxSize,
                                       NULL,
                                       kvImageEdgeExtend);
    
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(
                                             outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             kCGImageAlphaNoneSkipLast);
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);
    
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(imageRef);
    
    return returnImage;
}




-(void)GB_imagePickerDidFinish:(UIImage *)image{
    [GB_NetWorkUtils cancelRequest];
    NSMutableArray *params = [NSMutableArray array];
    NSMutableArray *imageParams = [NSMutableArray array];
    //    UIImage *resultImg = nil;
    //    if(image.size.width>680||image.size.height>680){
    //        int w,h;
    //        if(image.size.width>image.size.height){
    //            w = 680;
    //            h = image.size.height*680/image.size.width;
    //        }
    //        else{
    //            h = 680;
    //            w = image.size.width*680/image.size.height;
    //        }
    //        UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), NO, 0.0);
    //        [image drawInRect:CGRectIntegral(CGRectMake(0, 0, w, h))];
    //        UIImage *ALayoutImage = UIGraphicsGetImageFromCurrentImageContext();
    //        UIGraphicsEndImageContext();
    //        resultImg = ALayoutImage;
    //    }
    //    else{
    //        resultImg = image;
    //    }
    NSData* data = UIImagePNGRepresentation(image);
    [imageParams addObject:[[GB_KeyValue alloc]initWithKeyValue:@"userfile" value:[[GB_Data alloc]initWithData:data]]];
    [imageParams addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"0"]];
    [Static add:self.view msg:@"正在上传"];
    [GB_NetWorkUtils startMultipartPostAsyncRequest:[Url getFileUploadUrl] params:params imageParams:imageParams delegate:self tag:3];
}

-(void)GB_imagePickerDismissFinish:(UIImage *)image{
    
}

#pragma mark - 查看邀约记录
//查看邀约记录
- (void)btnClick
{
    DateRecordViewController *record = [[DateRecordViewController alloc]init];
    record.uid = self.uid;
    [self.navigationController pushViewController:record animated:YES];
}

#pragma mark - 举报按钮的点击
- (void)reportBtnClick
{
        self.isOne = YES;
        _actionSheet = [[UIActionSheet alloc]
                    initWithTitle:nil
                    delegate:self
                    cancelButtonTitle:@"取消"
                    destructiveButtonTitle:@"举报"
                    otherButtonTitles:nil];
        _actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [_actionSheet showInView:[self view]];
}

@end

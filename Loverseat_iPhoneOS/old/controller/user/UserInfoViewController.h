//
//  UserInfoViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
@class UserBean;
@class DateAllCenterViewController;
@interface UserInfoViewController : BaseViewController
<GB_LoadImageDelegate, UIActionSheetDelegate,GB_ImagePickerDelegate>
@property (nonatomic, assign) BOOL didEdit;
@property (nonatomic, strong) UserBean *bean;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) DateAllCenterViewController *centerViewController;
@property (nonatomic, strong) UIButton *iconBtn;


-(void)initBeanFrame;
-(void)initData;
@end

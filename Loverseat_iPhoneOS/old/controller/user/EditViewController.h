//
//  EditViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "EditDelegate.h"

@class UserBean;
@class UserInfoViewController;
@interface EditViewController : BaseViewController
<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate,EditDelegate>
@property(nonatomic,strong)UserBean *bean;
@property(nonatomic,strong)UserInfoViewController *userInfoViewController;
-(void)changeValue:(NSString *)value valueId:(NSString *)valueId type:(int)type;
@end

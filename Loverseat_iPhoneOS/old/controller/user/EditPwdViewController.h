//
//  EditPwdViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface EditPwdViewController : BaseViewController
<GB_NetWorkDelegate, UITableViewDataSource, UITableViewDelegate, GB_KeyboardDelegate>
@end

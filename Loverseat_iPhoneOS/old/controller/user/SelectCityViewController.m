//
//  SelectCityViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "SelectCityViewController.h"
#import "CityCell.h"
#import "EditViewController.h"

@interface SelectCityViewController()

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SelectCityViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"选择城市"];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    [tableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return _source.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CityCellIdentifier";
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:
                      identifier];
    if (cell == nil) {
        cell = [[CityCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    cell.title.text = [[_source objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = [_source objectAtIndex:indexPath.row];
    [self.delegate changeValue:[NSString stringWithFormat:@"%@:%@",__name,[dic objectForKey:@"name"]] valueId:[NSString stringWithFormat:@"%@:%@",[dic objectForKey:@"pid"],[dic objectForKey:@"id"]] type:self.type];
    [self.navigationController popToViewController:_delegate animated:YES];
    
}



@end

//
//  UserTagsViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserTagsViewController.h"
#import "EditViewController.h"
#import "UserEditCell.h"

@interface UserTagsViewController();
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, assign) UserEditCell *cell;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *tabsArr;
@end
@implementation UserTagsViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
    [GB_ToolUtils addKeyboardNotification:self];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:self.title];
    [NavUtils addRightButton:self title:@"添加" sel:@selector(c_add)];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60) style:UITableViewStyleGrouped];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight]+35+55, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-([NavUtils getNavHeight]+35+55+20))];
    scrollView.backgroundColor = [UIColor clearColor];
    self.tabsArr = [NSMutableArray arrayWithArray:[GB_StringUtils split:self.defaultValue separator:@","]];
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    [self reloadTagScrollView];
}

-(void)reloadTagScrollView{
    float height  = 10;
    for (UIView *v in _scrollView.subviews) {
        [v removeFromSuperview];
    }
    float maxWidth = [GB_DeviceUtils getScreenWidth]-20;
    float x = 0;
    float fontHeight = [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]];
    int i = 0;
    for (NSString *str  in _tabsArr) {
        float width = [GB_ToolUtils getTextWidth:str font:[Static getFont:24 isBold:NO] size:CGSizeMake(maxWidth, fontHeight)];
        width+=30;
        if(x + width > maxWidth){
            x = 0;
            height += fontHeight;
            height += 10;
            height += 10;
        }
       
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, fontHeight+10), NO, 0.0);
        [[GB_ToolUtils getPureImg:GB_UIColorFromRGB(251, 80, 87) size:CGSizeMake(width, fontHeight+10)]  drawInRect:CGRectIntegral(CGRectMake(0,0,width, fontHeight+10))];
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setObject:(id)[UIColor whiteColor] forKey:(NSString *)NSForegroundColorAttributeName];
        CTFontRef fontRef = CTFontCreateWithName((CFStringRef)[Static getFont:24 isBold:NO].fontName, [Static getFont:24 isBold:NO].pointSize, NULL);
        [attributes setObject:(id)CFBridgingRelease(fontRef) forKey:(NSString *)kCTFontAttributeName];
        [str drawInRect:CGRectMake(10, (fontHeight+10)*0.5-[GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]*0.5, width, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) withAttributes:attributes];
        
        
        [[UIImage imageNamed:@"icon_cha"] drawInRect:CGRectIntegral(CGRectMake(width-20, (fontHeight+10)*0.5-4, 8, 8))];
        UIImage *ALayoutImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(10+x, height, width, fontHeight+10) image:ALayoutImage imageH:nil id:self sel:@selector(c_tags:)];
        btn.layer.masksToBounds = YES;
        btn.tag = i;
        btn.layer.cornerRadius = fontHeight*0.5+5;
        [_scrollView addSubview:btn];
        
        x+=width;
        x+=5;
        i++;
    }
    
    height += fontHeight;
    height += 10;
    height += 10;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, height);
}

-(void)c_add{
    if([GB_StringUtils isBlank:_textField.text]){
        [Static alert:self.navigationController.view msg:@"描述不能为空"];
        return;
    }
    if(_textField.text.length>20){
        [Static alert:self.navigationController.view msg:@"最多20个字"];
        return;
    }
    [_tabsArr addObject:_textField.text];
    [self reloadTagScrollView];
    _textField.text = nil;
}

-(void)c_tags:(UIButton *)btn{
    [_tabsArr removeObjectAtIndex:btn.tag];
    [self reloadTagScrollView];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"UserEditCellIdentifier";
    UserEditCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          identifier];
    if (cell == nil) {
        cell = [[UserEditCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
        self.cell = cell;
        self.textField = cell.tf;
    }
    return cell;
}


-(void)GB_keyboardWillHide:(NSNotification *)note{
    NSDictionary *dic = [note userInfo];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[dic objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    self.scrollView.frame = CGRectMake(0, [NavUtils getNavHeight]+35+55, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-([NavUtils getNavHeight]+30+55));
    [UIView commitAnimations];
}

-(void)GB_keyboardWillShow:(NSNotification *)note{
    NSDictionary *dic = [note userInfo];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[dic objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    self.scrollView.frame = CGRectMake(0, [NavUtils getNavHeight]+35+55, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-([NavUtils getNavHeight]+30+55)-[[dic objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height);
    [UIView commitAnimations];
}

-(void)GB_resignFirstResponder{

}


-(void)cb_back{
    int i = 0;
    NSMutableString *rStr = [NSMutableString string];
    for (NSString *str in _tabsArr) {
        if(i>0){
            [rStr appendString:@","];
        }
        [rStr appendString:str];
        i++;
    }
    [self.editViewController changeValue:rStr valueId:nil type:self.type];
    [super cb_back];
}

@end

//
//  SelectCityViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "EditDelegate.h"

@interface SelectCityViewController : BaseViewController
<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *source;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *_name;
@property (nonatomic, strong) UIViewController<EditDelegate> *delegate;
@end

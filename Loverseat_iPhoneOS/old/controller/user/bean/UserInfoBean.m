//
//  UserInfoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-3.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserInfoBean.h"
#import <GeekBean4IOS/GB_ToolUtils.h>

@implementation UserInfoBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        UserInfoBean *bean = [UserInfoBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}


+(UserInfoBean *)copyBean:(UserInfoBean *)b{
    UserInfoBean *bean = [[UserInfoBean alloc]init];
    
    bean.tu_id = b.tu_id;
    bean.tu_username = b.tu_username;
    bean.tu_weixin = b.tu_weixin;
    bean.tu_nickname = b.tu_nickname;
    bean.tu_alipay_account = b.tu_alipay_account;
    bean.tu_alipay_name = b.tu_alipay_name;
    bean.tu_wallet_password = b.tu_wallet_password;
    bean.tu_gender = b.tu_gender;
    bean.tu_birthday = b.tu_birthday;
    bean.tu_star_sign = b.tu_star_sign;
    
    bean.tu_province = b.tu_province;
    bean.tu_province_id = b.tu_province_id;
    bean.tu_city = b.tu_city;
    bean.tu_portrait = b.tu_portrait;
    bean.tu_fakeid = b.tu_fakeid;
    bean.tu_password = b.tu_password;
    bean.tu_city_id = b.tu_city_id;
    bean.tu_address = b.tu_address;
    
    
    bean.tu_secondhand_cityid = b.tu_secondhand_cityid;
    bean.tu_category = b.tu_category;
    bean.tu_mobile = b.tu_mobile;
    bean.tu_email = b.tu_email;
    bean.tu_source = b.tu_source;
    bean.tu_authed = b.tu_authed;
    bean.tu_created = b.tu_created;
    bean.tu_signature = b.tu_signature;
    bean.tu_voice_signature = b.tu_voice_signature;
    bean.tu_occupation = b.tu_occupation;
    
    
    bean.tu_income = b.tu_income;
    bean.tu_marital_status = b.tu_marital_status;
    bean.tu_favour = b.tu_favour;
    bean.tu_purpose = b.tu_purpose;
    bean.tu_coin = b.tu_coin;
    bean.tu_score = b.tu_score;
    bean.tu_exper_value = b.tu_exper_value;
    bean.tu_tabs = b.tu_tabs;
  
    
    bean.tu_rank = b.tu_rank;
    return bean;
}
+(UserInfoBean *)getBean:(NSDictionary *)dic{
    UserInfoBean *bean = [[UserInfoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tu_star_sign = [dic objectForKey:@"tu_star_sign"];
        bean.tu_id = [dic objectForKey:@"tu_id"];
        bean.tu_username = [dic objectForKey:@"tu_username"];
        bean.tu_weixin = [dic objectForKey:@"tu_weixin"];
        bean.tu_nickname = [dic objectForKey:@"tu_nickname"];
        bean.tu_alipay_account = [dic objectForKey:@"tu_alipay_account"];
        bean.tu_alipay_name = [dic objectForKey:@"tu_alipay_name"];
        bean.tu_wallet_password = [dic objectForKey:@"tu_wallet_password"];
        bean.tu_gender = [dic objectForKey:@"tu_gender"];
        bean.tu_province_id = [dic objectForKey:@"tu_province_id"];
        bean.tu_birthday = [dic objectForKey:@"tu_birthday"];
        bean.tu_address = [dic objectForKey:@"tu_address"];
        
        bean.tu_province = [dic objectForKey:@"tu_province"];
        bean.tu_city = [dic objectForKey:@"tu_city"];
        bean.tu_portrait = [dic objectForKey:@"tu_portrait"];
        bean.tu_fakeid = [dic objectForKey:@"tu_fakeid"];
        bean.tu_password = [dic objectForKey:@"tu_password"];
        bean.tu_city_id = [dic objectForKey:@"tu_city_id"];
        
        bean.tu_secondhand_cityid = [dic objectForKey:@"tu_secondhand_cityid"];
        bean.tu_category = [dic objectForKey:@"tu_category"];
        bean.tu_mobile = [dic objectForKey:@"tu_mobile"];
        bean.tu_email = [dic objectForKey:@"tu_email"];
        bean.tu_source = [dic objectForKey:@"tu_source"];
        bean.tu_authed = [dic objectForKey:@"tu_authed"];
        bean.tu_created = [dic objectForKey:@"tu_created"];
        bean.tu_signature = [dic objectForKey:@"tu_signature"];
        bean.tu_voice_signature = [dic objectForKey:@"tu_voice_signature"];
        bean.tu_occupation = [dic objectForKey:@"tu_occupation"];
        bean.tu_income = [dic objectForKey:@"tu_income"];
        bean.tu_marital_status = [dic objectForKey:@"tu_marital_status"];
        
        bean.tu_favour = [dic objectForKey:@"tu_favour"];
        bean.tu_purpose = [dic objectForKey:@"tu_purpose"];
        bean.tu_coin = [dic objectForKey:@"tu_coin"];
        bean.tu_score = [dic objectForKey:@"tu_score"];
        bean.tu_exper_value = [dic objectForKey:@"tu_exper_value"];
        bean.tu_tabs = [dic objectForKey:@"tu_tabs"];
        bean.tu_rank = [dic objectForKey:@"tu_rank"];
    }
    return bean;
}
@end

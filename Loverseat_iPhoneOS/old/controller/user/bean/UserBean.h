//
//  UserBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/25.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface UserBean : BaseBean

@property (nonatomic, strong) NSString *tu_city;
@property (nonatomic, strong) NSString *tu_city_id;
@property (nonatomic, strong) NSString *tu_email;
@property (nonatomic, strong) NSString *tu_birthday;
@property (nonatomic, strong) NSString *tu_favour;
@property (nonatomic, strong) NSString *tu_gender;
@property (nonatomic, strong) NSString *tu_income;
@property (nonatomic, strong) NSString *tu_marital_status;
@property (nonatomic, strong) NSString *tu_mobile;
@property (nonatomic, strong) NSString *tu_nickname;
@property (nonatomic, strong) NSString *tu_occupation;
@property (nonatomic, strong) NSString *tu_portrait;
@property (nonatomic, strong) NSString *tu_province;
@property (nonatomic, strong) NSString *tu_province_id;
@property (nonatomic, strong) NSString *tu_purpose;
@property (nonatomic, strong) NSString *tu_signature;
@property (nonatomic, strong) NSString *tu_star_sign;
@property (nonatomic, strong) NSString *tu_tabs;
@property (nonatomic, strong) NSString *uid;

+(UserBean *)copyBean:(UserBean *)b;
+(UserBean *)getBean:(NSDictionary *)dic;
@end

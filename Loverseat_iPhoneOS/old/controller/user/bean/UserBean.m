//
//  UserBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/25.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation UserBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        UserBean *bean = [UserBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(UserBean *)copyBean:(UserBean *)b{
    UserBean *bean = [[UserBean alloc]init];
    bean.tu_city = b.tu_city;
    bean.tu_city_id = b.tu_city_id;
    bean.tu_email = b.tu_email;
    bean.tu_favour = b.tu_favour;
    bean.tu_gender = b.tu_gender;
    bean.tu_birthday = b.tu_birthday;
    bean.tu_income = b.tu_income;
    bean.tu_marital_status = b.tu_marital_status;
    bean.tu_mobile = b.tu_mobile;
    bean.tu_nickname = b.tu_nickname;
    bean.tu_occupation = b.tu_occupation;
    bean.tu_portrait = b.tu_portrait;
    bean.tu_province = b.tu_province;
    bean.tu_province_id = b.tu_province_id;
    bean.tu_purpose = b.tu_purpose;
    bean.tu_signature = b.tu_signature;
    bean.tu_star_sign = b.tu_star_sign;
    bean.tu_tabs = b.tu_tabs;
    bean.uid = b.uid;
    return bean;
}

+(UserBean *)getBean:(NSDictionary *)dic{
    UserBean *bean = [[UserBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){

        bean.tu_city = [dic objectForKey:@"tu_city"];
        bean.tu_city_id = [dic objectForKey:@"tu_city_id"];
        bean.tu_email = [dic objectForKey:@"tu_email"];
        bean.tu_favour = [dic objectForKey:@"tu_favour"];
        bean.tu_gender = [dic objectForKey:@"tu_gender"];
        bean.tu_income = [dic objectForKey:@"tu_income"];
        bean.tu_marital_status = [dic objectForKey:@"tu_marital_status"];
        bean.tu_mobile = [dic objectForKey:@"tu_mobile"];
        bean.tu_nickname = [dic objectForKey:@"tu_nickname"];
        bean.tu_occupation = [dic objectForKey:@"tu_occupation"];
        bean.tu_portrait = [dic objectForKey:@"tu_portrait"];
        bean.tu_province = [dic objectForKey:@"tu_province"];
        bean.tu_province_id = [dic objectForKey:@"tu_province_id"];
        bean.tu_purpose = [dic objectForKey:@"tu_purpose"];
        bean.tu_signature = [dic objectForKey:@"tu_signature"];
        bean.tu_star_sign = [dic objectForKey:@"tu_star_sign"];
        bean.tu_birthday = [dic objectForKey:@"tu_birthday"];
        bean.tu_tabs = [dic objectForKey:@"tu_tabs"];
        bean.uid = [dic objectForKey:@"uid"];
    }
    return bean;
}

@end

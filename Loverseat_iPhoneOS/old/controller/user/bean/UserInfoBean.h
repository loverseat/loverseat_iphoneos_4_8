//
//  UserInfoBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-3.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface UserInfoBean : BaseBean

@property(nonatomic,strong)NSString *tu_id;
@property(nonatomic,strong)NSString *tu_username;
@property(nonatomic,strong)NSString *tu_weixin;
@property(nonatomic,strong)NSString *tu_nickname;
@property(nonatomic,strong)NSString *tu_alipay_account;
@property(nonatomic,strong)NSString *tu_alipay_name;
@property(nonatomic,strong)NSString *tu_wallet_password;
@property(nonatomic,strong)NSString *tu_gender;
@property(nonatomic,strong)NSString *tu_birthday;
@property(nonatomic,strong)NSString *tu_province;
@property(nonatomic,strong)NSString *tu_city;
@property(nonatomic,strong)NSString *tu_portrait;
@property(nonatomic,strong)NSString *tu_fakeid;
@property(nonatomic,strong)NSString *tu_province_id;
@property(nonatomic,strong)NSString *tu_password;
@property(nonatomic,strong)NSString *tu_city_id;
@property(nonatomic,strong)NSString *tu_secondhand_cityid;
@property(nonatomic,strong)NSString *tu_category;
@property(nonatomic,strong)NSString *tu_mobile;
@property(nonatomic,strong)NSString *tu_email;
@property(nonatomic,strong)NSString *tu_source;
@property(nonatomic,strong)NSString *tu_authed;
@property(nonatomic,strong)NSString *tu_created;
@property(nonatomic,strong)NSString *tu_signature;
@property(nonatomic,strong)NSString *tu_voice_signature;
@property(nonatomic,strong)NSString *tu_occupation;
@property(nonatomic,strong)NSString *tu_income;
@property(nonatomic,strong)NSString *tu_marital_status;
@property(nonatomic,strong)NSString *tu_favour;
@property(nonatomic,strong)NSString *tu_purpose;
@property(nonatomic,strong)NSString *tu_coin;
@property(nonatomic,strong)NSString *tu_score;
@property(nonatomic,strong)NSString *tu_exper_value;
@property(nonatomic,strong)NSString *tu_tabs;
@property(nonatomic,strong)NSString *tu_rank;
@property(nonatomic,strong)NSString *tu_address;
@property(nonatomic,strong)NSString *tu_star_sign;

+(NSArray *)getBeanList:(NSArray *)arr;
+(UserInfoBean *)getBean:(NSDictionary *)dic;
+(UserInfoBean *)copyBean:(UserInfoBean *)b;

@end

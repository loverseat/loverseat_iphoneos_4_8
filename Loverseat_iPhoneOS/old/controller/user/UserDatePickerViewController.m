//
//  UserDatePickerViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserDatePickerViewController.h"
#import "UserEditCell.h"
#import "EditViewController.h"

@interface UserDatePickerViewController();
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, assign) UserEditCell *cell;
@property (nonatomic, strong) UITextField *textField;
@end
@implementation UserDatePickerViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:self.title];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60) style:UITableViewStyleGrouped];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date=[formatter dateFromString:self.defaultValue];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-216, [GB_DeviceUtils getScreenWidth], 216)];
    datePicker.maximumDate = [NSDate new];
    datePicker.date = date;
    [datePicker addTarget:self action:@selector(c_date:) forControlEvents:UIControlEventValueChanged];
    datePicker.backgroundColor = [UIColor whiteColor];
    datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [self.view addSubview:datePicker];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"UserEditCellIdentifier";
    UserEditCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          identifier];
    if (cell == nil) {
        cell = [[UserEditCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
        self.cell = cell;
        self.textField = cell.tf;
        cell.tf.text = self.defaultValue;
        cell.tf.enabled = NO;
    }
    return cell;
}

-(void)c_date:(UIDatePicker *)datePicker{
    NSDate *curDate =[datePicker date];
    _textField.text = [GB_DateUtils getFormatStringBy10Median:@"yyyy-MM-dd" timeMillis:[curDate timeIntervalSince1970]];
}

-(void)cb_back{
    [self.editViewController changeValue:self.textField.text valueId:nil type:self.type];
    [super cb_back];
}

@end

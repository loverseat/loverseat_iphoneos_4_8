//
//  PickerSelectViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "PickerSelectViewController.h"
#import "UserEditCell.h"

@interface PickerSelectViewController();
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, assign) UserEditCell *cell;
@property (nonatomic, strong) UITextField *textField;
@end
@implementation PickerSelectViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:self.title];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60) style:UITableViewStyleGrouped];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-216, [GB_DeviceUtils getScreenWidth], 216)];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [self.view addSubview:pickerView];
    
    int i = 0;
    for (GB_KeyValue *kv in _pickerData) {
        if([kv.key isEqualToString:_defaultValue]){
            [pickerView selectRow:i inComponent:0 animated:NO];
            break;
        }
        i++;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"UserEditCellIdentifier";
    UserEditCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          identifier];
    if (cell == nil) {
        cell = [[UserEditCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
        self.cell = cell;
        self.textField = cell.tf;
        _textField.enabled = NO;
        for (GB_KeyValue *kv in self.pickerData) {
            if([kv.key isEqualToString:self.defaultValue]){
                cell.tf.text = kv.value;
            }
        }
    }
    return cell;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.pickerData.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    GB_KeyValue *kv = [self.pickerData objectAtIndex:row];
    return kv.value;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    GB_KeyValue *kv = [self.pickerData objectAtIndex:row];
    self.textField.text = kv.value;
}

-(void)cb_back{
    for (GB_KeyValue *kv in self.pickerData) {
        if([kv.value isEqualToString:self.textField.text]){
            [_delegate changeValue:self.textField.text valueId:kv.key type:self.type];
            break;
        }
    }
    [super cb_back];
}

@end

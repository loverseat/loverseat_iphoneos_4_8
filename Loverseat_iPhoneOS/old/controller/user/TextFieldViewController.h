//
//  TextFieldViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "EditDelegate.h"

@interface TextFieldViewController : BaseViewController
<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *defaultValue;
@property (nonatomic, strong) id<EditDelegate> delegate;
@property (nonatomic, assign) BOOL canNull;
@property UIKeyboardType keyboardType;
@end

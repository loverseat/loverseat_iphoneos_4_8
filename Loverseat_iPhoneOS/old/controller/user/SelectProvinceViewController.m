//
//  SelectProvinceViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "SelectProvinceViewController.h"
#import "SelectCityViewController.h"
#import "CityCell.h"

@interface SelectProvinceViewController ()

@property (nonatomic, strong) NSMutableArray *contentData;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SelectProvinceViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.contentData = [NSMutableArray array];
    [self initFrame];
    [self initData];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"选择城市"];
    [NavUtils addBackButton:self];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    tableView.tag = 1;
    tableView.dataSource = self;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.backgroundColor = self.view.backgroundColor;
    tableView.backgroundView = nil;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    [self.view addSubview:tableView];
    self.tableView = tableView;
}


-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getCityUrl] list:arr delegate:self tag:1];
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    [_contentData setArray:[GB_JsonUtils getArrayByJsonString:str]];
    [_tableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _contentData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CityCellIdentifier";
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:
                      identifier];
    if (cell == nil) {
        cell = [[CityCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    cell.title.text = [[_contentData objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = [_contentData objectAtIndex:indexPath.row];
    SelectCityViewController *select = [[SelectCityViewController alloc]init];
    select.source = [dic objectForKey:@"citys"];
    select.delegate = self.delegate;
    select._name = [dic objectForKey:@"name"];
    select.type = _type;
    [self.navigationController pushViewController:select animated:YES];
}


@end

//
//  TicketDetailBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-23.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface TicketDetailBean : BaseBean

@property(nonatomic,strong)NSString *to_status;
@property(nonatomic,strong)NSString *to_event_id;
@property(nonatomic,strong)NSString *to_order_sn;
@property(nonatomic,strong)NSString *to_event_name;
@property(nonatomic,strong)NSString *to_show_time;
@property(nonatomic,strong)NSString *to_venue_name;
@property(nonatomic,strong)NSString *to_perprice;
@property(nonatomic,strong)NSString *to_count;
@property(nonatomic,strong)NSString *to_total_amount;
@property(nonatomic,strong)NSString *to_shipping_price;
@property(nonatomic,strong)NSString *to_buyer_name;
@property(nonatomic,strong)NSString *to_mobile;
@property(nonatomic,strong)NSString *to_shipping_name;
@property(nonatomic,strong)NSString *to_shipping_sn;
@property(nonatomic,strong)NSString *to_image;
@property(nonatomic,strong)NSString *to_discount;
@property(nonatomic,strong)NSString *to_valid;
@property(nonatomic,strong)NSString *to_pick_address;
@property(nonatomic,strong)NSString *to_pick_time;
@property(nonatomic,strong)NSString *to_ispick;

+(NSArray *)getBeanList:(NSArray *)arr;

+(TicketDetailBean *)getBean:(NSDictionary *)dic;
@end

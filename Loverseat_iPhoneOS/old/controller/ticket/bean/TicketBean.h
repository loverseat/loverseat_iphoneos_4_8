//
//  TicketBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface TicketBean : BaseBean
@property(nonatomic,strong)NSString *to_event_id;
@property(nonatomic,strong)NSString *to_event_name;
@property(nonatomic,strong)NSString *to_venue_name;
@property(nonatomic,strong)NSString *to_id;
@property(nonatomic,strong)NSString *to_show_time;
@property(nonatomic,strong)NSString *to_status;
@property(nonatomic,strong)NSString *image;
@property(nonatomic,strong)NSString *ticket_status;

+(NSArray *)getBeanList:(NSArray *)arr;
+(TicketBean *)getBean:(NSDictionary *)dic;
@end

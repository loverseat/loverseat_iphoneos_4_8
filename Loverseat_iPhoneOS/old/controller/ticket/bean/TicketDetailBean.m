//
//  TicketDetailBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-23.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TicketDetailBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation TicketDetailBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        TicketDetailBean *bean = [TicketDetailBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(TicketDetailBean *)getBean:(NSDictionary *)dic{
    TicketDetailBean *bean = [[TicketDetailBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.to_status = [dic objectForKey:@"to_status"];
        bean.to_event_id = [dic objectForKey:@"to_event_id"];
        bean.to_order_sn = [dic objectForKey:@"to_order_sn"];
        bean.to_event_name = [dic objectForKey:@"to_event_name"];
        bean.to_show_time = [dic objectForKey:@"to_show_time"];
        bean.to_perprice = [dic objectForKey:@"to_perprice"];
        bean.to_count = [dic objectForKey:@"to_count"];
        bean.to_total_amount = [dic objectForKey:@"to_total_amount"];
        bean.to_venue_name = [dic objectForKey:@"to_venue_name"];
        bean.to_shipping_price = [dic objectForKey:@"to_shipping_price"];
        
        bean.to_buyer_name = [dic objectForKey:@"to_buyer_name"];
        bean.to_mobile = [dic objectForKey:@"to_mobile"];
        bean.to_shipping_name = [dic objectForKey:@"to_shipping_name"];
        bean.to_shipping_sn = [dic objectForKey:@"to_shipping_sn"];
        bean.to_image = [dic objectForKey:@"to_image"];
        bean.to_discount = [dic objectForKey:@"to_discount"];
        
        bean.to_valid = [dic objectForKey:@"to_valid"];
        bean.to_pick_address = [dic objectForKey:@"to_pick_address"];
        bean.to_pick_time = [dic objectForKey:@"to_pick_time"];
        bean.to_ispick = [dic objectForKey:@"to_ispick"];
    }
    return bean;
}
@end

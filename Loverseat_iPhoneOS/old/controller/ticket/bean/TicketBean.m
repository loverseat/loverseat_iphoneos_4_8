//
//  TicketBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TicketBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation TicketBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        TicketBean *bean = [TicketBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(TicketBean *)getBean:(NSDictionary *)dic{
    TicketBean *bean = [[TicketBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.to_event_id = [dic objectForKey:@"to_event_id"];
        bean.to_event_name = [dic objectForKey:@"to_event_name"];
        bean.to_venue_name = [dic objectForKey:@"to_venue_name"];
        bean.to_id = [dic objectForKey:@"to_id"];
        bean.to_show_time = [dic objectForKey:@"to_show_time"];
        bean.to_status = [dic objectForKey:@"to_status"];
        bean.image = [dic objectForKey:@"image"];
        bean.ticket_status = [dic objectForKey:@"ticket_status"];
    }
    return bean;
}
@end

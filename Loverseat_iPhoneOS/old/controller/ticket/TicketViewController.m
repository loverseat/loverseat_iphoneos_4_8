//
//  TicketViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TicketViewController.h"
#import "TicketDetailViewController.h"
#import "TicketCell.h"
#import "TicketBean.h"

@interface TicketViewController ()
@property (nonatomic, strong) UIImageView *defaultBg;
@end

@implementation TicketViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contentData = [NSMutableArray array];
    [self initFrame];
    [self initData];
}


-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.backgroundView = nil;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    self.defaultBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"default_ticket"]];
    _defaultBg.center = self.view.center;
    [self.view addSubview:_defaultBg];
    _defaultBg.hidden = YES;
    
    //    ODRefreshControl *control = [[ODRefreshControl alloc]initInScrollView:tableView];
    //    [control setTintColor:GB_UIColorFromRGB(16, 16, 17)];
    //    [control addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    //    self.control = control;
    
    
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"我的票夹"];
    [NavUtils addBackButton:self];
}


-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"mobile" value:[User getUserInfo].tu_mobile]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getTicketPocketListUrl] list:arr delegate:self tag:1];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contentData.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TicketBean *bean = [self.contentData objectAtIndex:indexPath.row];
    if([bean.to_status integerValue] == 20){
        TicketDetailViewController *ticket = [[TicketDetailViewController alloc]init];
        ticket.orderId = bean.to_id;
        [self.navigationController pushViewController:ticket animated:YES];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Center5TopCellIdentifier";
    TicketCell *cell = [tableView dequeueReusableCellWithIdentifier:
                        identifier];
    if (cell == nil) {
        cell = [[TicketCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    TicketBean *bean = [self.contentData objectAtIndex:indexPath.row];
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
    
    NSDate *date =[dateFormat dateFromString:bean.to_show_time];
    NSLocale *zhLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    
    [dateFormat setLocale:zhLocale];
    NSString *info = [NSString stringWithFormat:@"%@ %@",bean.to_venue_name, [GB_DateUtils getFormatStringBy10Median:@"MM月dd日EEHH:mm" timeMillis:[date timeIntervalSince1970]]];
    [cell setTitle:bean.to_event_name info:info];
    
    if([bean.to_status integerValue] == 20){
        cell.arrow.hidden = NO;
        UIImage *arrow = [UIImage imageNamed:[User isMale]?@"icon_arrow_male":@"icon_arrow_female"];
        cell.arrow.image = arrow;
        cell.arrow.frame = CGRectMake([GB_DeviceUtils getScreenWidth]-15-7.5-arrow.size.width*0.5, 60-arrow.size.height*0.5, arrow.size.width, arrow.size.height);
    }
    else{
        cell.arrow.hidden = YES;
        cell.arrow.image = nil;
    }
    cell.bg.image = nil;
    [GB_NetWorkUtils loadImage:bean.image container:cell.bg type:GB_ImageCacheTypeAll delegate:self tag:indexPath.row];
    return cell;
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    [_contentData removeAllObjects];
    if([Error verify:str view:self.view]){
        self.isLoad = YES;
        NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
        [self.contentData setArray:[TicketBean getBeanList:[dic objectForKey:@"msg"]]];
    }
    
    if([GB_ToolUtils isBlank:_contentData]){
        _defaultBg.hidden = NO;
        _tableView.hidden = YES;
    }
    else{
        _defaultBg.hidden = YES;
        _tableView.hidden = NO;
    }
    [self.tableView reloadData];
}

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    [GB_NetWorkUtils useImage:[Static getFImage:image w:[GB_DeviceUtils getScreenWidth] h:120] container:container];
}

@end

//
//  TicketViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface TicketViewController : BaseViewController
<UITableViewDataSource, UITableViewDelegate, GB_LoadImageDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *contentData;
@end

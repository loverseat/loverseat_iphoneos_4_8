//
//  TicketDetailViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface TicketDetailViewController : BaseViewController
<GB_LoadImageDelegate>


@property (nonatomic, strong) NSString *orderId;

@end

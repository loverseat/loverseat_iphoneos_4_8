//
//  TicketDetailViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TicketDetailViewController.h"
#import "TicketDetailBean.h"

@interface TicketDetailViewController()
@property (nonatomic, strong) TicketDetailBean *bean;
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;
@end

@implementation TicketDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
    [self initData];
}

-(void)initFrame{
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 1885*0.5f)];
    iv.image = [UIImage imageNamed:@"bg_ticket_detail"];
    [self.view addSubview:iv];
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
    scrollView.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(8, 60, [GB_DeviceUtils getScreenWidth]-16, 160)];
    imageView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:imageView];
    self.iconView = imageView;
    
    UIImageView *imageViewModel = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth]-16, 9)];
    imageViewModel.image = [UIImage imageNamed:@"model_ticket_detail_icon"];
    [imageView addSubview:imageViewModel];

    
    
    UIImageView *contentView = [[UIImageView alloc]initWithFrame:CGRectMake(8, 60+160, [GB_DeviceUtils getScreenWidth]-16, 686)];
    contentView.image = [UIImage imageNamed:@"bg_ticket_detail_content"];
    contentView.userInteractionEnabled = YES;
    
    [scrollView addSubview:contentView];
    self.contentView = contentView;

    
    scrollView.contentSize = CGSizeMake([GB_DeviceUtils getScreenWidth], iv.frame.size.height);
    [self.view addSubview:scrollView];
    self.scrollView  = scrollView;
    
    [NavUtils addNavBgView:self];
    [NavUtils addBackButton:self];
}

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"order_id" value:self.orderId]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getOrderDetailUrl] list:arr delegate:self tag:1];
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.view]){
        self.bean = [TicketDetailBean getBean:[[Static getRequestData:str] lastObject]];
        [self.titleL removeFromSuperview];
        if([self.bean.to_total_amount doubleValue]==20){
            [NavUtils addNavTitleView:self text:@"邀约票"];
        }
        else{
            [NavUtils addNavTitleView:self text:@"演出票"];
        }
        [self reloadView];
    }
}

-(void)reloadView{
    for(UIView *v in self.contentView.subviews){
        [v removeFromSuperview];
    }
    
    [GB_NetWorkUtils loadImage:self.bean.to_image container:self.iconView type:GB_ImageCacheTypeAll delegate:self];
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
    
    NSDate *date =[dateFormat dateFromString:self.bean.to_show_time];
    NSLocale *zhLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    
    [dateFormat setLocale:zhLocale];
    NSString *info = [NSString stringWithFormat:@"%@",[GB_DateUtils getFormatStringBy10Median:@"MM月dd日EEHH:mm" timeMillis:[date timeIntervalSince1970]]];
    float height = 18.0f;
    UILabel *infoL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:[NSString stringWithFormat:@"%@  ·  %@",info,self.bean.to_venue_name] font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:infoL];
    height +=infoL.frame.size.height;
    height+=5;
    
    UILabel *titleL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static getFont:36 isBold:YES] numberOfLines:2]) title:self.bean.to_event_name font:[Static getFont:36 isBold:YES] color:GB_UIColorFromRGB(36, 36, 36)];
    titleL.numberOfLines = 2;
    [self.contentView addSubview:titleL];
    
    height += titleL.frame.size.height;
    height += 20;
    
    UIImageView *codeIv = [[UIImageView alloc]initWithFrame:CGRectMake((self.contentView.frame.size.width-110)*0.5, height, 110, 110)];
    [GB_NetWorkUtils loadImage:[Url getQRCodeUrl:self.bean.to_order_sn] container:codeIv type:GB_ImageCacheTypeNone];
    [self.contentView addSubview:codeIv];
    height += codeIv.frame.size.height;
    height += 20;
    
    float priceHeight = [GB_ToolUtils getFontHeight:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:[Static getFontSize:108]]];
    float danjiaHeight = [GB_ToolUtils getFontHeight:[Static getFont:36 isBold:YES]];
    float yuanHeight = [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]];
    float yuanWidth = [GB_ToolUtils getTextWidth:@"元" font:[Static getFont:24 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)];
    UILabel *yuanL = [GB_WidgetUtils getLabel:CGRectMake(self.contentView.frame.size.width-20-yuanWidth, height+priceHeight-yuanHeight-8, yuanWidth, yuanHeight) title:@"元" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:yuanL];
    
    UILabel *priceL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40-yuanWidth, priceHeight) title:GB_NSStringFromInt((int)[self.bean.to_perprice floatValue]) font:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:[Static getFontSize:108]] color:GB_UIColorFromRGB(45, 45, 45)];
    priceL.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:priceL];
    
    
    UILabel *danweiL = [GB_WidgetUtils getLabel:CGRectMake(20, height+priceHeight-danjiaHeight-8, self.contentView.frame.size.width-40-yuanWidth, danjiaHeight) title:@"单价" font:[Static getFont:36 isBold:YES] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:danweiL];
    
    height += priceHeight;

    UILabel *zhekouL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:[NSString stringWithFormat:@"双人座正价购买全场%.1f折",[self.bean.to_discount floatValue]*10] font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(235, 29, 29)];
    zhekouL.textAlignment = NSTextAlignmentRight;
    zhekouL.hidden = [self.bean.to_discount intValue]==1;
    [self.contentView addSubview:zhekouL];
    height += zhekouL.frame.size.height;
    
    height += 20;
    
    UILabel *subtitleL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, danjiaHeight) title:@"经典双人套票" font:[Static getFont:36 isBold:YES] color:GB_UIColorFromRGB(36, 36, 36)];
    subtitleL.hidden = YES;
    [self.contentView addSubview:subtitleL];
    
    height += subtitleL.frame.size.height;
    height += 5;
    
    UILabel *subinfoL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, yuanHeight) title:@"C区，前排靠中间座位" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    subinfoL.hidden = YES;
    [self.contentView addSubview:subinfoL];
    
    
    height += subinfoL.frame.size.height;
    
    UILabel *zhangL = [GB_WidgetUtils getLabel:CGRectMake(self.contentView.frame.size.width-20-yuanWidth, height-yuanHeight, yuanWidth, yuanHeight) title:@"张" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:zhangL];
    

    UILabel *numL = [GB_WidgetUtils getLabel:CGRectMake(20, height-priceHeight+8, self.contentView.frame.size.width-40-yuanWidth, priceHeight) title:@"2" font:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:[Static getFontSize:108]] color:GB_UIColorFromRGB(45, 45, 45)];
    numL.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:numL];
    
    height += 50;
    
    yuanL = [GB_WidgetUtils getLabel:CGRectMake(self.contentView.frame.size.width-20-yuanWidth, height+priceHeight-yuanHeight-8, yuanWidth, yuanHeight) title:@"元" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:yuanL];
    
    priceL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40-yuanWidth, priceHeight) title:GB_NSStringFromInt((int)([self.bean.to_total_amount floatValue]+[self.bean.to_shipping_price floatValue])) font:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:[Static getFontSize:108]] color:GB_UIColorFromRGB(45, 45, 45)];
    priceL.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:priceL];
    
    
    UILabel *hejiL = [GB_WidgetUtils getLabel:CGRectMake(20, height+priceHeight-danjiaHeight-8, self.contentView.frame.size.width-40-yuanWidth, danjiaHeight) title:@"合计" font:[Static getFont:36 isBold:YES] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:hejiL];
    
    height += priceHeight;
    
    NSString *price = [self.bean.to_shipping_price floatValue]==0?@"全免运费":[NSString stringWithFormat:@"包含运费：%@元",self.bean.to_shipping_price];
    UILabel *mianL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:price font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(235, 29, 29)];
    mianL.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:mianL];
    
    
    height += mianL.frame.size.height;
    height += 30;
    
    NSString *address = [NSString stringWithFormat:@"收票地址：%@",self.bean.to_pick_address];
    float addressHeight = [GB_ToolUtils getTextHeight:address font:[Static getFont:24 isBold:NO] size:CGSizeMake(self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO] numberOfLines:2])];
    UILabel *addressL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, addressHeight) title:address font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    addressL.numberOfLines = 0;
    [self.contentView addSubview:addressL];
    height += addressL.frame.size.height;
    height += 3;
    
    UILabel *nameL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static  getFont:24 isBold:NO]]) title:[NSString stringWithFormat:@"姓名：%@",self.bean.to_buyer_name] font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:nameL];
    height += nameL.frame.size.height;
    height += 3;
    
    UILabel *phoneL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static  getFont:24 isBold:NO]]) title:[NSString stringWithFormat:@"电话：%@",self.bean.to_mobile] font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:phoneL];
    height += phoneL.frame.size.height;
    height += 3;
    
    UILabel *emsL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static  getFont:24 isBold:NO]]) title:[NSString stringWithFormat:@"快递公司：%@",self.bean.to_shipping_name] font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:emsL];
    height += emsL.frame.size.height;
    height += 3;
    
    UILabel *noL = [GB_WidgetUtils getLabel:CGRectMake(20, height, self.contentView.frame.size.width-40, [GB_ToolUtils getFontHeight:[Static  getFont:24 isBold:NO]]) title:[NSString stringWithFormat:@"快递单号：%@",self.bean.to_shipping_sn] font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(36, 36, 36)];
    [self.contentView addSubview:noL];
    height += noL.frame.size.height;
    height += 3;
}

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    [GB_NetWorkUtils useImage:[Static getFImage:image w:self.iconView.frame.size.width h:self.iconView.frame.size.height] container:container];
}


@end

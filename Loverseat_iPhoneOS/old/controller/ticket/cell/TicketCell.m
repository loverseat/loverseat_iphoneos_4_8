//
//  TicketCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TicketCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"

@implementation TicketCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 119)];
        [self.contentView addSubview:bg];
        self.bg = bg;
        
        UIView *mask = [[UIView alloc]initWithFrame:CGRectMake(0, 0, bg.frame.size.width, bg.frame.size.height)];
        mask.backgroundColor = [UIColor whiteColor];
        mask.alpha = 0.85f;
        [bg addSubview:mask];
        
        UILabel *title = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[Static getFont:34 isBold:NO] color:GB_UIColorFromRGB(57, 63, 77)];
        [self.contentView addSubview:title];
        self.title = title;
        
        UILabel *info = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[Static getFont:22 isBold:NO] color:GB_UIColorFromRGB(251, 80, 87)];
        [self.contentView addSubview:info];
        self.info = info;
        
        UIImageView *arrow = [[UIImageView alloc]init];
        [self.contentView addSubview:arrow];
        self.arrow = arrow;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setTitle:(NSString *)title info:(NSString *)info{
    CGSize size = [GB_ToolUtils getTextSize:title font:self.title.font size:CGSizeMake([GB_DeviceUtils getScreenWidth]-30-20, [GB_ToolUtils getFontHeight:self.title.font numberOfLines:2])];
    int totalHeight = size.height + [GB_ToolUtils getFontHeight:self.info.font];

    self.title.frame = CGRectMake(15, 60-totalHeight*0.5, size.width, size.height);
    self.info.frame = CGRectMake(15, 60+totalHeight*0.5-[GB_ToolUtils getFontHeight:self.info.font], [GB_DeviceUtils getScreenWidth]-50, [GB_ToolUtils getFontHeight:self.info.font]);
    self.title.text = title;
    self.info.text = info;
    
    
}

@end

//
//  TicketCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketCell : UITableViewCell

@property (nonatomic, strong) UIImageView *bg;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *info;
@property (nonatomic, strong) UIImageView *arrow;

-(void)setTitle:(NSString *)title info:(NSString *)info;

@end

//
//  AddCommentViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "AddCommentViewController.h"
#import "EventCommentBean.h"
#import "EventViewController.h"

@interface AddCommentViewController()
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *countLabel;
@end

@implementation AddCommentViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = [Static getBgColor];
    
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], 140) textContainer:nil];
    textView.font = [Static getFont:28 isBold:NO];
    textView.delegate = self;
    [self.view addSubview:textView];
    self.textView = textView;
    
    UILabel *countLabel = [GB_WidgetUtils getLabel:CGRectMake(10, textView.frame.size.height-30, textView.frame.size.width-20, 30) title:@"0/120" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(251, 80, 87)];
    countLabel.textAlignment = NSTextAlignmentRight;
    [textView addSubview:countLabel];
    self.countLabel = countLabel;
    
    [self.view addSubview:[View getSpearLine:CGRectMake(0, [NavUtils getNavHeight]+textView.frame.size.height, [GB_DeviceUtils getScreenWidth], 0.5f)]];
    
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"发表评论"];
    [NavUtils addRightButton:self title:@"确定" sel:@selector(c_submit)];
    [self.rightBtn setTitleColor:[Static getGrayColor] forState:UIControlStateNormal];
    [self.rightBtn setTitleColor:[Static getGrayColor] forState:UIControlStateHighlighted];
    self.countLabel.textColor = [UIColor redColor];
    
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60.0f, 54.0f) image:[UIImage imageNamed:@"btn_back_rose"] imageH:nil id:self sel:@selector(cb_back)];
    [self.view addSubview:navBackBtn];
}

-(void)textViewDidChange:(UITextView *)textView{
    self.countLabel.text = [NSString stringWithFormat:@"%d/120",textView.text.length];
    if(textView.text.length > 120 || textView.text.length == 0){
        self.countLabel.textColor = [UIColor redColor];
        [self.rightBtn setTitleColor:[Static getGrayColor] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[Static getGrayColor] forState:UIControlStateHighlighted];
    }
    else{
        self.countLabel.textColor = GB_UIColorFromRGB(251, 80, 87);
        [self.rightBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateHighlighted];
    }
}

-(void)c_submit{
    [self.textView resignFirstResponder];
    if([GB_StringUtils isBlank:_textView.text]){
        return;
    }
    if(self.textView.text.length > 120)return;
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"eventid" value:self.event_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"detail" value:self.textView.text]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getAddCommentsUrl] list:arr delegate:self tag:1];
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.view]){
        EventCommentBean *bean = [EventCommentBean getBean:[Static getRequestData:str]];
        [_eventViewController addEventCommentBean:bean];
        [self cb_back];
    }
}
@end

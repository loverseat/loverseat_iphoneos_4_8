//
//  ZanListCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "ZanListCell.h"
#import "UserInfoViewController.h"

@interface ZanListCell()

@property (nonatomic, strong) UIButton *icon;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *desc;
@property (nonatomic, strong) UIImageView *gender;

@property (nonatomic, strong) ZanListBean *bean;

@end

@implementation ZanListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.icon = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 36, 36)];
        _icon.layer.masksToBounds = YES;
        _icon.layer.cornerRadius = 18;
        [self addSubview:_icon];
        
        
        self.title = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[UIFont boldSystemFontOfSize:14] color:[UIColor blackColor]];
        [self addSubview:_title];
        
        
        self.desc = [GB_WidgetUtils getLabel:CGRectMake(55, 30, [GB_DeviceUtils getScreenWidth]-65, 20) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(75, 75, 75)];
        [self addSubview:_desc];
        
        self.gender = [GB_WidgetUtils getImageView:CGRectZero image:nil];
        [self addSubview:_gender];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

-(void)setZanListBean:(ZanListBean *)bean{
    self.bean = bean;
    [GB_NetWorkUtils loadImage:bean.tai_portrait container:_icon type:GB_ImageCacheTypeAll delegate:nil tag:bean.tag];
    
    float width = [GB_ToolUtils getTextWidth:bean.tai_nickname font:_title.font size:CGSizeMake(200, 15)];
    _title.frame = CGRectMake(55, 10, width, 15);
    _title.text = bean.tai_nickname;
    
    _gender.image = [UIImage imageNamed:bean.tai_gender.intValue==0?@"icon_male":@"icon_female"];
    _gender.frame = CGRectMake(55+width+5, 10, 15, 15);
    [_icon addTarget:self action:@selector(c_user) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *info = [GB_StringUtils isBlank:bean.tai_star_sign]?[NSString stringWithFormat:@"%d岁",[Static getAgeByBirthday:bean.tai_birthday]]:[NSString stringWithFormat:@"%d岁，%@",[Static getAgeByBirthday:bean.tai_birthday],bean.tai_star_sign];
    _desc.text = info;
}

-(void)c_user{
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = _bean.tai_uid;
    [_bean.baseViewController.navigationController pushViewController:con animated:YES];
}

@end

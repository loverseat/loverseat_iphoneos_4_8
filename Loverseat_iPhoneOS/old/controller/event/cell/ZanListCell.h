//
//  ZanListCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZanListBean.h"

@interface ZanListCell : UITableViewCell

-(void)setZanListBean:(ZanListBean *)bean;

@end

//
//  EventViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventViewController.h"
#import "EventBean.h"
#import "LikeListViewController.h"
#import "AddCommentViewController.h"

#import "DateAllCenterViewController.h"
#import "DateAllLeftViewController.h"
#import "DateAllSwitchViewController.h"
#import "DateAllBuyViewController.h"
#import "UserInfoViewController.h"
#import "DateInvitePayViewController.h"


@interface EventViewController()
@property (nonatomic, strong) EventBean *eventBean;
@property (nonatomic, assign) float eventContentY;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *commentView;
@property (nonatomic, strong) UIButton *openBtn;
@property (nonatomic, strong) UIView *commentTool;
@property (nonatomic, strong) UILabel *commentLabel;

@property (nonatomic, assign) int zanIndex;
@property (nonatomic, assign) int caiIndex;
@property (nonatomic, strong) UIImageView *image;

@end

@implementation EventViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"repertoireDetails"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"repertoireDetails"];
}
float eventDescMinHeight = 100;

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
    [self initData];
}

-(void)initFrame{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
    [self.view addSubview:_scrollView];
    
    self.image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenWidth]*(330.0f/300.0f))];
    _image.backgroundColor = [UIColor blackColor];
    [_scrollView addSubview:_image];
    
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 40.0f, [NavUtils getNavHeight]) image:nil imageH:nil id:self sel:@selector(cb_back)];
    [navBackBtn setImage:[UIImage imageNamed:@"btn_back_gray"] forState:UIControlStateNormal];
    [_scrollView addSubview:navBackBtn];
    self.leftBtn = navBackBtn;

}

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getEventDetailUrl] list:arr delegate:self tag:1];
    }
}
//增加评论
-(void)addEventCommentBean:(EventCommentBean *)bean{
    if(_eventBean){
        NSMutableArray *comments = [NSMutableArray arrayWithArray:_eventBean.comments];
        if([GB_ToolUtils isBlank:comments]){
            [comments addObject:bean];
        }
        else{
            [comments insertObject:bean atIndex:0];
        }
        _eventBean.comments = comments;
        [self reCreateCommentDataSource];
        _commentLabel.text = [NSString stringWithFormat:@"评论（%lu）",(unsigned long)_eventBean.comments.count];
    }
}

-(void)createDataSource{
    UIView *bg = [[UIView alloc]initWithFrame:CGRectMake(0, 206, [GB_DeviceUtils getScreenWidth], 100)];
    bg.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:bg];
    
    int nameHeight = [GB_ToolUtils getTextHeight:_eventBean.event_info.tad_show_name font:[UIFont boldSystemFontOfSize:19] size:CGSizeMake(280, 48)];
    
    UILabel *name = [GB_WidgetUtils getLabel:CGRectMake(20, 10 + 40 -nameHeight, 280, nameHeight) title:nil font:[UIFont boldSystemFontOfSize:19] color:[UIColor whiteColor]];
    name.numberOfLines = 2;
    [bg addSubview:name];
    
    UILabel *venue = [GB_WidgetUtils getLabel:CGRectMake(20, 58, 280, 16) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
    [bg addSubview:venue];
    
    UILabel *time = [GB_WidgetUtils getLabel:CGRectMake(20, 74, 280, 16) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
    [bg addSubview:time];
    
    
    UIView *inviteBgView = [[UIView alloc]initWithFrame:CGRectMake(10, bg.frame.size.height+bg.frame.origin.y, 300, 60)];
    inviteBgView.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:inviteBgView];
    
    
    [GB_NetWorkUtils loadImage:[Url getImageUrl:_eventBean.event_info.tad_image] container:_image type:GB_ImageCacheTypeAll];
    name.text = _eventBean.event_info.tad_show_name;
    venue.text = _eventBean.event_info.tad_venue_name;
    time.text = [NSString stringWithFormat:@"%@ - %@",_eventBean.event_info.tad_begin_time,_eventBean.event_info.tad_end_time];
    
    for (UIView *v in inviteBgView.subviews) {
        [v removeFromSuperview];
    }
    if([GB_ToolUtils isNotBlank:_eventBean.event_buy_invite_info]){
        int index = 0;
        for (EventBuyInviteInfoBean *b in _eventBean.event_buy_invite_info) {
            [inviteBgView addSubview:[self getUserView:b index:b.index]];
            index = b.index;
        }
        [inviteBgView addSubview:[self getUserView:nil index:++index]];
    }
    else{
        UILabel *alert = [GB_WidgetUtils getLabel:CGRectMake(50, 0, 200, 30) title:@"还没有人发起邀约哦！" font:[UIFont boldSystemFontOfSize:17] color:GB_UIColorFromRGB(84, 84, 84)];
        [inviteBgView addSubview: alert];
        [inviteBgView addSubview:[self getUserView:nil index:6]];
    }
    
    
    UIFont *font = [UIFont systemFontOfSize:13];
    CGSize descSize = [GB_ToolUtils getTextSize:_eventBean.event_info.tad_intro font:font size:CGSizeMake([GB_DeviceUtils getScreenWidth]-20, INT_MAX)];
    if(descSize.height > eventDescMinHeight){
        self.descLabel = [GB_WidgetUtils getLabel:CGRectMake(10, inviteBgView.frame.origin.y+inviteBgView.frame.size.height+10, descSize.width, eventDescMinHeight) title:_eventBean.event_info.tad_intro font:font color:[UIColor blackColor]];
    }
    else{
        self.descLabel = [GB_WidgetUtils getLabel:CGRectMake(10, inviteBgView.frame.origin.y+inviteBgView.frame.size.height+10, descSize.width, descSize.height) title:_eventBean.event_info.tad_intro font:font color:[UIColor blackColor]];
    }
    _descLabel.numberOfLines = 0;
    [_scrollView addSubview:_descLabel];

    
    self.contentView = [[UIView alloc]initWithFrame:CGRectMake(0, _descLabel.frame.size.height+_descLabel.frame.origin.y+10, [GB_DeviceUtils getScreenWidth], 200)];
    [_scrollView addSubview:_contentView];
    
    
    self.eventContentY = _contentView.frame.origin.y;
    
    float height = 0;
    
     if(descSize.height > eventDescMinHeight){
         self.openBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 40) image:nil imageH:nil id:self sel:@selector(c_open)];
         [_openBtn setImage:[UIImage imageNamed:@"arrow_bottom"] forState:UIControlStateNormal];
         [_contentView addSubview:_openBtn];
         height+=40;
     }
    
    
    [_contentView addSubview:[Static getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0.5f) : GB_UIColorFromRGB(217, 217, 217)]];
    height+=0.5f;

    [_contentView addSubview:[Static getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 10) : GB_UIColorFromRGB(244, 244, 244)]];
    height+=10;
    
    [_contentView addSubview:[Static getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0.5f) : GB_UIColorFromRGB(217, 217, 217)]];
    height+=0.5f;
    
    UILabel *zanLabel = [GB_WidgetUtils getLabel:CGRectMake(10, height, [GB_DeviceUtils getScreenWidth]-20, 43.5f) title:[NSString stringWithFormat:@"%d个人点赞",_eventBean.like_count] font:[UIFont systemFontOfSize:13] color:[UIColor blackColor]];
    [_contentView addSubview:zanLabel];
    
    UIButton *zanBtn = [GB_WidgetUtils getButton:CGRectMake(10, height, [GB_DeviceUtils getScreenWidth]-15, 43.5f) image:nil imageH:nil id:self sel:@selector(c_zan)];
    [_contentView addSubview:zanBtn];
    
    UIImageView *zanArrow = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow_right"]];
    zanArrow.center = CGPointMake([GB_DeviceUtils getScreenWidth]-20, height+21.75f);
    [_contentView addSubview:zanArrow];
    
     height+=43.5f;
    
    [_contentView addSubview:[Static getLine:CGRectMake(10, height, [GB_DeviceUtils getScreenWidth]-10, 0.5f) : GB_UIColorFromRGB(217, 217, 217)]];
    height+=0.5f;
    
    self.commentLabel = [GB_WidgetUtils getLabel:CGRectMake(10, height, [GB_DeviceUtils getScreenWidth]-20, 43.5f) title:[NSString stringWithFormat:@"评论（%lu）",(unsigned long)_eventBean.comments.count] font:[UIFont systemFontOfSize:13] color:[UIColor blackColor]];
    [_contentView addSubview:_commentLabel];
    
   
    UILabel *conmment = [GB_WidgetUtils getLabel:CGRectMake([GB_DeviceUtils getScreenWidth]-60, height, 60, 43.5f) title:@"发表评论" font:[UIFont systemFontOfSize:13] color: GB_UIColorFromRGB(0, 122, 255)];
    [_contentView addSubview:conmment];
    
    UIImageView *iconComment = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_pencel"]];
    iconComment.center =CGPointMake([GB_DeviceUtils getScreenWidth]-70, conmment.center.y);
    [_contentView addSubview:iconComment];
    
    UIButton *commentBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-80, height, 80, 43.5f) image:nil imageH:nil id:self sel:@selector(c_comment)];
    [_contentView addSubview:commentBtn];
    height+=43.5f;
    [_contentView addSubview:[Static getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0.5f) : GB_UIColorFromRGB(217, 217, 217)]];
    height+=0.5f;

    
    self.commentView = [[UIView alloc]initWithFrame:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0)];
    [_contentView addSubview:_commentView];
    
    
    self.commentTool = [[UIView alloc]initWithFrame:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-44, [GB_DeviceUtils getScreenWidth], 44)];
    _commentTool.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8f];
    [self.view addSubview:_commentTool];
    
    [_commentTool addSubview:[Static getLine:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 0.5f) : [UIColor colorWithRed:217.0f/255.0f green:217.0f/255.0f blue:217.0f/255.0f alpha:0.8f]]];
    
    UIButton *date = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5-120, 10, 110, 24) image:nil imageH:nil id:self sel:@selector(c_date)];
    [date setTitle:@"去邀约" forState:UIControlStateNormal];
    [date setTitleColor:GB_UIColorFromRGB(0, 122, 255) forState:UIControlStateNormal];
    date.layer.masksToBounds = YES;
    date.layer.cornerRadius = 8;
    [_commentTool addSubview:date];
    
    
    UIButton *buy = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5+10, 10, 110, 24) image:nil imageH:nil id:self sel:@selector(c_buy)];
    [buy setTitle:@"去购票" forState:UIControlStateNormal];
    [buy setTitleColor:GB_UIColorFromRGB(0, 122, 255) forState:UIControlStateNormal];
    buy.layer.masksToBounds = YES;
    buy.layer.cornerRadius = 8;
    [_commentTool addSubview:buy];
    [self reCreateCommentDataSource];
}

-(void)c_user:(UIButton *)btn{
    EventCommentBean *b = [_eventBean.comments objectAtIndex:btn.tag];
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = b.tac_uid;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_user_invite:(UIButton *)btn{
    EventBuyInviteInfoBean *bean = [_eventBean.event_buy_invite_info objectAtIndex:btn.tag];
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = bean.tbi_uid;
    [self.navigationController pushViewController:con animated:YES];
}

#pragma mark - 评论
//评论
-(void)reCreateCommentDataSource{

    for (UIView *v in _commentView.subviews) {
        [v removeFromSuperview];
    }
    int height = 0;
    int index = 0;
    for (EventCommentBean *b in _eventBean.comments) {
        height+=10;
        UIButton *icon = [[UIButton alloc]initWithFrame:CGRectMake(10, height, 34, 34)];
        icon.layer.masksToBounds = YES;
        icon.layer.cornerRadius = 17;
        icon.tag = index;
        //头像点击事件
        //[icon addTarget:self action:@selector(c_user:) forControlEvents:UIControlEventTouchUpInside];
        [_commentView addSubview:icon];
        //头像填充
        [GB_NetWorkUtils loadImage:[Url getImageUrl:b.tac_thumb] container:icon type:GB_ImageCacheTypeAll delegate:nil tag:b.tag];
        
        UILabel *titleL = [GB_WidgetUtils getLabel:CGRectMake(49, height, [GB_DeviceUtils getScreenWidth]-120-49-10, 34) title:b.tac_username font:[UIFont boldSystemFontOfSize:16] color:GB_UIColorFromRGB(28, 31, 38)];
        [_commentView addSubview:titleL];
        
        UILabel *timeL = [GB_WidgetUtils getLabel:CGRectMake(_contentView.frame.size.width-130, height+2, 115, 24) title:b.tac_created font:[UIFont systemFontOfSize:11] color:GB_UIColorFromRGB(167, 167, 167)];
        timeL.textAlignment = NSTextAlignmentRight;
        [_commentView addSubview:timeL];
        
        height +=34;
        
        
        float descHeight = [GB_ToolUtils getTextHeight:b.tac_detail font:[UIFont systemFontOfSize:13] size:CGSizeMake(_contentView.frame.size.width-59, INT_MAX)];
        float d = 20 - [GB_ToolUtils getFontHeight:[Static getFont:28 isBold:NO]];
        descHeight +=d;
        descHeight = (descHeight<20?20:descHeight);
        
        
        UILabel *contentL = [GB_WidgetUtils getLabel:CGRectMake(49, height, _contentView.frame.size.width-59, descHeight) title:b.tac_detail font:[UIFont systemFontOfSize:13] color:GB_UIColorFromRGB(64, 64, 64)];
        contentL.numberOfLines = 0;
        [_commentView addSubview:contentL];
        height+=descHeight;
        
        height+=10;
        
        
        UIButton *btnZan = [[UIButton alloc]initWithFrame:CGRectMake(44, height-5, 23, 23)];
        [btnZan setImage:[UIImage imageNamed:(b.tac_up_flag == 1?@"icon_did_zan":@"icon_zan")] forState:UIControlStateNormal];
        [btnZan addTarget:self action:@selector(c_zan:) forControlEvents:UIControlEventTouchUpInside];
        [_commentView addSubview:btnZan];
        btnZan.tag = index;
        b.btnZan = btnZan;
        
        UIFont *font = [UIFont systemFontOfSize:11];
        
        float zanWidth = [GB_ToolUtils getTextHeight:GB_NSStringFromInt(b.tac_up_count) font:font size:CGSizeMake(120, 13)];
        UILabel *zan = [GB_WidgetUtils getLabel:CGRectMake(69, height, zanWidth, 13) title:GB_NSStringFromInt(b.tac_up_count) font:font color:GB_UIColorFromRGB(204, 204, 204)];
        [_commentView addSubview:zan];
        
        UIButton *btnCai = [[UIButton alloc]initWithFrame:CGRectMake(69+zanWidth+5, height-5, 23, 23)];
        [btnCai setImage:[UIImage imageNamed:(b.tac_down_flag == 1?@"icon_did_cai":@"icon_cai")] forState:UIControlStateNormal];
        [btnCai addTarget:self action:@selector(c_cai:) forControlEvents:UIControlEventTouchUpInside];
        [_commentView addSubview:btnCai];
        btnCai.tag = index;
        b.btnCai = btnCai;
        
        float caiWidth = [GB_ToolUtils getTextHeight:GB_NSStringFromInt(b.tac_down_count) font:font size:CGSizeMake(120, 13)];
        UILabel *cai = [GB_WidgetUtils getLabel:CGRectMake(69+zanWidth+30, height, caiWidth, 13) title:GB_NSStringFromInt(b.tac_down_count) font:font color:GB_UIColorFromRGB(204, 204, 204)];
        [_commentView addSubview:cai];
        
        UIButton *btnReport = [[UIButton alloc] init];
        btnReport.frame = CGRectMake([GB_DeviceUtils getScreenWidth] - 50, height, 40, 13);
        [btnReport setTitle:@"举报" forState:UIControlStateNormal];
        [btnReport setTitleColor:GB_UIColorFromRGB(204, 204, 204) forState:UIControlStateNormal];
        [btnReport.titleLabel setFont:font];
        [btnReport addTarget:self action:@selector(c_report:) forControlEvents:UIControlEventTouchDown];
        [_commentView addSubview:btnReport];
        
        height+=20;
        height+=10;
        
        [_commentView addSubview:[Static getLine:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 0.5f) : GB_UIColorFromRGB(217, 217, 217)]];
        height+=0.5f;
        
        index ++;
    }
    _commentView.frame = CGRectMake(_commentView.frame.origin.x, _commentView.frame.origin.y, _commentView.frame.size.width, height);
    _contentView.frame = CGRectMake(_contentView.frame.origin.x, _contentView.frame.origin.y, _contentView.frame.size.width, _commentView.frame.origin.y+_commentView.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake([GB_DeviceUtils getScreenWidth], _contentView.frame.size.height+_contentView.frame.origin.y+44);
    
}

- (void)c_report:(UIButton *)btn
{
    if(![User checkLogin])return;
    EventCommentBean *b = [_eventBean.comments objectAtIndex:btn.tag];
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"current_uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"target_id" value:GB_NSStringFromInt(b.tac_id)]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"1"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getCommentReportUrl] list:arr delegate:self tag:6];
    }
}

-(void)c_open{
    if(_contentView.frame.origin.y == _eventContentY){
        CGSize descSize = [GB_ToolUtils getTextSize:_eventBean.event_info.tad_intro font:_descLabel.font size:CGSizeMake([GB_DeviceUtils getScreenWidth]-20, INT_MAX)];
        _descLabel.frame = CGRectMake(_descLabel.frame.origin.x, _descLabel.frame.origin.y, _descLabel.frame.size.width, descSize.height);
        _contentView.frame = CGRectMake(_contentView.frame.origin.x, _contentView.frame.origin.y+descSize.height-eventDescMinHeight, _contentView.frame.size.width, _contentView.frame.size.height);
        [_openBtn setImage:[UIImage imageNamed:@"arrow_top"] forState:UIControlStateNormal];
    }
    else{
        _descLabel.frame = CGRectMake(_descLabel.frame.origin.x, _descLabel.frame.origin.y, _descLabel.frame.size.width, eventDescMinHeight);
        _contentView.frame = CGRectMake(_contentView.frame.origin.x, _eventContentY, _contentView.frame.size.width, _contentView.frame.size.height);
        [_openBtn setImage:[UIImage imageNamed:@"arrow_bottom"] forState:UIControlStateNormal];
    }
    _scrollView.contentSize = CGSizeMake([GB_DeviceUtils getScreenWidth], _contentView.frame.size.height+_contentView.frame.origin.y+44);
}

-(void)c_date{
    DateAllCenterViewController *center = [[DateAllCenterViewController alloc]init];
    center.event_id = _event_id;
    DateAllLeftViewController *left = [[DateAllLeftViewController alloc]init];
    DateAllSwitchViewController *con = [[DateAllSwitchViewController alloc]initWithLeftViewController:left centerViewController:center];
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_buy{
    if(!_eventBean)return;
    DateAllBuyViewController *buy = [[DateAllBuyViewController alloc]init];
    buy.url = _eventBean.buy_ticket_url;
    [self.navigationController pushViewController:buy animated:YES];
}

-(void)c_zan:(UIButton *)btn{
    if(![User checkLogin])return;
    EventCommentBean *b = [_eventBean.comments objectAtIndex:btn.tag];
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        self.zanIndex = btn.tag;
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"comment_id" value:GB_NSStringFromInt(b.tac_id)]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"remark_type" value:@"up"]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"remark_value" value:b.tac_up_flag==0?@"0":@"1"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getCommentRemarkUrl] list:arr delegate:self tag:b.tac_up_flag==0?2:3];
    }
}

-(void)c_cai:(UIButton *)btn{
    if(![User checkLogin])return;
    EventCommentBean *b = [_eventBean.comments objectAtIndex:btn.tag];
    self.caiIndex = btn.tag;
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"comment_id" value:GB_NSStringFromInt(b.tac_id)]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"remark_type" value:@"down"]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"remark_value" value:b.tac_down_flag==0?@"0":@"1"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getCommentRemarkUrl] list:arr delegate:self tag:b.tac_down_flag==0?4:5];
    }
}


-(void)c_zan{
    LikeListViewController *con = [[LikeListViewController alloc]init];
    con.event_id = _event_id;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_comment{
    AddCommentViewController *con = [[AddCommentViewController alloc]init];
    con.event_id = _event_id;
    con.eventViewController = self;
    [self.navigationController pushViewController:con animated:YES];
}

-(UIView *)getUserView:(EventBuyInviteInfoBean *)bean index:(int)index{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(10+index*40, 0, 32, 32)];
    bg.layer.masksToBounds = YES;
    bg.layer.cornerRadius = 16;
    bg.clipsToBounds = NO;
    
    if(bean){
        bg.backgroundColor = GB_UIColorFromRGB(91, 91, 91);
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, 28, 28)];
        [bg addSubview:img];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 14;
        [GB_NetWorkUtils loadImage:bean.tbi_portrait container:img type:GB_ImageCacheTypeAll delegate:nil tag:bean.tag];
        
        UILabel *icon = [GB_WidgetUtils getLabel:CGRectMake(22, 22, 12, 12) title:@"约" font:[UIFont systemFontOfSize:8] color:[UIColor whiteColor]];
        icon.backgroundColor = GB_UIColorFromRGB(251, 80, 87);
        icon.textAlignment = NSTextAlignmentCenter;
        icon.layer.masksToBounds = YES;
        icon.layer.cornerRadius = 6;
        [bg addSubview:icon];
        
        
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 32, 32) image:nil imageH:nil id:self sel:@selector(c_user_invite:)];
        btn.tag = index;
        [bg addSubview:btn];
        bg.userInteractionEnabled = YES;
    }
    else{
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 32, 32) image:nil imageH:nil id:self sel:@selector(c_date)];
        [bg addSubview:btn];
        bg.userInteractionEnabled = YES;
        bg.image = [UIImage imageNamed:@"btn_cell_event_list_add"];
    }
    return bg;
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            self.isLoad = YES;
            self.eventBean = [EventBean getBean:[Static getRequestData:str]];
            [self createDataSource];
        }
        if(tag == 2 || tag == 3){
            EventCommentBean *b = [_eventBean.comments objectAtIndex:_zanIndex];
            b.tac_up_flag = tag==2?1:0;
            b.tac_up_count =  b.tac_up_count+ (tag == 2?1:-1);
            b.tac_up_count = b.tac_up_count<0?0:b.tac_up_count;
            NSMutableArray *comments = [NSMutableArray arrayWithArray:_eventBean.comments];
            [comments replaceObjectAtIndex:_zanIndex withObject:b];
            _eventBean.comments = comments;
            [self reCreateCommentDataSource];
        }
        if(tag == 4 || tag == 5){
            EventCommentBean *b = [_eventBean.comments objectAtIndex:_caiIndex];
            b.tac_down_flag = (tag==4?1:0);
            b.tac_down_count = (b.tac_down_count+ (tag==4?1:-1));
            b.tac_down_count = (b.tac_down_count<0?0:b.tac_down_count);
            NSMutableArray *comments = [NSMutableArray arrayWithArray:_eventBean.comments];
            [comments replaceObjectAtIndex:_caiIndex withObject:b];
            _eventBean.comments = comments;
            [self reCreateCommentDataSource];
        }
        if(tag == 6){
            [Static alert:self.navigationController.view msg:@"举报成功"];
        }
    }
}



@end

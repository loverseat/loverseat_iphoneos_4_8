//
//  ZanListBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "ZanListBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation ZanListBean


+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            ZanListBean *bean = [ZanListBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(ZanListBean *)getBean:(NSDictionary *)dic{
    ZanListBean *bean = [[ZanListBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tai_id = [dic objectForKey:@"tai_id"];
        bean.tai_uid = [dic objectForKey:@"tai_uid"];
        bean.tai_event_id = [dic objectForKey:@"tai_event_id"];
        bean.tai_type = [dic objectForKey:@"tai_type"];
        bean.tai_nickname = [dic objectForKey:@"tai_nickname"];
        bean.tai_gender = [dic objectForKey:@"tai_gender"];
        bean.tai_portrait = [dic objectForKey:@"tai_portrait"];
        bean.tai_star_sign = [dic objectForKey:@"tai_star_sign"];
        bean.tai_birthday = [dic objectForKey:@"tai_birthday"];
        bean.tai_created = [dic objectForKey:@"tai_created"];
    }
    return bean;
}

@end

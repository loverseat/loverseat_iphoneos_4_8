//
//  ZanListBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface ZanListBean : BaseBean

@property (nonatomic, strong) NSString *tai_id;
@property (nonatomic, strong) NSString *tai_uid;
@property (nonatomic, strong) NSString *tai_event_id;
@property (nonatomic, strong) NSString *tai_type;
@property (nonatomic, strong) NSString *tai_nickname;
@property (nonatomic, strong) NSString *tai_gender;
@property (nonatomic, strong) NSString *tai_portrait;
@property (nonatomic, strong) NSString *tai_star_sign;
@property (nonatomic, strong) NSString *tai_birthday;
@property (nonatomic, strong) NSString *tai_created;

+(NSArray *)getBeanList:(NSArray *)arr;

+(ZanListBean *)getBean:(NSDictionary *)dic;
 
@end

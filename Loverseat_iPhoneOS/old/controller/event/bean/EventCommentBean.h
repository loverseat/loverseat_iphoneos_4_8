//
//  EventCommentBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface EventCommentBean : BaseBean

@property (nonatomic, strong) NSString *tac_created;
@property (nonatomic, strong) NSString *tac_detail;
@property (nonatomic, strong) NSString *tac_thumb;
@property (nonatomic, strong) NSString *tac_username;
@property (nonatomic, strong) NSString *tac_uid;
@property (nonatomic, assign) int tac_down_count;
@property (nonatomic, assign) int tac_down_flag;
@property (nonatomic, assign) int tac_id;
@property (nonatomic, assign) int tac_up_count;
@property (nonatomic, assign) int tac_up_flag;

@property (nonatomic, strong) UIButton *btnZan;
@property (nonatomic, strong) UIButton *btnCai;

+(NSArray *)getBeanList:(NSArray *)arr;

+(EventCommentBean *)getBean:(NSDictionary *)dic;

@end

//
//  EventBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventBean *bean = [EventBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventBean *)getBean:(NSDictionary *)dic{
    EventBean *bean = [[EventBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.event_info = [EventInfoBean getBean:[dic objectForKey:@"event_info"]];
        bean.event_buy_invite_info = [EventBuyInviteInfoBean getBeanList:[dic objectForKey:@"buy_invite_info"]];
        bean.comments = [EventCommentBean getBeanList:[dic objectForKey:@"comments"]];
        bean.like_count = [[dic objectForKey:@"like_count"] intValue];
        bean.like_flag = [[dic objectForKey:@"like_flag"] intValue];
        bean.buy_ticket_url = [dic objectForKey:@"buy_ticket_url"];
    }
    return bean;
}

@end

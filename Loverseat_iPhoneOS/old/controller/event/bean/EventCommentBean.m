//
//  EventCommentBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventCommentBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventCommentBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventCommentBean *bean = [EventCommentBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventCommentBean *)getBean:(NSDictionary *)dic{
    EventCommentBean *bean = [[EventCommentBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tac_created = [dic objectForKey:@"tac_created"];
        bean.tac_detail = [dic objectForKey:@"tac_detail"];
        bean.tac_username = [dic objectForKey:@"tac_username"];
        bean.tac_uid = [dic objectForKey:@"tac_uid"];
        bean.tac_thumb = [dic objectForKey:@"tac_thumb"];
        bean.tac_down_count = [[dic objectForKey:@"tac_down_count"] intValue];
        bean.tac_down_flag = [[dic objectForKey:@"tac_down_flag"] intValue];
        bean.tac_id = [[dic objectForKey:@"tac_id"] intValue];
        bean.tac_up_count = [[dic objectForKey:@"tac_up_count"] intValue];
        bean.tac_up_flag = [[dic objectForKey:@"tac_up_flag"] intValue];
    }
    return bean;
}


@end

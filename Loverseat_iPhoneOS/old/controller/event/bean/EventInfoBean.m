//
//  EventInfoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventInfoBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventInfoBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventInfoBean *bean = [EventInfoBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventInfoBean *)getBean:(NSDictionary *)dic{
    EventInfoBean *bean = [[EventInfoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tad_audio = [dic objectForKey:@"tad_audio"];
        bean.tad_begin_time = [dic objectForKey:@"tad_begin_time"];
        bean.tad_end_time = [dic objectForKey:@"tad_end_time"];
        bean.tad_image = [dic objectForKey:@"tad_image"];

        bean.tad_intro = [dic objectForKey:@"tad_intro"];
        
        bean.tad_intro = [bean.tad_intro stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        bean.tad_intro = [bean.tad_intro stringByReplacingOccurrencesOfString:@"\\r" withString:@"\r"];
        
        bean.tad_points = [dic objectForKey:@"tad_points"];
        bean.tad_show_name = [dic objectForKey:@"tad_show_name"];
        bean.tad_venue_name = [dic objectForKey:@"tad_venue_name"];
    }
    return bean;
}

@end

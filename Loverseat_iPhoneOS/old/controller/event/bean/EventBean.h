//
//  EventBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"
#import "EventInfoBean.h"
#import "EventBuyInviteInfoBean.h"
#import "EventCommentBean.h"

@interface EventBean : BaseBean

@property (nonatomic, strong) NSArray *event_buy_invite_info;
@property (nonatomic, strong) NSArray *comments;
@property (nonatomic, strong) EventInfoBean *event_info;
@property (nonatomic, strong) NSString *in_flag;
@property (nonatomic, assign) int like_count;
@property (nonatomic, assign) int like_flag;
@property (nonatomic, strong) NSString *buy_ticket_url;

+(NSArray *)getBeanList:(NSArray *)arr;

+(EventBean *)getBean:(NSDictionary *)dic;

@end

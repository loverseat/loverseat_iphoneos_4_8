//
//  EventInfoBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface EventInfoBean : BaseBean

@property (nonatomic, strong) NSString *tad_audio;
@property (nonatomic, strong) NSString *tad_begin_time;
@property (nonatomic, strong) NSString *tad_end_time;
@property (nonatomic, strong) NSString *tad_image;
@property (nonatomic, strong) NSString *tad_intro;
@property (nonatomic, strong) NSString *tad_points;
@property (nonatomic, strong) NSString *tad_show_name;
@property (nonatomic, strong) NSString *tad_venue_name;

+(NSArray *)getBeanList:(NSArray *)arr;

+(EventInfoBean *)getBean:(NSDictionary *)dic;

@end

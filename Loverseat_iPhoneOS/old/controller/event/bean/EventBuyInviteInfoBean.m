//
//  EventBuyInviteInfoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventBuyInviteInfoBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventBuyInviteInfoBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventBuyInviteInfoBean *bean = [EventBuyInviteInfoBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventBuyInviteInfoBean *)getBean:(NSDictionary *)dic{
    EventBuyInviteInfoBean *bean = [[EventBuyInviteInfoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tbi_portrait = [dic objectForKey:@"tbi_portrait"];
        bean.tbi_uid = [dic objectForKey:@"tbi_uid"];
    }
    return bean;
}

@end

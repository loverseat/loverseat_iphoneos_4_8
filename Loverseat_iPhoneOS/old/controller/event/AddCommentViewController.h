//
//  AddCommentViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@class EventViewController;
@interface AddCommentViewController : BaseViewController
<UITextViewDelegate,GB_NetWorkDelegate>

@property (nonatomic, strong) NSString *event_id;
@property (nonatomic, strong) EventViewController *eventViewController;
@end

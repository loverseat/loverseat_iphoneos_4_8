//
//  LikeListViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "LikeListViewController.h"
#import "ZanListBean.h"
#import "ZanListCell.h"

@interface LikeListViewController()
<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *contentArr;


@end

@implementation LikeListViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.contentArr = [NSMutableArray array];
    [self initFrame];
    [self initData];
}

-(void)initFrame{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0);
    tableView.backgroundView = nil;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    [NavUtils addNavBgView:self color:GB_UIColorFromRGB(248, 248, 249)];
    [NavUtils addBackGrayButton:self];
    [NavUtils addNavTitleView:self text:@"赞" color:GB_UIColorFromRGB(69, 69, 69)];
}

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getEventLikeListUrl:_event_id] list:arr delegate:self tag:1];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return _contentArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ZanListCellIdentifier";
    ZanListCell *cell = [tableView dequeueReusableCellWithIdentifier:
                          identifier];
    if (cell == nil) {
        cell = [[ZanListCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    ZanListBean *bean = [_contentArr objectAtIndex:indexPath.row];
    bean.baseViewController = self;
    [cell setZanListBean:bean];
    return cell;
}


-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        self.isLoad = YES;
        [_contentArr setArray:[ZanListBean getBeanList:[Static getRequestData:str]]];
        [_tableView reloadData];
        
    }
}

@end

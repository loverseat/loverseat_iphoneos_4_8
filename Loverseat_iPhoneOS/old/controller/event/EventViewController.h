//
//  EventViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/17.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
@class EventCommentBean;
@interface EventViewController : BaseViewController

@property (nonatomic, strong) NSString *event_id;

-(void)addEventCommentBean:(EventCommentBean *)bean;

@end

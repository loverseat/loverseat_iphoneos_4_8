//
//  DateAllSwitchViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@protocol ICSDateDrawerControllerChild;
@protocol ICSDateDrawerControllerPresenting;
@class DateAllLeftViewController;
@class DateAllCenterViewController;
@interface DateAllSwitchViewController : BaseViewController

@property(nonatomic, strong) DateAllLeftViewController<ICSDateDrawerControllerChild, ICSDateDrawerControllerPresenting> *leftViewController;

@property(nonatomic, strong) DateAllCenterViewController<ICSDateDrawerControllerChild, ICSDateDrawerControllerPresenting> *centerViewController;


- (id)initWithLeftViewController:(DateAllLeftViewController<ICSDateDrawerControllerChild, ICSDateDrawerControllerPresenting> *)leftViewController
            centerViewController:(DateAllCenterViewController<ICSDateDrawerControllerChild, ICSDateDrawerControllerPresenting> *)centerViewController;
-(void)open;
-(void)leftOpen;
-(void)close;
-(void)changeCenterViewController;
-(void)addCenterViewController;
//-(void)setEnabledPanGestureRecognizer:(BOOL)isEnabled;
@end


@protocol ICSDateDrawerControllerChild <NSObject>

@property(nonatomic, weak) DateAllSwitchViewController *drawer;

@end

@protocol  ICSDateDrawerControllerPresenting <NSObject>

@optional

- (void)drawerControllerWillOpen:(DateAllSwitchViewController *)drawerController;

- (void)drawerControllerDidOpen:(DateAllSwitchViewController *)drawerController;

- (void)drawerControllerWillClose:(DateAllSwitchViewController *)drawerController;

- (void)drawerControllerDidClose:(DateAllSwitchViewController *)drawerController;



@end

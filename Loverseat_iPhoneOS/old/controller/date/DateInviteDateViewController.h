//
//  DateInviteDateViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface DateInviteDateViewController : BaseViewController
@property (nonatomic, assign) int payType;
@property (nonatomic, strong) NSString *event_id;
@property (nonatomic, assign) int index;
@property (nonatomic, strong) NSArray *validDateArr;
@end

//
//  DateAllLeftViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllLeftViewController.h"
#import "DateAllCenterViewController.h"

@interface DateAllLeftViewController()
<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong) NSMutableArray *sexBtnArr;
@property (nonatomic, strong) NSMutableArray *paymentBtnArr;
@property (nonatomic, strong) NSMutableArray *freetimeBtnArr;
@end

@implementation DateAllLeftViewController

-(instancetype)init{
    self = [super init];
    if(self){
        self.paymentData = [NSMutableArray array];
        self.genderData = [NSMutableArray array];
        self.freetimeData = [NSMutableArray array];
        self.freetimeDateData = [NSMutableArray array];
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.sexBtnArr = [NSMutableArray array];
    self.paymentBtnArr = [NSMutableArray array];
    self.freetimeBtnArr = [NSMutableArray array];
}

-(void)clearData{
    [_paymentData removeAllObjects];
    [_genderData removeAllObjects];
    [_freetimeData removeAllObjects];
    [_freetimeDateData removeAllObjects];
    [_sexBtnArr removeAllObjects];
    [_paymentBtnArr removeAllObjects];
    [_freetimeBtnArr removeAllObjects];
    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
}

-(void)initFrame{
    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
    self.view.backgroundColor = GB_UIColorFromRGB(36, 40, 49);
    
    float height = 0.0f;
    UILabel *sex = [GB_WidgetUtils getLabel:CGRectMake(15, height, 200, [NavUtils getNavHeight]) title:@"邀约性别" font:[UIFont systemFontOfSize:12] color:[UIColor colorWithWhite:1 alpha:0.6f]];
    [self.view addSubview:sex];
    height += [NavUtils getNavHeight];
    
    [self.view addSubview:[View getLine:CGRectMake(0, height, 260, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    
    height+=0.5f;
    
    UIButton *firstGenderBtn;
    UIView *sexBg = [[UIView alloc]initWithFrame:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 80)];
    sexBg.backgroundColor = GB_UIColorFromRGB(29, 33, 42);
    
    for (int i =0 ; i<_genderData.count; i++) {
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(15+i*75, 12, 55, 55) image:nil imageH:nil id:self sel:@selector(c_gender:)];
        
        NSString *title = [_genderData objectAtIndex:i];
        if((self.drawer.centerViewController.searchGender && self.drawer.centerViewController.searchGender.intValue == title.intValue)||![_genderData containsObject:@"-1"]){
            firstGenderBtn = btn;
        }
        btn.tag = i;
        [sexBg addSubview:btn];
        [_sexBtnArr addObject:btn];
    }
    
    [self.view addSubview:sexBg];
    height +=80;
    
    if(firstGenderBtn){
        [self c_gender:firstGenderBtn];
    }
    
    [self.view addSubview:[View getLine:CGRectMake(0, height, 260, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    
    height+=0.5f;
    
    UILabel *payment = [GB_WidgetUtils getLabel:CGRectMake(15, height, 200, [NavUtils getNavHeight]) title:@"买单条件" font:[UIFont systemFontOfSize:12] color:[UIColor colorWithWhite:1 alpha:0.6f]];
    [self.view addSubview:payment];
    height += [NavUtils getNavHeight];
    
    [self.view addSubview:[View getLine:CGRectMake(0, height, 260, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    
    height+=0.5f;
    
    UIView *paymentBg = [[UIView alloc]initWithFrame:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 80)];
    paymentBg.backgroundColor = GB_UIColorFromRGB(29, 33, 42);
    UIButton *firstPaymentBtn;
    for (int i =0 ; i<_paymentData.count; i++) {
        NSString *title = [_paymentData objectAtIndex:i];
        int count = i%4;
        int row = floor(i/4);
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(count*60, 20+row*30, 60, 20) image:nil imageH:nil id:self sel:@selector(c_payment:)];
        if((self.drawer.centerViewController.searchPayment && self.drawer.centerViewController.searchPayment.intValue == title.intValue)||![_paymentData containsObject:@"-1"]){
            firstPaymentBtn = btn;
        }
        if(title.intValue == -1){
            title = @"全部";
        }
        else if(title.intValue == 1){
            title = @"我请客";
        }
        else if(title.intValue == 2){
            title = @"AA";
        }
        else if(title.intValue == 3){
            title = @"TA请客";
        }
        else if(title.intValue == 4){
            title = @"都可以";
        }
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        btn.tag = i;
        [paymentBg addSubview:btn];
        [_paymentBtnArr addObject:btn];
    }
    
    [self.view addSubview:paymentBg];
    height +=80;
    if(firstPaymentBtn){
        [self c_payment:firstPaymentBtn];
    }
    
    [self.view addSubview:[View getLine:CGRectMake(0, height, 260, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    
    height+=0.5f;
    
    
    UILabel *freetime = [GB_WidgetUtils getLabel:CGRectMake(15, height, 200, [NavUtils getNavHeight]) title:@"邀约时间" font:[UIFont systemFontOfSize:12] color:[UIColor colorWithWhite:1 alpha:0.6f]];
    [self.view addSubview:freetime];
    height += [NavUtils getNavHeight];
    
    [self.view addSubview:[View getLine:CGRectMake(0, height, 260, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    height+=0.5f;
    
    UIView *freetimeBg = [[UIView alloc]initWithFrame:CGRectMake(0, height, [GB_DeviceUtils getScreenWidth], 192)];
    freetimeBg.backgroundColor = GB_UIColorFromRGB(29, 33, 42);
    freetimeBg.clipsToBounds = YES;
    UIButton *firstFreetimeBtn;
    for (int i =0 ; i<_freetimeData.count; i++) {
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(i*60, 20, 60, 20) image:nil imageH:nil id:self sel:@selector(c_freetime:)];
        NSString *title = [_freetimeData objectAtIndex:i];
        if((self.drawer.centerViewController.searchFreeTime && self.drawer.centerViewController.searchFreeTime.intValue == title.intValue)||![_freetimeData containsObject:@"-1"]){
            firstFreetimeBtn = btn;
        }
        if(title.intValue == -1){
            title = @"全部";
        }
        else if(title.intValue == 1){
            title = @"都可以";
        }
        else if(title.intValue == 2){
            title = @"具体时间";
        }
        
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        btn.tag = i;
        [freetimeBg addSubview:btn];
        [_freetimeBtnArr addObject:btn];
    }

    self.picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 260, 0)];
    _picker.delegate = self;
    _picker.dataSource = self;
    _picker.backgroundColor = [UIColor clearColor];
    [freetimeBg addSubview:_picker];
    _picker.frame = CGRectMake(0, 35, 260, 162);
    
    
    if(firstFreetimeBtn){
        [self c_freetime:firstFreetimeBtn];
    }
    
    [self.view addSubview:freetimeBg];
    height +=192;
    
    [self.view addSubview:[View getLine:CGRectMake(0, height, 260, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    height+=0.5f;
    
    
    UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, height, 260, 48) image:nil imageH:nil id:self sel:@selector(c_confirm)];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:btn];
}

-(void)c_confirm{
    [self.drawer close];
}

-(void)c_gender:(UIButton *)btn{
    for (UIButton *b in _sexBtnArr) {
        int title = [[_genderData objectAtIndex:b.tag] intValue];
        if(title == -1){
            [b setImage:[UIImage imageNamed:btn==b?@"btn_date_all_h":@"btn_date_all_n"] forState:UIControlStateNormal];
        }
        if(title == 0){
            [b setImage:[UIImage imageNamed:btn==b?@"btn_date_all_male_h":@"btn_date_all_male_n"] forState:UIControlStateNormal];
        }
        if(title == 1){
            [b setImage:[UIImage imageNamed:btn==b?@"btn_date_all_female_h":@"btn_date_all_female_n"] forState:UIControlStateNormal];
        }
    }
     self.drawer.centerViewController.searchGender = [_genderData objectAtIndex:btn.tag];
    [self.drawer.centerViewController search];
}

-(void)c_payment:(UIButton *)btn{
    for (UIButton *b in _paymentBtnArr) {
        b.alpha = b == btn?1.0f:0.12f;
    }
    self.drawer.centerViewController.searchPayment = [_paymentData objectAtIndex:btn.tag];
    [self.drawer.centerViewController search];
}

-(void)c_freetime:(UIButton *)btn{
    for (UIButton *b in _freetimeBtnArr) {
        b.alpha = b == btn?1.0f:0.12f;
    }
    _picker.hidden = btn.tag != 2;
    self.drawer.centerViewController.searchFreeTime = [_freetimeData objectAtIndex:btn.tag];
    [self.drawer.centerViewController search];
}

#pragma mark UIPickerViewDelegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _freetimeDateData.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.drawer.centerViewController.searchFreeDateTime = [_freetimeDateData objectAtIndex:row];
    [self.drawer.centerViewController search];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc] init];
    label.text = [_freetimeDateData objectAtIndex:row];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}


- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)drawerControllerWillOpen:(DateAllSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidOpen:(DateAllSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(DateAllSwitchViewController *)drawerController
{
    [GB_NetWorkUtils cancelRequest];
//    [self.drawer.centerViewController initData];
    
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(DateAllSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

@end

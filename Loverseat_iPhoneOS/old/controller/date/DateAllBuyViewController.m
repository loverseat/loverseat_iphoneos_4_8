//
//  DateAllBuyViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/24.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllBuyViewController.h"

@interface DateAllBuyViewController()
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation DateAllBuyViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"购票"];
    [NavUtils addBackButton:self];
    
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    [self.view addSubview:_webView];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
}

@end

//
//  DateInvitePayViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateInvitePayViewController.h"
#import "DateInviteDateViewController.h"

@interface DateInvitePayViewController ()
@property (nonatomic, strong) NSMutableArray *buttonContentData;
@property (nonatomic, assign) int clickValue;
@end

@implementation DateInvitePayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.buttonContentData = [NSMutableArray array];
    self.clickValue = -1;
    [self initFrame];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"startInvite"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"startInvite"];
}
-(void)initFrame{
    [NavUtils addBackground:self];
    [NavUtils addNavTitleView:self text:@"邀约信息" color:[UIColor whiteColor]];
    [NavUtils addBackWhiteButton:self];
    [NavUtils addRightButton:self title:@"下一步" color:[UIColor whiteColor] sel:@selector(c_next)];
    [NavUtils addNavTitleLine:self];
    
    int width = [GB_DeviceUtils getScreenWidth]/3;
    for (int i = 0 ; i<4; i++) {
        int row = floor(i/3);
        int count = i%3;
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 75, 75) image:nil imageH:nil id:self sel:@selector(c_click:)];
        btn.center = CGPointMake(width*count+width*0.5f, [GB_DeviceUtils getScreenHeight]*0.3f+row*120);
        btn.tag = i+1;
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 37.5f;
        NSString *str;
        if(i == 0){
            str = @"我请客";
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_date_invite_mine_n"] forState:UIControlStateNormal];
        }
        if(i == 1){
            str = @"AA制度";
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_date_invite_aa_n"] forState:UIControlStateNormal];
        }
        if(i == 2){
            str = @"TA请客";
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_date_invite_he_n"] forState:UIControlStateNormal];
        }
        if(i == 3){
            str = @"都可以";
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_date_invite_ok_n"] forState:UIControlStateNormal];
        }
        [self.view addSubview:btn];
        [_buttonContentData addObject:btn];
        
        UILabel *l = [GB_WidgetUtils getLabel:CGRectMake(count*width, btn.frame.origin.y+85, width, 20) title:str font:[UIFont systemFontOfSize:13] color:[UIColor whiteColor]];
        l.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:l];
    }
}

-(void)c_click:(UIButton *)btn{
    for(UIButton *b in _buttonContentData) {
        if(b.tag == 1){
            [b setBackgroundImage:[UIImage imageNamed:btn==b?@"btn_date_invite_mine_h":@"btn_date_invite_mine_n"] forState:UIControlStateNormal];
        }
        if(b.tag == 2){
            [b setBackgroundImage:[UIImage imageNamed:btn==b?@"btn_date_invite_aa_h":@"btn_date_invite_aa_n"] forState:UIControlStateNormal];
        }
        if(b.tag == 3){
            [b setBackgroundImage:[UIImage imageNamed:btn==b?@"btn_date_invite_he_h":@"btn_date_invite_he_n"] forState:UIControlStateNormal];
        }
        if(b.tag == 4){
            [b setBackgroundImage:[UIImage imageNamed:btn==b?@"btn_date_invite_ok_h":@"btn_date_invite_ok_n"] forState:UIControlStateNormal];
        }
    }
    _clickValue = btn.tag;
}

-(void)c_next{
    if(_clickValue == -1){
        [Static alert:self.navigationController.view msg:@"请选择邀约信息"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在提交"];
        [GB_NetWorkUtils startGetAsyncRequest:[Url getAllSchedulesUrl:_event_id] delegate:self tag:1];
    }
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            NSMutableArray *arr = [NSMutableArray array];
            for (NSDictionary *dic in [Static getRequestData:str]) {
                [arr addObject:[dic objectForKey:@"valid_time"]];
            }
            DateInviteDateViewController *con = [[DateInviteDateViewController alloc]init];
            con.payType = _clickValue;
            con.event_id = _event_id;
            con.index = _index;
            con.validDateArr = arr;
            [self.navigationController pushViewController:con animated:YES];
            
        }
    }
}

@end

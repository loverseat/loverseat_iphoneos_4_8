//
//  DateAllCenterViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllCenterViewController.h"
#import "DateAllLeftViewController.h"
#import "DateAllLeftCell.h"
#import "DateAllRightCell.h"
#import "DateAllBuyViewController.h"
#import "UserInfoViewController.h"
#import "DateInvitePayViewController.h"
#import "DateAllSuccessViewController.h"
#import "DateAllLeftBean.h"
#import "TestViewController.h"
@interface DateAllCenterViewController()
<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    TestViewController *tvc;
}
@property (nonatomic, strong) UIButton *toolLeftBtn;
@property (nonatomic, strong) UIButton *toolRightBtn;
@property (nonatomic, strong) NSMutableArray *leftContentData;
@property (nonatomic, strong) NSMutableArray *currentLeftContentData;
@property (nonatomic, strong) NSMutableArray *rightContentData;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *bottomBtn;
@property (nonatomic, assign) int rejectIndex;
@property (nonatomic, assign) int agreeIndex;
@property (nonatomic, assign) int yueIndex;
@property (nonatomic, assign) int ignoreIndex;
@property (nonatomic, assign) BOOL shouldAlbum;

@property (nonatomic, strong) UIImageView *leftNoContent;
@property (nonatomic, strong) UIImageView *rightNoContent;
@property (nonatomic, strong) NSString *succeedStr;
@end

@implementation DateAllCenterViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getEventPersonUrl] list:arr delegate:self tag:1];
    }
    [MobClick beginLogPageView:@"inviteList"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"inviteList"];
}
-(instancetype)init{
    self = [super init];
    if(self){
        self.searchFreeTime = @"-1";
        self.searchGender = @"-1";
        self.searchPayment = @"-1";
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.leftContentData = [NSMutableArray array];
    self.rightContentData = [NSMutableArray array];
    self.currentLeftContentData = [NSMutableArray array];
    [self initFrame];
}

-(void)initFrame{
    [NavUtils addBackground:self];
    [NavUtils addCloseButton:self];
    [NavUtils addLeftButton:self title:@"筛选" color:[UIColor whiteColor] sel:@selector(c_drawer)];
    [NavUtils addNavTitleLine:self];
    
    float width = 65.5f;
    float height = 23.5f;
    self.toolLeftBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5-width, [NavUtils getNavHeight]*0.5-height*0.5, width, height) image:nil imageH:nil id:self sel:@selector(c_tool:)];
    _toolLeftBtn.tag = 1;
    [self.view addSubview:_toolLeftBtn];
    
    self.toolRightBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5, [NavUtils getNavHeight]*0.5-height*0.5, width, height) image:nil imageH:nil id:self sel:@selector(c_tool:)];
    _toolRightBtn.tag = 2;
    [self.view addSubview:_toolRightBtn];
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-48.5f)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.2f];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    
    self.leftNoContent = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_date_left_nocontent"]];
    [self.view addSubview:_leftNoContent];
    _leftNoContent.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5,[GB_DeviceUtils getScreenHeight]-48.5f-30-_leftNoContent.image.size.height *0.5f);
    _leftNoContent.hidden = YES;

    self.rightNoContent = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_date_right_nocontent"]];
    _rightNoContent.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5,[NavUtils getNavHeight]+_rightNoContent.image.size.height *0.5f);
    _rightNoContent.hidden = YES;
    [self.view addSubview:_rightNoContent];
    
    self.bottomBtn = [GB_WidgetUtils getButton:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-48, [GB_DeviceUtils getScreenWidth], 48) image:nil imageH:nil id:self sel:@selector(c_date)];
    [_bottomBtn setTitle:_isJoin?@"0位朋友正在 邀约中":@"我要邀约" forState:UIControlStateNormal];
    [_bottomBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _bottomBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:_bottomBtn];
    
    [_bottomBtn addSubview:[View getLine:CGRectMake(0,-0.5f, [GB_DeviceUtils getScreenWidth], 0.5f) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
    
    [self c_tool:_toolLeftBtn];
}

-(BOOL)isLeft{
    return !super.leftBtn.hidden;
}

-(void)initData:(BOOL)isForce{
//    if(!isForce){
//        BOOL isExecute = NO;
//        if([self isLeft]&&[GB_ToolUtils isNotBlank:_leftContentData]){
//            _tableView.tag = 1;
//            [self search];
//            isExecute = YES;
//        }
//        if(![self isLeft]&&[GB_ToolUtils isNotBlank:_rightContentData]){
//            _tableView.tag = 2;
//            [_tableView reloadData];
//            isExecute = YES;
//        }
//        if(isExecute){
//            if(_tableView.tag == 1){
//                _tableView.hidden = [GB_ToolUtils isBlank:_leftContentData];
//                _leftNoContent.hidden = !_tableView.hidden;
//                _rightNoContent.hidden = YES;
//            }
//            if(_tableView.tag == 2){
//                _tableView.hidden = [GB_ToolUtils isBlank:_rightContentData];
//                _rightNoContent.hidden = !_tableView.hidden;
//                _leftNoContent.hidden = YES;
//            }
//            return;
//        }
//    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
        if([self isLeft]){
            [GB_NetWorkUtils startPostAsyncRequest:[Url getEventPersonUrl] list:arr delegate:self tag:1];
        }
        else{
            [GB_NetWorkUtils startPostAsyncRequest:[Url getInvitePersonUrl] list:arr delegate:self tag:2];
        }
    }
}

-(void)search{
    NSMutableArray *dataArr =[NSMutableArray arrayWithArray:_leftContentData];
    NSMutableArray *arr =[NSMutableArray array];
    if(_searchPayment.intValue!=-1){
        for (DateAllLeftBean *bean in dataArr) {
            if(_searchPayment.intValue == 4){
                if(4 == bean.tbi_payment.intValue || [GB_StringUtils isBlank:bean.tbi_payment]){
                    [arr addObject:bean];
                }
            }
            else{
                if(_searchPayment.intValue == bean.tbi_payment.intValue){
                    [arr addObject:bean];
                }
            }
        }
        [dataArr setArray:arr];
    }

    if(_searchGender.intValue!=-1){
        [arr removeAllObjects];
        for (DateAllLeftBean *bean in dataArr) {
            if(_searchGender.intValue == bean.tu_gender.intValue){
                [arr addObject:bean];
            }
        }
        [dataArr setArray:arr];
    }
    
    if(_searchFreeTime.intValue!=-1){
        [arr removeAllObjects];
        for (DateAllLeftBean *bean in dataArr) {
            if(_searchFreeTime.intValue == 1){
                if([GB_StringUtils isBlank:bean.tbi_free_time]||[bean.tbi_free_time isEqualToString:@"all"]){
                    [arr addObject:bean];
                }
            }
            if(_searchFreeTime.intValue == 2){
                if([_searchFreeDateTime isEqualToString:bean.tbi_free_time]){
                    [arr addObject:bean];
                }
            }
        }
        [dataArr setArray:arr];
    }
    
    [_currentLeftContentData setArray:dataArr];
    [_tableView reloadData];
    
    if(_isJoin){
        [_bottomBtn setTitle:[NSString stringWithFormat:@"%d位朋友正在 邀约中",_currentLeftContentData.count] forState:UIControlStateNormal];
    }
}

-(void)setBeanIcon:(int)type{
    self.isJoin = YES;
    [_bottomBtn setTitle:[NSString stringWithFormat:@"%d位朋友正在 邀约中",_currentLeftContentData.count] forState:UIControlStateNormal];
}

-(void)clearData{
    [_drawer.leftViewController clearData];
    [_leftContentData removeAllObjects];
    [_currentLeftContentData removeAllObjects];
    [_rightContentData removeAllObjects];
    [_tableView reloadData];
}

-(void)paySuccess{
    [self clearData];
    [self initData:YES];
}

-(void)c_date{
    if(_isJoin)return;
    
    // 邀约信息
    DateInvitePayViewController *con = [[DateInvitePayViewController alloc]init];
    con.event_id = _event_id;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_tool:(UIButton *)btn{
    [self.toolLeftBtn setImage:[UIImage imageNamed:@"btn_date_tool_left_n"] forState:UIControlStateNormal];
    [self.toolRightBtn setImage:[UIImage imageNamed:@"btn_date_tool_right_n"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:btn.tag==1?@"btn_date_tool_left_h":@"btn_date_tool_right_h"] forState:UIControlStateNormal];
    super.leftBtn.hidden = btn.tag != 1;
    _bottomBtn.hidden = super.leftBtn.hidden;
    _tableView.frame = CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-(btn.tag==1?48.5f:0));
    [self initData:NO];
}

-(void)c_drawer{
    [GB_NetWorkUtils cancelRequest];
    [Static remove:self.navigationController.view];
    [self.drawer leftOpen];
}

-(void)c_yue:(UIButton *)btn{
    DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:btn?btn.tag:_yueIndex];
    if(bean.shouldAlpha)return;
    if(bean.invite_flag.intValue == 1||bean.invite_flag.intValue == 3||bean.mine_flag.intValue == 1)return;
    if(btn){
        _yueIndex = btn.tag;
    }
    if(_shouldAlbum){
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"您的用户资料不完整" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"去完善",@"继续赴约", nil];
        alertView.tag = 2;
        [alertView show];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"给他发送邀请语吧" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        UITextField *userTextField = [alertView textFieldAtIndex:0];
        userTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        userTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        userTextField.textAlignment = NSTextAlignmentCenter;
        userTextField.placeholder = @"请输入邀请语";
        
        alertView.tag = 1;
        [alertView show];
    }
}

-(void)c_agree:(UIButton *)btn{
    @synchronized(self){
        DateAllRightBean *bean = [_rightContentData objectAtIndex:btn.tag];
        if(bean.shouldAlpha)return;
        _agreeIndex = btn.tag;
        [Static add:self.navigationController.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbia_id" value:bean.tbia_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"1"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUpdateBuyInviteUrl] list:arr delegate:self tag:4];
    }
}

-(void)c_reject:(UIButton *)btn{
    @synchronized(self){
        DateAllRightBean *bean = [_rightContentData objectAtIndex:btn.tag];
        if(bean.shouldAlpha)return;
        _rejectIndex = btn.tag;
        [Static add:self.navigationController.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbia_id" value:bean.tbia_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"2"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getUpdateBuyInviteUrl] list:arr delegate:self tag:3];
    }
}

-(void)c_message:(UIButton *)btn{
    DateAllRightBean *bean = [_rightContentData objectAtIndex:btn.tag];
    if(bean.shouldAlpha)return;
   
#warning 跳转到聊天
}

-(void)c_buy:(UIButton *)btn{
    DateAllRightBean *bean = [_rightContentData objectAtIndex:btn.tag];
//    Buy1ViewController *buy = [[Buy1ViewController alloc]init];
//    buy.eventId = _event_id;
//    buy.viewController = self;
//    [self.navigationController pushViewController:buy animated:YES];
    
    DateAllBuyViewController *buy = [[DateAllBuyViewController alloc]init];
    buy.url = bean.buy_ticket_url;
    [self.navigationController pushViewController:buy animated:YES];
}

-(void)c_left_user:(UIButton *)btn{
    DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:btn.tag];
    if(bean.shouldAlpha)return;
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = bean.tbi_uid;
    con.centerViewController = self;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_right_user:(UIButton *)btn{
    DateAllRightBean *bean = [_rightContentData objectAtIndex:btn.tag];
    if(bean.shouldAlpha)return;
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = bean.tbia_uid;
    con.centerViewController = self;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        if(alertView.tag == 2){
            if(buttonIndex == 0){
                _shouldAlbum = NO;
                UserInfoViewController *con = [[UserInfoViewController alloc]init];
                con.uid = [User getUserInfo].uid;
                [self.navigationController pushViewController:con animated:YES];
            }
            if(buttonIndex == 1){
                _shouldAlbum = NO;
                [self c_yue:nil];
            }
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        if(alertView.tag == 1){
            NSString *str = [[alertView textFieldAtIndex:0] text];
            if([GB_StringUtils isBlank:str]){
                [Static alert:self.navigationController.view msg:@"邀请语不能为空"];
                return;
            }
            DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:_yueIndex];
            [Static add:self.navigationController.view msg:@"正在提交"];
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbi_uid" value:bean.tbi_uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbi_id" value:bean.tbi_id]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"invite_message" value:str]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getInviteTargetPersonUrl] list:arr delegate:self tag:5];
        }
    }
}

#define mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.tag == 1?100:145;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableView.tag == 1?_currentLeftContentData.count:_rightContentData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        static NSString *identifier = @"DateAllLeftTableIdentifier";
        
        DateAllLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                 identifier];
        
        if (cell == nil) {
            cell = [[DateAllLeftCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier: identifier];
        }
        DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:indexPath.row];
        bean.index = indexPath.row;
        [cell setDateAllLeftBean:bean delegate:self yueSel:@selector(c_yue:) userSel:@selector(c_left_user:)];
        return cell;
    }
    else{
        static NSString *identifier = @"DateAllRightTableIdentifier";
        DateAllRightCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                  identifier];
        
        if (cell == nil) {
            cell = [[DateAllRightCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier: identifier];
        }
        DateAllRightBean *bean = [_rightContentData objectAtIndex:indexPath.row];
        bean.index = indexPath.row;
        [cell setDateAllRightBean:bean delegate:self agreeSel:@selector(c_agree:) rejectSel:@selector(c_reject:) messageSel:@selector(c_message:) buySel:@selector(c_buy:) userSel:@selector(c_right_user:)];
        return cell;
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:indexPath.row];
        return bean.mine_flag.intValue != 1;
    }
    return NO;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        self.ignoreIndex = indexPath.row;
        DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:indexPath.row];
        [Static add:self.navigationController.view msg:@"正在忽略"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbi_id" value:bean.tbi_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getIgnoreInviteUrl] list:arr delegate:self tag:6];
        [_tableView setEditing:NO animated:YES];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"忽略";
}

#pragma mark - 跳到邀约详情
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        //推到详情页
        DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:indexPath.row];
        if(bean.mine_flag.intValue == 1){
            if(bean.tbi_status.intValue == 1){
            }
            else{
                if(!bean.shouldAlpha){
                    [self c_tool:_toolRightBtn];
                    return;
                }
                return;
            }
        }
        else{
            if([[[GB_JsonUtils getDictionaryByJsonString:_succeedStr] objectForKey:@"invite_success_flag"] intValue] == 1){
                if(bean.tbi_status.integerValue == 0){
                    return;
                }else{
                    NSString *str = [NSString stringWithFormat:@"%@%@/%@",[Url getInviteDetail],bean.tbi_uid,self.event_id];
                    [GB_NetWorkUtils startGetAsyncRequest:str delegate:self tag:9];
                    tvc = [[UIStoryboard storyboardWithName:@"my" bundle:nil]instantiateViewControllerWithIdentifier:@"TestViewController"];
                    tvc.bean = bean;
                    tvc.event_id = self.event_id;
                    [Static add:self.view msg:@"正在加载"];
                }
            }
            else{
                    NSString *str = [NSString stringWithFormat:@"%@%@/%@",[Url getInviteDetail],bean.tbi_uid,self.event_id];
                    [GB_NetWorkUtils startGetAsyncRequest:str delegate:self tag:9];
                    tvc = [[UIStoryboard storyboardWithName:@"my" bundle:nil]instantiateViewControllerWithIdentifier:@"TestViewController"];
                    tvc.bean = bean;
                    tvc.event_id = self.event_id;
                    [Static add:self.view msg:@"正在加载"];
            }
        }
        if(bean.target_id.intValue != [User getUserInfo].uid.intValue && bean.tbi_uid.intValue != [User getUserInfo].uid.intValue)return;
        DateAllSuccessViewController *con = [[DateAllSuccessViewController alloc]init];
        con.event_id = bean.tbi_event_id;
        con.target_uid = bean.target_id;
        con.target_nickname = bean.target_nickname;
        con.tbi_uid = bean.tbi_uid;
        con.tbi_nickname = bean.tu_nickname;
        con.viewController = self;
        [self.navigationController pushViewController:con animated:YES];
    }
     if(tableView.tag == 2){
         DateAllRightBean *bean = [_rightContentData objectAtIndex:indexPath.row];
         if(bean.tbia_status.intValue == 1){
             DateAllSuccessViewController *con = [[DateAllSuccessViewController alloc]init];
             con.event_id = bean.tbia_event_id;
             con.target_uid = bean.tbia_uid;
             con.target_nickname = bean.tu_nickname;
             con.viewController = self;
             [self.navigationController pushViewController:con animated:YES];
         }
     }
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    @synchronized(self){
        [Static remove:self.navigationController.view];
        if([Error verify:str view:self.navigationController.view]){
            self.isLoad = YES;
            self.succeedStr = str;
            if(tag == 1){
                if([[[GB_JsonUtils getDictionaryByJsonString:str] objectForKey:@"invite_success_flag"] intValue] == 1){
                    [self setBeanIcon:2];
                }
                [_leftContentData setArray:[DateAllLeftBean getBeanList:[[Static getRequestData:str] objectForKey:@"buy_invite_history"]]];
                               
                for (DateAllLeftBean *bean in _leftContentData) {
                    bean.shouldAlpha = [[[GB_JsonUtils getDictionaryByJsonString:str] objectForKey:@"invite_success_flag"] intValue] == 1 && bean.tbi_status.intValue != 1;
                }
                self.shouldAlbum = [[[Static getRequestData:str] objectForKey:@"album_flag"] intValue]==0;
                _tableView.tag = 1;
                DateAllLeftViewController *left = self.drawer.leftViewController;
                [left.paymentData removeAllObjects];
                [left.freetimeData removeAllObjects];
                [left.genderData removeAllObjects];
                for (DateAllLeftBean *bean in _leftContentData) {
                    NSString *payment = bean.tbi_payment;
                    payment = [GB_StringUtils isBlank:payment]?@"4":payment;
                    if(![left.paymentData containsObject:payment]){
                        [left.paymentData addObject:payment];
                    }
                    NSString *freetime = bean.tbi_free_time;
                    freetime = [GB_StringUtils isBlank:freetime]?@"1":freetime;
                    freetime = [freetime isEqualToString:@"all"]?@"1":freetime;
                    freetime = ![freetime isEqualToString:@"1"]&&![freetime isEqualToString:@"0"]?@"2":freetime;
                    if(![left.freetimeData containsObject:freetime]){
                        [left.freetimeData addObject:freetime];
                    }
                    if(freetime.intValue == 2){
                        if(![left.freetimeDateData containsObject:bean.tbi_free_time]){
                            [left.freetimeDateData addObject:bean.tbi_free_time];
                        }
                    }
                    
                    NSString *gender = bean.tu_gender;
                    gender = [GB_StringUtils isBlank:gender]?@"0":gender;
                    if(![left.genderData containsObject:gender]){
                        [left.genderData addObject:gender];
                    }
                }
//                if(left.paymentData.count>1){
                    [left.paymentData addObject:@"-1"];
//                }
//                if(left.freetimeData.count>1){
                    [left.freetimeData addObject:@"-1"];
//                }
//                if(left.genderData.count>1){
                    [left.genderData addObject:@"-1"];
//                }
                
                NSInteger (^ numberCompar)(NSString *a,NSString *str2) = ^NSInteger (NSString *str1, NSString *str2){
                    if(str1.intValue<str2.intValue){
                        return NSOrderedAscending;
                    }
                    if(str1.intValue>str2.intValue){
                        return NSOrderedDescending;
                    }
                    return NSOrderedSame;
                };
                
                [left.paymentData setArray:[left.paymentData sortedArrayUsingComparator:numberCompar]];
                [left.genderData setArray:[left.genderData sortedArrayUsingComparator:numberCompar]];
                [left.freetimeData setArray:[left.freetimeData sortedArrayUsingComparator:numberCompar]];
                [left.freetimeDateData setArray:[left.freetimeDateData sortedArrayUsingComparator:^NSInteger (NSString *str1 , NSString *str2){
                    int time1 = [GB_DateUtils get10MedianByFormatString:str1 format:@"yyyy-MM-dd"];
                    int time2 = [GB_DateUtils get10MedianByFormatString:str2 format:@"yyyy-MM-dd"];
                    if(time1<time2){
                        return NSOrderedAscending;
                    }
                    if(time1>time2){
                        return NSOrderedDescending;
                    }
                    return NSOrderedSame;
                }]];
                if([GB_ToolUtils isNotBlank:left.freetimeDateData] && [GB_StringUtils isBlank:_searchFreeDateTime]){
                    _searchFreeDateTime = [left.freetimeDateData objectAtIndex:0];
                }
                
                
                [left.picker reloadAllComponents];
                [left initFrame];
                
                if(_isJoin){
                    [_bottomBtn setTitle:[NSString stringWithFormat:@"%d位朋友正在 邀约中",_currentLeftContentData.count] forState:UIControlStateNormal];
                }
                [self search];
                if(_tableView.tag == 1){
                    _tableView.hidden = [GB_ToolUtils isBlank:_leftContentData];
                    _leftNoContent.hidden = !_tableView.hidden;
                    _rightNoContent.hidden = YES;
                }
                if(_tableView.tag == 2){
                    _tableView.hidden = [GB_ToolUtils isBlank:_rightContentData];
                    _rightNoContent.hidden = !_tableView.hidden;
                    _leftNoContent.hidden = YES;
                }
                return;
            }
            else if(tag == 2){
                [self.rightContentData setArray:[DateAllRightBean getBeanList:[[Static getRequestData:str] objectForKey:@"buy_invite_history"]]];
                _tableView.tag = 2;
                
                BOOL hasSuccess = NO;
                NSString *tbia_uid = nil;
                for (DateAllRightBean *bean in _rightContentData) {
                    if([bean.tbia_status intValue] == 1){
                        hasSuccess = YES;
                        tbia_uid = bean.tbia_uid;
                        break;
                    }
                }
                if(hasSuccess){
                    for (DateAllRightBean *bean in _rightContentData) {
                        bean.hasSuccess = [bean.tbia_status intValue] == 1;
                        bean.shouldAlpha = [bean.tbia_status intValue] != 1;
                    }
                    [self setBeanIcon:2];
                }
            }
            else if(tag == 3){
                //拒绝
                DateAllRightBean *bean = [_rightContentData objectAtIndex:_rejectIndex];
                bean.tbia_status = @"3";
                [_rightContentData replaceObjectAtIndex:_rejectIndex withObject:bean];
            }
            else if(tag == 4){
                //同意
                [self clearData];
                [self initData:YES];
                [self setBeanIcon:2];
            }
            else if(tag == 5){
                //约
                DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:_yueIndex];
                bean.invite_flag = @"1";
                [_currentLeftContentData replaceObjectAtIndex:_yueIndex withObject:bean];
                
                for (int i = 0; i<_leftContentData.count; i++) {
                    DateAllLeftBean *b = [_leftContentData objectAtIndex:i];
                    if(b.tbi_id.intValue == bean.tbi_id.intValue){
                        [_leftContentData replaceObjectAtIndex:i withObject:bean];
                    }
                }
            }
            else if(tag == 6){
                //忽略
                DateAllLeftBean *bean = [_currentLeftContentData objectAtIndex:_ignoreIndex];
                [_currentLeftContentData removeObjectAtIndex:_ignoreIndex];
                [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_ignoreIndex inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                if(_isJoin){
                    [_bottomBtn setTitle:[NSString stringWithFormat:@"%d位朋友正在 邀约中",_currentLeftContentData.count] forState:UIControlStateNormal];
                }
                for (int i = 0;i<_leftContentData.count;i++) {
                    DateAllLeftBean *b = [_leftContentData objectAtIndex:i];
                    if(b.tbi_id.intValue == bean.tbi_id.intValue){
                        [_leftContentData removeObjectAtIndex:i];
                        return;
                    }
                }
                
            }else if(tag == 9){
                NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
                tvc.dic = dic;
                 [self.navigationController pushViewController:tvc animated:YES];
            }
            [_tableView reloadData];
        }
    }
    if(_tableView.tag == 1){
        _tableView.hidden = [GB_ToolUtils isBlank:_leftContentData];
        _leftNoContent.hidden = !_tableView.hidden;
        _rightNoContent.hidden = YES;
    }
    if(_tableView.tag == 2){
        _tableView.hidden = [GB_ToolUtils isBlank:_rightContentData];
        _rightNoContent.hidden = !_tableView.hidden;
        _leftNoContent.hidden = YES;
    }
}

#pragma mark - Configuring the view’s layout behavior

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)drawerControllerWillOpen:(DateAllSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(DateAllSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(DateAllSwitchViewController *)drawerController
{
    
}

- (void)drawerControllerDidOpen:(DateAllSwitchViewController *)drawerController
{
    
}

@end

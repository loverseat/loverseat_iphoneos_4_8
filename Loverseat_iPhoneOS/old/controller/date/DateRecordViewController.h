//
//  DateRecordViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface DateRecordViewController : BaseViewController

@property (nonatomic, strong) NSString *uid;
@end

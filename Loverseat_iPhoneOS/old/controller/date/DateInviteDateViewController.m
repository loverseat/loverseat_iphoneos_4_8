//
//  DateInviteDateViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateInviteDateViewController.h"
#import "CalendarMonth.h"
#import "CalendarLogic.h"
#import "DateAllCenterViewController.h"

@interface DateInviteDateViewController ()
<CalendarLogicDelegate>
@property (nonatomic, strong) NSMutableArray *buttonContentData;
@property (nonatomic, assign) int clickValue;
@property (nonatomic, strong) CalendarMonth *calendarView;
@property (nonatomic, strong) CalendarLogic *calendarLogic;
@property (nonatomic, strong) NSDate *date;
@end

@implementation DateInviteDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.buttonContentData = [NSMutableArray array];
    self.clickValue = -1;
    [self initFrame];
}

-(void)initFrame{
    [NavUtils addBackground:self];
    [NavUtils addNavTitleView:self text:@"邀约时间" color:[UIColor whiteColor]];
    [NavUtils addBackWhiteButton:self];
    [NavUtils addRightButton:self title:@"确定" color:[UIColor whiteColor] sel:@selector(_submit)];
    [NavUtils addNavTitleLine:self];

    int width = [GB_DeviceUtils getScreenWidth]/2;
    for (int i = 0 ; i<([GB_ToolUtils isBlank:_validDateArr]?1:2); i++) {
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 75, 75) image:nil imageH:nil id:self sel:@selector(c_click:)];
        btn.center = CGPointMake(width*i+width*0.5f, [NavUtils getNavHeight]+65);
        btn.tag = i+([GB_ToolUtils isBlank:_validDateArr]?2:1);
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 37.5f;
        NSString *str;
        if(i == 0 && [GB_ToolUtils isNotBlank:_validDateArr]){
            str = @"某一天";
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_date_invite_time_n"] forState:UIControlStateNormal];
        }
        else{
            str = @"都可以";
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_date_invite_ok_n"] forState:UIControlStateNormal];
        }
        [self.view addSubview:btn];
        [_buttonContentData addObject:btn];
        
        UILabel *l = [GB_WidgetUtils getLabel:CGRectMake(i*width, btn.frame.origin.y+85, width, 20) title:str font:[UIFont systemFontOfSize:13] color:[UIColor whiteColor]];
        l.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:l];
    }
    if([GB_ToolUtils isNotBlank:_validDateArr]){
        self.date = [NSDate dateWithTimeIntervalSince1970:[GB_DateUtils get10MedianByFormatString:[_validDateArr objectAtIndex:0] format:@"yyyy-MM-dd"]];
        
        self.calendarLogic = [[CalendarLogic alloc] initWithDelegate:self referenceDate:_date];
        self.calendarView = [[CalendarMonth alloc] initWithFrame:CGRectMake(0, 190, 0, 0) logic:_calendarLogic validDateArr:_validDateArr];
        
        _calendarView.backgroundColor = [UIColor clearColor];
        _calendarView.hidden = YES;
        [self.view addSubview:_calendarView];
    }
}

-(void)c_click:(UIButton *)btn{
    for(UIButton *b in _buttonContentData) {
        if(b.tag == 1){
            [b setBackgroundImage:[UIImage imageNamed:btn==b?@"btn_date_invite_time_h":@"btn_date_invite_time_n"] forState:UIControlStateNormal];
        }
        if(b.tag == 2){
            [b setBackgroundImage:[UIImage imageNamed:btn==b?@"btn_date_invite_ok_h":@"btn_date_invite_ok_n"] forState:UIControlStateNormal];
        }
    }
    _clickValue = btn.tag;
    _calendarView.hidden = _clickValue != 1;
}

-(void)_submit{
    if(_clickValue == -1){
        [Static alert:self.navigationController.view msg:@"请选择日期"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在提交"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"payment" value:GB_NSStringFromInt(_payType)]];
        
        NSString *date = _clickValue==2?@"all":[GB_DateUtils getFormatStringBy10Median:@"yyyy-MM-dd" timeMillis:[_date timeIntervalSince1970]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"free_time" value:date]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getBuyInviteUrl] list:arr delegate:self tag:1];
    }
}

-(void)calendarLogic:(CalendarLogic *)aLogic dateSelected:(NSDate *)aDate{
    self.date = aDate;
    if ([_calendarLogic distanceOfDateFromCurrentMonth:aDate] == 0) {
        [_calendarView selectButtonForDate:aDate];
    }
}

-(void)calendarLogic:(CalendarLogic *)aLogic monthChangeDirection:(NSInteger)aDirection{
    [self.calendarView removeFromSuperview];
    self.calendarView = [[CalendarMonth alloc] initWithFrame:CGRectMake(0, 190, 0, 0) logic:_calendarLogic validDateArr:_validDateArr];
    _calendarView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_calendarView];
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[DateAllSwitchViewController class]]) {
                    DateAllSwitchViewController *con = (DateAllSwitchViewController *)controller;
                    [con.centerViewController setBeanIcon:1];
                    [self.navigationController popToViewController:con animated:YES];
                    [con.centerViewController initData:YES];
                }
            }
        }
    }
}

@end

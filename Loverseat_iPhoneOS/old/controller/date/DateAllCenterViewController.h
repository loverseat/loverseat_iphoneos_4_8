//
//  DateAllCenterViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "DateAllSwitchViewController.h"
@protocol ICSDateDrawerControllerChild;
@protocol ICSDateDrawerControllerPresenting;
@interface DateAllCenterViewController : BaseViewController<ICSDateDrawerControllerChild, ICSDateDrawerControllerPresenting>{

}
@property (nonatomic, strong) NSString *event_id;
@property (nonatomic, weak) DateAllSwitchViewController *drawer;
@property (nonatomic, strong) NSString *searchGender;
@property (nonatomic, strong) NSString *searchPayment;
@property (nonatomic, strong) NSString *searchFreeTime;
@property (nonatomic, strong) NSString *searchFreeDateTime;

@property (nonatomic, assign) BOOL isJoin;



-(void)setBeanIcon:(int)type;
-(void)c_drawer;
-(void)search;
-(void)initData:(BOOL)isForce;
-(void)clearData;
-(void)paySuccess;

@end

//
//  DateRecordCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateRecordCell.h"
#import "Static.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "User.h"

@interface DateRecordCell()
<GB_LoadImageDelegate>

@end

@implementation DateRecordCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *top = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 10)];
        top.backgroundColor = GB_UIColorFromRGB(245, 245, 245);
        [self addSubview:top];
        
        UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, [GB_DeviceUtils getScreenWidth], 110)];
        bg.backgroundColor = [UIColor whiteColor];
        [self addSubview:bg];
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 64, 94)];
        [bg addSubview:_icon];
        
        self.status = [GB_WidgetUtils getLabel:CGRectMake(0, 74, 64, 20) title:nil font:[Static getFont:24 isBold:NO] color:[UIColor whiteColor]];
        _status.textAlignment = NSTextAlignmentCenter;
        [_icon addSubview:_status];
        
        self.title = [GB_WidgetUtils getLabel:CGRectMake(15+64+15, 8, [GB_DeviceUtils getScreenWidth]-30-64-15, 40) title:nil font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(80, 80, 80)];
        _title.numberOfLines = 2;
        [bg addSubview:_title];
        
        
        self.time = [GB_WidgetUtils getLabel:CGRectMake(_title.frame.origin.x, 50, 230, 20) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(140, 140, 140)];
        [bg addSubview:_time];
        
        
        UIImageView *money = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_date_record_payment"]];
        money.center = CGPointMake(_title.frame.origin.x+11, 95);
        [self addSubview:money];
        
        
        UIImageView *date = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_date_record_date"]];
        date.center = CGPointMake(money.frame.origin.x+98, money.frame.origin.y+11);
        [self addSubview:date];
       
        
        self.money = [GB_WidgetUtils getLabel:CGRectMake(money.frame.origin.x+28, money.frame.origin.y, 70, 22) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(78, 78, 78)];
        [self addSubview:_money];
        
        
        self.date = [GB_WidgetUtils getLabel:CGRectMake(date.frame.origin.x+28, date.frame.origin.y, 70, 22) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(78, 78, 78)];
        [self addSubview:_date];
        
    }
    return self;
}

-(void)setDateRecordBean:(DateRecordBean *)bean{
    _title.text = bean.tad_show_name;
    switch ([bean.tbi_payment intValue]) {
        case 1:
            _money.text = @"我请客";
            break;
        case 2:
            _money.text = @"AA";
            break;
        case 3:
            _money.text = @"TA请客";
            break;
        case 4:
            _money.text = @"都可以";
            break;
        default:
            _money.text = @"－－";
            break;
    }

   
    _time.text = [NSString stringWithFormat:@"%@ - %@",[[bean.tad_begin_time substringToIndex:10] stringByReplacingOccurrencesOfString:@"-" withString:@"."],[[bean.tad_end_time substringToIndex:10] stringByReplacingOccurrencesOfString:@"-" withString:@"."]];
    if([GB_StringUtils isBlank:bean.tbi_free_time]||[bean.tbi_free_time isEqualToString:@"all"]){
        _date.text = @"都可以";
    }
    else{
        _date.text = bean.tbi_free_time;
    }
    
    [GB_NetWorkUtils loadImage:bean.tad_image container:_icon type:GB_ImageCacheTypeAll delegate:self tag:[bean.tbi_id intValue]];
    
    NSString *status;
    UIColor *color;
    if([bean.valid_flag intValue]==0){
        status = @"已结束";
        color = GB_UIColorFromRGB(80, 80, 80);
    }
    else{
        if([bean.tbi_status intValue] == 1){
            status = @"邀约成功";
            color = GB_UIColorFromRGB(251, 80, 87);
        }
        else if ([bean.tbi_status intValue] == 0){
            status = @"邀约中";
            color = GB_UIColorFromRGB(80, 177, 251);
        }else if([bean.tbi_status intValue] == 3){
            status = @"已结束";
            color = GB_UIColorFromRGB(80, 80, 80);
        }
    }
    _status.text = status;
    _status.backgroundColor = color;

}

#pragma mark GB_LoadImageDelegate

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    [GB_NetWorkUtils useImage:[Static getFImage:image w:_icon.frame.size.width h:_icon.frame.size.height] container:container];
}
@end

//
//  DateAllRightCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllRightCell.h"
#import "DateView.h"
#import <GeekBean4IOS/GB_WidgetUtils.h>
#import <GeekBean4IOS/GB_ToolUtils.h>
#import <GeekBean4IOS/GB_StringUtils.h>
#import <GeekBean4IOS/GB_MacroUtils.h>
#import <GeekBean4IOS/GB_DeviceUtils.h>
#import <GeekBean4IOS/GB_DateUtils.h>
#import "Static.h"
@interface DateAllRightCell()
@property (nonatomic, strong) DateView *icon;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *info;
@property (nonatomic, strong) UIImageView *sex;
@property (nonatomic, strong) UIImageView *success;
@property (nonatomic, strong) UILabel *message;
@property (nonatomic, strong) UIButton *agreeBtn;
@property (nonatomic, strong) UIButton *rejectBtn;
@property (nonatomic, strong) UIButton *didRejectBtn;
@property (nonatomic, strong) UIButton *msgBtn;
@property (nonatomic, strong) UIButton *buyBtn;
@end

@implementation DateAllRightCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.title = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[UIFont systemFontOfSize:15] color:[UIColor whiteColor]];
        [self addSubview:_title];
        
        self.success = [[UIImageView alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]-15-88, 15, 88, 16.5f)];
        _success.image = [UIImage imageNamed:@"icon_date_success_alert"];
        [self addSubview:_success];
        
        self.info = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[UIFont systemFontOfSize:11] color:[UIColor colorWithWhite:1 alpha:0.6f]];
        [self addSubview:_info];
        
        self.sex = [[UIImageView alloc]init];
        [self addSubview:_sex];

        
        self.message = [GB_WidgetUtils getLabel:CGRectMake(84, 68, [GB_DeviceUtils getScreenWidth]-84-15, 20) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
        [self addSubview:_message];
        
        
        self.agreeBtn = [GB_WidgetUtils getButton:CGRectMake(84, 104, 80, 24) image:nil imageH:nil id:nil sel:nil];
        [_agreeBtn setTitle:@"同意" forState:UIControlStateNormal];
        [_agreeBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _agreeBtn.layer.masksToBounds = YES;
        _agreeBtn.layer.cornerRadius = 5;
        _agreeBtn.layer.borderWidth = 1.0f;
        _agreeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _agreeBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_agreeBtn];
        
        self.rejectBtn = [GB_WidgetUtils getButton:CGRectMake(180, 104, 80, 24) image:nil imageH:nil id:nil sel:nil];
        [_rejectBtn setTitle:@"残忍拒绝" forState:UIControlStateNormal];
        [_rejectBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _rejectBtn.layer.masksToBounds = YES;
        _rejectBtn.layer.cornerRadius = 5;
        _rejectBtn.layer.borderWidth = 1.0f;
        _rejectBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _rejectBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_rejectBtn];
        
        self.msgBtn = [GB_WidgetUtils getButton:CGRectMake(84, 104, 80, 24) image:nil imageH:nil id:nil sel:nil];
        [_msgBtn setTitle:@"聊天" forState:UIControlStateNormal];
        [_msgBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _msgBtn.layer.masksToBounds = YES;
        _msgBtn.layer.cornerRadius = 5;
        _msgBtn.layer.borderWidth = 1.0f;
        _msgBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _msgBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_msgBtn];
        
        self.buyBtn = [GB_WidgetUtils getButton:CGRectMake(180, 104, 80, 24) image:nil imageH:nil id:nil sel:nil];
        [_buyBtn setTitle:@"立即购票" forState:UIControlStateNormal];
        [_buyBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _buyBtn.layer.masksToBounds = YES;
        _buyBtn.layer.cornerRadius = 5;
        _buyBtn.layer.borderWidth = 1.0f;
        _buyBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _buyBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_buyBtn];
        
        self.didRejectBtn = [GB_WidgetUtils getButton:CGRectMake(84, 104, 80, 24) image:nil imageH:nil id:nil sel:nil];
        [_didRejectBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
        [_didRejectBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _didRejectBtn.layer.masksToBounds = YES;
        _didRejectBtn.layer.cornerRadius = 5;
        _didRejectBtn.layer.borderWidth = 1.0f;
        _didRejectBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _didRejectBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_didRejectBtn];
        _didRejectBtn.alpha = 0.4f;
        

        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setDateAllRightBean:(DateAllRightBean *)bean delegate:(id)delegate agreeSel:(SEL)agreeSel rejectSel:(SEL)rejectSel messageSel:(SEL)messageSel buySel:(SEL)buySel userSel:(SEL)userSel{
    if(_icon)
        [_icon removeFromSuperview];
    
    int begin = [GB_DateUtils get10MedianByFormatString:bean.start_time format:@"yyyy-MM-dd HH:mm:ss"];
    int current = [GB_DateUtils get10MedianByFormatString:bean.current_time format:@"yyyy-MM-dd HH:mm:ss"];
    int end = [GB_DateUtils get10MedianByFormatString:bean.end_time
                                               format:@"yyyy-MM-dd HH:mm:ss"];
    float a = current-begin;
    float b = end-begin;
    float p = a/b;

    BOOL didReject = [bean.tbia_status intValue] == 3|| (current>end&&[bean.tbia_status intValue]==0);
    if(didReject){
        p = 1;
    }
    
    self.icon = [[DateView alloc]initWithAvatar:bean.tu_portrait percentage:p hasLock:NO hasModel:didReject frame:CGRectMake(15, 15, 54, 54) delegate:delegate action:userSel tag:bean.index];
    [self addSubview:_icon];
    float width = [GB_ToolUtils getTextWidth:bean.tu_nickname font:_title.font size:CGSizeMake(220, [GB_ToolUtils getFontHeight:_title.font])];
    
    _success.hidden = !bean.hasSuccess;
    _title.frame = CGRectMake(15+54+15, 15, width, 15.5f);
    _title.text = bean.tu_nickname;
    
    _sex.image = [UIImage imageNamed:[bean.tu_gender intValue] == 1?@"icon_female":@"icon_male"];
    _sex.frame = CGRectMake(_title.frame.size.width+_title.frame.origin.x+5, 15, 15.5f, 15.5f);
    _info.frame = CGRectMake(_title.frame.origin.x, 39, 220, 16);
    _info.text = [GB_StringUtils isBlank:bean.tu_star_sign]?[NSString stringWithFormat:@"%d岁",[Static getAgeByBirthday:bean.tu_birthday]]:[NSString stringWithFormat:@"%d岁 · %@",[Static getAgeByBirthday:bean.tu_birthday],bean.tu_star_sign];
    
    _message.text = bean.tbia_invite_message;
    
    [_agreeBtn addTarget:delegate action:agreeSel forControlEvents:UIControlEventTouchUpInside];
    [_rejectBtn addTarget:delegate action:rejectSel forControlEvents:UIControlEventTouchUpInside];
    [_msgBtn addTarget:delegate action:messageSel forControlEvents:UIControlEventTouchUpInside];
    [_buyBtn addTarget:delegate action:buySel forControlEvents:UIControlEventTouchUpInside];
    _agreeBtn.tag = bean.index;
    _rejectBtn.tag = bean.index;
    _msgBtn.tag = bean.index;
    _buyBtn.tag = bean.index;
    _didRejectBtn.tag = bean.index;
    
    BOOL shouldBuy = bean.buy_ticket_flag.intValue > 0;
    
    _agreeBtn.hidden = didReject||[bean.tbia_status intValue] == 1;
    _rejectBtn.hidden = didReject||[bean.tbia_status intValue] == 1;
    _didRejectBtn.hidden = !_agreeBtn.hidden||[bean.tbia_status intValue] == 1;
    _msgBtn.hidden = [bean.tbia_status intValue] != 1;
    _buyBtn.hidden = [bean.tbia_status intValue] != 1 || !shouldBuy;
    

    _icon.alpha = bean.shouldAlpha?0.4f:1.0f;
    _title.alpha = bean.shouldAlpha?0.4f:1.0f;
    _info.alpha = bean.shouldAlpha?0.4f:1.0f;
    _message.alpha = bean.shouldAlpha?0.4f:1.0f;
    _sex.alpha = bean.shouldAlpha?0.4f:1.0f;
    _success.alpha = bean.shouldAlpha?0.4f:1.0f;
    _agreeBtn.alpha = bean.shouldAlpha?0.4f:1.0f;
    _rejectBtn.alpha = bean.shouldAlpha?0.4f:1.0f;
    _didRejectBtn.alpha = bean.shouldAlpha?0.4f:1.0f;
}

@end

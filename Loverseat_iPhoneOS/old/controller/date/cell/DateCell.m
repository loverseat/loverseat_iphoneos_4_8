//
//  DateCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"
#import "User.h"

@interface DateCell()
<GB_LoadImageDelegate>

@end

@implementation DateCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *iconBg = [[UIView alloc]initWithFrame:CGRectMake(15, 11, 56, 56)];
        iconBg.layer.masksToBounds = YES;
        iconBg.layer.cornerRadius = 28;
        iconBg.backgroundColor = GB_UIColorFromRGB(67, 71, 71);
        self.icon = [[UIButton alloc]initWithFrame:CGRectMake(3, 3, 50, 50)];
        _icon.layer.masksToBounds = YES;
        _icon.layer.cornerRadius = 25;
        [iconBg addSubview:_icon];
        [self addSubview:iconBg];
       
        
        self.name = [GB_WidgetUtils getLabel:CGRectMake(82, 18, [GB_DeviceUtils getScreenWidth]-160, 17) title:nil font:[UIFont systemFontOfSize:16] color:[UIColor whiteColor]];
        [self addSubview:_name];
        
        
        self.status = [GB_WidgetUtils getLabel:CGRectMake(82, 45, [GB_DeviceUtils getScreenWidth]-160, 17) title:nil font:[UIFont systemFontOfSize:12] color:[UIColor colorWithWhite:1 alpha:0.6f]];
        [self addSubview:_status];
        
        self.yueBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-55, 26, 40, 25) image:nil imageH:nil id:nil sel:nil];
        [_yueBtn setTitle:nil forState:UIControlStateNormal];
        [_yueBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _yueBtn.layer.masksToBounds = YES;
        _yueBtn.layer.cornerRadius = 5;
        _yueBtn.layer.borderWidth = 1.0f;
        _yueBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _yueBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_yueBtn];
        
        self.yaoBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-75, 26, 60, 25) image:nil imageH:nil id:nil sel:nil];
        [_yaoBtn setTitle:nil forState:UIControlStateNormal];
        [_yaoBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _yaoBtn.layer.masksToBounds = YES;
        _yaoBtn.layer.cornerRadius = 5;
        _yaoBtn.layer.borderWidth = 1.0f;
        _yaoBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _yaoBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_yaoBtn];
    }
    return self;
}

-(void)setDateBean:(DateBean *)bean hasJoin:(BOOL)hasJoin{
    [GB_NetWorkUtils loadImage:bean.tu_portrait container:_icon type:GB_ImageCacheTypeAll delegate:self tag:[bean.tu_id intValue]];
    _name.text = bean.tu_nickname;
    if([User isMale]){
         _status.text = [GB_StringUtils isBlank:bean.tu_star_sign]?GB_NSStringFromInt([Static getAgeByBirthday:bean.tu_birthday]):[NSString stringWithFormat:@"%d岁 · %@",[Static getAgeByBirthday:bean.tu_birthday],bean.tu_star_sign];
    }
    else{
         _status.text = [GB_StringUtils isBlank:bean.tu_star_sign]?[NSString stringWithFormat:@"%d岁 · %@",[Static getAgeByBirthday:bean.tu_birthday],bean.payment]:[NSString stringWithFormat:@"%d岁 · %@ · %@",[Static getAgeByBirthday:bean.tu_birthday],bean.tu_star_sign,bean.payment];
    }
   
    
    _yueBtn.hidden = [User isMale];
    _yueBtn.enabled = [bean.invite_flag intValue]==0;
    _yueBtn.alpha = _yueBtn.enabled?1:0.2;
    [_yueBtn setTitle:_yueBtn.enabled?@"约他":@"已发送" forState:UIControlStateNormal];
    _yueBtn.frame = _yueBtn.enabled?CGRectMake([GB_DeviceUtils getScreenWidth]-55, 26, 40, 26):CGRectMake([GB_DeviceUtils getScreenWidth]-75, 26, 60, 25);
    _yaoBtn.hidden = ![User isMale]||!hasJoin;
    _yaoBtn.enabled = [bean.profile_flag intValue]==0;
    _yaoBtn.alpha = _yaoBtn.enabled?1:0.2;
    [_yaoBtn setTitle:_yaoBtn.enabled?@"邀请她":@"已邀请" forState:UIControlStateNormal];
}

#pragma mark GB_LoadImageDelegate

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    [GB_NetWorkUtils useImage:[Static getFImage:image w:50 h:50] container:container];
}

@end

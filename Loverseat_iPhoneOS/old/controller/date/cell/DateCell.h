//
//  DateCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateBean.h"

@interface DateCell : UITableViewCell

@property (nonatomic, strong) UIButton *icon;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *status;
@property (nonatomic, strong) UIButton *yueBtn;
@property (nonatomic, strong) UIButton *yaoBtn;

-(void)setDateBean:(DateBean *)bean hasJoin:(BOOL)hasJoin;

@end

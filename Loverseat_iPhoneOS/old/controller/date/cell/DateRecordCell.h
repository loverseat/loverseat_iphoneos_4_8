//
//  DateRecordCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateRecordBean.h"

@interface DateRecordCell : UITableViewCell
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *money;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) UILabel *time;

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *status;



-(void)setDateRecordBean:(DateRecordBean *)bean;
@end

//
//  DateAllRightCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateAllRightBean.h"

@interface DateAllRightCell : UITableViewCell
-(void)setDateAllRightBean:(DateAllRightBean *)bean delegate:(id)delegate agreeSel:(SEL)agreeSel rejectSel:(SEL)rejectSel messageSel:(SEL)messageSel buySel:(SEL)buySel userSel:(SEL)userSel;
@end

//
//  DateAllLeftCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllLeftCell.h"
#import "DateView.h"
#import <GeekBean4IOS/GB_WidgetUtils.h>
#import <GeekBean4IOS/GB_ToolUtils.h>
#import <GeekBean4IOS/GB_StringUtils.h>
#import <GeekBean4IOS/GB_MacroUtils.h>
#import <GeekBean4IOS/GB_DeviceUtils.h>
#import "Static.h"
#import "TestViewController.h"
@interface DateAllLeftCell()
@property (nonatomic, strong) DateView *icon;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *info;
@property (nonatomic, strong) UIImageView *sex;
@property (nonatomic, strong) UILabel *payment;
@property (nonatomic, strong) UILabel *freetime;
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) UIImageView *success;
@property (nonatomic, strong) UIImageView *reject;

@property (nonatomic, strong) UIImageView *iconDatePayment;
@property (nonatomic, strong) UIImageView *iconDateTime;
@end

@implementation DateAllLeftCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.title = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[UIFont systemFontOfSize:15] color:[UIColor whiteColor]];
        [self addSubview:_title];
        
        self.success = [[UIImageView alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]-15-88, 15, 88, 16.5f)];
        _success.image = [UIImage imageNamed:@"icon_date_success_alert"];
        [self addSubview:_success];
        
        self.reject = [[UIImageView alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]-15-88, 15, 88, 16.5f)];
        _reject.image = [UIImage imageNamed:@"icon_date_reject_alert"];
        [self addSubview:_reject];
        
        self.info = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[UIFont systemFontOfSize:11] color:[UIColor colorWithWhite:1 alpha:0.6f]];
        [self addSubview:_info];
        
        self.sex = [[UIImageView alloc]init];
        [self addSubview:_sex];
        
        self.iconDatePayment = [[UIImageView alloc]initWithFrame:CGRectMake(15+54+15, 68, 20, 20)];
        _iconDatePayment.image = [UIImage imageNamed:@"icon_date_payment"];
        [self addSubview:_iconDatePayment];
        
        self.payment = [GB_WidgetUtils getLabel:CGRectMake(_iconDatePayment.frame.origin.x+25, _iconDatePayment.frame.origin.y, 120, 20) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
        [self addSubview:_payment];
        
        self.iconDateTime = [[UIImageView alloc]initWithFrame:CGRectMake(160, 68, 20, 20)];
        _iconDateTime.image = [UIImage imageNamed:@"icon_date_time"];
        [self addSubview:_iconDateTime];
        
        self.freetime = [GB_WidgetUtils getLabel:CGRectMake(_iconDateTime.frame.origin.x+25, _iconDateTime.frame.origin.y, 120, 20) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
        [self addSubview:_freetime];
        
        
        self.btn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-60-15, 38, 60, 24) image:nil imageH:nil id:nil sel:nil];
        [_btn setTitle:nil forState:UIControlStateNormal];
        [_btn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
        _btn.layer.masksToBounds = YES;
        _btn.layer.cornerRadius = 5;
        _btn.layer.borderWidth = 1.0f;
        _btn.titleLabel.font = [UIFont systemFontOfSize:12];
        _btn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
        [self addSubview:_btn];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setDateAllLeftBean:(DateAllLeftBean *)bean delegate:(id)delegate yueSel:(SEL)yueSel userSel:(SEL)userSel{
    if(_icon)
       [_icon removeFromSuperview];
        self.icon = [[DateView alloc]initWithAvatar:bean.tu_portrait percentage:-1 hasLock:NO hasModel:[bean.invite_flag intValue]==3 frame:CGRectMake(15, 15, 54, 54) delegate:delegate action:userSel tag:bean.index];
        [self addSubview:_icon];
    float width = [GB_ToolUtils getTextWidth:bean.tu_nickname font:_title.font size:CGSizeMake(220, [GB_ToolUtils getFontHeight:_title.font])];
    _title.frame = CGRectMake(15+54+15, 15, width, 15.5f);
    _title.text = bean.tu_nickname;
    
    _sex.image = [UIImage imageNamed:[bean.tu_gender intValue] == 1?@"icon_female":@"icon_male"];
    _sex.frame = CGRectMake(_title.frame.size.width+_title.frame.origin.x+5, 15, 15.5f, 15.5f);
    _info.frame = CGRectMake(_title.frame.origin.x, 39, 220, 16);
    _info.text = [GB_StringUtils isBlank:bean.tu_star_sign]?[NSString stringWithFormat:@"%d岁",[Static getAgeByBirthday:bean.tu_birthday]]:[NSString stringWithFormat:@"%d岁 · %@",[Static getAgeByBirthday:bean.tu_birthday],bean.tu_star_sign];
    
    if([bean.tbi_payment isEqualToString:@"1"])
        _payment.text = @"我请客";
    else if([bean.tbi_payment isEqualToString:@"2"])
        _payment.text = @"AA";
    else if([bean.tbi_payment isEqualToString:@"3"])
        _payment.text = @"TA请客";
    else
        _payment.text = @"都可以";
    
    
    
    if([bean.tbi_free_time isEqualToString:@"all"]||[GB_StringUtils isBlank:bean.tbi_free_time])
        _freetime.text = @"都可以";
    else
        _freetime.text = bean.tbi_free_time;
    
    
    _btn.tag = bean.index;
    
    
    _icon.alpha = bean.shouldAlpha?0.4f:1.0f;
    _title.alpha = bean.shouldAlpha?0.4f:1.0f;
    _info.alpha = bean.shouldAlpha?0.4f:1.0f;
    _sex.alpha = bean.shouldAlpha?0.4f:1.0f;
    _payment.alpha = bean.shouldAlpha?0.4f:1.0f;
    _freetime.alpha = bean.shouldAlpha?0.4f:1.0f;
    _btn.alpha = bean.shouldAlpha?0.4f:1.0f;
    _iconDatePayment.alpha = bean.shouldAlpha?0.4f:1.0f;
    _iconDateTime.alpha = bean.shouldAlpha?0.4f:1.0f;

    
    
    
    
    _btn.hidden = NO;
    _success.hidden = NO;
    _reject.hidden = YES;
    
    //如果是我的
    if(bean.mine_flag.intValue == 1){
        if(bean.tbi_status.intValue == 0){
            _success.hidden = YES;
            //邀约中
            [_btn setTitle:@"邀约中" forState:UIControlStateNormal];
            
        }else if(bean.tbi_status.intValue == 1){
            //邀约成功
            _success.hidden = NO;
            [_btn setTitle:@"邀约成功" forState:UIControlStateNormal];
            _btn.hidden = YES;
        }
        _btn.alpha = 0.4f;
        //如果不是我的
    }else{
        if(bean.tbi_status.intValue == 1){
            //邀约成功
            _success.hidden = YES;
            _btn.hidden = NO;
            [_btn setTitle:@"邀约结束" forState:UIControlStateNormal];
            _btn.alpha = 0.4f;
        }else{
            if(bean.invite_flag.intValue ==0){
                _success.hidden = YES;
                [_btn setTitle:@"参与邀约" forState:UIControlStateNormal];
                if(!bean.shouldAlpha){
                    _btn.alpha = 1.0f;
                }
                [_btn addTarget:delegate action:yueSel forControlEvents:UIControlEventTouchUpInside];
                //我没约他（显示约他）
            }else if(bean.invite_flag.intValue == 1){
                //我约了，等待回音
                _success.hidden = YES;
                [_btn setTitle:@"等待回应" forState:UIControlStateNormal];
                _btn.alpha = 0.4f;
            }else if(bean.invite_flag.intValue == 3){
                //我约了，被拒绝
                _btn.hidden = YES;
                _success.hidden = YES;
                _reject.hidden = NO;
            }
        }
    }
}

@end

//
//  DateAllLeftCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateAllLeftBean.h"

@interface DateAllLeftCell : UITableViewCell
-(void)setDateAllLeftBean:(DateAllLeftBean *)bean delegate:(id)delegate yueSel:(SEL)yueSel userSel:(SEL)userSel;
@end

//
//  DateAllLeftViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "DateAllSwitchViewController.h"
@protocol ICSDateDrawerControllerChild;
@protocol ICSDateDrawerControllerPresenting;
@interface DateAllLeftViewController : BaseViewController<ICSDateDrawerControllerChild, ICSDateDrawerControllerPresenting>
@property(nonatomic, weak) DateAllSwitchViewController *drawer;
@property(nonatomic, strong)NSMutableArray *paymentData;
@property(nonatomic, strong)NSMutableArray *freetimeData;
@property(nonatomic, strong)NSMutableArray *freetimeDateData;
@property(nonatomic, strong)NSMutableArray *genderData;
@property (nonatomic, strong) UIPickerView *picker;
-(void)initFrame;
-(void)clearData;
@end

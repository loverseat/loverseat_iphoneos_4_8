//
//  DateAllSuccessViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllSuccessViewController.h"
#import "DateAllSuccessBean.h"
#import "DateAllCenterViewController.h"
#import "DateAllBuyViewController.h"
#import "UserInfoViewController.h"

@interface DateAllSuccessViewController()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *icon1;
@property (nonatomic, strong) UIButton *icon2;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UIButton *msgBtn;
@property (nonatomic, strong) UIButton *buyBtn;
@property (nonatomic, strong) UILabel *showName;
@property (nonatomic, strong) UILabel *venue;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *payment;
@property (nonatomic, strong) UILabel *freetime;
@property (nonatomic, assign) BOOL shouldReload;
@property (nonatomic, strong) DateAllSuccessBean *bean;
@end

@implementation DateAllSuccessViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    _tbi_uid=_tbi_uid?_tbi_uid:[User getUserInfo].uid;
    _tbi_nickname = _tbi_nickname?_tbi_nickname:[User getUserInfo].tu_nickname;

    [self initFrame];
    [self initData];
}

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"tbi_uid" value:_tbi_uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_event_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"target_id" value:_target_uid]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getSuccessInviteDetailUrl] list:arr delegate:self tag:1];
    }
}

-(void)initFrame{
    [NavUtils addBackground:self];
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 568);
    [self.view addSubview:_scrollView];
    [NavUtils addBackWhiteButton:self];
    
    UIImageView *success = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"date_success"]];
    success.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5, 49);
    [_scrollView addSubview:success];
    
    UIView *icon1Bg = [[UIView alloc]initWithFrame:CGRectMake(12, 84, 130, 130)];
    icon1Bg.backgroundColor = [UIColor colorWithWhite:1 alpha:0.25f];
    icon1Bg.layer.masksToBounds = YES;
    icon1Bg.layer.cornerRadius = 65;
    [_scrollView addSubview:icon1Bg];
    
    UIImageView *hert = [[UIImageView alloc]initWithFrame:CGRectMake(150, 157, 32.5f, 32.5f)];
    hert.image = [UIImage imageNamed:@"icon_peach_heart"];
    [_scrollView addSubview:hert];
    
    
    UIView *icon2Bg = [[UIView alloc]initWithFrame:CGRectMake(193, 136, 100, 100)];
    icon2Bg.backgroundColor = [UIColor colorWithWhite:1 alpha:0.25f];
    icon2Bg.layer.masksToBounds = YES;
    icon2Bg.layer.cornerRadius = 50;
    [_scrollView addSubview:icon2Bg];
    
    self.icon1 = [[UIButton alloc]initWithFrame:CGRectMake(5, 5, icon1Bg.frame.size.width-10, icon1Bg.frame.size.height-10)];
    _icon1.layer.masksToBounds = YES;
    _icon1.layer.cornerRadius = _icon1.frame.size.width*0.5f;
    [_icon1 addTarget:self action:@selector(c_icon1) forControlEvents:UIControlEventTouchUpInside];
    [icon1Bg addSubview:_icon1];
   
    self.icon2 = [[UIButton alloc]initWithFrame:CGRectMake(5, 5, icon2Bg.frame.size.width-10, icon2Bg.frame.size.height-10)];
    [_icon2 addTarget:self action:@selector(c_icon2) forControlEvents:UIControlEventTouchUpInside];
    _icon2.layer.masksToBounds = YES;

    _icon2.layer.cornerRadius = _icon2.frame.size.width*0.5f;
    [icon2Bg addSubview:_icon2];
    
    self.name = [GB_WidgetUtils getLabel:CGRectMake(0, 245, [GB_DeviceUtils getScreenWidth], 25) title:nil font:[UIFont boldSystemFontOfSize:20] color:[UIColor whiteColor]];
    _name.textAlignment = NSTextAlignmentCenter;
    [_scrollView addSubview:_name];
    _name.hidden = YES;
    
    
    self.msgBtn = [GB_WidgetUtils getButton:CGRectMake(58, 285, 95, 30) image:nil imageH:nil id:self sel:@selector(c_msg)];
    [_msgBtn setTitle:@"聊天" forState:UIControlStateNormal];
    [_msgBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
    _msgBtn.layer.masksToBounds = YES;
    _msgBtn.layer.cornerRadius = 5;
    _msgBtn.layer.borderWidth = 1.0f;
    _msgBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    _msgBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
    [_scrollView addSubview:_msgBtn];
    
    self.buyBtn = [GB_WidgetUtils getButton:CGRectMake(165, 285, 95, 30) image:nil imageH:nil id:self sel:@selector(c_buy)];
    [_buyBtn setTitle:@"立即购票" forState:UIControlStateNormal];
    [_buyBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.6f] forState:UIControlStateNormal];
    _buyBtn.layer.masksToBounds = YES;
    _buyBtn.layer.cornerRadius = 5;
    _buyBtn.layer.borderWidth = 1.0f;
    _buyBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    _buyBtn.layer.borderColor = [UIColor colorWithWhite:1 alpha:0.6f].CGColor;
    [_scrollView addSubview:_buyBtn];
    
    UIImageView *ticketBg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 354, [GB_DeviceUtils getScreenWidth], 120)];
    ticketBg.image = [UIImage imageNamed:@"bg_date_success_ticket"];
    [_scrollView addSubview:ticketBg];
    
    
    self.showName = [GB_WidgetUtils getLabel:CGRectMake(20, 25, [GB_DeviceUtils getScreenWidth]-40, 25) title:nil font:[UIFont boldSystemFontOfSize:20] color:[UIColor whiteColor]];
     _showName.textAlignment = NSTextAlignmentCenter;
    [ticketBg addSubview:_showName];
    
    self.venue = [GB_WidgetUtils getLabel:CGRectMake(20, 57, [GB_DeviceUtils getScreenWidth]-40, 20) title:nil font:[UIFont systemFontOfSize:12] color:[UIColor whiteColor]];
    _venue.textAlignment = NSTextAlignmentCenter;
    [ticketBg addSubview:_venue];
    
    self.time = [GB_WidgetUtils getLabel:CGRectMake(20, 80, [GB_DeviceUtils getScreenWidth]-40, 20) title:nil font:[UIFont systemFontOfSize:12] color:[UIColor whiteColor]];
    _time.textAlignment = NSTextAlignmentCenter;
    [ticketBg addSubview:_time];
    
    
    UIImageView *iconDatePayment = [[UIImageView alloc]initWithFrame:CGRectMake(50, 500, 30, 30)];
    iconDatePayment.image = [UIImage imageNamed:@"icon_date_payment"];
    [_scrollView addSubview:iconDatePayment];
    
    self.payment = [GB_WidgetUtils getLabel:CGRectMake(95, iconDatePayment.frame.origin.y, 110, 30) title:nil font:[UIFont systemFontOfSize:12] color:[UIColor whiteColor]];
    [_scrollView addSubview:_payment];
    
    UIImageView *iconDateTime = [[UIImageView alloc]initWithFrame:CGRectMake(170, 500, 30, 30)];
    iconDateTime.image = [UIImage imageNamed:@"icon_date_time"];
    [_scrollView addSubview:iconDateTime];
    
    self.freetime = [GB_WidgetUtils getLabel:CGRectMake(215, 500, 110, 30) title:nil font:[UIFont systemFontOfSize:12] color:[UIColor whiteColor]];
    [_scrollView addSubview:_freetime];
}

-(void)c_icon1{
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = _bean.target_uid;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_icon2{
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = _bean.user_uid;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)paySuccess{
    [self initData];
    self.shouldReload = YES;
}

-(void)cb_back{
    [super cb_back];
    if(_shouldReload && _viewController){
        [_viewController paySuccess];
    }
}

-(void)c_msg{
#warning 跳转到聊天
}

-(void)c_buy{
    DateAllBuyViewController *buy = [[DateAllBuyViewController alloc]init];
    buy.url = _bean.buy_ticket_url;
    [self.navigationController pushViewController:buy animated:YES];
//    Buy1ViewController *buy = [[Buy1ViewController alloc]init];
//    buy.eventId = _event_id;
//    buy.viewController = self;
//    [self.navigationController pushViewController:buy animated:YES];
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.navigationController.view]){
        self.bean = [DateAllSuccessBean getBean:[Static getRequestData:str]];
        [GB_NetWorkUtils loadImage:[Url getImageUrl:_bean.target_portrait] container:_icon1 type:GB_ImageCacheTypeAll];
        [GB_NetWorkUtils loadImage:[Url getImageUrl:_bean.user_portrait] container:_icon2 type:GB_ImageCacheTypeAll];
        _name.text = [NSString stringWithFormat:@"%@&%@",_bean.target_nickname,_bean.user_nickname];
        _showName.text = _bean.show_name;
        _venue.text = _bean.venue_name;
        _time.text = _bean.time;
        
        if([_bean.tbi_payment isEqualToString:@"1"])
            _payment.text = @"我请客";
        else if([_bean.tbi_payment isEqualToString:@"2"])
            _payment.text = @"AA";
        else if([_bean.tbi_payment isEqualToString:@"3"])
            _payment.text = @"TA请客";
        else
            _payment.text = @"都可以";
        
        
        
        if([_bean.tbi_free_time isEqualToString:@"all"]||[GB_StringUtils isBlank:_bean.tbi_free_time])
            _freetime.text = @"都可以";
        else
            _freetime.text = _bean.tbi_free_time;
        
        self.buyBtn.hidden = _bean.buy_ticket_flag.intValue <= 0;
        if(_buyBtn.hidden){
            _msgBtn.frame = CGRectMake(([GB_DeviceUtils getScreenWidth]-95)*0.5, 285, 95, 30);
        }
        else{
            _msgBtn.frame = CGRectMake(58, 285, 95, 30);
        }
    }
}



@end

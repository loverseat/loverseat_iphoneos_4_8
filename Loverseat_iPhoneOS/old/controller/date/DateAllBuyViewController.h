//
//  DateAllBuyViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/24.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface DateAllBuyViewController : BaseViewController

@property (nonatomic, strong) NSString *url;

@end

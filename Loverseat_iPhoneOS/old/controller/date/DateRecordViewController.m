//
//  DateRecordViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//  邀约记录

#import "DateRecordViewController.h"
#import "DateRecordBean.h"
#import "DateRecordCell.h"
#import "RecordRightCell.h"
#import "DateAllSuccessViewController.h"
#import "EventViewController.h"
#import "TestViewController.h"
#import "DateAllCenterViewController.h"
#import "DateAllSwitchViewController.h"
#import "DateAllLeftViewController.h"
#import "EventListBean.h"


@interface DateRecordViewController ()<UITableViewDataSource,UITableViewDelegate>


@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *contentData;
@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) NSMutableArray *rightContentData;
@property (nonatomic, assign) EventListBean *bean;
@property (nonatomic, strong) UIImageView *recordImg;
@end

@implementation DateRecordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contentData = [NSMutableArray array];
    self.rightContentData = [NSMutableArray array];
    [self initFrame];
    [self initData];
}

-(void)initFrame{
    self.view.backgroundColor = [UIColor grayColor];
    [NavUtils addNavBgView:self];
    [NavUtils addBackButton:self];
    float width = 65.5f;
    float height = 26.5f;
    self.leftBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5-width, [NavUtils getNavHeight]*0.5-height*0.5, width, height) image:nil imageH:nil id:self sel:@selector(btnClick:)];
    self.leftBtn.tag = 1;
    [self.view addSubview:self.leftBtn];
    
    self.rightBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5, [NavUtils getNavHeight]*0.5-height*0.5, width, height) image:nil imageH:nil id:self sel:@selector(btnClick:)];
    self.rightBtn.tag = 2;
    [self.view addSubview:self.rightBtn];
    
    
#warning 6、邀约记录 发起和参与为空时，文字有问题：文字改为：记录为空
    _recordImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"btn_record"]];
    _recordImg.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5,([GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])/2);
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.14f];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = GB_UIColorFromRGB(245, 245, 245);
    [self.view addSubview:_tableView];
    [self btnClick:self.leftBtn];
}

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:_uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"yaoyue"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getInviteHistoryUrl] list:arr delegate:self tag:1];
        }

}

- (void)btnClick:(UIButton *)btn{
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    if(btn.tag == 1){
        [self.leftBtn setImage:[UIImage imageNamed:@"btn_invite_left_h"] forState:UIControlStateNormal];
        [self.rightBtn setImage:[UIImage imageNamed:@"btn_invite_right_n"] forState:UIControlStateNormal];
        [self initData];
    }else{
        [self.leftBtn setImage:[UIImage imageNamed:@"btn_invite_left_n"] forState:UIControlStateNormal];
        [self.rightBtn setImage:[UIImage imageNamed:@"btn_invite_right_h"] forState:UIControlStateNormal];
        if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
            [Static add:self.navigationController.view msg:@"正在加载"];
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:_uid]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"fuyue"]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getInviteHistoryUrl] list:arr delegate:self tag:2];
        }
        
    }
}
#define mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableView.tag == 1?_contentData.count:_rightContentData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        static NSString *DateRecordTableIdentifier = @"DateRecordTableIdentifier";
    
        DateRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             DateRecordTableIdentifier];
    
    if (cell == nil) {
        cell = [[DateRecordCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier: DateRecordTableIdentifier];
    }
        DateRecordBean *bean = [_contentData objectAtIndex:indexPath.row];
        [cell setDateRecordBean:bean];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
        static NSString *DateRecordTableIdentifier1 = @"DateRecordTableIdentifier1";
        RecordRightCell *cell = [tableView dequeueReusableCellWithIdentifier:DateRecordTableIdentifier1];
        if(cell == nil){
            cell = [[RecordRightCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DateRecordTableIdentifier1];
        }
            DateRecordBean *bean = [_rightContentData objectAtIndex:indexPath.row];
            [cell setDateRecordBean:bean];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
    DateRecordBean *bean = [_contentData objectAtIndex:indexPath.row];
        if([bean.valid_flag intValue]==0){
            //此剧已下架
            
#warning 4.BUG结束的演出页面
            
//            EventViewController *event = [[EventViewController alloc]init];
//            event.event_id = bean.tbi_event_id;
            
            
//            LZFinishViewController *finishVc = [[LZFinishViewController alloc] init];
//            finishVc.event_id = bean.tbi_event_id;
//            [self.navigationController pushViewController:finishVc animated:YES];
            
#warning 该演出已经下架-----------
            
            //[MBProgressHUD showError:@"该演出已经下架"];
            
            
        }else{
        if(bean.tbi_status.intValue == 1){
#pragma mark - 邀约成功
            //邀约成功
            DateAllSuccessViewController *con = [[DateAllSuccessViewController alloc]init];
            con.event_id = bean.tbi_event_id;
            con.target_uid = bean.target_id;
            con.target_nickname = bean.tu_nickname;
            [self.navigationController pushViewController:con animated:YES];
        }else if (bean.tbi_status.intValue == 0||bean.tbi_status.intValue == 3){
            //邀约中
            DateAllCenterViewController *center = [[DateAllCenterViewController alloc]init];
            center.event_id = bean.tad_id;
            DateAllLeftViewController *left = [[DateAllLeftViewController alloc]init];
            DateAllSwitchViewController *con = [[DateAllSwitchViewController alloc]initWithLeftViewController:left centerViewController:center];
            [self.navigationController pushViewController:con animated:YES];
            }
        }
    }else{
        DateRecordBean *bean = [_rightContentData objectAtIndex:indexPath.row];
        if([bean.valid_flag intValue] == 0){
            EventViewController *event = [[EventViewController alloc]init];
            event.event_id = bean.tad_id;
            [self.navigationController pushViewController:event animated:YES];
        }else{
        if(bean.tbi_status.intValue == 1){
            DateAllSuccessViewController *con = [[DateAllSuccessViewController alloc]init];
            con.event_id = bean.tad_id;
            con.target_uid = bean.tbi_uid;
            con.target_nickname = bean.user_name;
            [self.navigationController pushViewController:con animated:YES];
        }else if(bean.tbi_status.intValue == 0||bean.tbi_status.intValue == 3){
            //邀约中
            DateAllCenterViewController *center = [[DateAllCenterViewController alloc]init];
            center.event_id = bean.tad_id;
            DateAllLeftViewController *left = [[DateAllLeftViewController alloc]init];
            DateAllSwitchViewController *con = [[DateAllSwitchViewController alloc]initWithLeftViewController:left centerViewController:center];
            [self.navigationController pushViewController:con animated:YES];
            }
        }
    }
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    self.isLoad = YES;
    //if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            _tableView.tag = 1;
            [self.contentData setArray:[DateRecordBean getBeanList:[Static getRequestData:str]]];
            if(self.contentData.count > 0){
                [_recordImg removeFromSuperview];
            }else{
                [self.view addSubview:_recordImg];
            }
        }
    else if (tag == 2){
        _tableView.tag = 2;
        [self.rightContentData setArray:[DateRecordBean getBeanList:[Static getRequestData:str]]];
        if(self.rightContentData.count >0){
            [_recordImg removeFromSuperview];
        }else{
            [self.view addSubview:_recordImg];
        }
    }
    [_tableView reloadData];
}

@end

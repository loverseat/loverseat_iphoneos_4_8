//
//  DateAllSuccessViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@class DateAllCenterViewController;
@interface DateAllSuccessViewController : BaseViewController

@property (nonatomic, strong) NSString *event_id;
@property (nonatomic, strong) NSString *tbi_uid;
@property (nonatomic, strong) NSString *tbi_nickname;
@property (nonatomic, strong) NSString *target_uid;
@property (nonatomic, strong) NSString *target_nickname;
@property (nonatomic, strong) DateAllCenterViewController *viewController;
-(void)paySuccess;
@end

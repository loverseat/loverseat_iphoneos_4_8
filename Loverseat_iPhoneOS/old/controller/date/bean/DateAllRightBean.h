//
//  DateAllRightBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateAllRightBean : NSObject

@property (nonatomic, strong) NSString *both_all_ticket;
@property (nonatomic, strong) NSString *buy_ticket_flag;
@property (nonatomic, strong) NSString *ticket_count;

@property (nonatomic, strong) NSString *current_time;
@property (nonatomic, strong) NSString *end_time;
@property (nonatomic, strong) NSString *start_time;

@property (nonatomic, strong) NSString *tbia_tbi_uid;
@property (nonatomic, strong) NSString *tbia_event_id;
@property (nonatomic, strong) NSString *tbia_uid;
@property (nonatomic, strong) NSString *tbia_id;
@property (nonatomic, strong) NSString *tbia_invite_message;
@property (nonatomic, strong) NSString *tbia_status;
@property (nonatomic, strong) NSString *tbia_tbi_id;
@property (nonatomic, strong) NSString *buy_ticket_url;

@property (nonatomic, strong) NSString *tu_star_sign;
@property (nonatomic, strong) NSString *tu_nickname;
@property (nonatomic, strong) NSString *tu_gender;
@property (nonatomic, strong) NSString *tu_portrait;
@property (nonatomic, strong) NSString *tu_birthday;
@property (nonatomic, assign) BOOL hasSuccess;
@property (nonatomic, assign) BOOL shouldAlpha;

@property (nonatomic, assign) int index;

+(NSArray *)getBeanList:(NSArray *)arr;

+(DateAllRightBean *)getBean:(NSDictionary *)dic;

@end

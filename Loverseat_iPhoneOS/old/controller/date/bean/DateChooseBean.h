//
//  DateChooseBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-11.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateChooseBean : NSObject
@property (nonatomic, strong) NSString *tu_nickname;
@property (nonatomic, strong) NSString *tu_id;
@property (nonatomic, strong) NSString *tu_portrait;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *current_time;
@property (nonatomic, strong) NSString *invite_time;
@property (nonatomic, strong) NSString *end_time;
@property (nonatomic, strong) NSString *buy_flag;

@property (nonatomic, strong) NSString *tu_birthday;
@property (nonatomic, strong) NSString *tu_gender;
@property (nonatomic, strong) NSString *tu_signature;
@property (nonatomic, strong) NSString *tu_star_sign;

+(NSArray *)getBeanList:(NSArray *)arr;
+(DateChooseBean *)getBean:(NSDictionary *)dic;
+(DateChooseBean *)copyBean:(DateChooseBean *)bean;
@end

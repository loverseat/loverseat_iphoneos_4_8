//
//  DateBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateBean : NSObject

@property (nonatomic, strong) NSString *invite_flag;
@property (nonatomic, strong) NSString *tu_birthday;
@property (nonatomic, strong) NSString *tu_id;
@property (nonatomic, strong) NSString *tu_nickname;
@property (nonatomic, strong) NSString *tu_portrait;
@property (nonatomic, strong) NSString *tu_star_sign;
@property (nonatomic, strong) NSString *profile_flag;
@property (nonatomic, strong) NSString *payment;
+(NSArray *)getBeanList:(NSArray *)arr;
+(DateBean *)getBean:(NSDictionary *)dic;
@end

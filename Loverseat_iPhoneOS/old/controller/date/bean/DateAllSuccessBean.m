//
//  DateAllSuccessBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllSuccessBean.h"

@implementation DateAllSuccessBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]])
        return resultList;
    for (NSDictionary *dic in arr) {
        DateAllSuccessBean *bean = [DateAllSuccessBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DateAllSuccessBean *)getBean:(NSDictionary *)dic{
    DateAllSuccessBean *bean = [[DateAllSuccessBean alloc]init];
    bean.tbi_payment = [dic objectForKey:@"tbi_payment"];
    bean.tbi_free_time = [dic objectForKey:@"tbi_free_time"];
    bean.show_name = [dic objectForKey:@"show_name"];
    bean.venue_name = [dic objectForKey:@"venue_name"];
    bean.user_nickname = [dic objectForKey:@"user_nickname"];
    bean.user_portrait = [dic objectForKey:@"user_portrait"];
     bean.target_uid = [dic objectForKey:@"target_uid"];
    bean.user_uid = [dic objectForKey:@"user_uid"];
    bean.target_nickname = [dic objectForKey:@"target_nickname"];
    bean.target_portrait = [dic objectForKey:@"target_portrait"];
    bean.buy_ticket_flag = [dic objectForKey:@"buy_ticket_flag"];
    bean.buy_ticket_url = [dic objectForKey:@"buy_ticket_url"];
    bean.time = [NSString stringWithFormat:@"%@ - %@",[dic objectForKey:@"begin_time"],[dic objectForKey:@"end_time"]];
    
    return bean;
}

@end

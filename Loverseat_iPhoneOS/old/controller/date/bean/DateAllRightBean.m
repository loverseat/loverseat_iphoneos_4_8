//
//  DateAllRightBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllRightBean.h"

@implementation DateAllRightBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]])
        return resultList;
    for (NSDictionary *dic in arr) {
        DateAllRightBean *bean = [DateAllRightBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DateAllRightBean *)getBean:(NSDictionary *)dic{
    DateAllRightBean *bean = [[DateAllRightBean alloc]init];
    bean.current_time = [dic objectForKey:@"current_time"];
    bean.end_time = [dic objectForKey:@"end_time"];
    bean.start_time = [dic objectForKey:@"tbia_created"];
    bean.tbia_event_id = [dic objectForKey:@"tbia_event_id"];
    bean.tbia_id = [dic objectForKey:@"tbia_id"];
    bean.tbia_invite_message = [dic objectForKey:@"tbia_invite_message"];
    bean.tbia_status = [dic objectForKey:@"tbia_status"];
    bean.tbia_tbi_id = [dic objectForKey:@"tbia_tbi_id"];
    bean.tbia_tbi_uid = [dic objectForKey:@"tbia_tbi_uid"];
    bean.tbia_uid = [dic objectForKey:@"tbia_uid"];
    bean.buy_ticket_url = [dic objectForKey:@"buy_ticket_url"];

    bean.both_all_ticket = [dic objectForKey:@"both_all_ticket"];
    bean.buy_ticket_flag = [dic objectForKey:@"buy_ticket_flag"];
    bean.ticket_count = [dic objectForKey:@"ticket_count"];
    
    
    bean.tu_star_sign = [[dic objectForKey:@"users"] objectForKey:@"tu_star_sign"];
    bean.tu_nickname = [[dic objectForKey:@"users"] objectForKey:@"tu_nickname"];
    bean.tu_gender = [[dic objectForKey:@"users"] objectForKey:@"tu_gender"];
    bean.tu_portrait = [[dic objectForKey:@"users"] objectForKey:@"tu_portrait"];
    bean.tu_birthday = [[dic objectForKey:@"users"] objectForKey:@"tu_birthday"];
    return bean;
}
@end

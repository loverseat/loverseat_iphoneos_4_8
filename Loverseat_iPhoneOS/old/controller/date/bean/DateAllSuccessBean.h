//
//  DateAllSuccessBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateAllSuccessBean : NSObject
@property (nonatomic, strong) NSString *tbi_payment;
@property (nonatomic, strong) NSString *tbi_free_time;
@property (nonatomic, strong) NSString *show_name;
@property (nonatomic, strong) NSString *venue_name;
@property (nonatomic, strong) NSString *user_nickname;
@property (nonatomic, strong) NSString *user_portrait;
@property (nonatomic, strong) NSString *target_nickname;
@property (nonatomic, strong) NSString *target_portrait;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *buy_ticket_url;
@property (nonatomic, strong) NSString *buy_ticket_flag;

@property (nonatomic, strong) NSString *target_uid;
@property (nonatomic, strong) NSString *user_uid;


+(NSArray *)getBeanList:(NSArray *)arr;
+(DateAllSuccessBean *)getBean:(NSDictionary *)dic;
@end

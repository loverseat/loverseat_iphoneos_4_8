//
//  DateAllLeftBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DateAllLeftBean : NSObject
@property (nonatomic, strong) NSString *tbi_uid;//所点击的用户的id
@property (nonatomic, strong) NSString *tbi_event_id;
@property (nonatomic, strong) NSString *tbi_payment;//吃饭类型
@property (nonatomic, strong) NSString *tbi_free_time;//邀约时间
@property (nonatomic, strong) NSString *tbi_id;//邀约记录的id;
@property (nonatomic, strong) NSString *target_id;
@property (nonatomic, strong) NSString *target_nickname;

@property (nonatomic, strong) NSString *tu_star_sign;//星座
@property (nonatomic, strong) NSString *tu_nickname;//名字
@property (nonatomic, strong) NSString *tu_gender;//性别
@property (nonatomic, strong) NSString *tu_portrait;//头像
@property (nonatomic, strong) NSString *tu_birthday;//年龄
@property (nonatomic, strong) NSString *tbi_status;

@property (nonatomic, assign) BOOL shouldAlpha;


@property (nonatomic, strong) NSString *mine_flag;//1、我的 0、不是我的
@property (nonatomic, strong) NSString *invite_flag;

@property (nonatomic, assign) int index;

+(NSArray *)getBeanList:(NSArray *)arr;

+(DateAllLeftBean *)getBean:(NSDictionary *)dic;
@end

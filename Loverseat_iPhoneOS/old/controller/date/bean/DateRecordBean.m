//
//  DateRecordBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateRecordBean.h"
#import <GeekBean4IOS/GB_ToolUtils.h>
@implementation DateRecordBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]])
        return resultList;
    for (NSDictionary *dic in arr) {
        DateRecordBean *bean = [DateRecordBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DateRecordBean *)getBean:(NSDictionary *)dic{
    DateRecordBean *bean = [[DateRecordBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tad_begin_time = [dic objectForKey:@"begin_time"];
        bean.tad_end_time = [dic objectForKey:@"end_time"];
        bean.target_id = [dic objectForKey:@"target_id"];
        bean.tu_nickname = [dic objectForKey:@"target_nickname"];
        bean.tad_image = [dic objectForKey:@"tad_image"];
        bean.tad_show_name = [dic objectForKey:@"show_name"];
        bean.tbi_city = [dic objectForKey:@"tbi_city"];
        bean.tbi_created = [dic objectForKey:@"tbi_created"];
        bean.tbi_event_id = [dic objectForKey:@"tbi_event_id"];
        bean.venue_name = [dic objectForKey:@"venue_name"];
        bean.tbi_free_time = [dic objectForKey:@"free_time"];
        bean.tbi_gender = [dic objectForKey:@"tbi_gender"];
        bean.tbi_id = [dic objectForKey:@"tbi_id"];
        bean.tad_id = [dic objectForKey:@"tad_id"];
        
        bean.tbi_payment = [dic objectForKey:@"payment"];
        bean.tbi_portrait = [dic objectForKey:@"tbi_portrait"];
        bean.tbi_status = [dic objectForKey:@"tbi_status"];
        bean.tbi_target_id = [dic objectForKey:@"tbi_target_id"];
        
        bean.tbi_type = [dic objectForKey:@"tbi_type"];
        bean.tbi_uid = [dic objectForKey:@"tbi_uid"];
        bean.ticket_count = [dic objectForKey:@"ticket_count"];
        bean.tu_birthday = [dic objectForKey:@"tu_birthday"];
       
        bean.tu_occupation = [dic objectForKey:@"tu_occupation"];
        bean.tu_signature = [dic objectForKey:@"tu_signature"];
        bean.valid_flag = [dic objectForKey:@"valid_flag"];
        
        bean.user_name = [dic objectForKey:@"user_name"];
        bean.gender = [[dic objectForKey:@"gender"]intValue];
        bean.portrait = [dic objectForKey:@"portrait"];

    }
    return bean;
}
@end

//
//  DateAllLeftBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/11/12.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateAllLeftBean.h"

@implementation DateAllLeftBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]])
        return resultList;
    for (NSDictionary *dic in arr) {
        DateAllLeftBean *bean = [DateAllLeftBean getBean:dic];
        [resultList addObject:bean];
    }
    
    NSArray *sortedArray = [resultList sortedArrayUsingComparator:^NSComparisonResult(DateAllLeftBean *bean1, DateAllLeftBean *bean2){
        if(bean1.mine_flag.intValue < bean2.mine_flag.intValue){
            return NSOrderedDescending;
        }
        if(bean1.mine_flag.intValue > bean2.mine_flag.intValue){
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
    return sortedArray;
}

+(DateAllLeftBean *)getBean:(NSDictionary *)dic{
    DateAllLeftBean *bean = [[DateAllLeftBean alloc]init];
    bean.tbi_uid = [dic objectForKey:@"tbi_uid"];
    bean.target_id = [dic objectForKey:@"target_id"];
    bean.target_nickname = [dic objectForKey:@"target_nickname"];
    bean.tbi_event_id = [dic objectForKey:@"tbi_event_id"];
    bean.tbi_free_time = [dic objectForKey:@"tbi_free_time"];
    bean.tbi_payment = [dic objectForKey:@"tbi_payment"];
    bean.tbi_id = [dic objectForKey:@"tbi_id"];
    bean.mine_flag = [dic objectForKey:@"mine_flag"];
    bean.tbi_status = [dic objectForKey:@"tbi_status"];
    
    bean.tu_star_sign = [[dic objectForKey:@"users"] objectForKey:@"tu_star_sign"];
    bean.tu_nickname = [[dic objectForKey:@"users"] objectForKey:@"tu_nickname"];
    bean.tu_gender = [[dic objectForKey:@"users"] objectForKey:@"tu_gender"];
    bean.tu_portrait = [[dic objectForKey:@"users"] objectForKey:@"tu_portrait"];
    bean.tu_birthday = [[dic objectForKey:@"users"] objectForKey:@"tu_birthday"];
    bean.invite_flag = [[dic objectForKey:@"users"] objectForKey:@"invite_flag"];
    return bean;
}

@end

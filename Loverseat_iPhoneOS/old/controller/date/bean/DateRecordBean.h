//
//  DateRecordBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateRecordBean : NSObject
@property(nonatomic,strong)NSString *tad_begin_time;
@property(nonatomic,strong)NSString *tad_end_time;
@property(nonatomic,strong)NSString *tbi_free_time;
@property(nonatomic,strong)NSString *tad_image;
@property(nonatomic,strong)NSString *show_name;
@property(nonatomic,strong)NSString *tad_show_name;
@property(nonatomic,strong)NSString *venue_name;
@property(nonatomic,strong)NSString *tbi_city;
@property(nonatomic,strong)NSString *tbi_created;
@property(nonatomic,strong)NSString *tbi_event_id;
@property(nonatomic,strong)NSString *tbi_gender;
@property(nonatomic,strong)NSString *tbi_id;
@property(nonatomic,strong)NSString *tbi_payment;
@property(nonatomic,strong)NSString *tbi_portrait;
@property(nonatomic,strong)NSString *tbi_status;
@property(nonatomic,strong)NSString *tbi_target_id;
@property(nonatomic,strong)NSString *tbi_type;
@property(nonatomic,strong)NSString *tbi_uid;
@property(nonatomic,strong)NSString *ticket_count;
@property(nonatomic,strong)NSString *tu_birthday;
@property(nonatomic,strong)NSString *tu_nickname;
@property(nonatomic,strong)NSString *tu_occupation;
@property(nonatomic,strong)NSString *tu_signature;
@property(nonatomic,strong)NSString *valid_flag;
@property(nonatomic,strong)NSString *target_id;
@property(nonatomic,strong)NSString *user_name;
@property(nonatomic,assign)int *gender;
@property(nonatomic,strong)NSString *portrait;
@property(nonatomic,strong)NSString *tad_id;
+(NSArray *)getBeanList:(NSArray *)arr;

+(DateRecordBean *)getBean:(NSDictionary *)dic;
@end

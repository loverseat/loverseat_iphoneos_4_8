//
//  DateChooseBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-11.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateChooseBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>

@implementation DateChooseBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]])
        return resultList;
    for (NSDictionary *dic in arr) {
        DateChooseBean *bean = [DateChooseBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DateChooseBean *)getBean:(NSDictionary *)dic{
    DateChooseBean *bean = [[DateChooseBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tu_nickname = [dic objectForKey:@"tu_nickname"];
        bean.tu_id = [dic objectForKey:@"tu_id"];
        bean.tu_portrait = [dic objectForKey:@"tu_portrait"];
        bean.status = [dic objectForKey:@"status"];
        bean.current_time = [dic objectForKey:@"current_time"];
        bean.invite_time = [dic objectForKey:@"invite_time"];
        bean.end_time = [dic objectForKey:@"end_time"];
        bean.buy_flag = [dic objectForKey:@"buy_flag"];
        bean.tu_birthday = [dic objectForKey:@"tu_birthday"];
        bean.tu_gender = [dic objectForKey:@"tu_gender"];
        bean.tu_signature = [dic objectForKey:@"tu_signature"];
        bean.tu_star_sign = [dic objectForKey:@"tu_star_sign"];
    }
    return bean;
}

+(DateChooseBean *)copyBean:(DateChooseBean *)bean{
    DateChooseBean *copyBean = [[DateChooseBean alloc]init];
    if([GB_ToolUtils isNotBlank:bean]){
        copyBean.tu_nickname = bean.tu_nickname;
        copyBean.tu_id = bean.tu_id;
        copyBean.tu_portrait = bean.tu_portrait;
        copyBean.status = bean.status;
        copyBean.current_time = bean.current_time;
        copyBean.invite_time = bean.invite_time;
        copyBean.end_time = bean.end_time;
        copyBean.buy_flag = bean.buy_flag;
        copyBean.tu_birthday = bean.tu_birthday;
        copyBean.tu_gender = bean.tu_gender;
        copyBean.tu_signature = bean.tu_signature;
        copyBean.tu_star_sign = bean.tu_star_sign;
    }
    return copyBean;
}


@end

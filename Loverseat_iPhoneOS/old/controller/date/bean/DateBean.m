//
//  DateBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateBean.h"
#import <GeekBean4IOS/GB_ToolUtils.h>
#import "User.h"

@implementation DateBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]])
        return resultList;
    for (NSDictionary *dic in arr) {
        DateBean *bean = [DateBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DateBean *)getBean:(NSDictionary *)dic{
    DateBean *bean = [[DateBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.invite_flag = [dic objectForKey:@"invite_flag"];
        bean.payment = [dic objectForKey:@"payment"];
        if([User isMale]){
            bean.profile_flag = [dic objectForKey:@"profile_flag"];
            bean.tu_birthday = [[dic objectForKey:@"users"] objectForKey:@"tu_birthday"];
            
            bean.tu_id = [[dic objectForKey:@"users"] objectForKey:@"tu_id"];
            bean.tu_nickname = [[dic objectForKey:@"users"] objectForKey:@"tu_nickname"];
            bean.tu_portrait = [[dic objectForKey:@"users"] objectForKey:@"tu_portrait"];
            bean.tu_star_sign = [[dic objectForKey:@"users"] objectForKey:@"tu_star_sign"];
        }
        else{
            bean.tu_nickname = [dic objectForKey:@"tu_nickname"];
            bean.tu_star_sign = [dic objectForKey:@"tu_star_sign"];
            bean.tu_id = [dic objectForKey:@"tu_id"];
            bean.tu_portrait = [dic objectForKey:@"tu_portrait"];
            bean.tu_birthday = [dic objectForKey:@"tu_birthday"];
        }
    }
    return bean;
}
@end

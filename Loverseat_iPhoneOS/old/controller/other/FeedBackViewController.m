//
//  FeedBackViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "FeedBackViewController.h"

@interface FeedBackViewController ()

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UIButton *submitBtn;

@end

@implementation FeedBackViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
    [GB_ToolUtils addKeyboardNotification:self];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"意见反馈"];
    [NavUtils addBackButton:self];
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    [self.view addSubview:_textView];
    
    UIButton *submitBtn = [GB_WidgetUtils getButton:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-60, [GB_DeviceUtils getScreenWidth], 60) image:[GB_ToolUtils getPureImg:GB_UIColorFromRGB(251, 80, 87) size:CGSizeMake([GB_DeviceUtils getScreenWidth], 60)] imageH:nil id:self sel:@selector(c_submit)];
    [submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [submitBtn setTitle:@"提交" forState:UIControlStateHighlighted];
    [self.view addSubview:submitBtn];
    self.submitBtn = submitBtn;
}

-(void)c_submit{
    [self GB_resignFirstResponder];
    if([GB_StringUtils isBlank:_textView.text]){
        [Static alert:self.navigationController.view msg:@"反馈内容不能为空"];
        return;
    }
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"feedback_content" value:_textView.text]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"hardware_info" value:[GB_DeviceUtils getModel]]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"software_info" value:[NSString stringWithFormat:@"%@_%f_%@_%d",[GB_DeviceUtils getOperatingSystem],[GB_DeviceUtils getOSVersion],[Static getVersion],[Static getVersionCode]]]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getFeedBackUrl] list:arr delegate:self tag:1];
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    if([Error verify:str view:self.view]){
        [Static alert:self.navigationController.view msg:@"反馈成功"];
        [self cb_back];
    }
}

-(void)GB_resignFirstResponder{
    [_textView resignFirstResponder];
}

-(void)GB_keyboardWillHide:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    self.textView.frame = CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-60);
    self.submitBtn.frame = CGRectMake(0, [GB_DeviceUtils getScreenHeight]-60, [GB_DeviceUtils getScreenWidth], 60);
     _textView.contentSize = _textView.frame.size;
    [UIView commitAnimations];
}

-(void)GB_keyboardWillShow:(NSNotification *)note{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:[[[note userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    int keyBoardHeight = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    _textView.frame = CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]-keyBoardHeight-60);
    self.submitBtn.frame = CGRectMake(0, [GB_DeviceUtils getScreenHeight]-60-keyBoardHeight, [GB_DeviceUtils getScreenWidth], 60);
    _textView.contentSize = _textView.frame.size;
    [UIView commitAnimations];
    
}

@end

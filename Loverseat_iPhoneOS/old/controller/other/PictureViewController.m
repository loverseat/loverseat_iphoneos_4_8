//
//  PictureViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "PictureViewController.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "NavUtils.h"
#import "User.h"
#import "Static.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Error.h"
#import "UserInfoViewController.h"
#import "Static.h"
#import "HomeViewController.h"
#import "HomeLeftViewController.h"
#import "HomeCenterViewController.h"
@implementation PictureViewController{
    UIImagePickerController *pc;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [self initFrame];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //编辑按钮
    if([self.uid intValue] == [[User getUserInfo].uid intValue]){
        UIButton *rightBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-60-[GB_ToolUtils getTextWidth:@"编辑" font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]+28, 2, 60+[GB_ToolUtils getTextWidth:@"编辑" font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]-28, [NavUtils getNavHeight]) image:nil imageH:nil id:self sel:@selector(c_edit)];
        rightBtn.titleLabel.font = [Static getFont:32 isBold:NO];
        [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [rightBtn setTitle:@"编辑" forState:UIControlStateHighlighted];
        [rightBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateHighlighted];
        [rightBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateNormal];
        [self.view addSubview:rightBtn];
    }
}
-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(35, 35, 35);
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [NavUtils getNavHeight])];
    iv.image = [UIImage imageNamed:@"bg_nav"];
    [self.view addSubview:iv];
    
    UILabel *l = [GB_WidgetUtils getLabel:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [NavUtils getNavHeight]) title:@"预览" font:[Static getFont:36 isBold:NO] color:GB_UIColorFromRGB(251, 80, 87)];
    l.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:l];
    

    
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60.0f, [NavUtils getNavHeight]) image:[UIImage imageNamed:@"btn_back_rose"] imageH:nil id:self sel:@selector(cb_back)];
    [self.view addSubview:navBackBtn];
}

- (void)c_edit{
    UIActionSheet *ac = nil;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ac = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:@"取消"
                           destructiveButtonTitle:nil
                                otherButtonTitles:@"相册",@"拍照",nil];
    }
    else
    {
        ac = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:@"取消"
                           destructiveButtonTitle:nil
                                otherButtonTitles:@"相册",nil];
    }
    ac.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [ac showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    pc = [[UIImagePickerController alloc]init];
    pc.delegate = self;
    if(buttonIndex == 0){
        NSLog(@"相册");
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            pc.allowsEditing = YES;
            pc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pc.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pc.sourceType];
            [[UIApplication sharedApplication]setStatusBarHidden:YES];
            [self presentViewController:pc animated:YES completion:nil];
        }
    }
    if(buttonIndex == 1){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            pc.allowsEditing = YES;
            pc.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pc animated:YES completion:nil];
        }
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"选择结束");
    [self dismissViewControllerAnimated:YES completion:nil];
    [Static add:self.navigationController.view msg:@"正在上传"];
    NSString *typeStr = [info objectForKey:UIImagePickerControllerMediaType];
    if([typeStr isEqualToString:(NSString *)kUTTypeImage]){
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        [self imagePickerDidFinish:image];
    }
}
//上传图片
- (void)imagePickerDidFinish:(UIImage *)image{
    [GB_NetWorkUtils cancelRequest];
    NSMutableArray *params = [NSMutableArray array];
    NSMutableArray *imageParams = [NSMutableArray array];
    NSData* data = UIImagePNGRepresentation(image);
    [imageParams addObject:[[GB_KeyValue alloc]initWithKeyValue:@"userfile" value:[[GB_Data alloc]initWithData:data]]];
    [imageParams addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:@"0"]];
    [GB_NetWorkUtils startMultipartPostAsyncRequest:[Url getFileUploadUrl] params:params imageParams:imageParams delegate:self tag:3];
}
-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    str = [Error getCurrentStr:str];
    if([Error verify:str view :self.view]){
        NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
        if(tag == 3){
            [Static remove:self.navigationController.view];
            NSMutableArray *arr1 = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            [arr1 addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:self.uid]];
            [arr1 addObject:[[GB_KeyValue alloc]initWithKeyValue:@"portrait" value:[dic objectForKey:@"path"]]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getUserInfoUpdateUrl] list:arr1 delegate:self tag:4];
        }else if (tag == 4){
           [Static remove:self.navigationController.view];
            for(UIViewController *controller in self.navigationController.viewControllers) {
                if([controller isKindOfClass:[UserInfoViewController class]]){
                    UserInfoViewController *user = (UserInfoViewController *)controller;
                    [user initData];
                    [User login:[Static getRequestData:str]];
                    [self.navigationController popToViewController:user animated:YES];
                    [[PublicDao shareInstance].rootViewController.leftViewController changeStatus];
                }
            }
        }
    }
}
-(void)cb_back{
    [GB_NetWorkUtils cancelRequest];
    [Static remove:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:YES];
}
@end

//
//  ScrollViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "ScrollViewController.h"
#import "HomeViewController.h"

@interface ScrollViewController ()

@end

@implementation ScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        for (int i = 0; i<3; i++) {
            UIImageView *bg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"loading_%d",i+1]]];
            bg.center = CGPointMake((i+0.5f)*[GB_DeviceUtils getScreenWidth], _scrollView.frame.size.height*0.5-(568-_scrollView.frame.size.height)*0.5);
            [_scrollView addSubview:bg];
        }
        
        _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width*3, _scrollView.frame.size.height);
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(_scrollView.frame.size.width*2+65, [GB_DeviceUtils getScreenHeight]-30-50, 190, 50) image:nil imageH:nil id:self sel:@selector(finish)];
        btn.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:btn];
        [self.view addSubview:_scrollView];
    }
    return self;
}

-(void)finish{
         [_root checkLogin];
    [self dismissViewControllerAnimated:YES completion:^{
        //[_root checkLogin];
    }];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

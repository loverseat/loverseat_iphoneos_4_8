
//
//  InputCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-30.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "InputCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"

@implementation InputCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *title = [GB_WidgetUtils getLabel:CGRectMake(15, 0, 120, 55) title:nil font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(57, 63, 77)];
        [self.contentView addSubview:title];
        self.title = title;
        
        UITextField *tf = [[UITextField alloc]initWithFrame:CGRectMake(15+120+10, 0, [GB_DeviceUtils getScreenWidth]-30-120-10, 55)];
        tf.textColor = GB_UIColorFromRGB(142, 142, 147);
        tf.font = [Static getFont:32 isBold:NO];
        tf.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        tf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        tf.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:tf];
        self.tf = tf;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


@end

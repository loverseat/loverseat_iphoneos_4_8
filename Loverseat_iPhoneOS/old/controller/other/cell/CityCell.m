//
//  CityCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "CityCell.h"
#import "Static.h"
#import <GeekBean4IOS/GB_GeekBean.h>


@implementation CityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *title = [GB_WidgetUtils getLabel:CGRectMake(15, 0, [GB_DeviceUtils getScreenWidth]-30, 55) title:nil font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(57, 63, 77)];
        [self.contentView addSubview:title];
        self.title = title;
    }
    return self;
}

@end

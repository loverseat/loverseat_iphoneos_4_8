//
//  SettingCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *state;
@property (nonatomic, strong) UIImageView *arrow;
@property (nonatomic, strong) UISwitch *switchView;

-(void)setStateArrowFrame;
-(void)setStateNormalFrame;
@end

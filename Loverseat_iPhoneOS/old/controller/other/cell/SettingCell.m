//
//  SettingCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "SettingCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"

@implementation SettingCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *title = [GB_WidgetUtils getLabel:CGRectMake(15, 0, [GB_DeviceUtils getScreenWidth]-30, 55) title:nil font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(57, 63, 77)];
        [self.contentView addSubview:title];
        self.title = title;
        
        UILabel *state = [GB_WidgetUtils getLabel:CGRectZero title:nil font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(142, 142, 147)];
        [self.contentView addSubview:state];
        state.textAlignment = NSTextAlignmentRight;
        self.state = state;
        
        UIImageView *arrow = [[UIImageView alloc]init];
        [self.contentView addSubview:arrow];
        self.arrow = arrow;
        
        UISwitch *swithView = [[UISwitch alloc]init];
        swithView.center = CGPointMake([GB_DeviceUtils getScreenWidth]-40, 55*0.5);
        [self.contentView addSubview:swithView];
        self.switchView = swithView;
    }
    return self;
}

-(void)setStateArrowFrame{
    self.state.frame = CGRectMake(15, 0, [GB_DeviceUtils getScreenWidth]-30-20, 55);
}

-(void)setStateNormalFrame{
    self.state.frame = CGRectMake(15, 0, [GB_DeviceUtils getScreenWidth]-30, 55);
}


@end

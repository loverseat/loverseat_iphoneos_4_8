//
//  InputCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-30.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputCell : UITableViewCell

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UITextField *tf;

@end

//
//  UserEditCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserEditCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"

@interface UserEditCell()

@property (nonatomic, strong) UIButton *clearBtn;

@end

@implementation UserEditCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UITextField *tf = [[UITextField alloc]initWithFrame:CGRectMake(15, 0, [GB_DeviceUtils getScreenWidth]-55, 55)];
        tf.textColor = GB_UIColorFromRGB(142, 142, 147);
        tf.font = [Static getFont:32 isBold:NO];
        tf.delegate = self;
        tf.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        tf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.contentView addSubview:tf];
        self.tf = tf;
        
        
        
        UIButton *clearBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-30, 20.5, 14, 14) image:[UIImage imageNamed:@"btn_clear"] imageH:nil id:self sel:@selector(c_clear)];
        clearBtn.hidden = YES;
        [self.contentView addSubview:clearBtn];
        self.clearBtn = clearBtn;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setDefaultValue:(NSString *)defaultValue{
    _clearBtn.hidden = [GB_StringUtils isBlank:defaultValue];
    _tf.text = defaultValue;
}

-(void)c_clear{
    self.tf.text = nil;
    self.clearBtn.hidden = YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(str.length == 0 && [GB_StringUtils isBlank:string]){
        self.clearBtn.hidden = YES;
    }
    else{
        self.clearBtn.hidden = NO;
    }
    return YES;
}

@end

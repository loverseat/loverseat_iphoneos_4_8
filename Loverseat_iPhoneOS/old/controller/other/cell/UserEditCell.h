//
//  UserEditCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserEditCell : UITableViewCell
<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *tf;

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

-(void)setDefaultValue:(NSString *)defaultValue;

@end

//
//  LogoutCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "LogoutCell.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"

@implementation LogoutCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *title = [GB_WidgetUtils getLabel:CGRectMake(15, 0, [GB_DeviceUtils getScreenWidth]-30, 55) title:@"注销登录" font:[Static getFont:32 isBold:NO] color:GB_UIColorFromRGB(255, 0, 24)];
        title.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:title];

    }
    return self;
}
@end

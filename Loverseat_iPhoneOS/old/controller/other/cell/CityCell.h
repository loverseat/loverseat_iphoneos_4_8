//
//  CityCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-7.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityCell : UITableViewCell
@property (nonatomic, strong) UILabel *title;
@end

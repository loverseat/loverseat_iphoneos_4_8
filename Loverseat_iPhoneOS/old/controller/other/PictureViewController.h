//
//  PictureViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <GeekBean4IOS/GB_PictureViewController.h>
#import <GeekBean4IOS/GB_NetWorkDelegate.h>
@interface PictureViewController : GB_PictureViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,GB_NetWorkDelegate>
@property (nonatomic, strong) NSString *uid;

@end

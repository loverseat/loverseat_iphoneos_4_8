//
//  DetailBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DetailBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>

@implementation DetailBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        DetailBean *bean = [DetailBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DetailBean *)getBean:(NSDictionary *)dic{
    DetailBean *bean = [[DetailBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){        
        bean.tad_show_name = [dic objectForKey:@"tad_show_name"];
        bean.tad_begin_time = [dic objectForKey:@"tad_begin_time"];
        bean.tad_end_time = [dic objectForKey:@"tad_end_time"];
        bean.tad_venue_name = [dic objectForKey:@"tad_venue_name"];
        bean.tad_points = [dic objectForKey:@"tad_points"];
        bean.tad_intro = [dic objectForKey:@"tad_intro"];
        bean.tad_image = [dic objectForKey:@"tad_image"];
        bean.tad_audio = [dic objectForKey:@"tad_audio"];
    }
    return bean;
}


@end

//
//  ScheduleBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface ScheduleBean : BaseBean

@property (nonatomic, strong) NSString *tss_id;
@property (nonatomic, strong) NSString *tss_final_id;
@property (nonatomic, strong) NSString *tss_show_time;
@property (nonatomic, strong) NSArray *tickets;

+(NSArray *)getBeanList:(NSArray *)arr;
+(ScheduleBean *)getBean:(NSDictionary *)dic;

@end

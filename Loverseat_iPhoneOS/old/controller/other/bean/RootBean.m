//
//  RootBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-3.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "RootBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation RootBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *beanList = [NSMutableArray array];
    NSMutableArray *topList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        RootBean *bean = [RootBean getBean:dic];
        if([bean.tad_topic integerValue] == 1){
            [topList addObject:bean];
        }
        else{
            [beanList addObject:bean];
        }
    }
    NSMutableArray *resultList = [NSMutableArray array];
    [resultList addObjectsFromArray:topList];
    [resultList addObjectsFromArray:beanList];
    return resultList;
}

+(RootBean *)getBean:(NSDictionary *)dic{
    RootBean *bean = [[RootBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.history_count = [dic objectForKey:@"history_count"];
        bean.tad_category_name = [dic objectForKey:@"tad_category_name"];
        bean.tad_city_name = [dic objectForKey:@"tad_city_name"];
        bean.tad_image = [dic objectForKey:@"tad_image"];
        bean.tad_show_name = [dic objectForKey:@"tad_show_name"];
        bean.tad_topic = [dic objectForKey:@"tad_topic"];
        bean.tad_id = [dic objectForKey:@"tad_id"];
    }
    return bean;
}

@end

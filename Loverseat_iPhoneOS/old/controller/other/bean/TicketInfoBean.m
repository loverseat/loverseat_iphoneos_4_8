//
//  TicketInfoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TicketInfoBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation TicketInfoBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        TicketInfoBean *bean = [TicketInfoBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(TicketInfoBean *)getBean:(NSDictionary *)dic{
    TicketInfoBean *bean = [[TicketInfoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.discount = [dic objectForKey:@"discount"];
        bean.electronic = [dic objectForKey:@"electronic"];
        bean.express = [dic objectForKey:@"express"];
        bean.address = [dic objectForKey:@"address"];
        bean.time = [dic objectForKey:@"time"];
        bean.deposit = [dic objectForKey:@"deposit"];
        bean.lowest_price = [dic objectForKey:@"lowest_price"];
        
        bean.shipping_address = [dic objectForKey:@"shipping_address"];
        bean.shipping_city = [dic objectForKey:@"shipping_city"];
        bean.shipping_city_id = [dic objectForKey:@"shipping_city_id"];
        bean.shipping_province = [dic objectForKey:@"shipping_province"];
        bean.shipping_province_id = [dic objectForKey:@"shipping_province_id"];
        
        bean.schedules = [ScheduleBean getBeanList:[dic objectForKey:@"schedules"]];
    }
    return bean;
}

@end

//
//  CommentBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "CommentBean.h"
#import <GeekBean4IOS/GB_ToolUtils.h>

@implementation CommentBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if(![arr isKindOfClass:[NSArray class]]){
        return resultList;
    }
    for (NSDictionary *dic in arr) {
        CommentBean *bean = [CommentBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(CommentBean *)getBean:(NSDictionary *)dic{
    CommentBean *bean = [[CommentBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tac_id = [dic objectForKey:@"tac_id"];
        bean.tac_event_id = [dic objectForKey:@"tac_event_id"];
        bean.tac_uid = [dic objectForKey:@"tac_uid"];
        bean.tac_detail = [dic objectForKey:@"tac_detail"];
        bean.tac_username = [dic objectForKey:@"tac_username"];
        bean.tac_thumb = [dic objectForKey:@"tac_thumb"];
        bean.tac_created = [dic objectForKey:@"tac_created"];
    }
    return bean;
}

@end

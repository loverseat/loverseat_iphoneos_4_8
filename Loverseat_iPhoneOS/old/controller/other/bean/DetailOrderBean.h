//
//  DetailOrderBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-2.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface DetailOrderBean : BaseBean
@property (nonatomic, strong) NSString *to_id;
@property (nonatomic, strong) NSString *to_show_time;
@property (nonatomic, strong) NSString *to_event_name;
@property (nonatomic, strong) NSString *to_venue_name;
@property (nonatomic, strong) NSString *to_perprice;
@property (nonatomic, strong) NSString *to_portrait;

@property (nonatomic, strong) NSString *to_uid;
@property (nonatomic, strong) NSString *to_username;
+(NSArray *)getBeanList:(NSArray *)arr;

+(DetailOrderBean *)getBean:(NSDictionary *)dic;

@end

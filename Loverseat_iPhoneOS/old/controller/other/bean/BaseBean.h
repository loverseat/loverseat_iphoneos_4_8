//
//  BaseBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-3.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "Url.h"
#import "Static.h"


@class BaseViewController;
@interface BaseBean : NSObject

@property (nonatomic, assign) int index;
@property (nonatomic, assign) int tag;
@property (nonatomic, assign) SEL sel;
@property (nonatomic, strong) BaseViewController *baseViewController;

@end

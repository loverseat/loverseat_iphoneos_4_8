//
//  ScheduleBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "ScheduleBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation ScheduleBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        ScheduleBean *bean = [ScheduleBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(ScheduleBean *)getBean:(NSDictionary *)dic{
    ScheduleBean *bean = [[ScheduleBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tss_id = [dic objectForKey:@"tss_id"];
        bean.tss_final_id = [dic objectForKey:@"tss_final_id"];
        bean.tss_show_time = [dic objectForKey:@"tss_show_time"];
        bean.tickets = [dic objectForKey:@"tickets"];
    }
    return bean;
}

@end

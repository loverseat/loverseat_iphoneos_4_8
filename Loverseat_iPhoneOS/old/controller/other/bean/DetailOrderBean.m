//
//  DetailOrderBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-2.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DetailOrderBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation DetailOrderBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        DetailOrderBean *bean = [DetailOrderBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(DetailOrderBean *)getBean:(NSDictionary *)dic{
    DetailOrderBean *bean = [[DetailOrderBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.to_id = [dic objectForKey:@"to_id"];
        bean.to_show_time = [dic objectForKey:@"to_show_time"];
        bean.to_event_name = [dic objectForKey:@"to_event_name"];
        bean.to_venue_name = [dic objectForKey:@"to_venue_name"];
        bean.to_perprice = [dic objectForKey:@"to_perprice"];
        bean.to_portrait = [dic objectForKey:@"to_portrait"];
        
    }
    return bean;
}

@end

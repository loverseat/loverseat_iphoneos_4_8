//
//  FuyueBean.h
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/1/26.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FuyueBean : NSObject
@property (nonatomic,strong)NSString *tbia_id;
@property (nonatomic,strong)NSString *tbia_invite_message;
@property (nonatomic,strong)NSString *tbia_uid;
@property (nonatomic,strong)NSString *tu_birthday;
@property (nonatomic,strong)NSString *tu_gender;
@property (nonatomic,strong)NSString *tu_nickname;
@property (nonatomic,strong)NSString *tu_portrait;
@property (nonatomic,strong)NSString *tu_star_sign;
+(NSArray *)getBeanList:(NSArray *)arr;
+(FuyueBean*)getBean:(NSDictionary *)dic;
@end

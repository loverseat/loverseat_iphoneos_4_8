//
//  PhotoBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface PhotoBean : BaseBean
@property(nonatomic,strong)NSString *ta_id;
@property(nonatomic,strong)NSString *ta_uid;
@property(nonatomic,strong)NSString *ta_photo_url;
@property(nonatomic,strong)NSString *ta_created;
@property(nonatomic,strong)NSString *ta_thumb_url;

+(NSArray *)getBeanList:(NSArray *)arr;
+(PhotoBean *)getBean:(NSDictionary *)dic;
@end

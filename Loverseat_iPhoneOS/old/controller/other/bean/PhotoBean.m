//
//  PhotoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-6-5.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "PhotoBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation PhotoBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        PhotoBean *bean = [PhotoBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(PhotoBean *)getBean:(NSDictionary *)dic{
    PhotoBean *bean = [[PhotoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.ta_id = [dic objectForKey:@"ta_id"];
        bean.ta_uid = [dic objectForKey:@"ta_uid"];
        bean.ta_photo_url = [dic objectForKey:@"ta_photo_url"];
        bean.ta_created = [dic objectForKey:@"ta_created"];
        bean.ta_thumb_url = [dic objectForKey:@"ta_thumb_url"];
    }
    return bean;
}

@end

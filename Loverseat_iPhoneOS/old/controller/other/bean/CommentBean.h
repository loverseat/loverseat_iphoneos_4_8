//
//  CommentBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentBean : NSObject

@property (nonatomic, strong) NSString *tac_id;
@property (nonatomic, strong) NSString *tac_event_id;
@property (nonatomic, strong) NSString *tac_uid;
@property (nonatomic, strong) NSString *tac_detail;
@property (nonatomic, strong) NSString *tac_username;
@property (nonatomic, strong) NSString *tac_thumb;
@property (nonatomic, strong) NSString *tac_created;

+(NSArray *)getBeanList:(NSArray *)arr;

+(CommentBean *)getBean:(NSDictionary *)dic;

@end

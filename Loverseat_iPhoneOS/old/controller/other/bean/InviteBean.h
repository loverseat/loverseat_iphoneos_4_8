//
//  InviteBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-18.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface InviteBean : BaseBean

@property (nonatomic, strong) NSString *tbi_uid;
@property (nonatomic, strong) NSString *tbi_portrait;
@property (nonatomic, strong) NSString *tbi_target_id;
@property (nonatomic, strong) NSString *tbi_status;
@property (nonatomic, strong) NSString *tu_nickname;
@property (nonatomic, strong) NSString *tu_mobile;
@property (nonatomic, strong) NSString *tu_province;
@property (nonatomic, strong) NSString *tu_city;
@property (nonatomic, strong) NSString *tu_age;
@property (nonatomic, strong) NSString *tu_signature;
@property (nonatomic, strong) NSString *tu_voice_signature;
@property (nonatomic, strong) NSString *tu_rank;
@property (nonatomic, strong) NSString *tu_tabs;
@property (nonatomic, strong) NSString *tu_occupation;
@property (nonatomic, strong) NSString *tu_income;
@property (nonatomic, strong) NSString *tu_marital_status;
@property (nonatomic, strong) NSString *tu_favour;
@property (nonatomic, strong) NSString *tu_purpose;
@property (nonatomic, strong) NSString *tu_ablums;
@property (nonatomic, strong) NSString *tu_coin;
@property (nonatomic, strong) NSString *tu_score;
@property (nonatomic, strong) NSString *tu_invite_flag;
+(NSArray *)getBeanList:(NSArray *)arr;

+(InviteBean *)getBean:(NSDictionary *)dic;

@end

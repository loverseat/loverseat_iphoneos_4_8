//
//  DetailBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-9.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailBean : NSObject

@property(nonatomic,strong)NSString *tad_show_name;
@property(nonatomic,strong)NSString *tad_begin_time;
@property(nonatomic,strong)NSString *tad_end_time;
@property(nonatomic,strong)NSString *tad_venue_name;
@property(nonatomic,strong)NSString *tad_points;
@property(nonatomic,strong)NSString *tad_intro;
@property(nonatomic,strong)NSString *tad_image;
@property(nonatomic,strong)NSString *tad_audio;
@property(nonatomic,strong)NSString *tad_in;
@property(nonatomic,assign)BOOL choosed;
@property(nonatomic,strong)NSString *target_uid;
@property(nonatomic,assign)BOOL tad_btn_hidden;
@property(nonatomic,assign)BOOL interest_flag;
@property(nonatomic,strong)NSArray *buy_invite_history;
+(NSArray *)getBeanList:(NSArray *)arr;
+(DetailBean *)getBean:(NSDictionary *)dic;

@end

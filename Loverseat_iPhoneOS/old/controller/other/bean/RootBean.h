//
//  RootBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-3.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface RootBean : BaseBean

@property(nonatomic,strong)NSString *history_count;
@property(nonatomic,strong)NSString *tad_category_name;
@property(nonatomic,strong)NSString *tad_city_name;
@property(nonatomic,strong)NSString *tad_image;
@property(nonatomic,strong)NSString *tad_show_name;
@property(nonatomic,strong)NSString *tad_topic;
@property(nonatomic,strong)NSString *tad_id;

+(NSArray *)getBeanList:(NSArray *)arr;
+(RootBean *)getBean:(NSDictionary *)dic;

@end

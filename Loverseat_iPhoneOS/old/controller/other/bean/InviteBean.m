//
//  InviteBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-18.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "InviteBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation InviteBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        InviteBean *bean = [InviteBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}

+(InviteBean *)getBean:(NSDictionary *)dic{
    InviteBean *bean = [[InviteBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tbi_uid = [dic objectForKey:@"tbi_uid"];
        bean.tbi_portrait = [dic objectForKey:@"tbi_portrait"];
        bean.tbi_target_id = [dic objectForKey:@"tbi_target_id"];
        bean.tbi_status = [dic objectForKey:@"tbi_status"];
        bean.tu_nickname = [dic objectForKey:@"tu_nickname"];
        bean.tu_province = [dic objectForKey:@"tu_province"];
        bean.tu_city = [dic objectForKey:@"tu_city"];
        bean.tu_age = [dic objectForKey:@"tu_age"];
        bean.tu_signature = [dic objectForKey:@"tu_signature"];
        bean.tu_voice_signature = [dic objectForKey:@"tu_voice_signature"];
        bean.tu_rank = [dic objectForKey:@"tu_rank"];
        bean.tu_tabs = [dic objectForKey:@"tu_tabs"];
        bean.tu_occupation = [dic objectForKey:@"tu_occupation"];
        bean.tu_income = [dic objectForKey:@"tu_income"];
        bean.tu_marital_status = [dic objectForKey:@"tu_marital_status"];
        bean.tu_favour = [dic objectForKey:@"tu_favour"];
        bean.tu_purpose = [dic objectForKey:@"tu_purpose"];
        bean.tu_ablums = [dic objectForKey:@"tu_ablums"];
        bean.tu_coin = [dic objectForKey:@"tu_coin"];
        bean.tu_score = [dic objectForKey:@"tu_score"];
        bean.tu_mobile = [dic objectForKey:@"tu_mobile"];

    }
    return bean;
}
@end

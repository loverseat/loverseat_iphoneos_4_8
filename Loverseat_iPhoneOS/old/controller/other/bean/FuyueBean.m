//
//  FuyueBean.m
//  Loverseat_iPhoneOS
//
//  Created by Xicheng on 15/1/26.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "FuyueBean.h"
#import <GeekBean4IOS/GB_ToolUtils.h>
@implementation FuyueBean
+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        FuyueBean *bean = [FuyueBean getBean:dic];
        [resultList addObject:bean];
    }
    return resultList;
}
+(FuyueBean*)getBean:(NSDictionary *)dic{
    FuyueBean *bean = [[FuyueBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tbia_id = [dic objectForKey:@"tbia_id"];
        bean.tbia_uid = [dic objectForKey:@"tbia_uid"];
        bean.tbia_invite_message = [dic objectForKey:@"tbia_invite_message"];
        bean.tu_birthday = [[dic objectForKey:@"user"]objectForKey:@"tu_birthday"];
        bean.tu_gender = [[dic objectForKey:@"user"]objectForKey:@"tu_gender"];
        bean.tu_nickname = [[dic objectForKey:@"user"] objectForKey:@"tu_nickname"];
        bean.tu_portrait = [[dic objectForKey:@"user"] objectForKey:@"tu_portrait"];
        bean.tu_star_sign = [[dic objectForKey:@"user"] objectForKey:@"tu_star_sign"];
    }
    return bean;
}
@end

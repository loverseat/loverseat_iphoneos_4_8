//
//  TicketInfoBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"
#import "ScheduleBean.h"

@interface TicketInfoBean : BaseBean

@property (nonatomic, strong) NSString *discount;
@property (nonatomic, strong) NSString *electronic;
@property (nonatomic, strong) NSString *express;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *deposit;
@property (nonatomic, strong) NSString *lowest_price;
@property (nonatomic, strong) NSArray *schedules;


@property (nonatomic, strong) NSString *shipping_address;
@property (nonatomic, strong) NSString *shipping_city;
@property (nonatomic, strong) NSString *shipping_city_id;
@property (nonatomic, strong) NSString *shipping_province;
@property (nonatomic, strong) NSString *shipping_province_id;


+(NSArray *)getBeanList:(NSArray *)arr;
+(TicketInfoBean *)getBean:(NSDictionary *)dic;

@end

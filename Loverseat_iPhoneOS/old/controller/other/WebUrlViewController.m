//
//  WebUrlViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-26.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "WebUrlViewController.h"

@interface WebUrlViewController ()
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation WebUrlViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFrame];
//    [self initData];
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:self.title];
    [NavUtils addBackButton:self];
    
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    [self.view addSubview:_webView];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
}
@end

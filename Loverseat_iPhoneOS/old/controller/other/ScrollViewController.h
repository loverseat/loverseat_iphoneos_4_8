//
//  ScrollViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@class HomeViewController;
@interface ScrollViewController : BaseViewController
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) HomeViewController *root;
@end

//
//  LoadViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//  版本新特性

#import "BaseViewController.h"

@interface LoadViewController : BaseViewController
<GB_NetWorkDelegate,UIAlertViewDelegate>
@end

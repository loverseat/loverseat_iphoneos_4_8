//
//  BaseViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@implementation BaseViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden: YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    if([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]){
        [self setAutomaticallyAdjustsScrollViewInsets:NO];
    }
    self.view.backgroundColor = GB_UIColorFromRGB(43, 43, 44);;
}

-(void)addWhiteStatusBarText{
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];
#endif
}

-(void)addBlackStatusBarText{
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self setNeedsStatusBarAppearanceUpdate];
#endif
}

-(void)cb_back{
    [GB_NetWorkUtils cancelRequest];
    [Static remove:self.navigationController.view];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GB_requestDidFailed:(int)tag{
    
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
}
@end

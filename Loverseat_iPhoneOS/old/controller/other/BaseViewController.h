//
//  BaseViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"
#import "Url.h"
#import "User.h"
#import "View.h"
#import "Error.h"
#import "NavUtils.h"

#define minHeight 180.0f
#define minDescHeight 50.0f
@protocol SelfPickerDelegate <NSObject>

-(void)cb_picker_did_select:(NSDictionary *)pickerData rowData:(NSDictionary *)rowData;

@end

@interface BaseViewController : UIViewController
<GB_NetWorkDelegate>
@property (nonatomic, strong) UIImageView *iv;
@property (nonatomic, strong) UILabel *l;
@property (nonatomic, strong) UIButton *navBackBtn;

@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, assign) BOOL isLoad;


-(void)cb_back;

@end

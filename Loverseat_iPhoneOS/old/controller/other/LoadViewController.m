//
//  LoadViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "LoadViewController.h"

@implementation LoadViewController

int beginTime;

-(id)init{
    self = [super init];
    if(self){
        beginTime = [[[NSDate new] autorelease] timeIntervalSince1970];
        UIImageView *bg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_load"]];
        bg.center = self.view.center;
        [self.view addSubview:bg];
        [bg release];
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
     [self startMain];
}

-(void)startMain{
    int time = 3-[[[NSDate new] autorelease] timeIntervalSince1970]+beginTime;
    if(time < 0)time = 0;
    [self performSelector:@selector(goMain) withObject:nil afterDelay:time];
}

-(void)GB_requestDidFailed:(int)tag{
    if(tag == 1){
        [self startMain];
    }
}

-(void)goMain{
    [GB_ToolUtils postNotificationName:@"GB_GoMain" object:nil];
}


@end

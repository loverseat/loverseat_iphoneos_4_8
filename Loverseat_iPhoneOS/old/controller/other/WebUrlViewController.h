//
//  WebUrlViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-7-26.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@interface WebUrlViewController : BaseViewController
<UIWebViewDelegate>
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@end

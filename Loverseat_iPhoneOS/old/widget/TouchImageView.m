//
//  TouchImageView.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "TouchImageView.h"

@interface TouchImageView()

@property (nonatomic, assign) CGPoint point;

@end

@implementation TouchImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint point = [[touches anyObject] previousLocationInView:self.view];
    [self.delegate begin:point view:self];
     _point = point;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint newPoint = [[touches anyObject] previousLocationInView:self.view];
    [self.delegate move:newPoint view:self];
    [self.delegate change:_point newPoint:newPoint view:self];
    _point = newPoint;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.delegate end:[[touches anyObject] previousLocationInView:self.view] view:self];
}

@end

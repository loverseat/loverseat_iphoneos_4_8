//
//  CalendarMonth.m
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright 2010 Savage Media Pty Ltd. All rights reserved.
//

#import "CalendarMonth.h"

#import "CalendarLogic.h"
#import <GeekBean4IOS/GB_DateUtils.h>

#define kCalendarDayWidth	30.0f
#define kCalendarDayHeight	30.0f

@implementation CalendarMonth


#pragma mark -
#pragma mark Getters / setters

@synthesize calendarLogic;
@synthesize datesIndex;
@synthesize buttonsIndex;

@synthesize numberOfDaysInWeek;
@synthesize selectedButton;
@synthesize selectedDate;



#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	self.calendarLogic = nil;
	self.datesIndex = nil;
	self.buttonsIndex = nil;
	self.selectedDate = nil;

}

#pragma mark -
#pragma mark Initialization

// Calendar object init
- (id)initWithFrame:(CGRect)frame logic:(CalendarLogic *)aLogic validDateArr:(NSArray *)validDateArr{

	// Size is static
	NSInteger numberOfWeeks = 5;
	frame.size.width = 320;
	frame.size.height = ((numberOfWeeks + 1) * 34) + 60;
	selectedButton = -1;
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];	
	NSDate *todayDate = [calendar dateFromComponents:components];
	
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		self.opaque = YES;
		self.clipsToBounds = NO;
		self.clearsContextBeforeDrawing = NO;
		
		
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		NSArray *daySymbols = [formatter shortWeekdaySymbols];
		self.numberOfDaysInWeek = [daySymbols count];
		
		
		UILabel *aLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
		aLabel.backgroundColor = [UIColor clearColor];
		aLabel.textAlignment = NSTextAlignmentCenter;
		aLabel.font = [UIFont systemFontOfSize:15];
		aLabel.textColor = [UIColor whiteColor];
		
		[formatter setDateFormat:@"yyyy年MM月"];
		aLabel.text = [formatter stringFromDate:aLogic.currentDate];
		[self addSubview:aLabel];
        
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        leftBtn.frame = CGRectMake(15, 9, 22, 21.5f);
        [leftBtn setImage:[UIImage imageNamed:@"btn_calendar_left"] forState:UIControlStateNormal];
        [leftBtn addTarget:aLogic action:@selector(selectPreviousMonth) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:leftBtn];
        
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightBtn setImage:[UIImage imageNamed:@"btn_calendar_right"] forState:UIControlStateNormal];
        [rightBtn addTarget:aLogic action:@selector(selectNextMonth) forControlEvents:UIControlEventTouchUpInside];
        rightBtn.frame = CGRectMake(self.frame.size.width-15-22, 9, 22, 21.5f);
        [self addSubview:rightBtn];
		
		NSInteger firstWeekday = [calendar firstWeekday] - 1;
        int width = 320.0f/numberOfDaysInWeek;
		for (NSInteger aWeekday = 0; aWeekday < numberOfDaysInWeek; aWeekday ++) {
 			NSInteger symbolIndex = aWeekday + firstWeekday;
			if (symbolIndex >= numberOfDaysInWeek) {
				symbolIndex -= numberOfDaysInWeek;
			}
			NSString *symbol = [daySymbols objectAtIndex:symbolIndex];
			CGFloat positionX = (aWeekday * width) - 1;
			CGRect aFrame = CGRectMake(positionX, 40, width, 20);
			
			aLabel = [[UILabel alloc] initWithFrame:aFrame];
			aLabel.backgroundColor = [UIColor clearColor];
			aLabel.textAlignment = NSTextAlignmentCenter;
			aLabel.text = symbol;
			aLabel.textColor = [UIColor whiteColor];
			aLabel.font = [UIFont systemFontOfSize:12];
			[self addSubview:aLabel];
		}
		
		NSMutableArray *aDatesIndex = [[NSMutableArray alloc] init];
		NSMutableArray *aButtonsIndex = [[NSMutableArray alloc] init];
        
        
		for (NSInteger aWeek = 0; aWeek <= numberOfWeeks; aWeek ++) {
			CGFloat positionY = (aWeek * 34.0f) + 60;
			
			for (NSInteger aWeekday = 1; aWeekday <= numberOfDaysInWeek; aWeekday ++) {
				CGFloat positionX = ((aWeekday - 1) * width) - 1 + (width-kCalendarDayWidth)*0.5;
				CGRect dayFrame = CGRectMake(positionX, positionY+2, kCalendarDayWidth, kCalendarDayHeight);
				NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday 
														 onWeek:aWeek 
												  referenceDate:[aLogic currentDate]];
				NSDateComponents *dayComponents = [calendar
												   components:NSDayCalendarUnit fromDate:dayDate];
				
				UIColor *titleColor = [UIColor whiteColor];
                
                BOOL hasPressed = YES;
                if(![validDateArr containsObject:[GB_DateUtils getFormatStringBy10Median:@"yyyy-MM-dd" timeMillis:[dayDate timeIntervalSince1970]]]){
                    titleColor = [UIColor colorWithRed:162.0f/255.0f green:157.0f/255.0f blue:167.0f/255.0f alpha:0.6f];
                    hasPressed = NO;
                }
				else if ([aLogic distanceOfDateFromCurrentMonth:dayDate] != 0) {
					titleColor = [UIColor colorWithRed:162.0f/255.0f green:157.0f/255.0f blue:167.0f/255.0f alpha:1];
				}
                else if([dayDate compare:todayDate] == NSOrderedSame){
                    titleColor = [UIColor colorWithRed:251.0f/255.0f green:80.0f/255.0f blue:87.0f/255.0f alpha:1];
                }
				
				UIButton *dayButton = [UIButton buttonWithType:UIButtonTypeCustom];
				dayButton.opaque = YES;
				dayButton.clipsToBounds = NO;
				dayButton.clearsContextBeforeDrawing = NO;
				dayButton.frame = dayFrame;
				dayButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
				dayButton.titleLabel.font = [UIFont systemFontOfSize:15];
				dayButton.tag = [aDatesIndex count];
				dayButton.adjustsImageWhenHighlighted = NO;
				dayButton.adjustsImageWhenDisabled = NO;
                dayButton.layer.masksToBounds = YES;
                dayButton.layer.cornerRadius = 15.0f;
				
				[dayButton setTitle:[NSString stringWithFormat:@"%d", [dayComponents day]] 
						   forState:UIControlStateNormal];
				
				
                NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
                [outputFormatter setDateFormat:@"dd"];
        
				if ([dayDate compare:todayDate] == NSOrderedSame) {
                    leftBtn.hidden = YES;
				}
                [dayButton setTitleColor:titleColor forState:UIControlStateNormal];
				
                if(hasPressed){
                    [dayButton addTarget:self action:@selector(dayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                }
				[self addSubview:dayButton];
				
				// Save
				[aDatesIndex addObject:dayDate];
				[aButtonsIndex addObject:dayButton];
			}
		}
		
		// save
		self.calendarLogic = aLogic;
		self.datesIndex = [aDatesIndex copy];
		self.buttonsIndex = [aButtonsIndex copy];
        
        
        // Save
        if([aLogic distanceOfDateFromCurrentMonth:aLogic.referenceDate] == 0){
            self.selectedButton = [calendarLogic indexOfCalendarDate:aLogic.referenceDate];
            self.selectedDate = aLogic.referenceDate;
            NSDate *todayDate = [CalendarLogic dateForToday];
            UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
            [button setTitleColor:[UIColor colorWithRed:105.0f/255.0f green:118.0f/255.0f blue:127.0f/255.0f alpha:1] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor colorWithRed:221.0f/255.0f green:214.0f/255.0f blue:210.0f/255.0f alpha:1];
            
            CGRect selectedFrame = button.frame;
            if ([aLogic.referenceDate compare:todayDate] != NSOrderedSame) {
                selectedFrame.origin.y = selectedFrame.origin.y - 1;
                selectedFrame.size.width = kCalendarDayWidth + 1;
                selectedFrame.size.height = kCalendarDayHeight + 1;
            }
            
            button.selected = YES;
            button.frame = selectedFrame;
            [self bringSubviewToFront:button];
        }
    }
    return self;
}



#pragma mark -
#pragma mark UI Controls

- (void)dayButtonPressed:(UIButton *)sender {
	[calendarLogic setReferenceDate:[datesIndex objectAtIndex:[sender tag]]];
}

- (void)selectButtonForDate:(NSDate *)aDate {
	if (selectedButton >= 0) {
		NSDate *todayDate = [CalendarLogic dateForToday];
		UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
        
        UIColor *titleColor = [UIColor whiteColor];
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:selectedDate];
        if ([[calendar dateFromComponents:components] compare:todayDate] == NSOrderedSame){
            titleColor = [UIColor colorWithRed:251.0f/255.0f green:80.0f/255.0f blue:87.0f/255.0f alpha:1];
        }
        else if ([self.calendarLogic distanceOfDateFromCurrentMonth:aDate] != 0) {
            titleColor = [UIColor colorWithRed:152.0f/255.0f green:147.0f/255.0f blue:157.0f/255.0f alpha:1];
        }
        
        [button setTitleColor:titleColor forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        CGRect selectedFrame = button.frame;
		if ([selectedDate compare:todayDate] != NSOrderedSame) {
			selectedFrame.origin.y = selectedFrame.origin.y + 1;
			selectedFrame.size.width = kCalendarDayWidth;
			selectedFrame.size.height = kCalendarDayHeight;
		}
		
		button.selected = NO;
		button.frame = selectedFrame;
		
		self.selectedButton = -1;
		self.selectedDate = nil;
	}
	
	if (aDate != nil) {
		// Save
		self.selectedButton = [calendarLogic indexOfCalendarDate:aDate];
		self.selectedDate = aDate;
		
		NSDate *todayDate = [CalendarLogic dateForToday];
		UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
        [button setTitleColor:[UIColor colorWithRed:105.0f/255.0f green:118.0f/255.0f blue:127.0f/255.0f alpha:1] forState:UIControlStateNormal];
        button.backgroundColor = [UIColor colorWithRed:221.0f/255.0f green:214.0f/255.0f blue:210.0f/255.0f alpha:1];

		CGRect selectedFrame = button.frame;
		if ([aDate compare:todayDate] != NSOrderedSame) {
			selectedFrame.origin.y = selectedFrame.origin.y - 1;
			selectedFrame.size.width = kCalendarDayWidth + 1;
			selectedFrame.size.height = kCalendarDayHeight + 1;
		}
		
		button.selected = YES;
		button.frame = selectedFrame;
		[self bringSubviewToFront:button];	
	}
}

@end

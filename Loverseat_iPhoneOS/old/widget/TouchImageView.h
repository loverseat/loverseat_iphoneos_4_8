//
//  TouchImageView.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-6-19.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchImageViewDelegate.h"

@interface TouchImageView : UIImageView

@property id<TouchImageViewDelegate> delegate;
@property (nonatomic, strong) UIView *view;

@end

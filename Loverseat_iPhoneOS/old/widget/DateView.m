//
//  DateView.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-10.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "DateView.h"
#import <Accelerate/Accelerate.h>
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Static.h"

@interface DateView()
<GB_LoadImageDelegate>

@property (nonatomic, strong)UIImageView *icon;
@property (nonatomic, strong)UIImageView *model;
@property (nonatomic, strong)UIImageView *lock;
@property (nonatomic, strong)UIButton *btn;
@property (nonatomic, assign)float percentage;
@property (nonatomic, assign)float s;
@property (nonatomic, assign)BOOL hasModel;
@property (nonatomic, assign)BOOL hasLock;
@end

@implementation DateView

- (id)initWithAvatar:(NSString *)avatar percentage:(float)percentage hasLock:(BOOL)hasLock hasModel:(BOOL)hasModel frame:(CGRect)frame delegate:(id)delegate action:(SEL)sel tag:(int)tag;
{
    //线宽2f
    //点直径4;
    self = [super initWithFrame:frame];
    if (self) {
        self.percentage = percentage;
        _percentage = _percentage==1?0.999999f:_percentage;
        _percentage = _percentage>0.999999f?0.999999f:_percentage;
        self.hasModel = hasModel;
        self.hasLock = hasLock;
        self.s = frame.size.width/79;
        // Initialization code
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(_s*5, _s*5, frame.size.width-_s*10, frame.size.height-_s*10)];
        _icon.userInteractionEnabled = YES;
        _icon.layer.masksToBounds = YES;
        _icon.layer.cornerRadius = _icon.frame.size.width*0.5;
        [self addSubview:_icon];
    
        [GB_NetWorkUtils loadImage:avatar container:_icon type:GB_ImageCacheTypeAll delegate:self tag:hasLock?(-1*tag):tag];
        
        self.model = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _icon.frame.size.width, _icon.frame.size.height)];
        _model.image = [UIImage imageNamed:@"model_reject"];
        _model.layer.masksToBounds = YES;
        _model.layer.cornerRadius = _model.frame.size.width*0.5f;
        [_icon addSubview:_model];
        _model.hidden = !hasModel;
        
        self.lock = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _icon.frame.size.width, _icon.frame.size.height)];
        _lock.layer.masksToBounds = YES;
        _lock.layer.cornerRadius = _lock.frame.size.width*0.5f;
        
        
        
        UIView *lockBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _icon.frame.size.width, _icon.frame.size.height)];
        lockBg.layer.masksToBounds = YES;
        lockBg.layer.cornerRadius = _lock.frame.size.width*0.5f;
        lockBg.backgroundColor = [UIColor colorWithWhite:1 alpha:0.17f];
        [_lock addSubview:lockBg];
        
        UIImageView *l = [[UIImageView alloc]initWithImage:[Static getFImage:[UIImage imageNamed:@"icon_lock"] w:[UIImage imageNamed:@"icon_lock"].size.width*_s h:[UIImage imageNamed:@"icon_lock"].size.height*_s]];
        [_lock addSubview:l];
        l.center = CGPointMake(_lock.frame.size.width*0.5, _lock.frame.size.height*0.5);
        [_icon addSubview:_lock];
        _lock.hidden = !hasLock;
        
        self.btn = [GB_WidgetUtils getButton:_model.frame image:nil imageH:nil id:delegate sel:sel];
        _btn.tag = tag;
        [_icon addSubview:_btn];
        
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (UIImage *)blurryImage:(UIImage *)image withBlurLevel:(CGFloat)blur {
    if (blur < 0.f || blur > 1.f) {
        blur = 0.5f;
    }
    int boxSize = (int)(blur * 100);
    boxSize = boxSize - (boxSize % 2) + 1;
    
    CGImageRef img = image.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    
    void *pixelBuffer;
    
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) *
                         CGImageGetHeight(img));
    
    if(pixelBuffer == NULL)
        NSLog(@"No pixelbuffer");
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    error = vImageBoxConvolve_ARGB8888(&inBuffer,
                                       &outBuffer,
                                       NULL,
                                       0,
                                       0,
                                       boxSize,
                                       boxSize,
                                       NULL,
                                       kvImageEdgeExtend);
    
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(
                                             outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             kCGImageAlphaNoneSkipLast);
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);
    
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(imageRef);
    
    return returnImage;
}



-(void)drawRect:(CGRect)rect{
    CGContextRef ctx=UIGraphicsGetCurrentContext();
    float width = (rect.size.width-4);
   
    
    if(_percentage!=-1){
        CGContextSetRGBStrokeColor(ctx, 239.0f/255.0f, 65.0f/255.0f, 98.0f/255.0f, 1);
        CGContextSetLineWidth(ctx, 2);
        CGContextAddArc(ctx, rect.size.width*0.5, rect.size.height*0.5, width*0.5, -M_PI_2, -2 * M_PI*(1-_percentage)-M_PI_2, 1);
        CGContextStrokePath(ctx);
        CGContextSetRGBStrokeColor(ctx, 239.0f/255.0f, 65.0f/255.0f, 98.0f/255.0f, 0.2);
        CGContextAddArc(ctx, rect.size.width*0.5, rect.size.height*0.5, width*0.5, -M_PI_2, -2 * M_PI*(1-_percentage)-M_PI_2, 0);
        CGContextStrokePath(ctx);
        
        CGColorSpaceRef rgbSpace = CGColorSpaceCreateDeviceRGB();
        CGFloat rgbValue[] = {1, 1, 1, 1};
        CGContextSetFillColorWithColor(ctx, CGColorCreate(rgbSpace, rgbValue));
        CGContextAddArc(ctx, rect.size.width*0.5+width*0.5*cos(-2 * M_PI*(1-_percentage)-M_PI_2), rect.size.width*0.5+width*0.5*sin(2 * M_PI*(1-_percentage)-M_PI_2), 2, 0, 2 * M_PI, 0);
    }
    else{
        CGContextSetRGBStrokeColor(ctx, 1,1,1,0.2f);
        CGContextSetLineWidth(ctx, 2);
        CGContextAddArc(ctx, rect.size.width*0.5, rect.size.height*0.5, width*0.5, 0, 2*M_PI, 0);
        CGContextStrokePath(ctx);
    }
    
    CGContextFillPath(ctx);
}

#pragma mark GB_LoadImageDelegate

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    if(tag < 0){
        [GB_NetWorkUtils useImage:[self blurryImage:[Static getFImage:image w:_icon.frame.size.width h:_icon.frame.size.height] withBlurLevel:4] container:container];
    }
    else{
        [GB_NetWorkUtils useImage:[Static getFImage:image w:_icon.frame.size.width h:_icon.frame.size.height] container:container];
    }
}

@end

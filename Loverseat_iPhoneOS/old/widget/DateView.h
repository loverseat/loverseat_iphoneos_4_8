//
//  DateView.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-10-10.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateView : UIView
- (id)initWithAvatar:(NSString *)avatar percentage:(float)percentage hasLock:(BOOL)hasLock hasModel:(BOOL)hasModel frame:(CGRect)frame delegate:(id)delegate action:(SEL)sel tag:(int)tag;
@end

//
//  Static.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Static : NSObject

+(NSInteger)getFontSize:(NSInteger)size;

+(UIFont *)getFont:(NSInteger)size isBold:(BOOL)isBold;

+(void)add:(UIView *)view msg:(NSString *)msg;
+(void)remove:(UIView *)view;
+(void)alert:(UIView *)view msg:(NSString *)msg;
+(id)getRequestData:(NSString *)str;




+(UIImage *)getFImage:(UIImage *)image w:(int)w h:(int)h;
//+(UIColor *)getCurrentSexColor;
+(UIColor *)getMaleColor;
+(NSString *)getVersion;
+(NSInteger )getVersionCode;
+(UIColor *)getBgColor;
+(UIColor *)getGrayColor;
+(UIColor *)getFemaleColor;
+(UIView *)getLine:(CGRect)rect;
+(UIView *)getLine:(CGRect)rect : (UIColor *)color;
+(int)getAgeByBirthday:(NSString *)birthday;
@end

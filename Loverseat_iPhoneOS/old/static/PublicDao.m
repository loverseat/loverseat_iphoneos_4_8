//
//  PublicDao.m
//  DongMai_iPhoneOS
//
//  Created by GaoHang on 14-10-28.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import "PublicDao.h"

@interface PublicDao()
@end
@implementation PublicDao
static PublicDao *dao = nil;
- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.codeTimeData = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dao = [[self alloc] init];
    });
    return dao;
}
@end

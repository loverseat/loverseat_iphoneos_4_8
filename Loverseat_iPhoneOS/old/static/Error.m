//
//  Error.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "Error.h"

@implementation Error

+(NSString *)getCurrentStr:(NSString *)str{
    return [ResponseUtils getCurrentStr:str];
}

+(BOOL)verify:(NSString *)str view:(UIView *)view{
    return [ResponseUtils check:str view:view];
}

+(BOOL)isRequestSuccess:(NSString *)str{
    return [ResponseUtils isRequestSuccess:str];
}

+(BOOL)isRequestErrType:(NSString *)data str:(NSString *)str{
    return [ResponseUtils isRequestErrType:data res:str];
}
@end

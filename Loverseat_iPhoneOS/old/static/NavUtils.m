//
//  NavUtils.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "NavUtils.h"
#import "BaseViewController.h"

@implementation NavUtils

+(void)addNavBgView:(BaseViewController *)viewController color:(UIColor *)color{
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [NavUtils getNavHeight])];
    iv.backgroundColor = color;
    [viewController.view addSubview:iv];
}

+(void)addNavBgView:(BaseViewController *)viewController{
    
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [NavUtils getNavHeight])];
    iv.image = [UIImage imageNamed:@"bg_nav"];
    viewController.iv = iv;
    [viewController.view addSubview:iv];
}

+(void)addNavTitleView:(BaseViewController *)viewController text:(NSString *)str{
    [self addNavTitleView:viewController text:str color:GB_UIColorFromRGB(251, 80, 87)];
}

+(void)addNavTitleView:(BaseViewController *)viewController text:(NSString *)str color:(UIColor *)color{
    UILabel *l = [GB_WidgetUtils getLabel:CGRectMake(60, 0, [GB_DeviceUtils getScreenWidth]-120, [NavUtils getNavHeight]) title:str font:[Static getFont:36 isBold:NO] color:color];
    l.textAlignment = NSTextAlignmentCenter;
    [viewController.view addSubview:l];
    viewController.titleL = l;
    viewController.l = l;
}

+(void)addNavTitleLine:(BaseViewController *)viewController{
    [viewController.view addSubview:[View getLine:CGRectMake(0, [self getNavHeight], [GB_DeviceUtils getScreenWidth], 0.5) color:[UIColor colorWithWhite:1 alpha:0.2f]]];
}

+(void)addLeftButton:(BaseViewController *)viewController icon:(UIImage *)icon sel:(SEL)sel{
    [NavUtils addLeftButton:viewController iconN:icon iconH:icon sel:sel];
}

+(void)addRightButton:(BaseViewController *)viewController icon:(UIImage *)icon sel:(SEL)sel{
    [NavUtils addRightButton:viewController iconN:icon iconH:icon sel:sel];
}

+(void)addLeftButton:(BaseViewController *)viewController title:(NSString *)str sel:(SEL)sel{
    [self addLeftButton:viewController title:str color:GB_UIColorFromRGB(251, 80, 87) sel:sel];
}

+(void)addLeftButton:(BaseViewController *)viewController title:(NSString *)str color:(UIColor *)color sel:(SEL)sel{
    UIButton *leftBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60+[GB_ToolUtils getTextWidth:str font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]-28, [NavUtils getNavHeight]) image:nil imageH:nil id:viewController sel:sel];
    leftBtn.titleLabel.font = [Static getFont:32 isBold:NO];
    [leftBtn setTitle:str forState:UIControlStateNormal];
    [leftBtn setTitle:str forState:UIControlStateHighlighted];
    [leftBtn setTitleColor:color forState:UIControlStateHighlighted];
    [leftBtn setTitleColor:color forState:UIControlStateNormal];
    [viewController.view addSubview:leftBtn];
    viewController.leftBtn = leftBtn;
}

+(void)addRightButton:(BaseViewController *)viewController title:(NSString *)str color:(UIColor *)color sel:(SEL)sel{
    UIButton *rightBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-60-[GB_ToolUtils getTextWidth:str font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]+28, 2, 60+[GB_ToolUtils getTextWidth:str font:[Static getFont:32 isBold:NO] size:CGSizeMake(INT_MAX, INT_MAX)]-28, [NavUtils getNavHeight]) image:nil imageH:nil id:viewController sel:sel];
    rightBtn.titleLabel.font = [Static getFont:32 isBold:NO];
    [rightBtn setTitle:str forState:UIControlStateNormal];
    [rightBtn setTitle:str forState:UIControlStateHighlighted];
    [rightBtn setTitleColor:color forState:UIControlStateHighlighted];
    [rightBtn setTitleColor:color forState:UIControlStateNormal];
    [viewController.view addSubview:rightBtn];
    viewController.rightBtn = rightBtn;
}

+(void)addRightButton:(BaseViewController *)viewController title:(NSString *)str sel:(SEL)sel{
    [self addRightButton:viewController title:str color:GB_UIColorFromRGB(251, 80, 87) sel:sel];
}

+(void)addBackButton:(BaseViewController *)viewController{
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60.0f, [self getNavHeight]) image:[UIImage imageNamed:@"btn_back_rose"] imageH:nil id:viewController sel:@selector(cb_back)];
    [viewController.view addSubview:navBackBtn];
    viewController.leftBtn = navBackBtn;
    viewController.navBackBtn = navBackBtn;
}

+(void)addBackWhiteButton:(BaseViewController *)viewController{
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60.0f, [self getNavHeight]) image:nil imageH:nil id:viewController sel:@selector(cb_back)];
    [navBackBtn setImage:[UIImage imageNamed:@"btn_back_white"] forState:UIControlStateNormal];
    [viewController.view addSubview:navBackBtn];
    viewController.leftBtn = navBackBtn;
}

+(void)addBackGrayButton:(BaseViewController *)viewController{
    UIButton *navBackBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 40.0f, [self getNavHeight]) image:nil imageH:nil id:viewController sel:@selector(cb_back)];
    [navBackBtn setImage:[UIImage imageNamed:@"btn_back_gray"] forState:UIControlStateNormal];
    [viewController.view addSubview:navBackBtn];
    viewController.leftBtn = navBackBtn;
}

+(void)addCloseButton:(BaseViewController *)viewController{
    UIButton *closeBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-43.0f, 0, 43.0f, 43.0f) image:[UIImage imageNamed:@"btn_close"] imageH:nil id:viewController sel:@selector(cb_back)];
    [viewController.view addSubview:closeBtn];
    viewController.rightBtn = closeBtn;
}

+(void)addBackground:(BaseViewController *)viewController{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], 568)];
    bg.image = [UIImage imageNamed:@"bg_date"];
    [viewController.view addSubview:bg];
}

+(void)addLeftButton:(BaseViewController *)viewController iconN:(UIImage *)iconN iconH:(UIImage *)iconH sel:(SEL)sel{
    int w = 70;
    int h = [NavUtils getNavHeight];
    if([GB_ToolUtils isNotBlank:iconN]){
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), NO, 0.0);
        [iconN drawInRect:CGRectIntegral(CGRectMake(w*0.5-iconN.size.width*0.5,h*0.5-iconN.size.height*0.5,iconN.size.width,iconN.size.height))];
        iconN = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    if([GB_ToolUtils isNotBlank:iconH]){
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), NO, 0.0);
        [iconH drawInRect:CGRectIntegral(CGRectMake(w*0.5-iconH.size.width*0.5,h*0.5-iconH.size.height*0.5,iconH.size.width,iconH.size.height))];
        iconH = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIButton *leftBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 60, 55) image:iconN imageH:iconH id:viewController sel:sel];
    [viewController.view addSubview:leftBtn];
    viewController.leftBtn = leftBtn;
}

+(void)addRightButton:(BaseViewController *)viewController iconN:(UIImage *)iconN iconH:(UIImage *)iconH sel:(SEL)sel{
    UIButton *rightBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-50, 0, 50, [NavUtils getNavHeight]) image:nil imageH:nil id:viewController sel:sel];
    [rightBtn setImage:iconN forState:UIControlStateNormal];
    [rightBtn setImage:iconH forState:UIControlStateHighlighted];
    [viewController.view addSubview:rightBtn];
    viewController.rightBtn = rightBtn;
}

+(CGFloat)getNavHeight{
    return 55;
}


@end

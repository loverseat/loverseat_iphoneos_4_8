//
//  View.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-28.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface View : NSObject
+(UIView *)getLine:(CGRect)rect;
+(UIView *)getSpearLine:(CGRect)rect;
+(UIView *)getLine:(UIImage *)image rect:(CGRect)rect;
+(UIView *)getLine:(CGRect)rect color:(UIColor *)color;
@end

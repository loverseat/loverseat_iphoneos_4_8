//
//  Url+old.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//
#import "User.h"
#import "Url.h"
@interface Url (_old)
+(NSString *)getEventListUrl;
+(NSString *)getEventDetailUrl;
+(NSString *)getEventLikeListUrl:(NSString *)event_id;
+(NSString *)getCommentRemarkUrl;
+(NSString *)getEventInterestUrl;
+(NSString *)getShareUrl:(NSString *)eventId;
+(NSString *)getUserLoginUrl;
+(NSString *)getCheckUserMobileUrl;
+(NSString *)getUserRegisterUrl;
+(NSString *)getRestPwdUrl;
+(NSString *)getforgetPwdUrl;
+(NSString *)getUserInfoUpdateUrl;
+(NSString *)getCancelInviteHistory;
+(NSString *)getInviteDetail;
+(NSString *)getPersonHallInfoUrl;
+(NSString *)getCommentReportUrl;
+(NSString *)getValidTopicsUrl;
+(NSString *)getTopicResultUrl;

+(NSString *)getMorePersonHallDateUrl:(int)number;
+(NSString *)getUserOtherLoginUrl;
+(NSString *)getRegUrl;

+(NSString *)getFileUploadUrl;

+(NSString *)getCommentsUrl;
+(NSString *)getAddCommentsUrl;

+(NSString *)getMessageListUrl;
+(NSString *)getTicketPocketListUrl;
+(NSString *)getOrderDetailUrl;
+(NSString *)getMessageDetailUrl;
+(NSString *)getAddMessageUrl;
+(NSString *)getLastestMessageUrl;

+(NSString *)getQRCodeUrl:(NSString *)sn;
+(NSString *)getDealAlbumUrl;
+(NSString *)getCityUrl;
+(NSString *)getFeedBackUrl;
+(NSString *)getPayOrderUrl;
+(NSString *)getResetWalletPasswordUrl;
+(NSString *)getWalletCashUrl;
+(NSString *)getTicketInfoUrl;
+(NSString *)getTicketBuyUrl;
+(NSString *)getWalletOrderPayUrl;
+(NSString *)getUpdateBuyUpdateUrl;
+(NSString *)getVerionInfoUrl;
+(NSString *)getHelpInfoUrl;
+(NSString *)getUpgradeUrl;
+(NSString *)getTokenUrl;
+(NSString *)getLogoutUrl;
+(NSArray *)getParmsArr;

//二期

+(NSString *)getWalletCoinHistoryUrl;
+(NSString *)getWalletInfoUrl;

+(NSString *)getUserInfoUrl;
+(NSString *)getInviteHistoryUrl;
+(NSString *)getCheckBuyInviteStatusUrl;
+(NSString *)getEventOtherPersonUrl;
+(NSString *)getBuyInviteUrl;
+(NSString *)getCoinCategoryUrl;
+(NSString *)getUserBuyAuthorityUrl;
+(NSString *)getFemaleInviteHistoryUrl;
+(NSString *)getUpdateBuyInviteUrl;
+(NSString *)getCheckEventBuyStatusUrl;
+(NSString *)getAlipayNotifyUrl;
+(NSString *)getLoginInfoUrl;
+(NSString *)getEventDetaiSpeciallUrl;
+(NSString *)getInvitePersonUrl;
+(NSString *)getEventPersonUrl;
+(NSString *)getInviteTargetPersonUrl;
+(NSString *)getAllSchedulesUrl:(NSString *)eventId;
+(NSString *)getIgnoreInviteUrl;
+(NSString *)getDeleAppMessageUrl;
+(NSString *)getSuccessInviteDetailUrl;

+(NSString *)getWalletRechargeUrl;
@end

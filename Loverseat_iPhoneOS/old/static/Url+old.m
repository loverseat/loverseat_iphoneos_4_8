//
//  Url+old.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "Url+old.h"


@implementation Url (_old)
//干净的接口
+(NSString *)getEventListUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_event_list/",[self getBaseUrl]];
}

+(NSString *)getEventDetailUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_event_detail/",[self getBaseUrl]];
}

+(NSString *)getEventLikeListUrl:(NSString *)event_id{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_event_like_list/%@",[self getBaseUrl],event_id];
}

+(NSString *)getCommentRemarkUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/comment_remark",[self getBaseUrl]];
}

+(NSString *)getCommentReportUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/comment_report",[self getBaseUrl]];
}




+(NSString *)getEventInterestUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/event_interest/",[self getBaseUrl]];
}


#pragma mark - 分享的链接地址  http://www.7dianban.cn/index.php?c=lover_seat&m=event_detail&id=2722&sys_type=0
+(NSString *)getShareUrl:(NSString *)eventId
{
    //    if(![User isLogin])return [NSString stringWithFormat:@"%@/showapi_v3/share/",[self getBaseUrl]];
    //    return [NSString stringWithFormat:@"%@/showapi_v3/share/%@/%@",[self getBaseUrl],[User getUserInfo].uid,eventId];
    
    if(![User isLogin])return [NSString stringWithFormat:@"%@/showapi_v3/share/",[self getBaseUrl]];
    return [NSString stringWithFormat:@"%@/index.php?c=lover_seat&m=event_detail&id=%@&sys_type=0",[self getBaseUrl],eventId];
}

+(NSString *)getUserLoginUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/user_login/",[self getBaseUrl]];
}

+(NSString *)getUserOtherLoginUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/app_register",[self getBaseUrl]];
}

+(NSString *)getCheckUserMobileUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/check_user_mobile/",[self getBaseUrl]];
}

+(NSString *)getUserRegisterUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/user_register/",[self getBaseUrl]];
}

+(NSString *)getRestPwdUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/reset_password/",[self getBaseUrl]];
}

+(NSString *)getforgetPwdUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/forgot_password/",[self getBaseUrl]];
}

+(NSString *)getUserInfoUpdateUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/update_userinfo/",[self getBaseUrl]];
}

+(NSString *)getInviteDetail{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_invite_detail/",[self getBaseUrl]];
}
+(NSString *)getMorePersonHallDateUrl:(int)number{
    return [NSString stringWithFormat:@"%@/app_user_api/ajax_get_threads/%d",[self getBaseUrl],number];
}



+(NSString *)getCityUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_province_city/",[self getBaseUrl]];
}

+(NSString *)getRegUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/verify_captcha/",[self getBaseUrl]];
}


+(NSString *)getFileUploadUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/file_upload/",[self getBaseUrl]];
}

+(NSString *)getLogoutUrl{
    return [NSString stringWithFormat:@"%@/index.php?c=showapi_v3&m=user_logout&uid=%@",[self getBaseUrl],[User getUserInfo].uid];
}

+(NSString *)getCommentsUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_comments/",[self getBaseUrl]];
}

+(NSString *)getAddCommentsUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/add_comments/",[self getBaseUrl]];
}

+(NSString *)getMessageListUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_all_message/",[self getBaseUrl]];
}

+(NSString *)getTicketPocketListUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/ticket_pocket/",[self getBaseUrl]];
}

+(NSString *)getOrderDetailUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_order_detail/",[self getBaseUrl]];
}

+(NSString *)getMessageDetailUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_message_detail/",[self getBaseUrl]];
}

+(NSString *)getAddMessageUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/add_message/",[self getBaseUrl]];
}

+(NSString *)getLastestMessageUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_lastest_message/%@",[self getBaseUrl],[User getUserInfo].uid];
}

+(NSString *)getQRCodeUrl:(NSString *)sn{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_qrcode/%@",[self getBaseUrl],sn];
}

+(NSString *)getDealAlbumUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/deal_album",[self getBaseUrl]];
}

+(NSString *)getFeedBackUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/app_feedback",[self getBaseUrl]];
}

+(NSString *)getPayOrderUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/pay_order",[self getBaseUrl]];
}

+(NSString *)getResetWalletPasswordUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/reset_wallet_password",[self getBaseUrl]];
}

+(NSString *)getWalletCashUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/wallet_cash",[self getBaseUrl]];
}

+(NSString *)getTicketInfoUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_ticket_info",[self getBaseUrl]];
}

+(NSString *)getTicketBuyUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/buy_ticket",[self getBaseUrl]];
}

+(NSString *)getWalletOrderPayUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/wallet_pay_order",[self getBaseUrl]];
}

+(NSString *)getUpdateBuyUpdateUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/update_buy_invite",[self getBaseUrl]];
}

+(NSString *)getVerionInfoUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_version",[self getBaseUrl]];
}

+(NSString *)getHelpInfoUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/app_help_info",[self getBaseUrl]];
}


+(NSString *)getUpgradeUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/app_upgrade_info",[self getBaseUrl]];
}


+(NSString *)getTokenUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/deal_token",[self getBaseUrl]];
}


//二期
+(NSString *)getWalletInfoUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_wallet_info/",[self getBaseUrl]];
}

+(NSString *)getWalletCoinHistoryUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_coin_history/",[self getBaseUrl]];
}

+(NSString *)getUserInfoUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_userinfo/",[self getBaseUrl]];
}

+(NSString *)getInviteHistoryUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_invite_history/",[self getBaseUrl]];
}

+(NSString *)getCheckBuyInviteStatusUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/check_buy_invite_status/",[self getBaseUrl]];
}

+(NSString *)getEventOtherPersonUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_event_other_person/",[self getBaseUrl]];
}

+(NSString *)getBuyInviteUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/buy_invite/",[self getBaseUrl]];
}

+(NSString *)getCoinCategoryUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_coin_category/",[self getBaseUrl]];
}

+(NSString *)getUserBuyAuthorityUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/user_buy_authority/",[self getBaseUrl]];
}

+(NSString *)getFemaleInviteHistoryUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_female_invite_history/",[self getBaseUrl]];
}

+(NSString *)getUpdateBuyInviteUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/update_buy_invite/",[self getBaseUrl]];
}

+(NSString *)getCheckEventBuyStatusUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/check_event_buy_status/",[self getBaseUrl]];
}

+(NSString *)getWalletRechargeUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/wallet_recharge/",[self getBaseUrl]];
}

+(NSString *)getAlipayNotifyUrl{
    return [NSString stringWithFormat:@"%@/alipay_app/notify_url.php",[self getBaseUrl]];
}

+(NSString *)getLoginInfoUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_login_info/",[self getBaseUrl]];
}


+(NSString *)getEventDetaiSpeciallUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_event_detail_special/",[self getBaseUrl]];
}


+(NSString *)getEventPersonUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_event_person/",[self getBaseUrl]];
}

+(NSString *)getInvitePersonUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_invite_person/",[self getBaseUrl]];
}

+(NSString *)getInviteTargetPersonUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/invite_target_person/",[self getBaseUrl]];
}

+(NSString *)getAllSchedulesUrl:(NSString *)eventId{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_all_schedules/%@",[self getBaseUrl],eventId];
}

+(NSString *)getIgnoreInviteUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/ignore_invite/",[self getBaseUrl]];
}


+(NSString *)getDeleAppMessageUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/delete_app_message/",[self getBaseUrl]];
}


+(NSString *)getSuccessInviteDetailUrl{
    return [NSString stringWithFormat:@"%@/showapi_v3/get_success_invite_detail/",[self getBaseUrl]];
}


+(NSString *)getCancelInviteHistory{
    return [NSString stringWithFormat:@"%@/showapi_v3/cancel_invite_history/",[self getBaseUrl]];
}



+(NSString *)getPersonHallInfoUrl{
    return [NSString stringWithFormat:@"%@/app_user_api/user_lobby",[self getBaseUrl]];
}
@end

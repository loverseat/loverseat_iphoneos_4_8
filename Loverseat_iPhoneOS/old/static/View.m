//
//  View.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-28.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "View.h"
#import <GeekBean4IOS/GB_GeekBean.h>

@implementation View
+(UIView *)getLine:(CGRect)rect{
    UIView *line = [[UIView alloc]initWithFrame:rect];
    line.userInteractionEnabled = NO;
    line.backgroundColor = GB_UIColorFromRGB(53, 56, 59);
    return line;
}

+(UIView *)getLine:(CGRect)rect color:(UIColor *)color{
    UIView *line = [[UIView alloc]initWithFrame:rect];
    line.userInteractionEnabled = NO;
    line.backgroundColor = color;
    return line;
}

+(UIView *)getSpearLine:(CGRect)rect{
    UIView *line = [[UIView alloc]initWithFrame:rect];
    line.userInteractionEnabled = NO;
    line.backgroundColor = GB_UIColorFromRGB(206, 209, 214);
    return line;
}

+(UIView *)getLine:(UIImage *)image rect:(CGRect)rect{
    UIImageView *line = [[UIImageView alloc]initWithFrame:rect];
    line.userInteractionEnabled = NO;
    line.image = image;
    return line;
}
@end

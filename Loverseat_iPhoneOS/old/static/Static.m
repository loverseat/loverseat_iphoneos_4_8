//
//  Static.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "Static.h"
#import <GeekBean4IOS/GB_GeekBean.h>
#import "Error.h"
#import "User.h"

@implementation Static

+(NSString *)getVersion{
    return @"v1.4.1";
}

+(NSInteger )getVersionCode{
    return 9;
}


+(NSInteger)getFontSize:(NSInteger)size{
    return size/2;
}

+(UIView *)getLine:(CGRect)rect : (UIColor *)color{
    UIView *line = [[UIView alloc]initWithFrame:rect];
    line.backgroundColor = color;
    return line;
}

+(UIView *)getLine:(CGRect)rect{
    UIView *line = [[UIView alloc]initWithFrame:rect];
    line.backgroundColor = GB_UIColorFromRGB(200, 199, 204);
    return line;
}

+(UIFont *)getFont:(NSInteger)size isBold:(BOOL)isBold{
    return isBold?[UIFont boldSystemFontOfSize:[Static getFontSize:size]]:[UIFont systemFontOfSize:[Static getFontSize:size]];
}

+(void)add:(UIView *)view msg:(NSString *)msg{
    [AlertUtils add:view msg:msg];
}

+(void)remove:(UIView *)view{
    [AlertUtils remove:view];
}

+(void)alert:(UIView *)view msg:(NSString *)msg{
    [AlertUtils alert:view msg:msg];
}

+(id)getRequestData:(NSString *)str{
    return [ResponseUtils getRequestData:str];
}

//w为显示框宽度、h为显示框高度
+(UIImage *)getFImage:(UIImage *)image w:(int)w h:(int)h{
    float _w = image.size.width;
    float _h = image.size.height;
    float __w = 0.0f;
    float __h = 0.0f;
    float __x = 0.0f;
    float __y = 0.0f;
    float scale = .1f;
    if(_w/w>_h/h){
        //横
        scale = _h/h;
        __w = _w/scale;
        __h = h;
        __x = (__w-w)*0.5f;
        __y = 0;
    }
    else{
        //竖
        scale = _w/w;
        __h = _h/scale;
        __w = w;
        __y = (__h-h)*0.5f;
        __x = 0;
    }
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), NO, 0.0);
    [image drawInRect:CGRectIntegral(CGRectMake(-1*__x, -1*__y, __w, __h))];
    UIImage *ALayoutImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return ALayoutImage;
}

+(int)getAgeByBirthday:(NSString *)birthday{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"yyyy-MM-dd"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
    NSDate *date =[dateFormat dateFromString:birthday];
    
     return [[GB_DateUtils getFormatStringByNow:@"yyyy"] intValue]-[[GB_DateUtils getFormatStringBy10Median:@"yyyy" timeMillis:[date timeIntervalSince1970]] intValue];
}
//
//+(UIColor *)getCurrentSexColor{
//    return [User isMale]?[Static getMaleColor]:[Static getFemaleColor];
//}

+(UIColor *)getMaleColor{
    return GB_UIColorFromRGB(0, 132, 255);
}

+(UIColor *)getFemaleColor{
    return GB_UIColorFromRGB(255, 49, 107);
}

+(UIColor *)getBgColor{
    return GB_UIColorFromRGB(236, 236, 236);
}

+(UIColor *)getGrayColor{
    return GB_UIColorFromRGB(178, 178, 178);
}
@end

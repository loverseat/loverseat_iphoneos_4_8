//
//  PublicDao.h
//  DongMai_iPhoneOS
//
//  Created by GaoHang on 14-10-28.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//


@class HomeViewController;
@interface PublicDao : NSObject

#define kCode @"k_code"

@property (nonatomic, weak) HomeViewController *rootViewController;
+ (instancetype)shareInstance;
@property (strong, nonatomic)NSMutableDictionary *codeTimeData;
@end

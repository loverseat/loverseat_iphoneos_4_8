//
//  User.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-24.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "User.h"
#import "UserBean.h"
#import "HomeViewController.h"

@implementation User

+(UserBean *)getUserInfo{
    return [UserBean getBean:[GB_SharedPreferenceUtils getObjectForKey:k_User]];
}

+(void)login:(NSDictionary *)dic{
    [GB_SharedPreferenceUtils setObject:dic forKey:k_User];
}

+(void)logout{
    [GB_SharedPreferenceUtils removeObjectForKey:k_User];
}

+(BOOL)isLogin{
    return [GB_SharedPreferenceUtils hasObject:k_User];
}

+(NSString *)getUserToken{
    return [GB_SharedPreferenceUtils getObjectForKey:k_Token];
}

+(void)setUserToken:(NSString *)token{
    [GB_SharedPreferenceUtils setObject:token forKey:k_Token];
}

+(BOOL)checkLogin{
    if(![User isLogin]){
        [[PublicDao shareInstance].rootViewController popLoginViewController];
    }
    return [GB_SharedPreferenceUtils hasObject:k_User];
}

+(BOOL)isMale{
    return [[User getUserInfo].tu_gender intValue]==0;
}

@end

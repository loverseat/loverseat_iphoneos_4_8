//
//  Error.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Error : NSObject
+(BOOL)verify:(NSString *)str view:(UIView *)view;

+(BOOL)isRequestSuccess:(NSString *)str;

+(BOOL)isRequestErrType:(NSString *)data str:(NSString *)str;

+(NSString *)getCurrentStr:(NSString *)str;
@end

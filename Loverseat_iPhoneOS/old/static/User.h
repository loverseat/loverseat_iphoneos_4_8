//
//  User.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-24.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserBean.h"

#define k_User @"kUser"
#define k_Token @"kToken"

@interface User : NSObject

+(NSString *)getUserToken;

+(void)setUserToken:(NSString *)token;

+(UserBean *)getUserInfo;

+(void)login:(NSDictionary *)dic;

+(void)logout;

+(BOOL)isLogin;

+(BOOL)isMale;

+(BOOL)checkLogin;

@end

//
//  NavUtils.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-6.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseViewController;
@interface NavUtils : NSObject

+(void)addNavBgView:(BaseViewController *)viewController;

+(void)addNavBgView:(BaseViewController *)viewController color:(UIColor *)color;

+(void)addNavTitleView:(BaseViewController *)viewController text:(NSString *)str;

+(void)addNavTitleView:(BaseViewController *)viewController text:(NSString *)str color:(UIColor *)color;

+(void)addNavTitleLine:(BaseViewController *)viewController;

+(void)addLeftButton:(BaseViewController *)viewController icon:(UIImage *)icon sel:(SEL)sel;

+(void)addRightButton:(BaseViewController *)viewController icon:(UIImage *)icon sel:(SEL)sel;

+(void)addLeftButton:(BaseViewController *)viewController title:(NSString *)str sel:(SEL)sel;

+(void)addLeftButton:(BaseViewController *)viewController title:(NSString *)str color:(UIColor *)color sel:(SEL)sel;

+(void)addRightButton:(BaseViewController *)viewController title:(NSString *)str sel:(SEL)sel;

+(void)addLeftButton:(BaseViewController *)viewController iconN:(UIImage *)iconN iconH:(UIImage *)iconH sel:(SEL)sel;

+(void)addRightButton:(BaseViewController *)viewController iconN:(UIImage *)iconN iconH:(UIImage *)iconH sel:(SEL)sel;

+(void)addRightButton:(BaseViewController *)viewController title:(NSString *)str color:(UIColor *)color sel:(SEL)sel;

+(void)addBackground:(BaseViewController *)viewController;

+(CGFloat)getNavHeight;

+(void)addBackButton:(BaseViewController *)viewController;

+(void)addBackWhiteButton:(BaseViewController *)viewController;

+(void)addBackGrayButton:(BaseViewController *)viewController;

+(void)addCloseButton:(BaseViewController *)viewController;

@end

//
//  AppDelegate+Home.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/9.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "AppDelegate.h"
@interface AppDelegate (home)
- (void)homeApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
@end

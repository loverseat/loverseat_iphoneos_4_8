//
//  AppDelegate+UMeng.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/9.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//


#import "AppDelegate.h"
#import "WXApi.h"
@interface AppDelegate (umeng)<WXApiDelegate>
- (void)umengApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (BOOL)umengApplication:(UIApplication *)application handleOpenURLU:(NSURL *)url;
- (BOOL)umengApplication:(UIApplication *)application
                 openURL:(NSURL *)url
       sourceApplication:(NSString *)sourceApplication
              annotation:(id)annotation;
@end

//
//  Url.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "Url.h"

@implementation Url

+(NSString *)getBaseUrl{
    return @"http://www.7dianban.cn";
}

+(NSArray *)getParmsArr{
    NSMutableArray *arr = [NSMutableArray array];
    return arr;
}

+(NSString *)getImageUrl:(NSString *)url{
    if([url hasPrefix:@"http"])return url;
    if(![url hasPrefix:@"/"]){
        url = [NSString stringWithFormat:@"/%@",url];
    }
    return [NSString stringWithFormat:@"%@%@",@"http://www.7dianban.cn",url];
}





@end

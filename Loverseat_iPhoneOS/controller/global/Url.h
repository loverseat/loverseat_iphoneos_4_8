//
//  Url.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-20.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

@interface Url : NSObject
+(NSString *)getBaseUrl;
+(NSArray *)getParmsArr;
+(NSString *)getImageUrl:(NSString *)url;
@end

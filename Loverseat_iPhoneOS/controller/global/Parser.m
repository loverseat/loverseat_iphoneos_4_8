//
//  Parser.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/1.
//  Copyright (c) 2015年 LEIZHEN. All rights reserved.
//

#import "Parser.h"


@implementation Parser

+(Parser *)sharedInstance{
    static Parser *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Parser alloc]init];
    });
    return sharedInstance;
}

@end

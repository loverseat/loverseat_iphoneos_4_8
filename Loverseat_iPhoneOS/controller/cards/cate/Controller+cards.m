//
//  Controller+cards.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "Controller+cards.h"
#import "CardsFlashViewController.h"

@implementation Controller (cards)

+(void)cards:(UINavigationController *)nav{
    CardsFlashViewController *con = [[CardsFlashViewController alloc]init];
    [nav pushViewController:con animated:YES];
}

@end

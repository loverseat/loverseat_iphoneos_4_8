//
//  Controller+cards.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

@interface Controller (cards)

+(void)cards:(UINavigationController *)nav;

@end

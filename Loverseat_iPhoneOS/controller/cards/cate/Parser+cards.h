//
//  Parser+cards.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/1.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

@class CardsFlashBean;
@interface Parser (cards)
-(CardsFlashBean *)flashBeanParser:(NSString *)str;
@end
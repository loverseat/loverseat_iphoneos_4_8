//
//  Parser+cards.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/1.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "Parser+cards.h"
#import "CardsFlashBean.h"

@implementation Parser(cards)
-(CardsFlashBean *)flashBeanParser:(NSString *)str{
    return [CardsFlashBean getBean:[[GB_JsonUtils getDictionaryByJsonString:str] objectForKey:@"topic_today"]];
}
@end

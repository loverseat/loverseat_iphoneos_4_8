//
//  Url+cards.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "Url+cards.h"

@implementation Url (cards)
+(NSString *)getValidTopicsUrl{
    return [NSString stringWithFormat:@"%@/app_user_api/get_valid_topics/",[self getBaseUrl]];
}

+(NSString *)getTopicResultUrl{
    return [NSString stringWithFormat:@"%@/app_user_api/topic_result/",[self getBaseUrl]];
}
@end

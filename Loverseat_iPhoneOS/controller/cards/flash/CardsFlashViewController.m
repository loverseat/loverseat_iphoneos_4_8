//
//  FlashViewController.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/3/31.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "CardsFlashViewController.h"
#import "CardsFlashBean.h"
#import "CardsFlashView.h"
#import "Parser+cards.h"
#import "Url+cards.h"
#import "CardsFlashViewDelegate.h"

@interface CardsFlashViewController ()
<GB_NetWorkDelegate, CardsFlashViewDelegate>

@property (nonatomic, strong) CardsFlashBean *dataSource;

@property (nonatomic, strong) NSMutableDictionary *viewDic;

@property (nonatomic, assign) NSInteger index;

@end

@implementation CardsFlashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
}

#pragma mark ui

-(void)initFrame{
    
}

-(NSMutableDictionary *)viewDic{
    if(_viewDic == nil){
        _viewDic = [NSMutableDictionary dictionary];
    }
    return _viewDic;
}

#pragma mark data

-(void)initData{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        [AlertUtils add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getValidTopicsUrl] list:arr delegate:self tag:1];
    }
}

-(void)refresh{
    [self refresh:nil];
}

-(void)refresh:(CardsFlashBean *)dataSource{
    if(dataSource!=nil)
        self.dataSource = dataSource;
    if([GB_ToolUtils isNotBlank:self.dataSource.topic_today]){
        NSInteger i = 0;
        for (CardsFlashEventBean *bean in self.dataSource.topic_today) {
            CardsFlashView *fv = nil;
            if([[self.viewDic allKeys] containsObject:bean.tad_id]){
                fv = [self.viewDic objectForKey:bean.tad_id];
            }
            else{
                fv = [[CardsFlashView alloc] initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]) : self.dataSource : bean];
                fv.delegate = self;
                [self.viewDic setObject:fv forKey:bean.tad_id];
            }
            if(_index == i){
                [fv showUI];
                if(![fv superview]){
                    [self.view addSubview:fv];
                }
            }
            else{
                [fv hideUI];
            }
            i++;
        }
    }
}

#pragma mark delegate

-(void)GB_didSelect{
    if(self.index < self.dataSource.topic_today.count-1){
        self.index ++ ;
        [self refresh];
    }
    else{
        if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
            [AlertUtils add:self.navigationController.view msg:@"正在加载"];
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
            NSMutableDictionary *resultDic = [NSMutableDictionary dictionary];
            [resultDic setObject:self.dataSource.tt_id forKeyedSubscript:@"topic_id"];
            [resultDic setObject:[User getUserInfo].uid forKeyedSubscript:@"uid"];
            NSMutableArray *resultArr = [NSMutableArray array];
            for (CardsFlashView *fv in [self.viewDic allValues]) {
                [resultArr addObject:@{@"event_id":[fv eventId],@"item":[fv result]}];
            }
            [resultDic setObject:resultArr forKeyedSubscript:@"final_result"];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"final_result" value:[GB_JsonUtils getJsonStringByArray:resultArr]]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"topic_id" value:self.dataSource.tt_id]];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
            [GB_NetWorkUtils startPostAsyncRequest:[Url getTopicResultUrl] list:arr delegate:self tag:2];
        }
        
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [AlertUtils remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [AlertUtils remove:self.navigationController.view];
    if([ResponseUtils check:str view:self.navigationController.view]){
        if(tag == 1){
            self.index = 0;
            [self refresh:[[Parser sharedInstance] flashBeanParser:str]];
        }
        if(tag == 2){
            
        }
    }
}

@end

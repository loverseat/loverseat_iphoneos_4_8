//
//  FlashView.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/2.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardsFlashViewDelegate.h"

@class CardsFlashEventBean;
@class CardsFlashBean;
@interface CardsFlashView : UIView

@property (nonatomic, weak) id<CardsFlashViewDelegate> delegate;

-(instancetype)initWithFrame:(CGRect)frame :(CardsFlashBean *)dayDataSource : (CardsFlashEventBean *)eventDataSource;
-(NSString *)result;
-(NSString *)eventId;
-(void)showUI;
-(void)hideUI;

@end

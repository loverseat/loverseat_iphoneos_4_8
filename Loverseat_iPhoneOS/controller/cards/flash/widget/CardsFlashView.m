//
//  FlashView.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/2.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "CardsFlashView.h"
#import "CardsFlashEventBean.h"
#import "CardsFlashBean.h"

@interface CardsFlashView()
<GB_LoadImageDelegate>

@property (nonatomic, strong) CardsFlashEventBean *eventDataSource;
@property (nonatomic, strong) CardsFlashBean *dayDataSource;

@property (nonatomic, assign) BOOL isCreated;
@property (nonatomic, strong) UIImageView *bg,*img,*model,*top,*cover;
@property (nonatomic, strong) UILabel *title,*week,*day;

@property (nonatomic, strong) UIButton *yes,*no,*unknown,*favorite;

@property (nonatomic, strong) UIPageControl *control;

@property (nonatomic, assign) NSInteger current;

@end

@implementation CardsFlashView

-(instancetype)initWithFrame:(CGRect)frame :(CardsFlashBean *)dayDataSource : (CardsFlashEventBean *)eventDataSource{
    self = [super initWithFrame:frame];
    if(self){
        self.eventDataSource = eventDataSource;
        self.dayDataSource = dayDataSource;
    }
    return self;
}

-(UIImageView *)bg{
    if(!_bg){
        UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight])];
        [self addSubview:bg];
        self.bg = bg;
    }
    return _bg;
}

-(UIImageView *)img{
    if(!_img){
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(-54, [GB_DeviceUtils getScreenHeight]*0.5f-155.75f, 311.5f, 311.5f)];
        img.layer.cornerRadius = 155.75f;
        img.layer.masksToBounds = YES;
        [self addSubview:img];
        self.img = img;
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(59, 240, 200, 20)];
        title.font = [UIFont systemFontOfSize:18];
        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor whiteColor];
        title.text = self.eventDataSource.tad_show_name;
        [_img addSubview:title];
        
        UILabel *desc = [[UILabel alloc]initWithFrame:CGRectMake(79, 264, 160, 30)];
        desc.font = [UIFont systemFontOfSize:11];
        desc.numberOfLines = 2;
        desc.textAlignment = NSTextAlignmentCenter;
        desc.textColor = [UIColor whiteColor];
        desc.text = self.eventDataSource.tad_intro;
        [_img addSubview:desc];
    }
    return _img;
}

-(UIImageView *)model{
    if(!_model){
        UIImageView *model = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.img.frame.origin.y-42.5f, 320, 423)];
        model.image = [UIImage imageNamed:@"model_cards_flash_content.png"];
        [self addSubview:model];
        self.model = model;
    }
    return _model;
}

-(UIImageView *)top{
    if(!_top){
        UIImageView *top = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 59.5f)];
        top.image = [UIImage imageNamed:@"bg_cards_flash_top.png"];
        [self addSubview:top];
        self.top = top;
    }
    return _top;
}

-(UIImageView *)icon{
    if(!_cover){
        UIImageView *cover = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 55)];
        [self addSubview:cover];
        self.cover = cover;
    }
    return _cover;
}

-(UILabel *)day{
    if(!_day){
        UILabel *day = [[UILabel alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]-33-15, 12, 33, 20)];
        day.font = [UIFont systemFontOfSize:24];
        day.textAlignment = NSTextAlignmentCenter;
        day.textColor = [UIColor whiteColor];
        day.text = self.dayDataSource.tt_day;
        [self addSubview:day];
        self.day = day;
    }
    return _day;
}

-(UILabel *)week{
    if(!_week){
        UILabel *week = [[UILabel alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]-33-15, 32, 33, 18)];
        week.font = [UIFont systemFontOfSize:14];
        week.textAlignment = NSTextAlignmentCenter;
        week.textColor = [UIColor whiteColor];
        week.text = self.dayDataSource.tt_week;
        [self addSubview:week];
        self.week = week;
    }
    return _week;
}

-(UILabel *)title{
    if(!_title){
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, [GB_DeviceUtils getScreenWidth]-100, 55)];
        title.font = [UIFont systemFontOfSize:18];
        title.textColor = [UIColor whiteColor];
        title.text = self.dayDataSource.tt_title;
        [self addSubview:title];
        self.title = title;
    }
    return _title;
}


-(UIButton *)yes{
    if(!_yes){
        UIButton *yes = [[UIButton alloc]initWithFrame:CGRectMake(260, self.img.frame.origin.y+260, 50, 50)];
        yes.tag = 4;
        [yes setImage:[UIImage imageNamed:@"btn_cards_flash_yes.png"] forState:UIControlStateNormal];
        [yes addTarget:self action:@selector(c_action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:yes];
        self.yes = yes;
    }
    return _yes;
}
-(UIButton *)no{
    if(!_no){
        UIButton *no = [[UIButton alloc]initWithFrame:CGRectMake(198, self.img.frame.origin.y+320, 50, 50)];
        no.tag = 3;
        [no setImage:[UIImage imageNamed:@"btn_cards_flash_no.png"] forState:UIControlStateNormal];
        [no addTarget:self action:@selector(c_action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:no];
        self.no = no;
    }
    return _no;
}

-(UIButton *)unknown{
    if(!_unknown){
        UIButton *unknown = [[UIButton alloc]initWithFrame:CGRectMake(117, self.img.frame.origin.y+350, 50, 50)];
        unknown.tag = 2;
        [unknown setImage:[UIImage imageNamed:@"btn_cards_flash_unknown.png"] forState:UIControlStateNormal];
        [unknown addTarget:self action:@selector(c_action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:unknown];
        self.unknown = unknown;
    }
    return _unknown;
}

-(UIButton *)favorite{
    if(!_favorite){
        UIButton *favorite = [[UIButton alloc]initWithFrame:CGRectMake(30, self.img.frame.origin.y+340, 50, 50)];
        favorite.tag = 1;
        [favorite setImage:[UIImage imageNamed:@"btn_cards_flash_favorite.png"] forState:UIControlStateNormal];
        [favorite addTarget:self action:@selector(c_action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:favorite];
        self.favorite = favorite;
    }
    return _favorite;
}

-(UIPageControl *)control{
    if(!_control){
        UIPageControl *control = [[UIPageControl alloc]initWithFrame:CGRectMake(300-[GB_DeviceUtils getScreenHeight]*0.25f, 55+[GB_DeviceUtils getScreenHeight]*0.25f, [GB_DeviceUtils getScreenHeight]*0.5f, 20)];
        control.transform = CGAffineTransformRotate(control.transform, M_PI/2);
        control.numberOfPages = self.dayDataSource.topic_today.count;
        NSInteger i = 0;
        for (CardsFlashEventBean *bean in self.dayDataSource.topic_today) {
            if([bean.tad_id isEqualToString:self.eventDataSource.tad_id]){
                break;
            }
            i++;
        }

        [self addSubview:control];
        control.currentPage = i;
        self.control = control;
    }
    return _control;
}

-(void)showUI{
    if(!_isCreated){
        self.isCreated = YES;
        [self addSubview:self.bg];
        [self addSubview:self.img];
        [self addSubview:self.model];
        [self addSubview:self.top];
        [self addSubview:self.icon];
        [self addSubview:self.week];
        [self addSubview:self.day];
        [self addSubview:self.title];
        [self addSubview:self.yes];
        [self addSubview:self.no];
        [self addSubview:self.unknown];
        [self addSubview:self.favorite];
        [self addSubview:self.control];
        [GB_NetWorkUtils loadImage:[Url getImageUrl:_eventDataSource.tad_image] container:self.bg type:GB_ImageCacheTypeAll delegate:self tag:1];
        [GB_NetWorkUtils loadImage:[Url getImageUrl:self.eventDataSource.tad_image] container:self.img type:GB_ImageCacheTypeAll delegate:self tag:2];
        [GB_NetWorkUtils loadImage:[Url getImageUrl:self.dayDataSource.tt_coverurl] container:self.cover type:GB_ImageCacheTypeAll delegate:self tag:3];
    }
    self.hidden = NO;
}

-(void)hideUI{
    if(!_isCreated){
        return;
    }
    self.hidden = YES;
}

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    if(tag == 1){
        [GB_NetWorkUtils useImage:[GB_ImageUtils addFillingEffect:[GB_ImageUtils addBlurryEffect:image level:0.3f] w:self.bg.frame.size.width h:self.bg.frame.size.height]  container:container];
    }
    if(tag == 2){
        [GB_NetWorkUtils useImage:[GB_ImageUtils addFillingEffect:image w:self.img.frame.size.width h:self.img.frame.size.height]  container:container];
    }
    if(tag == 3){
        if(image){
            UIImageView *iv = container;
            iv.frame = CGRectMake(25-image.size.width*0.25f, 27.5f-image.size.height*0.25f, image.size.width*0.5f, image.size.height*0.5f);
        }
        [GB_NetWorkUtils useImage:image container:container];
    }
}

-(void)c_action:(UIButton *)btn{
    self.current = btn.tag;
    if(_delegate){
        [_delegate GB_didSelect];
    }
    
}

-(NSString *)result{
    switch (_current) {
        case 1:
            return @"A";
        case 2:
            return @"B";
        case 3:
            return @"C";
        case 4:
            return @"D";
        default:
            return @"";
    }
}

-(NSString *)eventId{
    return self.eventDataSource.tad_id;
}

@end

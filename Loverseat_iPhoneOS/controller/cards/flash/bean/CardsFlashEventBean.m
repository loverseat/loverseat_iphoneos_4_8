//
//  FlashEventBean.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/1.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "CardsFlashEventBean.h"

@implementation CardsFlashEventBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            CardsFlashEventBean *bean = [CardsFlashEventBean getBean:dic];
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(CardsFlashEventBean *)getBean:(NSDictionary *)dic{
    CardsFlashEventBean *bean = [[CardsFlashEventBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tad_show_name = [dic objectForKey:@"tad_show_name"];
        bean.tad_image = [dic objectForKey:@"tad_image"];
        bean.tad_intro = [dic objectForKey:@"tad_intro"];
        bean.tad_id = [dic objectForKey:@"tad_id"];
    }
    return bean;
}

@end

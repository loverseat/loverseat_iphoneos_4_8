//
//  FlashBean.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/3/31.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "CardsFlashEventBean.h"

@interface CardsFlashBean : NSObject

@property (nonatomic, strong) NSString *tt_id;
@property (nonatomic, strong) NSString *tt_title;
@property (nonatomic, strong) NSString *tt_coverurl;
@property (nonatomic, strong) NSString *tt_day;
@property (nonatomic, strong) NSString *tt_week;
@property (nonatomic, strong) NSString *tt_events;
@property (nonatomic, strong) NSString *tt_cate;
@property (nonatomic, strong) NSArray *topic_today;


+(NSArray *)getBeanList:(NSArray *)arr;
+(CardsFlashBean *)getBean:(NSDictionary *)dic;

@end

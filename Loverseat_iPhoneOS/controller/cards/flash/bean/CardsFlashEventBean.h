//
//  FlashEventBean.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/1.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

@interface CardsFlashEventBean : NSObject

@property (nonatomic, strong) NSString *tad_show_name;
@property (nonatomic, strong) NSString *tad_image;
@property (nonatomic, strong) NSString *tad_intro;
@property (nonatomic, strong) NSString *tad_id;

+(NSArray *)getBeanList:(NSArray *)arr;

+(CardsFlashEventBean *)getBean:(NSDictionary *)dic;

@end

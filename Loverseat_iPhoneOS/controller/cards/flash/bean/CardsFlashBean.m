//
//  FlashBean.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/3/31.
//  Copyright (c) 2015年 gaohang. All rights reserved.
//

#import "CardsFlashBean.h"

@implementation CardsFlashBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            CardsFlashEventBean *bean = [CardsFlashEventBean getBean:dic];
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(CardsFlashBean *)getBean:(NSDictionary *)dic{
    CardsFlashBean *bean = [[CardsFlashBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tt_id = [dic objectForKey:@"tt_id"];
        bean.tt_cate = [dic objectForKey:@"tt_cate"];
        bean.tt_title = [dic objectForKey:@"tt_title"];
        bean.tt_coverurl = [dic objectForKey:@"tt_coverurl"];
        
        NSString *time = [dic objectForKey:@"tt_time"];
        if([GB_StringUtils isNotBlank:time]){
            long timestamp = [GB_DateUtils get10MedianByFormatString:time format:@"yyyy-MM-dd"];
            bean.tt_day = [GB_DateUtils getFormatStringBy10Median:@"dd" timeMillis:timestamp];
            bean.tt_week = [self weekdayStringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
        }
        
        bean.tt_events = [dic objectForKey:@"tt_events"];
        bean.topic_today = [CardsFlashEventBean getBeanList:[dic objectForKey:@"topic_event"]];
    }
    return bean;
}

+(NSString *)weekdayStringFromDate:(NSDate*)inputDate {
    
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"SUN", @"MON", @"TUE", @"WED", @"THU", @"FRI", @"SAT", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSWeekdayCalendarUnit;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}

@end

//
//  SwitchViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"

@protocol ICSDrawerControllerChild;
@protocol ICSDrawerControllerPresenting;
@class HomeCenterViewController;
@class HomeLeftViewController;
@class HomeRightViewController;
@interface HomeSwitchViewController : BaseViewController

@property(nonatomic, strong) HomeLeftViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *leftViewController;

@property(nonatomic, strong) HomeCenterViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *centerViewController;

@property(nonatomic, strong) HomeRightViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *rightViewController;

- (id)initWithLeftViewController:(HomeLeftViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *)leftViewController
            centerViewController:(HomeCenterViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *)centerViewController
             rightViewController:(HomeRightViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *)rightViewController;
-(void)open;
-(void)leftOpen;
-(void)rightOpen;
-(void)close;
-(void)pickerView:(NSDictionary *)dic delegate:(id<SelfPickerDelegate>)delegate;
-(void)pickerViewDismiss;
-(void)changeCenterViewController;
-(void)addCenterViewController;
-(void)setEnabledPanGestureRecognizer:(BOOL)isEnabled;
@end

@protocol ICSDrawerControllerChild <NSObject>

@property(nonatomic, weak) HomeSwitchViewController *drawer;

@end

@protocol  ICSDrawerControllerPresenting <NSObject>

@optional

- (void)drawerControllerWillOpen:(HomeSwitchViewController *)drawerController;

- (void)drawerControllerDidOpen:(HomeSwitchViewController *)drawerController;

- (void)drawerControllerWillClose:(HomeSwitchViewController *)drawerController;

- (void)drawerControllerDidClose:(HomeSwitchViewController *)drawerController;

@end

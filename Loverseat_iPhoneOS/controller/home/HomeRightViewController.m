//
//  RightViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-27.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeRightViewController.h"
@implementation HomeRightViewController

-(id)init{
    self = [super init];
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)drawerControllerWillOpen:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidOpen:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}
@end

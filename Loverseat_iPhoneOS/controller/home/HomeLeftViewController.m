//
//  LeftViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeLeftViewController.h"
#import "HomeEventViewController.h"
#import "HomeUserCenterViewController.h"
#import "HomeSettingViewController.h"
#import "HomeMessageViewController.h"

#import "DateRecordViewController.h"
#import "HomeEventRightViewController.h"


static NSString * const kICSColorsViewControllerCellReuseId = @"kICSColorsViewControllerCellReuseId";

@interface HomeLeftViewController ()

@property(nonatomic, assign) NSInteger previousRow;
@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) UIButton *icon;

@property(nonatomic, strong) UILabel *infoL;
@property(nonatomic, strong) UIButton *coinBtn;
@property(nonatomic, strong) UIButton *dateBtn;
@property(nonatomic, strong) UILabel *nicknameL;

@property(nonatomic, strong) UILabel *messageLabel;

@end

@implementation HomeLeftViewController

-(id)init{
    self = [super init];
    if(self){
        [self initFrame];
        
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
   
}

-(void)changeStatus{
    if([User isLogin]){
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
        [dateFormat setDateFormat:@"yyyy-MM-dd"];//设定时间格式,要注意跟下面的dateString匹配，否则日起将无效
        _nicknameL.text = [User getUserInfo].tu_nickname;
    

        _infoL.text = [GB_StringUtils isBlank:[User getUserInfo].tu_star_sign]?[NSString stringWithFormat:@"%d岁",[Static getAgeByBirthday:[User getUserInfo].tu_birthday]]:[NSString stringWithFormat:@"%d岁，%@",[Static getAgeByBirthday:[User getUserInfo].tu_birthday],[User getUserInfo].tu_star_sign];
        [_dateBtn setTitle:[NSString stringWithFormat:@"邀约记录"] forState:UIControlStateNormal];
        [GB_NetWorkUtils loadImage:[Url getImageUrl:[User getUserInfo].tu_portrait] container:_icon type:GB_ImageCacheTypeAll];
    }
    else{
        _infoL.text = nil;
        _nicknameL.text = nil;
        [_coinBtn setTitle:@"金币：0" forState:UIControlStateNormal];
        [_dateBtn setTitle:@"邀约记录" forState:UIControlStateNormal];
    }
    if(_userCenterViewController){
        _userCenterViewController.isLoad = NO;
    }
}

-(void)GB_paySuccess{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [GB_NetWorkUtils startSinglePostAsyncRequest:[Url getLoginInfoUrl] list:arr delegate:self tag:1];
    }
}

-(void)setCurrentCount:(int)count
{
    _messageLabel.hidden = count == 0;
    count = count>99?99:count;
    _messageLabel.text = GB_NSStringFromInt(count);
    [_eventListViewController setCurrentCount:count];
}

-(void)minusCurrentCount
{
    int count = [_messageLabel.text intValue]-1;
    count = count<0?0:count;
    [self setCurrentCount:count];
}

-(void)initFrame
{
    self.view.clipsToBounds = YES;
    [NavUtils addBackground:self];
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 275, [GB_DeviceUtils getScreenHeight])];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.bounces = NO;

    
    int y = 240;
    int height = 60;
    int width = 275;
    NSArray *titleArr = @[@"热门演出",@"消息",@"设定"];
    for (int i = 0; i<titleArr.count; i++) {
        [scrollView addSubview:[View getLine:CGRectMake(22, y+i*height, scrollView.frame.size.width-22, 0.5f) color:[UIColor colorWithWhite:1 alpha:0.14f]]];
        
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, y+i*height, width, height) image:nil imageH:nil id:self sel:@selector(c_left:)];
        UILabel *l = [GB_WidgetUtils getLabel:CGRectMake(22, 5, btn.frame.size.width, btn.frame.size.height-10) title:[titleArr objectAtIndex:i] font:[Static getFont:32 isBold:NO] color:[UIColor whiteColor]];
        [btn addSubview:l];
        btn.tag = i+1;
        [scrollView addSubview:btn];
        
        if(i == 1){
            self.messageLabel = [GB_WidgetUtils getLabel:CGRectMake(240, y+i*height+22, 16, 16) title:nil font:[UIFont systemFontOfSize:10] color:[UIColor whiteColor]];
            _messageLabel.backgroundColor = GB_UIColorFromRGB(251, 80, 87);
            _messageLabel.layer.masksToBounds = YES;
            _messageLabel.layer.cornerRadius = 8;
            _messageLabel.textAlignment = NSTextAlignmentCenter;
            _messageLabel.hidden = YES;
            [scrollView addSubview:_messageLabel];
        }
    }

    self.nicknameL = [GB_WidgetUtils getLabel:CGRectMake(0, 15, width, 35) title:[User getUserInfo].tu_nickname font:[Static getFont:40 isBold:YES] color:[UIColor whiteColor]];
    _nicknameL.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:_nicknameL];
   
    UIView *iconBg = [[UIView alloc]initWithFrame:CGRectMake((width-76)*0.5, 60, 76, 76)];
    iconBg.backgroundColor = [UIColor whiteColor];
    iconBg.layer.masksToBounds = YES;
    iconBg.layer.cornerRadius = 38;
    [scrollView addSubview:iconBg];
    
    self.icon = [[UIButton alloc]initWithFrame:CGRectMake(3, 3, 70, 70)];
    _icon.layer.masksToBounds = YES;
    _icon.layer.cornerRadius = 35;
    [iconBg addSubview:_icon];
    [_icon addTarget:self action:@selector(c_self) forControlEvents:UIControlEventTouchUpInside];
    [GB_NetWorkUtils loadImage:[Url getImageUrl:[User getUserInfo].tu_portrait] container:_icon type:GB_ImageCacheTypeAll];
    
    self.infoL = [GB_WidgetUtils getLabel:CGRectMake(0, 140, width, 35) title:[GB_StringUtils isBlank:[User getUserInfo].tu_star_sign]?[NSString stringWithFormat:@"%d岁",[Static getAgeByBirthday:[User getUserInfo].tu_birthday]]:[NSString stringWithFormat:@"%d岁，%@",[Static getAgeByBirthday:[User getUserInfo].tu_birthday],[User getUserInfo].tu_star_sign] font:[Static getFont:24 isBold:YES] color:[UIColor whiteColor]];
    _infoL.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:_infoL];
    
#pragma mark - 邀约记录
    self.dateBtn = [GB_WidgetUtils getButton:CGRectMake(60, 180, 140, 30) image:nil imageH:nil id:self sel:@selector(c_date)];
    [_dateBtn setTitle:[NSString stringWithFormat:@"邀约记录"] forState:UIControlStateNormal];
    [_dateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _dateBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    _dateBtn.layer.masksToBounds = YES;
    _dateBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _dateBtn.layer.cornerRadius = 5;
    _dateBtn.layer.borderWidth = 0.5f;
    [scrollView addSubview:_dateBtn];
    
    [self.view addSubview:scrollView];
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, y+titleArr.count*height);
    self.scrollView = scrollView;
}

-(void)c_date{
    DateRecordViewController *con = [[DateRecordViewController alloc]init];
    con.uid = [User getUserInfo].uid;
    [self.navigationController pushViewController:con animated:YES];
}

-(void)c_self{
    if(self.drawer.centerViewController.type != 4){
        [GB_NetWorkUtils cancelRequest];
        if([GB_ToolUtils isBlank:self.userCenterViewController]){
            HomeUserCenterViewController *con = [[HomeUserCenterViewController alloc]init];
            self.userCenterViewController = con;
        }
        self.drawer.centerViewController = self.userCenterViewController;
        self.drawer.rightViewController = nil;
        
        [self.drawer changeCenterViewController];
    }
    [self.drawer close];
}


-(void)c_left:(UIButton *)btn{
    int type = btn.tag;
    //if(self.drawer.centerViewController.type != type){
        [GB_NetWorkUtils cancelRequest];
        [self.drawer setEnabledPanGestureRecognizer:type == 1];
        if(type == 1){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"homestatue" object:nil];
            self.drawer.centerViewController = self.eventListViewController;
            self.drawer.rightViewController = self.eventRightViewController;
        }
        else if(type == 2){
            if([GB_ToolUtils isBlank:self.messageViewController]){
                HomeMessageViewController *con = [[HomeMessageViewController alloc]init];
                self.drawer.centerViewController = con;
                self.messageViewController = con;
            }
            else{
                self.drawer.centerViewController = self.messageViewController;
            }
            self.drawer.rightViewController = nil;
            
        }
        else if(type == 3){
            if([GB_ToolUtils isBlank:self.settingViewController]){
                HomeSettingViewController *con = [[HomeSettingViewController alloc]init];
                self.settingViewController = con;
            }
            self.drawer.centerViewController = self.settingViewController;
            self.drawer.rightViewController = nil;
        }
        [self.drawer changeCenterViewController];
    //}
    [self.drawer close];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        [User logout];
        [GB_ToolUtils postNotificationName:@"GB_ReStart" object:nil];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)drawerControllerWillOpen:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidOpen:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(HomeSwitchViewController *)drawerController
{
    [GB_NetWorkUtils cancelRequest];
    [self.drawer.centerViewController initData];
    
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

#pragma mark GB_NetWorkDelegate

-(void)GB_requestDidFailed:(int)tag{
    [super GB_requestDidFailed:tag];
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [super GB_requestDidSuccess:str tag:tag];
    if([Error verify:str view:self.navigationController.view]){
        if(tag == 1){
            [User login:[Static getRequestData:str]];
            [self changeStatus];
        }
    }
}

@end

//
//  MessageViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//


#import "HomeMessageViewController.h"
#import "HomeViewController.h"
#import "HomeLeftViewController.h"

@interface HomeMessageViewController ()
<UITableViewDataSource,UITableViewDelegate,GB_LoadImageDelegate>
@end

@implementation HomeMessageViewController

-(id)init{
    self = [super init];
    if(self){
        self.type = 2;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFrame];
    
}

-(void)initFrame{
    self.view.backgroundColor = GB_UIColorFromRGB(246, 246, 246);
    
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"消息中心"];
    [NavUtils addLeftButton:self iconN:[UIImage imageNamed:@"btn_drawer_rose"] iconH:nil sel:@selector(c_drawer)];
}
@end

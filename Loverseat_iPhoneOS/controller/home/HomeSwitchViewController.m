//
//  SwitchViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//
//首页三页面的切换
#import "HomeSwitchViewController.h"
#import "HomeCenterViewController.h"
#import "HomeLeftViewController.h"
#import "HomeRightViewController.h"

static const CGFloat kICSDrawerControllerDrawerDepth = 275.0f;
static const CGFloat kICSDrawerControllerLeftViewInitialOffset = -60.0f;
static const CGFloat kICSDrawerControllerRightViewInitialOffset = 60.0f;



static const NSTimeInterval kICSDrawerControllerAnimationDuration = 0.5;
static const CGFloat kICSDrawerControllerOpeningAnimationSpringDamping = 0.7f;
static const CGFloat kICSDrawerControllerOpeningAnimationSpringInitialVelocity = 0.1f;
static const CGFloat kICSDrawerControllerClosingAnimationSpringDamping = 1.0f;
static const CGFloat kICSDrawerControllerClosingAnimationSpringInitialVelocity = 0.5f;

typedef NS_ENUM(NSUInteger, ICSDrawerControllerState)
{
    ICSDrawerControllerStateClosed = 0,
    ICSDrawerControllerStateOpening,
    ICSDrawerControllerStateLeftOpen,
    ICSDrawerControllerStateClosing,
    ICSDrawerControllerStateRightOpen,
};



@interface HomeSwitchViewController () <UIGestureRecognizerDelegate>

@property(nonatomic, strong) UIView *leftView;
@property(nonatomic, strong) UIView *centerView;
@property(nonatomic, strong) UIView *rightView;

@property(nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property(nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property(nonatomic, assign) CGPoint panGestureStartLocation;

@property(nonatomic, assign) BOOL isLeft;

@property(nonatomic, assign) ICSDrawerControllerState drawerState;

@end

@implementation HomeSwitchViewController

- (id)initWithLeftViewController:(HomeLeftViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *)leftViewController
            centerViewController:(HomeCenterViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *)centerViewController
             rightViewController:(HomeRightViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting> *)rightViewController
{
    NSParameterAssert(leftViewController);
    NSParameterAssert(centerViewController);
    
    self = [super init];
    if (self) {
        _leftViewController = leftViewController;
        _centerViewController = centerViewController;
        _rightViewController = rightViewController;
        
        if ([_leftViewController respondsToSelector:@selector(setDrawer:)]) {
            _leftViewController.drawer = self;
        }
        if ([_rightViewController respondsToSelector:@selector(setDrawer:)]) {
            _rightViewController.drawer = self;
        }
    }
    
    return self;
}

-(void)pickerView:(NSDictionary *)dic delegate:(id<SelfPickerDelegate>)delegate{

}

-(void)pickerViewDismiss{
    
}

-(void)changeRightViewController{
    NSParameterAssert(self.rightViewController);
    NSParameterAssert(self.rightView);
    
    [self.rightViewController removeFromParentViewController];
    [self.rightViewController.view removeFromSuperview];
    [self addCenterViewController];
}

-(void)changeCenterViewController{
    NSParameterAssert(self.centerViewController);
    NSParameterAssert(self.centerView);
    
    [self.centerViewController removeFromParentViewController];
    [self.centerViewController.view removeFromSuperview];
    [self addCenterViewController];
}

- (void)addCenterViewController
{
    NSParameterAssert(self.centerViewController);
    NSParameterAssert(self.centerView);
    
    [self addChildViewController:self.centerViewController];
    self.centerViewController.view.frame = self.view.bounds;
    [self.centerView addSubview:self.centerViewController.view];
    [self.centerViewController didMoveToParentViewController:self];
}

#pragma mark - Managing the view

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    // Initialize left and center view containers
    self.leftView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.centerView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.rightView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.leftView.autoresizingMask = self.view.autoresizingMask;
    self.centerView.autoresizingMask = self.view.autoresizingMask;
    self.rightView.autoresizingMask = self.view.autoresizingMask;
    
    
    // Add the center view container
    [self.view addSubview:self.centerView];
    
    // Add the center view controller to the container
    [self addCenterViewController];
    
    [self setupGestureRecognizers];
}

#pragma mark - Configuring the view’s layout behavior

- (UIViewController *)childViewControllerForStatusBarHidden
{
    NSParameterAssert(self.leftViewController);
    NSParameterAssert(self.centerViewController);
    
    if (self.drawerState == ICSDrawerControllerStateOpening) {
        return self.isLeft?self.leftViewController:self.rightViewController;
    }
    return self.centerViewController;
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
    NSParameterAssert(self.leftViewController);
    NSParameterAssert(self.centerViewController);
    NSParameterAssert(self.rightViewController);
    
    if (self.drawerState == ICSDrawerControllerStateOpening) {
        return self.isLeft?self.leftViewController:self.rightViewController;
    }
    return self.centerViewController;
}

#pragma mark - Gesture recognizers

- (void)setupGestureRecognizers
{
    NSParameterAssert(self.centerView);
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
    self.panGestureRecognizer.maximumNumberOfTouches = 1;
    self.panGestureRecognizer.delegate = self;
    [self.centerView addGestureRecognizer:self.panGestureRecognizer];
    [self setEnabledPanGestureRecognizer:NO];
}

-(void)setEnabledPanGestureRecognizer:(BOOL)isEnabled{
    [self.panGestureRecognizer setEnabled:isEnabled];
}

- (void)addClosingGestureRecognizers
{
    NSParameterAssert(self.centerView);
    NSParameterAssert(self.panGestureRecognizer);
    
    [self.centerView addGestureRecognizer:self.tapGestureRecognizer];
}

- (void)removeClosingGestureRecognizers
{
    NSParameterAssert(self.centerView);
    NSParameterAssert(self.panGestureRecognizer);
    
    [self.centerView removeGestureRecognizer:self.tapGestureRecognizer];
}

#pragma mark Tap to close the drawer
- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (tapGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [self close];
    }
}

#pragma mark Pan to open/close the drawer
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    NSParameterAssert([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]);
    CGPoint velocity = [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:self.view];
    
    if (self.drawerState == ICSDrawerControllerStateClosed && velocity.x != 0.0f) {
        return YES;
    }
    else if (self.drawerState == ICSDrawerControllerStateLeftOpen && velocity.x < 0.0f) {
        return YES;
    }
    else if (self.drawerState == ICSDrawerControllerStateRightOpen && velocity.x > 0.0f) {
        return YES;
    }
    
    return NO;
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)panGestureRecognizer
{
    NSParameterAssert(self.leftView);
    NSParameterAssert(self.centerView);
    
    UIGestureRecognizerState state = panGestureRecognizer.state;
    //表示触摸在view这个视图上的位置，这里返回的位置是针对view的坐标系的。
    CGPoint location = [panGestureRecognizer locationInView:self.view];
    //你拖动这个图片的时候肯定有个速度，因此返回值就是你拖动时X和Y轴上的速度，速度是矢量，有方向
    CGPoint velocity = [panGestureRecognizer velocityInView:self.view];
    
    switch (state) {
            
        case UIGestureRecognizerStateBegan:
            self.panGestureStartLocation = location;
            if (self.drawerState == ICSDrawerControllerStateClosed) {
                [self willOpen];
            }
            else {
                [self willClose];
            }
            break;
            
        case UIGestureRecognizerStateChanged:
        {
            CGFloat delta = 0.0f;
            if (self.drawerState == ICSDrawerControllerStateOpening) {
                delta = location.x - self.panGestureStartLocation.x;
                self.isLeft = delta > 0;
            }
            else if (self.drawerState == ICSDrawerControllerStateClosing) {
                if(self.isLeft){
                    delta = kICSDrawerControllerDrawerDepth - (self.panGestureStartLocation.x - location.x);
                }
                else{
                    delta = -kICSDrawerControllerDrawerDepth - (self.panGestureStartLocation.x - location.x);
                }
            }
            if(self.isLeft||[GB_ToolUtils isNotBlank:self.rightViewController]){
                self.rightView.hidden = self.isLeft;
                self.leftView.hidden = !self.isLeft;
                CGRect l = self.leftView.frame;
                CGRect c = self.centerView.frame;
                CGRect r = self.rightView.frame;
                if (delta > kICSDrawerControllerDrawerDepth) {
                    if(self.isLeft){
                        l.origin.x = 0.0f;
                        c.origin.x = kICSDrawerControllerDrawerDepth;
                    }
                    else{
                        r.origin.x = c.size.width - kICSDrawerControllerDrawerDepth;
                        c.origin.x = 0 - kICSDrawerControllerDrawerDepth;
                    }
                    
                }
                else{
                    // While the centerView can move up to kICSDrawerControllerDrawerDepth points, to achieve a parallax effect
                    // the leftView has move no more than kICSDrawerControllerLeftViewInitialOffset points
                    if(self.isLeft){
                        l.origin.x = kICSDrawerControllerLeftViewInitialOffset
                        - (delta * kICSDrawerControllerLeftViewInitialOffset) / kICSDrawerControllerDrawerDepth;
                    }
                    else{
                        r.origin.x = c.size.width-kICSDrawerControllerDrawerDepth+kICSDrawerControllerRightViewInitialOffset
                        - (delta * kICSDrawerControllerLeftViewInitialOffset) / kICSDrawerControllerDrawerDepth;
                    }
                    c.origin.x = delta;
                }
                self.leftView.frame = l;
                self.centerView.frame = c;
                self.rightView.frame = r;
            }
            break;
        }
            
        case UIGestureRecognizerStateEnded:
            if(self.isLeft||[GB_ToolUtils isNotBlank:self.rightViewController]){
                if (self.drawerState == ICSDrawerControllerStateOpening) {
                    CGFloat centerViewLocation = self.centerView.frame.origin.x;
                    if (centerViewLocation == kICSDrawerControllerDrawerDepth || centerViewLocation == kICSDrawerControllerDrawerDepth*-1) {
                        // Open the drawer without animation, as it has already being dragged in its final position
                        [self setNeedsStatusBarAppearanceUpdate];
                        [self didOpen];
                    }
                    else if ((self.isLeft&&centerViewLocation > self.view.bounds.size.width / 3
                              && velocity.x > 0.0f)||(!self.isLeft && centerViewLocation < self.view.bounds.size.width / -3
                                                      && velocity.x < 0.0f)) {
                        // Animate the drawer opening
                        [self animateOpening];
                    }
                    else {
                        // Animate the drawer closing, as the opening gesture hasn't been completed or it has
                        // been reverted by the user
                        [self didOpen];
                        [self willClose];
                        [self animateClosing];
                    }
                    
                } else if (self.drawerState == ICSDrawerControllerStateClosing) {
                    CGFloat centerViewLocation = self.centerView.frame.origin.x;
                    if (centerViewLocation == 0.0f) {
                        // Close the drawer without animation, as it has already being dragged in its final position
                        [self setNeedsStatusBarAppearanceUpdate];
                        [self didClose];
                    }
                    else if ((self.isLeft&&centerViewLocation < (2 * self.view.bounds.size.width) / 3
                              && velocity.x < 0.0f)||(!self.isLeft&&centerViewLocation > (2 * self.view.bounds.size.width) / -3
                                                      && velocity.x > 0.0f)) {
                        // Animate the drawer closing
                        [self animateClosing];
                    }
                    else {
                        // Animate the drawer opening, as the opening gesture hasn't been completed or it has
                        // been reverted by the user
                        [self didClose];
                        
                        // Here we save the current position for the leftView since
                        // we want the opening animation to start from the current position
                        // and not the one that is set in 'willOpen'
                        if(self.isLeft){
                            CGRect l = self.leftView.frame;
                            [self willOpen];
                            self.leftView.frame = l;
                        }
                        else{
                            CGRect l = self.rightView.frame;
                            [self willOpen];
                            self.rightView.frame = l;
                        }
                        
                        
                        [self animateOpening];
                    }
                }
            }
            else{
                self.drawerState = ICSDrawerControllerStateClosed;
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - Animations
#pragma mark Opening animation
- (void)animateOpening
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateOpening);
    NSParameterAssert(self.leftView);
    NSParameterAssert(self.centerView);
    NSParameterAssert(self.rightView);
    // Calculate the final frames for the container views
    CGRect leftViewFinalFrame = self.leftView.frame;
    CGRect centerViewFinalFrame = self.centerView.frame;
    CGRect rightViewFinalFrame = self.rightView.frame;
    if(self.isLeft){
        leftViewFinalFrame.origin.x = 0;
    }
    else{
        rightViewFinalFrame.origin.x = centerViewFinalFrame.size.width - kICSDrawerControllerDrawerDepth;
    }
    centerViewFinalFrame.origin.x = self.isLeft?kICSDrawerControllerDrawerDepth:kICSDrawerControllerDrawerDepth*-1;
    if(self.isLeft){
        [UIView animateWithDuration:kICSDrawerControllerAnimationDuration
                              delay:0
             usingSpringWithDamping:kICSDrawerControllerOpeningAnimationSpringDamping
              initialSpringVelocity:kICSDrawerControllerOpeningAnimationSpringInitialVelocity
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.centerView.frame = centerViewFinalFrame;
                             self.leftView.frame = leftViewFinalFrame;
                             [self setNeedsStatusBarAppearanceUpdate];
                         }
                         completion:^(BOOL finished) {
                             [self didOpen];
                         }];
        
    }
    else{
        [UIView animateWithDuration:kICSDrawerControllerAnimationDuration
                              delay:0
             usingSpringWithDamping:kICSDrawerControllerOpeningAnimationSpringDamping
              initialSpringVelocity:kICSDrawerControllerOpeningAnimationSpringInitialVelocity
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.centerView.frame = centerViewFinalFrame;
                             self.rightView.frame = rightViewFinalFrame;
                             
                             [self setNeedsStatusBarAppearanceUpdate];
                         }
                         completion:^(BOOL finished) {
                             [self didOpen];
                         }];
        
    }
}
#pragma mark Closing animation
- (void)animateClosing
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateClosing);
    NSParameterAssert(self.leftView);
    NSParameterAssert(self.centerView);
    
    // Calculate final frames for the container views
    CGRect leftViewFinalFrame = self.leftView.frame;
    leftViewFinalFrame.origin.x = kICSDrawerControllerLeftViewInitialOffset;
    CGRect centerViewFinalFrame = CGRectMake(0, 0, self.centerView.frame.size.width, self.centerView.frame.size.height);
    CGRect rightViewFinalFrame = self.rightView.frame;
    rightViewFinalFrame.origin.x = centerViewFinalFrame.size.width-rightViewFinalFrame.size.width +kICSDrawerControllerRightViewInitialOffset;
    if(self.isLeft){
        //usingSpringWithDamping:usingSpringWithDamping 的范围为 0.0f 到 1.0f ，数值越小「弹簧」的振动效果越明显。
        //initialSpringVelocity 则表示初始的速度，数值越大一开始移动越快。
        [UIView animateWithDuration:kICSDrawerControllerAnimationDuration
                              delay:0
             usingSpringWithDamping:kICSDrawerControllerClosingAnimationSpringDamping
              initialSpringVelocity:kICSDrawerControllerClosingAnimationSpringInitialVelocity
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.centerView.frame = centerViewFinalFrame;
                             self.leftView.frame = leftViewFinalFrame;
                             
                             [self setNeedsStatusBarAppearanceUpdate];
                         }
                         completion:^(BOOL finished) {
                             [self didClose];
                         }];
        
    }
    else{
        [UIView animateWithDuration:kICSDrawerControllerAnimationDuration
                              delay:0
             usingSpringWithDamping:kICSDrawerControllerClosingAnimationSpringDamping
              initialSpringVelocity:kICSDrawerControllerClosingAnimationSpringInitialVelocity
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.centerView.frame = centerViewFinalFrame;
                             self.rightView.frame = rightViewFinalFrame;
                             
                             [self setNeedsStatusBarAppearanceUpdate];
                         }
                         completion:^(BOOL finished) {
                             [self didClose];
                         }];
    }
}

#pragma mark - Opening the drawer

-(void)leftOpen{
    self.isLeft = YES;
    self.rightView.hidden = YES;
    self.leftView.hidden = NO;
    [self open];
}

-(void)rightOpen{
    self.isLeft = NO;
    self.rightView.hidden = NO;
    self.leftView.hidden = YES;
    [self open];
}

- (void)open
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateClosed);
    [self willOpen];
    [self animateOpening];
}

- (void)willOpen
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateClosed);
    NSParameterAssert(self.leftView);
    NSParameterAssert(self.centerView);
    NSParameterAssert(self.rightView);
    NSParameterAssert(self.leftViewController);
    NSParameterAssert(self.centerViewController);
    // Keep track that the drawer is opening
    self.drawerState = ICSDrawerControllerStateOpening;
    
    // Position the left view
    CGRect l = self.view.bounds;
    l.origin.x = kICSDrawerControllerLeftViewInitialOffset;
    NSParameterAssert(l.origin.x < 0.0f);
    self.leftView.frame = l;
    
    CGRect r = self.view.bounds;
    r.origin.x = kICSDrawerControllerRightViewInitialOffset;
    NSParameterAssert(r.origin.x > 0.0f);
    self.rightView.frame = r;
    
    // Start adding the left view controller to the container
    if([GB_ToolUtils isNotBlank:self.rightViewController])
        [self addChildViewController:self.rightViewController];
    [self addChildViewController:self.leftViewController];
    self.leftViewController.view.frame = self.leftView.bounds;
    if([GB_ToolUtils isNotBlank:self.rightViewController])
        self.rightViewController.view.frame = self.rightView.bounds;
    if([GB_ToolUtils isNotBlank:self.rightViewController])
        [self.rightView addSubview:self.rightViewController.view];
    [self.leftView addSubview:self.leftViewController.view];
    // Add the left view to the view hierarchy
    [self.view insertSubview:self.leftView belowSubview:self.centerView];
    if([GB_ToolUtils isNotBlank:self.rightViewController])
        [self.view insertSubview:self.rightView belowSubview:self.centerView];
    // Notify the child view controllers that the drawer is about to open
    if ([self.leftViewController respondsToSelector:@selector(drawerControllerWillOpen:)]) {
        [self.leftViewController drawerControllerWillOpen:self];
    }
    if([GB_ToolUtils isNotBlank:self.rightViewController]){
        if ([self.rightViewController respondsToSelector:@selector(drawerControllerWillOpen:)]) {
            [self.rightViewController drawerControllerWillOpen:self];
        }
    }
    
    if ([self.centerViewController respondsToSelector:@selector(drawerControllerWillOpen:)]) {
        [self.centerViewController drawerControllerWillOpen:self];
    }
}

- (void)didOpen
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateOpening);
    NSParameterAssert(self.leftViewController);
    NSParameterAssert(self.centerViewController);
    
    // Complete adding the left controller to the container
    if(self.isLeft){
        [self.leftViewController didMoveToParentViewController:self];
    }
    else{
        [self.rightViewController didMoveToParentViewController:self];
    }
    
    
    [self addClosingGestureRecognizers];
    
    // Keep track that the drawer is open
    self.drawerState = self.isLeft?ICSDrawerControllerStateLeftOpen:ICSDrawerControllerStateRightOpen;
    
    // Notify the child view controllers that the drawer is open
    if ([self.leftViewController respondsToSelector:@selector(drawerControllerDidOpen:)]) {
        [self.leftViewController drawerControllerDidOpen:self];
    }
    if([GB_ToolUtils isNotBlank:self.rightViewController]){
        if ([self.rightViewController respondsToSelector:@selector(drawerControllerDidOpen:)]) {
            [self.rightViewController drawerControllerDidOpen:self];
        }
    }
    if ([self.centerViewController respondsToSelector:@selector(drawerControllerDidOpen:)]) {
        [self.centerViewController drawerControllerDidOpen:self];
    }
}

#pragma mark - Closing the drawer

- (void)close
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateLeftOpen || self.drawerState == ICSDrawerControllerStateRightOpen);
    
    [self willClose];
    
    [self animateClosing];
}

- (void)willClose
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateLeftOpen||self.drawerState == ICSDrawerControllerStateRightOpen);
    NSParameterAssert(self.leftViewController);
    NSParameterAssert(self.centerViewController);
    
    // Start removing the left controller from the container
    if(self.drawerState == ICSDrawerControllerStateLeftOpen)
        [self.leftViewController willMoveToParentViewController:nil];
    else
        [self.rightViewController willMoveToParentViewController:nil];
    
    // Keep track that the drawer is closing
    self.drawerState = ICSDrawerControllerStateClosing;
    
    // Notify the child view controllers that the drawer is about to close
    if ([self.leftViewController respondsToSelector:@selector(drawerControllerWillClose:)]) {
        [self.leftViewController drawerControllerWillClose:self];
    }
    if ([self.centerViewController respondsToSelector:@selector(drawerControllerWillClose:)]) {
        [self.centerViewController drawerControllerWillClose:self];
    }
    if ([self.rightViewController respondsToSelector:@selector(drawerControllerWillClose:)]) {
        [self.rightViewController drawerControllerWillClose:self];
    }
}

- (void)didClose
{
    NSParameterAssert(self.drawerState == ICSDrawerControllerStateClosing);
    NSParameterAssert(self.leftView);
    NSParameterAssert(self.centerView);
    NSParameterAssert(self.leftViewController);
    NSParameterAssert(self.centerViewController);
    
    // Complete removing the left view controller from the container
    [self.leftViewController.view removeFromSuperview];
    [self.rightViewController.view removeFromSuperview];
    [self.leftViewController removeFromParentViewController];
    [self.rightViewController removeFromParentViewController];
    
    // Remove the left view from the view hierarchy
    [self.leftView removeFromSuperview];
    [self.rightView removeFromSuperview];
    
    [self removeClosingGestureRecognizers];
    
    // Keep track that the drawer is closed
    self.drawerState = ICSDrawerControllerStateClosed;
    
    // Notify the child view controllers that the drawer is closed
    if ([self.leftViewController respondsToSelector:@selector(drawerControllerDidClose:)]) {
        [self.leftViewController drawerControllerDidClose:self];
    }
    if ([self.centerViewController respondsToSelector:@selector(drawerControllerDidClose:)]) {
        [self.centerViewController drawerControllerDidClose:self];
    }
    
    if ([self.rightViewController respondsToSelector:@selector(drawerControllerWillClose:)]) {
        [self.rightViewController drawerControllerWillClose:self];
    }
}

@end

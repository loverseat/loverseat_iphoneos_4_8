//
//  UserCenterViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-28.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeUserCenterViewController.h"


#import "UserCenterCell.h"
#import "UserCenterAvatarCell.h"
#import "UserInfoViewController.h"
#import "TicketViewController.h"
#import "DateRecordViewController.h"

@interface HomeUserCenterViewController ()
<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,GB_NetWorkDelegate>
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation HomeUserCenterViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(_tableView)
        [_tableView reloadData];
}

-(id)init{
    self = [super init];
    if(self){
        self.type = 4;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFrame];
}

-(void)initData{
    [super initData];
    if(self.isLoad){
        return;
    }
    [self reloadData];
}

-(void)reloadData{
    [_tableView reloadData];
}

-(void)initFrame{
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundView = nil;
    tableView.separatorColor = GB_UIColorFromRGB(208, 208, 208);
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"个人中心"];
    [NavUtils addLeftButton:self iconN:[UIImage imageNamed:@"btn_drawer_rose"] iconH:nil sel:@selector(c_drawer)];
}

#pragma mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0)
        return 80;
    else
        return 55;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }
    else if(section == 1){
        return 1;
    }
    else if(section == 2){
        return 1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0){
        UserInfoViewController *con = [[UserInfoViewController alloc]init];
        con.uid = [User getUserInfo].uid;
        [self.navigationController pushViewController:con animated:YES];
    }
    else if(indexPath.section == 1){
//        if(indexPath.row == 0){
//            TicketViewController *con = [[TicketViewController alloc]init];
//            [self.navigationController pushViewController:con animated:YES];
//        }
        if(indexPath.row == 0){
            DateRecordViewController *con = [[DateRecordViewController alloc]init];
            con.uid = [User getUserInfo].uid;
            [self.navigationController pushViewController:con animated:YES];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        static NSString *identifier = @"UserCenterTableIdentifier";
        UserCenterAvatarCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                identifier];
        if (cell == nil) {
            cell = [[UserCenterAvatarCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier: identifier];
        }
        [cell setAvatarUrl:[Url getImageUrl:[User getUserInfo].tu_portrait]
                  nickname:[User getUserInfo].tu_nickname];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    else{
        static NSString *identifier = @"UserCenterTableIdentifier";
        UserCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                 identifier];
        if (cell == nil) {
            cell = [[UserCenterCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier: identifier];
        }
        
        if(indexPath.section == 1){
            
//            if(indexPath.row == 0){
//                [cell setTitle:@"我的演出票" status:@"0张" icon:[UIImage imageNamed:@"icon_ticket_rose"]];
//            }
            if(indexPath.row == 0){
                [cell setTitle:@"邀约记录" status:[NSString stringWithFormat:@"10个约会正在进行中"] icon:[UIImage imageNamed:@"icon_date_rose"]];
            }
        }
        if(indexPath.section == 2){
            [cell setTitle:@"我的金币" status:@"0" icon:[UIImage imageNamed:@"icon_ticket_rose"]];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
}

@end

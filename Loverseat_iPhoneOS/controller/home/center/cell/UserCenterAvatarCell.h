//
//  UserCenterAvatarCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCenterAvatarCell : UITableViewCell

-(void)setAvatarUrl:(NSString *)url nickname:(NSString *)nickname;

@end

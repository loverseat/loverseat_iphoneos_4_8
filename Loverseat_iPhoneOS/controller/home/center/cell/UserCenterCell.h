//
//  UserCenterCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCenterCell : UITableViewCell

-(void)setTitle:(NSString *)title status:(NSString *)status icon:(UIImage *)icon;

@end

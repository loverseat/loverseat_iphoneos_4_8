//
//  UserCenterCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserCenterCell.h"
#import <GeekBean4IOS/GB_DeviceUtils.h>
#import <GeekBean4IOS/GB_WidgetUtils.h>
#import <GeekBean4IOS/GB_MacroUtils.h>
@interface UserCenterCell()
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *status;
@end

@implementation UserCenterCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.icon = [[UIImageView alloc]init];
        [self addSubview:_icon];
        
        self.title = [GB_WidgetUtils getLabel:CGRectMake(60, 0, [GB_DeviceUtils getScreenWidth]-75, 55) title:nil font:[UIFont systemFontOfSize:14] color:GB_UIColorFromRGB(80, 80, 80)];
        [self addSubview:_title];
        
        self.status = [GB_WidgetUtils getLabel:CGRectMake([GB_DeviceUtils getScreenWidth]-15, 0, [GB_DeviceUtils getScreenWidth]-75, 55) title:nil font:[UIFont systemFontOfSize:12] color:GB_UIColorFromRGB(251, 80, 87)];
        _status.textAlignment = NSTextAlignmentRight;
        [self addSubview:_title];
    }
    return self;
}

-(void)setTitle:(NSString *)title status:(NSString *)status icon:(UIImage *)icon{
    _icon.image = icon;
    CGRect rect ;
    rect.size = icon.size;
    _icon.frame = rect;
    _icon.center = CGPointMake(30, 27.5f);
    _title.text = title;
    _status.text = status;
}



@end

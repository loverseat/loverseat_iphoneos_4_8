//
//  UserCenterAvatarCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "UserCenterAvatarCell.h"
#import <GeekBean4IOS/GB_DeviceUtils.h>
#import <GeekBean4IOS/GB_WidgetUtils.h>
#import <GeekBean4IOS/GB_MacroUtils.h>
#import <GeekBean4IOS/GB_NetWorkUtils.h>

@interface UserCenterAvatarCell()
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *title;
@end

@implementation UserCenterAvatarCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 60, 60)];
        [self addSubview:_icon];
        
        self.title = [GB_WidgetUtils getLabel:CGRectMake(90, 0, [GB_DeviceUtils getScreenWidth]-105, 80) title:nil font:[UIFont systemFontOfSize:16] color:GB_UIColorFromRGB(80, 80, 80)];
        [self addSubview:_title];
    }
    return self;
}

-(void)setAvatarUrl:(NSString *)url nickname:(NSString *)nickname{
    [GB_NetWorkUtils loadImage:url container:_icon type:GB_ImageCacheTypeAll];
    _title.text = nickname;
}

@end

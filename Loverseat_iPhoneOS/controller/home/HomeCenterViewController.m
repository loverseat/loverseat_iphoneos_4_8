//
//  CenterViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeCenterViewController.h"
#import "HomeRightViewController.h"
#import "ODRefreshControl.h"


@implementation HomeCenterViewController
-(void)initData{
    
}

-(void)reloadData{
    
}

-(void)clearData{
    
}

- (void)c_drawer
{
    [GB_NetWorkUtils cancelRequest];
    [Static remove:self.navigationController.view];
    [self.drawer leftOpen];
}


#pragma mark - Configuring the view’s layout behavior

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)drawerControllerWillOpen:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(HomeSwitchViewController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(HomeSwitchViewController *)drawerController
{

}

@end

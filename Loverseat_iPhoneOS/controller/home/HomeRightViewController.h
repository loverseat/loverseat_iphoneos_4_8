//
//  RightViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-27.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeSwitchViewController.h"
@interface HomeRightViewController : BaseViewController
<ICSDrawerControllerChild, ICSDrawerControllerPresenting>

@property(nonatomic, assign) NSInteger type;
@property(nonatomic, weak) HomeSwitchViewController *drawer;

@end

//
//  RootViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeSwitchViewController.h"

@interface HomeViewController : HomeSwitchViewController
<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) NSMutableDictionary *pickerData;
@property (strong, nonatomic) UIView *pickerBgView;
@property (strong, nonatomic) UIButton *modelBtn;

-(void)pickerView:(NSDictionary *)dic delegate:(id<SelfPickerDelegate>)delegate;
-(void)pickerViewDismiss;
-(void)checkLogin;
-(void)popLoginViewController;
@end

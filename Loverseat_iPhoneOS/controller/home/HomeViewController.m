//
//  RootViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCenterViewController.h"
#import "HomeSwitchViewController.h"
#import "HomeLeftViewController.h"


#import "LoginViewController.h"

@interface HomeViewController()
@property id<SelfPickerDelegate> pickerDelegate;
@end

@implementation HomeViewController

static int lastestId;

- (void)addCenterViewController{
    [super addCenterViewController];
    if ([self.centerViewController respondsToSelector:@selector(setDrawer:)]) {
        self.centerViewController.drawer = self;
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];    
    lastestId = -1;
    self.pickerData = [NSMutableDictionary dictionary];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GB_Alert:)
                                                 name:@"GB_Alert"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GB_Add:)
                                                 name:@"GB_Add"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GB_refreshMsg:)
                                                 name:@"GB_refreshMsg"
                                               object:nil];
    [self initPickerView];
    [NSThread detachNewThreadSelector:@selector(msg) toTarget:self withObject:nil];
}

-(void)popLoginViewController{
    LoginViewController *login = [[LoginViewController alloc]init];
    [self.navigationController pushViewController:login animated:NO];
}

-(void)initPickerView{
    UIButton *modelBtn = [GB_WidgetUtils getButton:CGRectMake(0, 0, [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]) image:nil imageH:nil id:self sel:@selector(c_model)];
    modelBtn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    modelBtn.alpha = 0;
    [self.view addSubview:modelBtn];
    self.modelBtn = modelBtn;
    
    UIView *pickerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, [GB_DeviceUtils getScreenHeight], [GB_DeviceUtils getScreenWidth], 246)];
    pickerBgView.backgroundColor = [UIColor whiteColor];
    
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 30, [GB_DeviceUtils getScreenWidth], 216)];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.backgroundColor = [UIColor whiteColor];
    [pickerBgView addSubview:pickerView];
    self.pickerView = pickerView;
    
    UIButton *cancelBtn = [GB_WidgetUtils getButton:CGRectMake(0, 10, 80, 30) image:nil imageH:nil id:self sel:@selector(c_picker_cancel)];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitle:@"取消" forState:UIControlStateHighlighted];
    [cancelBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateNormal];
    [cancelBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateHighlighted];
    [pickerBgView addSubview:cancelBtn];
    
    UIButton *doneBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]-80, 10, 80, 30) image:nil imageH:nil id:self sel:@selector(c_picker_done)];
    [doneBtn setTitle:@"完成" forState:UIControlStateNormal];
    [doneBtn setTitle:@"完成" forState:UIControlStateHighlighted];
    [doneBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateNormal];
    [doneBtn setTitleColor:GB_UIColorFromRGB(251, 80, 87) forState:UIControlStateHighlighted];
    [pickerBgView addSubview:doneBtn];
    
    [self.view addSubview:pickerBgView];
    self.pickerBgView = pickerBgView;
}


-(void)pickerView:(NSDictionary *)dic delegate:(id<SelfPickerDelegate>)delegate{
    self.pickerDelegate = delegate;
    [self.pickerData setDictionary:dic];
    [self.pickerView reloadAllComponents];

    int index = 0;
    for (NSString *key in self.pickerData.allKeys) {
        if([GB_StringUtils isNumeric:key]){
            [self.pickerView selectRow:[[self.pickerData objectForKey:[NSString stringWithFormat:@"r_%d",index]] integerValue] inComponent:index animated:NO];
            index ++ ;
        }
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25f];
    [self.pickerBgView setCenter:CGPointMake([GB_DeviceUtils getScreenWidth]*0.5, [GB_DeviceUtils getScreenHeight]-self.pickerBgView.frame.size.height*0.5)];
    self.modelBtn.alpha = 1.0f;
    [UIView commitAnimations];
}

-(void)pickerViewDismiss{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25f];
    [self.pickerBgView setCenter:CGPointMake([GB_DeviceUtils getScreenWidth]*0.5, [GB_DeviceUtils getScreenHeight]-[GB_DeviceUtils getStatusBarHeight]+self.pickerBgView.frame.size.height*0.5)];
    self.modelBtn.alpha = 0.0f;
    [UIView commitAnimations];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component == 1){
        int row1 = [pickerView selectedRowInComponent:0];
        if(row < row1){
            [pickerView selectRow:row1 inComponent:1 animated:YES];
        }
    }
    else{
        int row2 = [pickerView selectedRowInComponent:1];
        if(row > row2){
            [pickerView selectRow:row2 inComponent:0 animated:YES];
        }
    }
    NSMutableDictionary *rowDic = [NSMutableDictionary dictionary];
    for (NSString *str in self.pickerData.allKeys) {
        [rowDic setObject:GB_NSStringFromInt([self.pickerView selectedRowInComponent:[str integerValue]]) forKey:str];
    }
    [self.pickerDelegate performSelector:@selector(cb_picker_did_select:rowData:) withObject:self.pickerData withObject:rowDic];
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    int index = 0;
    for (NSString *key in self.pickerData.allKeys) {
        if([GB_StringUtils isNumeric:key])
            index ++ ;
    }
    return index;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[self.pickerData objectForKey:GB_NSStringFromInt(component)] count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *arr = [self.pickerData objectForKey:GB_NSStringFromInt(component)];
    return [arr objectAtIndex:row];
}

-(void)checkLogin{
    if([User isLogin]){
        [self.centerViewController initData];
        [self.leftViewController changeStatus];
    }
    else{
        [self popLoginViewController];
    }
}

-(void)GB_Alert:(id)sender{
    [Static alert:self.navigationController.view msg:[sender object]];
}

-(void)GB_Add:(id)sender{
    [Static add:self.navigationController.view msg:[sender object]];
}

-(void)c_model{
    [self c_picker_cancel];
}

-(void)c_picker_cancel{
    [self pickerViewDismiss];
}

-(void)c_picker_done{
    [self pickerViewDismiss];
}



-(void)msg{
    while (YES) {
        if([User isLogin]){
            NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:[Url getLastestMessageUrl]] encoding:NSUTF8StringEncoding error:nil];
            if([GB_StringUtils isNumeric:str]){
                if(lastestId != -1 && [str intValue] != lastestId){
                    [self performSelectorOnMainThread:@selector(msgNew) withObject:nil waitUntilDone:YES];
                }
                lastestId = [str intValue];
            }
        }
        [NSThread sleepForTimeInterval:3];
    }
}

-(void)GB_refreshMsg:(id)sender{
    if(lastestId<[[sender object]intValue])
        lastestId = [[sender object]intValue];
}

-(void)msgNew{
    [GB_ToolUtils postNotificationName:@"GB_Msg" object:nil];
}

@end

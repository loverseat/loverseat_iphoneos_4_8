//
//  LeftViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeSwitchViewController.h"

@class HomeMessageViewController;
@class HomeSettingViewController;
@class HomeUserCenterViewController;
@class HomeEventViewController;


@class HomeEventRightViewController;

@interface HomeLeftViewController : BaseViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting>{
    void(^statusChange)(void);
}

@property(nonatomic, weak) HomeSwitchViewController *drawer;
@property(nonatomic, strong) HomeMessageViewController *messageViewController;
@property(nonatomic, strong) HomeSettingViewController *settingViewController;
@property(nonatomic, strong) HomeUserCenterViewController *userCenterViewController;
@property(nonatomic, strong) HomeEventViewController *eventListViewController;
@property(nonatomic, strong) HomeEventRightViewController *eventRightViewController;
-(void)setCurrentCount:(int)count;
-(void)changeStatus;
-(void)minusCurrentCount;
@end

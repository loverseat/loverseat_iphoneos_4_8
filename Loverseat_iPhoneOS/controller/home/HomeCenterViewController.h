//
//  CenterViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeSwitchViewController.h"
#import "HomeViewController.h"

@interface HomeCenterViewController : BaseViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting>

@property(nonatomic, assign) NSInteger type;
@property(nonatomic, weak) HomeViewController *drawer;
-(void)initData;
-(void)reloadData;
-(void)clearData;
- (void)c_drawer;
@end

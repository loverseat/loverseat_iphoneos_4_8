//
//  Right1ViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-4.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeEventRightViewController.h"
#import "HomeEventViewController.h"

//#define KCacheCategoryId @"kcache_category_id"

@interface HomeEventRightViewController (){
    UITextField *tf;
    UITextField *tf2;
    BOOL isSearch;
}

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) NSMutableArray *cityBtnArr;
@property(nonatomic, strong) NSMutableArray *styleBtnArr;
@property(nonatomic, strong) NSMutableArray *categoryBtnArr;
@property(nonatomic, strong) NSMutableArray *hallCityBtnArr;
@property(nonatomic, strong) NSMutableArray *stateBtnArr;
@property(nonatomic, strong) NSMutableArray *genderBtnArr;
@property(nonatomic, strong) NSMutableArray *ageBtnArr;
@end

@implementation HomeEventRightViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"repertoireSearch"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"repertoireSearch"];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [tf resignFirstResponder];
    [tf2 resignFirstResponder];
}
-(id)init
{
    self = [super init];
    if(self){
        self.cityBtnArr = [NSMutableArray array];
        self.hallCityBtnArr = [NSMutableArray array];
        self.imgDic = [NSMutableDictionary dictionary];
        self.categoryBtnArr = [NSMutableArray array];
        self.styleBtnArr = [NSMutableArray array];
        self.stateBtnArr = [NSMutableArray array];
        self.genderBtnArr = [NSMutableArray array];
        self.ageBtnArr = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        self.age = -2;
        self.gender = -2;
        self.state = -2;
        self.style = -2;
        self.category = -2;
        self.city = -2;
        self.hallCity = -2;
    }
    return self;
}

#pragma mark - 键盘处理
- (void)keyboardWillShow:(NSNotification *)aNotification{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    self.height = keyboardRect.size.height;
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UIScrollView class]]){
            UIScrollView *scrollview = (UIScrollView *)view;
            scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, [GB_DeviceUtils getScreenHeight]+self.height);
        }
    }
}
- (void)keyboardWillHide:(NSNotification *)aNotification{
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UIScrollView class]]){
            UIScrollView *scrollview = (UIScrollView *)view;
            scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, [GB_DeviceUtils getScreenHeight]);
        }
    }
}

- (void)initFrame1
{
    [NavUtils addBackground:self];
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 275, [GB_DeviceUtils getScreenHeight])];
    self.scrollView = scrollView;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.bounces = NO;
    [self.view addSubview:scrollView];
    
    UIImageView *searchImg = [[UIImageView alloc]initWithFrame:CGRectMake(23, 20, 18, 18)];
    searchImg.image = [UIImage imageNamed:@"btn_search"];
    [self.scrollView addSubview:searchImg];
    
    tf2 = [[UITextField alloc]initWithFrame:CGRectMake(43, 20, self.scrollView.frame.size.width - searchImg.frame.origin.x-searchImg.frame.size.width-2, 20)];
    UIColor *color = [UIColor colorWithWhite:1 alpha:0.5f];
    UIFont *font = [UIFont systemFontOfSize:17];
    tf2.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"搜索人物" attributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font}];
    tf2.textColor = [UIColor colorWithWhite:1 alpha:0.2f];
    tf2.clearsOnBeginEditing = YES;
    tf2.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf2.keyboardType = UIKeyboardAppearanceDefault;
    tf2.returnKeyType = UIReturnKeySearch;
    tf2.delegate = self;
    [self.scrollView addSubview:tf2];
    
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(10, 45, scrollView.frame.size.width-20, 1)];
    iv.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2f];
    [self.scrollView addSubview:iv];
    
    UILabel *title1 = [GB_WidgetUtils getLabel:CGRectMake(23, 60, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择城市" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title1];
    isSearch = NO;
    //[self.cityBtnArr removeAllObjects];
    [self addHallCityBtn];
    
    UILabel *title3 = [GB_WidgetUtils getLabel:CGRectMake(23, 90+60, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择性别" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title3];
    NSArray *titleArr = @[@"全部",@"女",@"男"];
    for(int i = 0;i<titleArr.count;i++){
        UIButton *btn1 = [GB_WidgetUtils getButton:CGRectMake(10+60*i,title3.frame.size.height + title3.frame.origin.y , 40, 40) image:nil imageH:nil id:self sel:@selector(c_gender:)];
        
        if(i == 0){
            btn1.tag = 3;
        }else{
            if(i == 1){
                btn1.tag = 1;
            }else if (i == 2){
                btn1.tag = 0;
            }
            [btn1 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [btn1 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }

        [btn1 setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn1 setTitle:titleArr[i] forState:UIControlStateHighlighted];
        btn1.titleLabel.font = [Static getFont:30 isBold:NO];
        [self.genderBtnArr addObject:btn1];
        [scrollView addSubview:btn1];
    }

    UILabel *title4 = [GB_WidgetUtils getLabel:CGRectMake(23, 90+130, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择年龄" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title4];
    NSArray *title4Arr = @[@"全部",@"20岁以下",@"20岁-30岁"];
    for(int i = 0;i<title4Arr.count;i++){
        UIButton *btn2 = [GB_WidgetUtils getButton:CGRectMake(80*i-15,title4.frame.size.height + title4.frame.origin.y , 90, 40) image:nil imageH:nil id:self sel:@selector(c_age:)];
        if(i == 0){
            btn2.tag = 9;
            [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }else{
            if(i == 1){
                btn2.tag = 0;
            }else if (i == 2){
                btn2.tag = 1;
            }
            [btn2 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [btn2 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn2 setTitle:title4Arr[i] forState:UIControlStateNormal];
        [btn2 setTitle:title4Arr[i] forState:UIControlStateHighlighted];
        btn2.titleLabel.font = [Static getFont:30 isBold:NO];
        [self.ageBtnArr addObject:btn2];
        [scrollView addSubview:btn2];
    }
    UIButton *ageBtn = [GB_WidgetUtils getButton:CGRectMake(72, title4.frame.size.height + title4.frame.origin.y +40, 80, 40) image:nil imageH:nil id:self sel:@selector(c_age:)];
    [ageBtn setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
    [ageBtn setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
    [ageBtn setTitle:@"30岁-40岁" forState:UIControlStateNormal];
    [ageBtn setTitle:@"30岁-40岁" forState:UIControlStateHighlighted];
    ageBtn.tag = 2;
    ageBtn.titleLabel.font = [Static getFont:30 isBold:NO];
    [self.ageBtnArr addObject:ageBtn];
    [scrollView addSubview:ageBtn];
    
    UIButton *ageBtn1 = [GB_WidgetUtils getButton:CGRectMake(ageBtn.frame.origin.x + ageBtn.frame.size.width, title4.frame.size.height + title4.frame.origin.y +40, 80, 40) image:nil imageH:nil id:self sel:@selector(c_age:)];
    [ageBtn1 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
    [ageBtn1 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
    [ageBtn1 setTitle:@"40岁以上" forState:UIControlStateNormal];
    [ageBtn1 setTitle:@"40岁以上" forState:UIControlStateHighlighted];
    ageBtn1.tag = 3;
    ageBtn1.titleLabel.font = [Static getFont:30 isBold:NO];
    [self.ageBtnArr addObject:ageBtn1];
    [scrollView addSubview:ageBtn1];
    
    UILabel *title5 = [GB_WidgetUtils getLabel:CGRectMake(23, 90+240, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择状态" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title5];
    NSArray *statueArr = @[@"全部",@"邀约中",@"未邀约"];
    for(int i = 0;i<statueArr.count;i++){
        UIButton *btn3 = [GB_WidgetUtils getButton:CGRectMake(60*i, title5.frame.origin.y + title5.frame.size.height, 60, 40) image:nil imageH:nil id:self sel:@selector(c_state:)];
        if(i == 0){
            btn3.tag = 3;
            [btn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }else{
            if(i == 1){
                btn3.tag = 1;
            }else if(i == 2){
                btn3.tag = 2;
            }
            [btn3 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [btn3 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn3 setTitle:statueArr[i] forState:UIControlStateNormal];
        [btn3 setTitle:statueArr[i] forState:UIControlStateHighlighted];
        btn3.titleLabel.font = [Static getFont:30 isBold:NO];
        [self.stateBtnArr addObject:btn3];
        [scrollView addSubview:btn3];
    }
    
    UIButton *submitBtn = [GB_WidgetUtils getButton:CGRectMake(20, [GB_DeviceUtils getScreenHeight]-50, scrollView.frame.size.width-40, 30) image:nil imageH:nil id:self sel:@selector(c_hallSubmit)];
    [submitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [submitBtn setTitle:@"确定" forState:UIControlStateHighlighted];
    submitBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    submitBtn.layer.masksToBounds = YES;
    submitBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    submitBtn.layer.cornerRadius = 5;
    submitBtn.layer.borderWidth = 0.5f;
    
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [scrollView addSubview:submitBtn];
    
}

- (void)addHallCityBtn{
    int x = 7.5;
    int w = 60;
    int i = 0;
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 50+40, self.scrollView.frame.size.width, 40)];
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    for (NSDictionary *dic in self.hallCityArr) {
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(x+i*w-5, 0, w, 40) image:nil imageH:nil id:self sel:@selector(c_hallCity:)];
//        if([[dic objectForKey:@"name"]isEqualToString:@"全部"]){
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//        }
        if([[dic objectForKey:@"id"] intValue]==_hallCity){
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
        else{
            [btn setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [btn setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitle:[dic objectForKey:@"name"] forState:UIControlStateNormal];
        [btn setTitle:[dic objectForKey:@"name"] forState:UIControlStateHighlighted];
        
        btn.tag = i+1;
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [scrollView addSubview:btn];
        [self.hallCityBtnArr addObject:btn];
        i++;
    }
    scrollView.contentSize = CGSizeMake(x+i*w+x, scrollView.frame.size.height);
    [self.scrollView addSubview:scrollView];
}
-(void)c_hallCity:(UIButton *)btn{
    @synchronized(self){
        //知道按钮的城市的id
        self.hallCity = [[[_hallCityArr objectAtIndex:btn.tag-1] objectForKey:@"id"] intValue];
        for (UIButton *b in self.hallCityBtnArr) {
            if(b == btn)continue;
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
//    @synchronized(self)  {
//        if(isSearch){
//            self.category = 0;
//            [self addCategoryBtn];
//        }else
//            return;
//    }
}
-(void)initFrame{
    [NavUtils addBackground:self];
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 275, [GB_DeviceUtils getScreenHeight])];
    self.scrollView = scrollView;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.bounces = NO;
    [self.view addSubview:scrollView];
    

    UIImageView *searchImg = [[UIImageView alloc]initWithFrame:CGRectMake(23, 20, 18, 18)];
    searchImg.image = [UIImage imageNamed:@"btn_search"];
    [self.scrollView addSubview:searchImg];
    
    tf = [[UITextField alloc]initWithFrame:CGRectMake(43, 20, self.scrollView.frame.size.width - searchImg.frame.origin.x-searchImg.frame.size.width-2, 20)];
    UIColor *color = [UIColor colorWithWhite:1 alpha:0.5f];
    UIFont *font = [UIFont systemFontOfSize:17];
    tf.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"搜索演出" attributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font}];
    tf.textColor = [UIColor colorWithWhite:1 alpha:0.2f];
    tf.clearsOnBeginEditing = YES;
    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf.keyboardType = UIKeyboardAppearanceDefault;
    tf.returnKeyType = UIReturnKeySearch;
    tf.delegate = self;
    [self.scrollView addSubview:tf];
    
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(10, 45, scrollView.frame.size.width-20, 1)];
    iv.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2f];
    [self.scrollView addSubview:iv];
    
    UILabel *title1 = [GB_WidgetUtils getLabel:CGRectMake(23, 60, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择城市" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title1];
    [self addCityBtn];
    [scrollView addSubview:[View getLine:[UIImage imageNamed:@"right_line"] rect:CGRectMake(0, 100, scrollView.frame.size.width, 1)]];
    
    UILabel *title2 = [GB_WidgetUtils getLabel:CGRectMake(23, 90+60, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择分类" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title2];
    [self addCategoryBtn];
    [scrollView addSubview:[View getLine:[UIImage imageNamed:@"right_line"] rect:CGRectMake(0, 100+90+80+40, scrollView.frame.size.width, 1)]];
    
    UILabel *title3 = [GB_WidgetUtils getLabel:CGRectMake(23, 90+20+90+40+80+40, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"排序方式" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    [scrollView addSubview:title3];
    
    UILabel *title4 = [GB_WidgetUtils getLabel:CGRectMake(23, 90+20+90+40+80+20+90, 140, [GB_ToolUtils getFontHeight:[Static getFont:24 isBold:NO]]) title:@"选择状态" font:[Static getFont:24 isBold:NO] color:GB_UIColorFromRGB(168, 165, 181)];
    //[scrollView addSubview:title4];
    
    int x = 7.5;
    UIButton *btn1 = [GB_WidgetUtils getButton:CGRectMake(x, 100+90+80+40+30+40, 100, 40) image:nil imageH:nil id:self sel:@selector(c_style:)];
    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [btn1 setTitle:@"按演出时间" forState:UIControlStateNormal];
    [btn1 setTitle:@"按演出时间" forState:UIControlStateHighlighted];
    btn1.tag = 0;
    btn1.titleLabel.font = [Static getFont:30 isBold:NO];
    [scrollView addSubview:btn1];

    
    UIButton *btn4 = [GB_WidgetUtils getButton:CGRectMake(x, title4.frame.origin.y + title4.frame.size.height, 80, 40) image:nil imageH:nil id:self sel:@selector(c_state:)];
    [btn4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn4 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [btn4 setTitle:@"全部" forState:UIControlStateNormal];
    [btn4 setTitle:@"全部" forState:UIControlStateHighlighted];
    btn4.tag = 2;
    btn4.titleLabel.font = [Static getFont:30 isBold:NO];
    //[scrollView addSubview:btn4];
    
    
    UIButton *btn5 = [GB_WidgetUtils getButton:CGRectMake(80+x, title4.frame.origin.y + title4.frame.size.height, 80, 40) image:nil imageH:nil id:self sel:@selector(c_state:)];
    [btn5 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
    [btn5 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
    [btn5 setTitle:@"邀约中" forState:UIControlStateNormal];
    [btn5 setTitle:@"邀约中" forState:UIControlStateHighlighted];
    btn5.tag = 1;
    btn5.titleLabel.font = [Static getFont:30 isBold:NO];
   // [scrollView addSubview:btn5];
    
    UIButton *btn6 = [GB_WidgetUtils getButton:CGRectMake(160+x, title4.frame.origin.y + title4.frame.size.height, 80, 40) image:nil imageH:nil id:self sel:@selector(c_state:)];
    [btn6 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
    [btn6 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
    [btn6 setTitle:@"未邀约" forState:UIControlStateNormal];
    [btn6 setTitle:@"未邀约" forState:UIControlStateHighlighted];
    btn6.tag = 0;
    btn6.titleLabel.font = [Static getFont:30 isBold:NO];
   // [scrollView addSubview:btn6];
    
    
    UIButton *btn2 = [GB_WidgetUtils getButton:CGRectMake(x+100, 100+90+80+40+30+40, 80, 40) image:nil imageH:nil id:self sel:@selector(c_style:)];
    [btn2 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
    [btn2 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
    [btn2 setTitle:@"按评分" forState:UIControlStateNormal];
    [btn2 setTitle:@"按评分" forState:UIControlStateHighlighted];
    btn2.tag = 1;
    btn2.titleLabel.font = [Static getFont:30 isBold:NO];
    [scrollView addSubview:btn2];
    
    
    UIButton *btn3 = [GB_WidgetUtils getButton:CGRectMake(x+180, 100+90+80+40+30+40, 80, 40) image:nil imageH:nil id:self sel:@selector(c_style:)];
    [btn3 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
    [btn3 setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
    [btn3 setTitle:@"按热度" forState:UIControlStateNormal];
    [btn3 setTitle:@"按热度" forState:UIControlStateHighlighted];
    btn3.tag = 2;
    btn3.titleLabel.font = [Static getFont:30 isBold:NO];
    [scrollView addSubview:btn3];
    
    [self.styleBtnArr addObject:btn1];
    [self.styleBtnArr addObject:btn2];
    [self.styleBtnArr addObject:btn3];
    
//    [self.stateBtnArr addObject:btn4];
//    [self.stateBtnArr addObject:btn5];
//    [self.stateBtnArr addObject:btn6];
    
    [scrollView addSubview:[View getLine:[UIImage imageNamed:@"right_line"] rect:CGRectMake(0, [GB_DeviceUtils getScreenHeight]-51.0f, scrollView.frame.size.width, 1)]];
    
    UIButton *submitBtn = [GB_WidgetUtils getButton:CGRectMake(20, [GB_DeviceUtils getScreenHeight]-50, scrollView.frame.size.width-40, 30) image:nil imageH:nil id:self sel:@selector(c_submit)];
    [submitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [submitBtn setTitle:@"确定" forState:UIControlStateHighlighted];
    submitBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    submitBtn.layer.masksToBounds = YES;
    submitBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    submitBtn.layer.cornerRadius = 5;
    submitBtn.layer.borderWidth = 0.5f;
    
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [scrollView addSubview:submitBtn];
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, [GB_DeviceUtils getScreenHeight]);
}

-(void)addCityBtn{
    int x = 7.5;
    int w = 50;
    int i = 0;
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 50+40, self.scrollView.frame.size.width, 40)];
    scrollView.bounces = NO;
    
    for (NSDictionary *dic in self.cityArr) {
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(x+i*w, 0, w, 40) image:nil imageH:nil id:self sel:@selector(c_city:)];
        if([[dic objectForKey:@"id"] intValue]==_city){
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
        else{
            [btn setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [btn setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitle:[dic objectForKey:@"name"] forState:UIControlStateNormal];
        [btn setTitle:[dic objectForKey:@"name"] forState:UIControlStateHighlighted];
        
        btn.tag = i+1;
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [scrollView addSubview:btn];
        [self.cityBtnArr addObject:btn];
        i++;
    }
    scrollView.contentSize = CGSizeMake(x+i*w+x, scrollView.frame.size.height);
    [self.scrollView addSubview:scrollView];
}

-(void)addCategoryBtn{
    int x = 7.5;
    int h = 80;
    int w = 65;
    int i = 0;
    int j = 0;
    for (UIButton *btn in _categoryBtnArr) {
        [btn removeFromSuperview];
    }
    [_categoryBtnArr removeAllObjects];
    for (NSDictionary *dic in self.categoryArr) {
        NSString *categoryId = GB_NSStringFromInt([[dic objectForKey:@"id"] intValue]);
        if(_city!=-1&&![[_cityCategoryDic objectForKey:GB_NSStringFromInt(_city)] containsObject:categoryId]){
            i++;
            continue;
        }
        NSString *hImg = [dic objectForKey:@"n"];
        NSString *nImg = [dic objectForKey:@"h"];
        
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(x+j%4*w, j>3?250:170, w, h) image:nil imageH:nil id:self sel:@selector(c_category:)];
        btn.tag = i+1;
        
        int tag = [[dic objectForKey:@"id"] intValue]+1;
        [GB_NetWorkUtils loadImage:nImg container:btn type:GB_ImageCacheTypeAll delegate:self tag:tag];
        [GB_NetWorkUtils loadImage:hImg container:btn type:GB_ImageCacheTypeAll delegate:self tag:-1*tag];
        [self.scrollView addSubview:btn];
        [self.categoryBtnArr addObject:btn];
        i++;
        j++;
    }
}


- (void)c_age:(UIButton *)btn{
    @synchronized(self){
        self.age = btn.tag;
        for (UIButton *b in self.ageBtnArr){
            if(b == btn)continue;
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
}
- (void)c_gender:(UIButton *)btn{
    @synchronized(self){
        self.gender = btn.tag;
        for (UIButton *b in self.genderBtnArr){
            if(b == btn)continue;
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
}

#pragma mark - 搜索确定按钮
-(void)c_hallSubmit{
    NSString *city = GB_NSStringFromInt(_hallCity);
    [GB_SharedPreferenceUtils setObject:city forKey:kCacheHallCityId];
    
    for (NSDictionary *dic in _hallCityArr) {
        if(_hallCity == [[dic objectForKey:@"id"] intValue]){
            NSString *cityName = [dic objectForKey:@"name"];
            [GB_SharedPreferenceUtils setObject:cityName forKey:kCacheHallCityName];
            break;
        }
    }
    [self.drawer close];
    [self.drawer.centerViewController clearData];
    [self.drawer.centerViewController reloadData];
}

-(void)c_submit{
    NSString *city = GB_NSStringFromInt(_city);
    [GB_SharedPreferenceUtils setObject:city forKey:kCacheCityId];

    for (NSDictionary *dic in _cityArr) {
        if(_city == [[dic objectForKey:@"id"] intValue]){
            NSString *cityName = [dic objectForKey:@"name"];
            [GB_SharedPreferenceUtils setObject:cityName forKey:kCacheCityName];
            break;
        }
    }
    [self.drawer close];
    [self.drawer.centerViewController clearData];
    [self.drawer.centerViewController reloadData];
}
//选择类别
-(void)c_category:(UIButton *)btn{
    @synchronized(self)  {
        self.category = [[[_categoryArr objectAtIndex:btn.tag-1]objectForKey:@"id"] intValue];
        for (UIButton *b in self.categoryBtnArr) {
            int keyId = [[[_categoryArr objectAtIndex:b.tag-1]objectForKey:@"id"] intValue];
            if(b == btn){
                if([GB_ToolUtils isNotBlank:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_h",keyId]]]){
                    [b setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_h",keyId]] forState:UIControlStateNormal];
                    [b setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_h",keyId]] forState:UIControlStateHighlighted];
                }
            }
            else{
                if([GB_ToolUtils isNotBlank:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_n",keyId]]]){
                    [b setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_n",keyId]] forState:UIControlStateNormal];
                    [b setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_n",keyId]] forState:UIControlStateHighlighted];
                }
            }
        }
    }
}
//点击选择城市的按钮
-(void)c_city:(UIButton *)btn{
    @synchronized(self){
        //知道按钮的城市的id
        self.city = [[[_cityArr objectAtIndex:btn.tag-1] objectForKey:@"id"] intValue];
        for (UIButton *b in self.cityBtnArr) {
            if(b == btn)continue;
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
    @synchronized(self)  {
        self.category = 0;
        [self addCategoryBtn];
    }
}
- (void)c_state:(UIButton *)btn{
    @synchronized(self){
        self.state = btn.tag;
        for (UIButton *b in self.stateBtnArr){
            if(b == btn)continue;
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
}
//设置分类的选中状态
-(void)c_style:(UIButton *)btn{
    @synchronized(self)  {
        self.style = btn.tag;
        for (UIButton *b in self.styleBtnArr) {
            if(b == btn)continue;
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateNormal];
            [b setTitleColor:GB_UIColorFromRGB(140, 140, 140) forState:UIControlStateHighlighted];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
}

-(void)c_left:(UIButton *)btn{
    
}

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
    @synchronized(self)  {
        int absTag = abs(tag)-1;
        for (NSDictionary *dic in self.categoryArr) {
            if([[dic objectForKey:@"id"] intValue] == absTag){
                if(tag < 0){
                    [self.imgDic setObject:[self getCurrentCategoryBtn:image :[dic objectForKey:@"name"]]forKey:[NSString stringWithFormat:@"img_%d_h",absTag]];
                }
                else{
                    [self.imgDic setObject:[self getCurrentCategoryBtn:image :[dic objectForKey:@"name"]] forKey:[NSString stringWithFormat:@"img_%d_n",absTag]];
                }
                break;
            }
        }
       
        if([GB_ToolUtils isNotBlank:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_h",absTag]]]&&[GB_ToolUtils isNotBlank:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_n",absTag]]]){
            if(_category == absTag){
                [container setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_h",absTag]] forState:UIControlStateNormal];
                [container setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_h",absTag]] forState:UIControlStateHighlighted];
            }
            else{
                [container setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_n",absTag]] forState:UIControlStateNormal];
                [container setBackgroundImage:[self.imgDic objectForKey:[NSString stringWithFormat:@"img_%d_n",absTag]] forState:UIControlStateHighlighted];
            }
        }
    }
}

-(UIImage *)getCurrentCategoryBtn:(UIImage *)img : (NSString *)str{
    int w = 65;
    int h = 80;
    
    int width = 0;
    int height= 0;
    float imgWidth = 32.0f;
    float imgHeight = 33.5f;
    if(imgHeight/33 > imgWidth/32){
        width = 33 * imgWidth / imgHeight;
        height = 33;
    }
    else{
        width = 32;
        height = 32 * imgHeight / imgWidth;
    }
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc]init];
    paraStyle.alignment = NSTextAlignmentCenter;
    NSDictionary *nDic = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSParagraphStyleAttributeName:paraStyle};
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), NO, 0.0);
    [img drawInRect:CGRectIntegral(CGRectMake(w*0.5-width*0.5f,w*0.5-height*0.5f,width,height))];
    if([str respondsToSelector:@selector(drawInRect:withAttributes:)]){
        [str drawInRect:CGRectMake(0, w, w, h-w) withAttributes:nDic];
    }
    UIImage *ALayoutImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return ALayoutImage;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [tf resignFirstResponder];
    [tf2 resignFirstResponder];
    self.searchContent = tf.text;
    self.searchContent = tf2.text;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [tf resignFirstResponder];
    [tf2 resignFirstResponder];
    [self c_submit];
    return YES;
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [tf resignFirstResponder];
//    [self.view endEditing:YES];
//}

@end

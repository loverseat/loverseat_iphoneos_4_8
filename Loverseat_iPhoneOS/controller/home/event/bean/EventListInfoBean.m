//
//  EventListInfoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventListInfoBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventListInfoBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventListInfoBean *bean = [EventListInfoBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventListInfoBean *)getBean:(NSDictionary *)dic{
    EventListInfoBean *bean = [[EventListInfoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tad_begin_time = [dic objectForKey:@"tad_begin_time"];
        bean.tad_end_time = [dic objectForKey:@"tad_end_time"];
        bean.tad_id = [dic objectForKey:@"tad_id"];
        bean.tad_image = [dic objectForKey:@"tad_image"];
        bean.tad_show_name = [dic objectForKey:@"tad_show_name"];
        bean.tad_venue_name = [dic objectForKey:@"tad_venue_name"];
        bean.tad_category_name = [dic objectForKey:@"tad_category_name"];
    }
    return bean;
}

@end

//
//  EventListBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//  

#import "EventListBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventListBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventListBean *bean = [EventListBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventListBean *)getBean:(NSDictionary *)dic{
    EventListBean *bean = [[EventListBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.event_info = [EventListInfoBean getBean:[dic objectForKey:@"event_info"]];
        bean.event_buy_invite_info = [EventListBuyInviteInfoBean getBeanList:[dic objectForKey:@"buy_invite_info"]];
        bean.like_count = [[dic objectForKey:@"like_count"] intValue];
        bean.like_flag = [[dic objectForKey:@"like_flag"] intValue];
    }
    return bean;
}

@end

//
//  EventListBuyInviteInfoBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface EventListBuyInviteInfoBean : BaseBean

@property (nonatomic, strong) NSString *tbi_portrait;
@property (nonatomic, strong) NSString *tbi_uid;

+(NSArray *)getBeanList:(NSArray *)arr;
+(EventListBuyInviteInfoBean *)getBean:(NSDictionary *)dic;

@end

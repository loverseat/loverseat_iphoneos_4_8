//
//  EventListInfoBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "BaseBean.h"

@interface EventListInfoBean : BaseBean

@property (nonatomic, strong) NSString *tad_begin_time;
@property (nonatomic, strong) NSString *tad_end_time;
@property (nonatomic, strong) NSString *tad_id;
@property (nonatomic, strong) NSString *tad_image;
@property (nonatomic, strong) NSString *tad_show_name;
@property (nonatomic, strong) NSString *tad_venue_name;
@property (nonatomic, strong) NSString *tad_category_name;
+(NSArray *)getBeanList:(NSArray *)arr;
+(EventListInfoBean *)getBean:(NSDictionary *)dic;

@end

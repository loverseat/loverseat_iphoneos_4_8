//
//  EventListBuyInviteInfoBean.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventListBuyInviteInfoBean.h"
#import <GeekBean4IOS/GB_GeekBean.h>
@implementation EventListBuyInviteInfoBean

+(NSArray *)getBeanList:(NSArray *)arr{
    NSMutableArray *resultList = [NSMutableArray array];
    if([arr isKindOfClass:[NSArray class]]){
        int tag = [[GB_SecurityUtils getRandomNumber:6] intValue];
        int index = 0;
        for (NSDictionary *dic in arr) {
            EventListBuyInviteInfoBean *bean = [EventListBuyInviteInfoBean getBean:dic];
            bean.tag = tag;
            bean.index = index;
            [resultList addObject:bean];
            tag ++;
            index ++;
        }
    }
    return resultList;
}

+(EventListBuyInviteInfoBean *)getBean:(NSDictionary *)dic{
    EventListBuyInviteInfoBean *bean = [[EventListBuyInviteInfoBean alloc]init];
    if([GB_ToolUtils isNotBlank:dic]){
        bean.tbi_portrait = [dic objectForKey:@"tbi_portrait"];
        bean.tbi_uid = [dic objectForKey:@"tbi_uid"];
    }
    return bean;
}

@end

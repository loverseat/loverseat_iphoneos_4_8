//
//  EventListBean.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventListInfoBean.h"
#import "EventListBuyInviteInfoBean.h"
#import "BaseBean.h"

@interface EventListBean : BaseBean

@property (nonatomic, strong) EventListInfoBean *event_info;
@property (nonatomic, strong) NSArray *event_buy_invite_info;
@property (nonatomic, assign) int like_count;
@property (nonatomic, assign) int like_flag;

+(NSArray *)getBeanList:(NSArray *)arr;
+(EventListBean *)getBean:(NSDictionary *)dic;

@end

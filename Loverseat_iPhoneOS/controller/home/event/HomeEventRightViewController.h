//
//  Right1ViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-5-4.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeRightViewController.h"
#define kCacheCityName @"kcache_city_name"
#define kCacheHallCityName @"kcache_hallCity_name"
#define kCacheCityId @"kcache_city_id"
#define kCacheHallCityId @"kcache_hallCity_id"
@interface HomeEventRightViewController : HomeRightViewController<GB_LoadImageDelegate,UITextFieldDelegate>

@property(nonatomic, weak) HomeSwitchViewController *drawer;
@property (nonatomic, assign)NSInteger height;
@property(nonatomic, assign) NSInteger city;

@property(nonatomic, assign) NSInteger hallCity;

@property(nonatomic, assign) NSInteger category;

@property(nonatomic, assign) NSInteger style;

@property(nonatomic, assign) NSInteger state;

@property(nonatomic, assign) NSInteger gender;

@property(nonatomic, assign) NSInteger age;

@property(nonatomic, strong) NSArray *categoryArr;

@property(nonatomic, strong) NSArray *hallCityArr;

@property(nonatomic, strong) NSArray *cityArr;

@property(nonatomic, strong) NSDictionary *cityCategoryDic;

@property(nonatomic, strong) NSMutableDictionary *imgDic;

@property(nonatomic, strong) NSString *searchContent;
-(void)initFrame;
- (void)initFrame1;
@end

//
//  EventListCell.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventListCell.h"
#import "User.h"
#import "HomeViewController.h"
#import "DateAllCenterViewController.h"
#import "DateAllLeftViewController.h"
#import "UserInfoViewController.h"
#import "UMSocial.h"
@interface EventListCell()
<GB_NetWorkDelegate,UMSocialUIDelegate,GB_LoadImageDelegate>
@property (nonatomic, strong) UIImageView *image;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *venue;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UIView *inviteBgView;
@property (nonatomic, strong) UIButton *shareBtn;
@property (nonatomic, strong) UIButton *likeBtn;
@property (nonatomic, strong) UILabel *likeLabel;
@property (nonatomic, strong) UIImageView *typeImg;

@property (nonatomic, assign) EventListBean *bean;

@end

@implementation EventListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        
        self.image = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 300, 330)];
        _image.backgroundColor = [UIColor blackColor];
        [self addSubview:_image];
        
        self.typeImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 40, 40)];
        [self addSubview:_typeImg];
        
        
        
        self.shareBtn = [GB_WidgetUtils getButton:CGRectMake(268, 20, 31, 31) image:[UIImage imageNamed:@"btn_event_list_share"] imageH:nil id:self sel:@selector(c_share)];
        [self addSubview:_shareBtn];
        
        self.likeBtn = [GB_WidgetUtils getButton:CGRectMake(268, 65, 31, 31) image:[UIImage imageNamed:@"btn_event_list_dis_like"] imageH:nil id:self sel:@selector(c_like)];
        [self addSubview:_likeBtn];
        
        self.likeLabel = [GB_WidgetUtils getLabel:CGRectMake(5, 17, 21, 10) title:nil font:[UIFont systemFontOfSize:9] color:[UIColor whiteColor]];
        _likeLabel.textAlignment = NSTextAlignmentCenter;
        [_likeBtn addSubview:_likeLabel];
        
      
        
        self.name = [GB_WidgetUtils getLabel:CGRectMake(20, 200, 280, 48) title:nil font:[UIFont boldSystemFontOfSize:19] color:[UIColor whiteColor]];
        _name.numberOfLines = 2;
        [self addSubview:_name];
        
        self.venue = [GB_WidgetUtils getLabel:CGRectMake(20, 248, 280, 16) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
        [self addSubview:_venue];
        
        self.time = [GB_WidgetUtils getLabel:CGRectMake(20, 264, 280, 16) title:nil font:[UIFont systemFontOfSize:11] color:[UIColor whiteColor]];
        [self addSubview:_time];

        
        self.inviteBgView = [[UIView alloc]initWithFrame:CGRectMake(10, 295, 300, 40)];
        _inviteBgView.backgroundColor = [UIColor clearColor];
        [self addSubview:_inviteBgView];
        
        

        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

#pragma mark - 演出大厅的分享按钮
-(void)c_share{
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:[Url getImageUrl:_bean.event_info.tad_image]];
    [UMSocialData defaultData].extConfig.wechatSessionData.title = @"双人座看演出，约吗？";
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.url =  [Url getShareUrl:_bean.event_info.tad_id];

    
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = [Url getShareUrl:_bean.event_info.tad_id];
    
    [UMSocialSnsService presentSnsIconSheetView:_bean.baseViewController
                                         appKey:nil
                                      shareText:[NSString stringWithFormat:@"我想看 %@，来双人座约吧~",_bean.event_info.tad_show_name]
                                     shareImage:nil
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToWechatSession,UMShareToWechatTimeline,nil]
                                       delegate:self];
}

-(void)c_like{
    if(![User checkLogin])return;
    if([GB_NetWorkUtils checkNetWork:[PublicDao shareInstance].rootViewController.navigationController.view]){
        //[Static add:[PublicDao shareInstance].rootViewController.navigationController.view msg:_bean.like_flag == 0?@"正在关注":@"正在取消关注"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"event_id" value:_bean.event_info.tad_id]];
        [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"type" value:_bean.like_flag == 0?@"0":@"1"]];
        [GB_NetWorkUtils startPostAsyncRequest:[Url getEventInterestUrl] list:arr delegate:self tag:(_bean.like_flag == 0?1:2)];
    }
}


-(void)GB_requestDidFailed:(int)tag{
    [Static remove:[PublicDao shareInstance].rootViewController.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:[PublicDao shareInstance].rootViewController.navigationController.view];
    if([Error verify:str view:[PublicDao shareInstance].rootViewController.navigationController.view]){
        if(tag == 1){
            _bean.like_flag = 1;
            _bean.like_count += 1;
        }
        if(tag == 2){
            _bean.like_flag = 0;
             _bean.like_count -= 1;
        }
        _bean.like_count = _bean.like_count<0?0:_bean.like_count;
        _likeLabel.text = GB_NSStringFromInt(_bean.like_count);
        [_likeBtn setBackgroundImage:[UIImage imageNamed:_bean.like_flag == 1? @"btn_event_list_did_like":@"btn_event_list_dis_like"] forState:UIControlStateNormal];
    }
}

-(void)setEventListBean:(EventListBean *)bean{
    if([bean.event_info.tad_category_name isEqualToString:@"电影"]){
        _typeImg.image = [UIImage imageNamed:@"bg_type_movie"];
    }else{
        _typeImg.image = [UIImage imageNamed:@"bg_type_show"];
    }
    self.bean = bean;
    _image.image = nil;
    _likeLabel.text = GB_NSStringFromInt(bean.like_count);
    [GB_NetWorkUtils loadImage:[Url getImageUrl:bean.event_info.tad_image] container:_image type:GB_ImageCacheTypeAll delegate:nil tag:bean.tag];
    
      int nameHeight = [GB_ToolUtils getTextHeight:bean.event_info.tad_show_name font:[UIFont boldSystemFontOfSize:19] size:CGSizeMake(280, 48)];
    _name.frame = CGRectMake(20, 200+40-nameHeight, 280, nameHeight);
    _name.text = bean.event_info.tad_show_name;
    _venue.text = bean.event_info.tad_venue_name;
    _time.text = [NSString stringWithFormat:@"%@ - %@",bean.event_info.tad_begin_time,bean.event_info.tad_end_time];
    
    for (UIView *v in _inviteBgView.subviews) {
        [v removeFromSuperview];
    }
    if([GB_ToolUtils isNotBlank:bean.event_buy_invite_info]){
        int index = 0;
        for (EventListBuyInviteInfoBean *b in bean.event_buy_invite_info) {
            [_inviteBgView addSubview:[self getUserView:b index:b.index]];
            index = b.index;
        }
        [_inviteBgView addSubview:[self getUserView:nil index:++index]];
    }
    else{
        UILabel *alert = [GB_WidgetUtils getLabel:CGRectMake(50, 0, 200, 30) title:@"还没有人发起邀约哦！" font:[UIFont boldSystemFontOfSize:17] color:GB_UIColorFromRGB(84, 84, 84)];
        [_inviteBgView addSubview: alert];
        [_inviteBgView addSubview:[self getUserView:nil index:6]];
    }
    
    [_likeBtn setBackgroundImage:[UIImage imageNamed:bean.like_flag == 1? @"btn_event_list_did_like":@"btn_event_list_dis_like"] forState:UIControlStateNormal];
}

-(UIView *)getUserView:(EventListBuyInviteInfoBean *)bean index:(int)index{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(10+index*40, 0, 32, 32)];
    bg.layer.masksToBounds = YES;
    bg.layer.cornerRadius = 16;
    bg.clipsToBounds = NO;
    
    if(bean){
        bg.backgroundColor = GB_UIColorFromRGB(91, 91, 91);
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, 28,28)];
        [bg addSubview:img];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 14;
        [GB_NetWorkUtils loadImage:[Url getImageUrl:bean.tbi_portrait] container:img type:GB_ImageCacheTypeAll delegate:self tag:bean.tag];
        
        UILabel *icon = [GB_WidgetUtils getLabel:CGRectMake(22, 22, 12, 12) title:@"约" font:[UIFont systemFontOfSize:8] color:[UIColor whiteColor]];
        icon.backgroundColor = GB_UIColorFromRGB(251, 80, 87);
        icon.textAlignment = NSTextAlignmentCenter;
        icon.layer.masksToBounds = YES;
        icon.layer.cornerRadius = 6;
        [bg addSubview:icon];
        
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 32, 32) image:nil imageH:nil id:self sel:@selector(c_user:)];
        btn.tag = index;
        [bg addSubview:btn];
        bg.userInteractionEnabled = YES;
    }
    else{
        UIButton *btn = [GB_WidgetUtils getButton:CGRectMake(0, 0, 32, 32) image:nil imageH:nil id:self sel:@selector(c_date)];
        [bg addSubview:btn];
        bg.userInteractionEnabled = YES;
        bg.image = [UIImage imageNamed:@"btn_cell_event_list_add"];
    }
    return bg;
}

-(void)c_user:(UIButton *)btn{
    int index = btn.tag;
    EventListBuyInviteInfoBean *bean = [_bean.event_buy_invite_info objectAtIndex:index];
    UserInfoViewController *con = [[UserInfoViewController alloc]init];
    con.uid = bean.tbi_uid;
    [_bean.baseViewController.navigationController pushViewController:con animated:YES];
}
//邀约剧目的小椅子按钮
-(void)c_date{
    DateAllCenterViewController *center = [[DateAllCenterViewController alloc]init];
    center.event_id = _bean.event_info.tad_id;
    DateAllLeftViewController *left = [[DateAllLeftViewController alloc]init];
    
    DateAllSwitchViewController *con = [[DateAllSwitchViewController alloc]initWithLeftViewController:left centerViewController:center];
    [_bean.baseViewController.navigationController pushViewController:con animated:YES];
}

#pragma mark GB_LoadImageDelegate

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int)tag{
        if([GB_ToolUtils isNotBlank:image])
            [GB_NetWorkUtils useImage:[Static getFImage:image w:28 h:28] container:container];
}


@end

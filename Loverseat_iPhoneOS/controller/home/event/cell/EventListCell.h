//
//  EventListCell.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "EventListBean.h"

@interface EventListCell : UITableViewCell

-(void)setEventListBean:(EventListBean *)bean;

@end

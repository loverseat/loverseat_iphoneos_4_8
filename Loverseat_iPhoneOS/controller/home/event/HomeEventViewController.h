//
//  EventListViewController.h
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//  

#import "HomeCenterViewController.h"
#import "PersonhallCell.h"
@interface HomeEventViewController : HomeCenterViewController<UITableViewDataSource,UITableViewDelegate,chatButtonClickDelegate>

-(void)setCurrentCount:(int)count;

@property (nonatomic, strong) NSString *event_id;



@end

//
//  EventListViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14/12/15.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeEventViewController.h"
#import "HomeEventRightViewController.h"
#import "HomeLeftViewController.h"


#import "EventListBean.h"
#import "EventListCell.h"
#import "EventViewController.h"




#import "UIScrollView+AH3DPullRefresh.h"
#import "PersonhallHeadView.h"
#import "PersonhallCell.h"
#import "ExpertView.h"
#import "UserInfoViewController.h"
#import "DateRecordViewController.h"
#import "TestViewController.h"
#import "DateAllSuccessViewController.h"
#import "DateAllCenterViewController.h"
#import "DateAllLeftViewController.h"
#import "DateAllSwitchViewController.h"

#import "CardsFlashViewController.h"

/** 邀约详情 */
#import "TestViewController.h"
#import "Url.h"
#import "EditViewController.h"
#import "BaseBean.h"
#import "DateAllLeftBean.h"


//两次提示的默认间隔
static const CGFloat kDefaultPlaySoundInterval = 3.0;

@interface HomeEventViewController()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSString  *_tu_gender;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, strong) NSMutableArray *contentData;
@property (nonatomic, strong) NSMutableArray *rightData;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIImageView *searchBg;
@property (nonatomic, strong) UIButton *dramaBtn;
@property (nonatomic, strong) UIButton *personBtn;
@property (nonatomic, strong) NSMutableArray *expertInfoArr;
@property (nonatomic, strong) NSMutableArray *inviteInfoArr;
@property (nonatomic, strong) NSMutableArray *expertImageViewArr;
@property (nonatomic, strong) NSMutableArray *inviteImageViewArr;


@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@end

@implementation HomeEventViewController
{
    PersonhallHeadView *personhall;
    NSNumber *pageNum;
    int currentPage;
    HomeEventRightViewController *eventRightViewController;
    
    NSInteger *_count;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"homepage"];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"homepage"];
}

-(id)init
{
    self = [super init];
    if(self){
        self.type = 1;
        currentPage = 2;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeS:) name:@"homestatue" object:nil];
    }
    return self;
}

#pragma mark - 初始化方法
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.contentData = [NSMutableArray array];
    self.rightData = [NSMutableArray array];
    self.expertInfoArr = [NSMutableArray array];
    self.inviteInfoArr = [NSMutableArray array];
    self.expertImageViewArr = [NSMutableArray array];
    self.inviteImageViewArr = [NSMutableArray array];
    
    [self initFrame];
    
    //获取未读消息数，此时并没有把self注册为SDK的delegate，读取出的未读数是上次退出程序时的
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUntreatedApplyCount) name:@"setupUntreatedApplyCount" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callOutWithChatter:) name:@"callOutWithChatter" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callControllerClose:) name:@"callControllerClose" object:nil];
}

#pragma mark - 环信相关上面--------------------------------

-(void)changeS:(NSNotification *)info
{
    //    LZLog(@"热门演出---");
    //
    [self btnClick:self.dramaBtn];
    //[self btnClick:self.personBtn];
}

//今日达人点击事件
- (void)tapClick:(UITapGestureRecognizer *)tap
{
    //    NSUInteger n = tap.view.tag -200;
    //    ExpertInfo *info = [_expertInfoArr objectAtIndex:n];
    //    UserInfoViewController *user = [[UserInfoViewController alloc]init];
    //    user.uid = info.tu_id;
    //    [self.navigationController pushViewController:user animated:YES];
}

//最近邀约点击事件
- (void)inviteClick:(UITapGestureRecognizer *)tap{
    NSUInteger n = tap.view.tag - 300;
    
    //    InviteInfo *info = [_inviteInfoArr objectAtIndex:n];
    ////#warning 3.邀约详情
    //
    //    UserInfoViewController *user = [[UserInfoViewController alloc]init];
    //    user.uid = info.tbi_uid;
    
    
    //#warning 3.参数传递不对啊
    //    NSString *str = [NSString stringWithFormat:@"%@%@/%@",[Url getInviteDetail],info.tbi_uid,self.event_id];
    //    [GB_NetWorkUtils startGetAsyncRequest:str delegate:self tag:9];
    //    tvc = [[UIStoryboard storyboardWithName:@"my" bundle:nil]instantiateViewControllerWithIdentifier:@"TestViewController"];
    ////    tvc.bean = bean;
    //    tvc.event_id = self.event_id;
    
    
    //    [self.navigationController pushViewController:user animated:YES];
}


#pragma mark - 添加控件
-(void)initFrame
{
    self.view.backgroundColor = GB_UIColorFromRGB(58, 58, 58);
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.backgroundView = nil;
    tableView.separatorColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.hidden = NO;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    
    //    UITableView *rightTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight])];
    //    rightTableView.dataSource = self;
    //    rightTableView.delegate = self;
    //    rightTableView.backgroundColor = [UIColor clearColor];
    //    rightTableView.backgroundView = nil;
    //    rightTableView.separatorColor = [UIColor clearColor];
    //    rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    [rightTableView addFooterWithTarget:self action:@selector(footerRefresh)];
    //    personhall = [[[NSBundle mainBundle]loadNibNamed:@"PersonhallHeadView" owner:nil options:nil]lastObject];
    //    personhall.expertScrollView.showsHorizontalScrollIndicator = NO;
    //    personhall.inviteScrollView.showsHorizontalScrollIndicator = NO;
    //    rightTableView.tableHeaderView = personhall;
    //    [self.view addSubview:rightTableView];
    //    rightTableView.hidden = YES;
    //    self.rightTableView = rightTableView;
    
    _searchBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_search"]];
    _searchBg.center = CGPointMake([GB_DeviceUtils getScreenWidth]*0.5,[NavUtils getNavHeight]+_searchBg.image.size.height *0.5f);
    
    [NavUtils addNavBgView:self color:GB_UIColorFromRGB(37, 37, 37)];
    [NavUtils addRightButton:self icon:[UIImage imageNamed:@"btn_search"] sel:@selector(c_search)];
    float width = 65.5f;
    float height = 30.5f;
    
    // 演出大厅
    self.dramaBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5-width-5, [NavUtils getNavHeight]*0.5-height*0.5, width, height) image:nil imageH:nil id:self sel:@selector(btnClick:)];
    self.dramaBtn.tag = 100;
    [self.view addSubview:self.dramaBtn];
    
    // 人物大厅
    self.personBtn = [GB_WidgetUtils getButton:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5+5, [NavUtils getNavHeight]*0.5-height*0.5, width, height) image:nil imageH:nil id:self sel:@selector(btnClick:)];
    self.personBtn.tag = 101;
    [self.view addSubview:self.personBtn];
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake([GB_DeviceUtils getScreenWidth]*0.5-1, [NavUtils getNavHeight]*0.5-height*0.5+8, 2, height-18)];
    lineView.image = [UIImage imageNamed:@"btn_index_line"];
    [self.view addSubview:lineView];
    [NavUtils addLeftButton:self iconN:[UIImage imageNamed:@"btn_drawer"] iconH:nil sel:@selector(c_drawer)];
    
    //    self.messageLabel = [GB_WidgetUtils getLabel:CGRectMake(23, 10, 16, 16) title:nil font:[UIFont systemFontOfSize:10] color:[UIColor whiteColor]];
    //    _messageLabel.backgroundColor = GB_UIColorFromRGB(251, 80, 87);
    //    _messageLabel.layer.masksToBounds = YES;
    //    _messageLabel.layer.cornerRadius = 8;
    //    _messageLabel.textAlignment = NSTextAlignmentCenter;
    //     _messageLabel.hidden = NO;
    //    [self.view addSubview:_messageLabel];
    
    [self btnClick:_personBtn];
}

#pragma mark - 人物大厅的上拉刷新
- (void)footerRefresh
{
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        if(currentPage <= [pageNum intValue]){
            [Static add:self.navigationController.view msg:@"正在加载"];
            
#warning --- 在这里修改BUG页面
            //            if ([_tu_gender isEqualToString:@""]) {
            [GB_NetWorkUtils startGetAsyncRequest:[Url getMorePersonHallDateUrl:currentPage] delegate:self tag:4];
            //            }else{
            //                [GB_NetWorkUtils startGetAsyncRequest:[NSString stringWithFormat:@"%@?a=%@&b=%@",[Url getMorePersonHallDateUrl:currentPage],@"123",@"344"] delegate:self tag:5];
            //            }
            
            currentPage ++;
            
        }else{
            [Static add:self.navigationController.view msg:@"没有更多的数据"];
            [self performSelector:@selector(cancelView:) withObject:self.navigationController.view afterDelay:0.6];
            //            [_rightTableView footerEndRefreshing];
        }
    }
}

- (void)cancelView:(UIView *)view
{
    [Static remove:self.navigationController.view];
}

//-(void)initData{
//    [super initData];
////    if(self.isLoad){
////        return;
////    }
//    [self reloadData];
//}


- (void)btnClick:(UIButton *)btn
{
#pragma mark - 点击演出大厅
    if(btn.tag == 100){
        
        _tableView.hidden = NO;
        _rightTableView.hidden = YES;
        [self.dramaBtn setImage:[UIImage imageNamed:@"btn_index_left_n"] forState:UIControlStateNormal];
        [self.personBtn setImage:[UIImage imageNamed:@"btn_index_right_h"] forState:UIControlStateNormal];
        [self reloadData];
        
    }else if(btn.tag == 101){
        [_searchBg removeFromSuperview];
        _tableView.hidden = YES;
        _rightTableView.hidden = NO;
        currentPage = 2;
        [self.dramaBtn setImage:[UIImage imageNamed:@"btn_index_left_h"] forState:UIControlStateNormal];
        [self.personBtn setImage:[UIImage imageNamed:@"btn_index_right_n"] forState:UIControlStateNormal];
        [GB_NetWorkUtils startGetAsyncRequest:[Url getPersonHallInfoUrl] delegate:self tag:3];
        [Static add:self.navigationController.view msg:@"正在加载"];
    }
}

#pragma mark - 演出大厅的剧目列表
//邀约剧目列表
-(void)reloadData{
    [super reloadData];
    if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
        //[Static add:self.navigationController.view msg:@"正在加载"];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        
        if([User isLogin])
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"uid" value:[User getUserInfo].uid]];
        if(_tableView.hidden){
            HomeEventRightViewController *right = (HomeEventRightViewController *)[PublicDao shareInstance].rootViewController.rightViewController;
            
            if(right.hallCity == -2){
                if([GB_SharedPreferenceUtils hasObject:kCacheHallCityName] && [GB_SharedPreferenceUtils hasObject:kCacheHallCityId]){
                    right.hallCity = [[GB_SharedPreferenceUtils getObjectForKey:kCacheHallCityId] intValue];
                }
            }
            if(right.hallCity !=-2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"city_id" value:GB_NSStringFromInt(right.hallCity)]];
            }
            if(right.state != -2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"invite_flag" value:GB_NSStringFromInt(right.state)]];
            }
            if(right.gender != -2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"gender" value:GB_NSStringFromInt(right.gender)]];
            }
            if(right.age != -2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"age_field" value:GB_NSStringFromInt(right.age)]];
            }
            NSString *str = right.searchContent;
            NSString *searchStr =[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            //LZLog(@"%@",[[GB_KeyValue alloc]initWithKeyValue:@"keywords" value:searchStr]);
            
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"keywords" value:searchStr]];
            
            
            
            [GB_NetWorkUtils startPostAsyncRequest:[Url getPersonHallInfoUrl] list:arr delegate:self tag:5];
        }else{
            HomeEventRightViewController *right = self.drawer.leftViewController.eventRightViewController;
            if(right.category != -2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"cate" value:GB_NSStringFromInt(right.category)]];
            }
            if(right.city == -2){
                if([GB_SharedPreferenceUtils hasObject:kCacheCityName] && [GB_SharedPreferenceUtils hasObject:kCacheCityId]){
                    right.city = [[GB_SharedPreferenceUtils getObjectForKey:kCacheCityId] intValue];
                }
            }
            if(right.city !=-2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"city_id" value:GB_NSStringFromInt(right.city)]];
            }
            
            
            if(right.style != -2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"order_field" value:GB_NSStringFromInt(right.style)]];
            }
            
            if(right.state != -2){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"invite_tag" value:GB_NSStringFromInt(right.state)]];
            }
            
            NSString *str = right.searchContent;
            NSString *searchStr =[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"keywords" value:searchStr]];
           HomeEventRightViewController *rightViewController = (HomeEventRightViewController *)self.drawer.rightViewController;
            BOOL shouldLoad = [GB_ToolUtils isBlank:rightViewController.cityArr]||[GB_ToolUtils isBlank:rightViewController.categoryArr];
            if(shouldLoad){
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"cate_city" value:@"1"]];
            }
            else{
                [arr addObject:[[GB_KeyValue alloc]initWithKeyValue:@"cate_city" value:@"0"]];
            }
            [GB_NetWorkUtils startPostAsyncRequest:[Url getEventListUrl] list:arr delegate:self tag:shouldLoad?1:2];
        }
    }
}

-(void)c_search
{
    //    if(_tableView.hidden){
    //
    //    }else{
    //        [self.drawer rightOpen];
    //    }
    
    CardsFlashViewController *flash = [[CardsFlashViewController alloc]init];
    [self.navigationController pushViewController:flash animated:YES];
}

-(void)c_drawer{
    [super c_drawer];
}

#pragma mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _tableView){
        return 340;
    }else{
        return 90;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView ==  _tableView){
        return _contentData.count;
    }else{
        return _rightData.count;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _tableView){
        EventListBean *bean = [_contentData objectAtIndex:indexPath.row];
        EventViewController *event = [[EventViewController alloc]init];
        event.event_id = bean.event_info.tad_id;
        [self.navigationController pushViewController:event animated:YES];
    }else{
        //        TreadsInfo *info = [_rightData objectAtIndex:indexPath.row];
        //        if([info.tut_type isEqualToString:@"invite_success"]||[info.tut_type isEqualToString:@"fuyue_success"]){
        //            NSString *idInfo = info.tut_tread_id;
        //            NSArray *arr = [idInfo componentsSeparatedByString:@"_"];
        //            DateAllSuccessViewController *success = [[DateAllSuccessViewController alloc]init];
        //            if([arr[1] isEqualToString:[User getUserInfo].uid]){
        //                success.tbi_uid = arr[1];
        //                success.target_uid = arr[2];
        //            }else{
        //                success.tbi_uid = arr[2];
        //                success.target_uid = arr[1];
        //            }
        //                success.event_id = arr[0];
        //            [self.navigationController pushViewController:success animated:YES];
        //        }else if ([info.tut_type isEqualToString:@"update_portrait"]||[info.tut_type isEqualToString:@"update_profile"]){
        //#pragma mark - 个人信息控制器
        //             UserInfoViewController *user = [[UserInfoViewController alloc]init];
        //            user.uid = info.tut_uid;
        //            [self.navigationController pushViewController:user animated:YES];
        //
        //        }else if ([info.tut_type isEqualToString:@"event_like"]||[info.tut_type isEqualToString:@"event_add_comment"]){
        //            EventViewController *event = [[EventViewController alloc]init];
        //            event.event_id = info.tut_tread_id;
        //            [self.navigationController pushViewController:event animated:YES];
        //        }else if ([info.tut_type isEqualToString:@"create_invite"]||[info.tut_type isEqualToString:@"cancel_fuyue"]){
        //            //NSString *idInfo = info.tut_tread_id;
        //            //NSArray *arr = [idInfo componentsSeparatedByString:@"_"];
        //            DateAllCenterViewController *center = [[DateAllCenterViewController alloc]init];
        //            //center.event_id = arr[0];
        //            center.event_id = info.tut_tread_id;
        //            DateAllLeftViewController *left = [[DateAllLeftViewController alloc]init];
        //            DateAllSwitchViewController *con = [[DateAllSwitchViewController alloc]initWithLeftViewController:left centerViewController:center];
        //            [self.navigationController pushViewController:con animated:YES];
        //        }else if([info.tut_type isEqualToString:@"create_fuyue"]){
        //            EventViewController *event = [[EventViewController alloc]init];
        //            event.event_id = info.tut_tread_id;
        //            [self.navigationController pushViewController:event animated:YES];
        //        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _tableView){
        static NSString *identifier = @"eventListCellIdentifier";
        //    EventListCell *cell = [tableView dequeueReusableCellWithIdentifier:
        //                     identifier];
        //    if (cell == nil) {
        EventListCell *cell = [[EventListCell alloc]
                               initWithStyle:UITableViewCellStyleDefault
                               reuseIdentifier:identifier];
        //    }
        
#pragma mark - 演出大厅控制器
        EventListBean *bean = [_contentData objectAtIndex:indexPath.row];
        bean.baseViewController = self;
        [cell setEventListBean:[_contentData objectAtIndex:indexPath.row]];
        
        return cell;
    }else{
        PersonhallCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(cell == nil){
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PersonhallCell" owner:self options:nil]lastObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        cell.delegate = self;
        TreadsInfo *info = [_rightData objectAtIndex:indexPath.row];
        [cell makeCell:info];
        return cell;
    }
}

- (void)removeAllObjects
{
    [_expertInfoArr removeAllObjects];
    [_inviteInfoArr removeAllObjects];
    [_rightData removeAllObjects];
    [_expertImageViewArr removeAllObjects];
    [_inviteInfoArr removeAllObjects];
}

#pragma mark GB_NetWorkDelegate
-(void)GB_requestDidFailed:(int)tag
{
    [Static remove:self.navigationController.view];
    [_tableView refreshFinished];
    //    [_rightTableView footerEndRefreshing];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag
{
    [Static remove:self.navigationController.view];
    [_tableView refreshFinished];
    //    [_rightTableView footerEndRefreshing];
    //if([Error verify:str view:self.navigationController.view]){
    //    if(!_tableView.hidden){
    if(tag == 2 || tag == 1){
        self.isLoad = YES;
        if(tag == 1){
            eventRightViewController = self.drawer.leftViewController.eventRightViewController;
            NSDictionary *responseDic = [Static getRequestData:str];
            NSMutableArray *cityArr = [NSMutableArray array];
            [cityArr addObject:@{@"id":[NSNumber numberWithInt:-1],@"name":@"全部"}];
            [cityArr addObjectsFromArray:[[Static getRequestData:str]objectForKey:@"city_ids"]];
            eventRightViewController.cityArr = cityArr;
            //类别数组
            eventRightViewController.categoryArr = [[Static getRequestData:str] objectForKey:@"categorys"];
            eventRightViewController.city = [[responseDic objectForKey:@"city_id"] intValue];
            eventRightViewController.category = [[responseDic objectForKey:@"cate"] intValue];
            eventRightViewController.style = [[responseDic objectForKey:@"order_field"] intValue];
            
            //right1ViewController1.state = [[responseDic objectForKey:@"invite_tag"]intValue];
            NSDictionary *valid_city_cates = [[Static getRequestData:str] objectForKey:@"valid_city_cates"];
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            for (NSDictionary *cityDic in cityArr) {
                NSString *cityId = [cityDic objectForKey:@"id"];
                NSArray *arr = [valid_city_cates objectForKey:cityId];
                NSMutableSet *set = [NSMutableSet set];
                for (NSDictionary *vCityDic in arr) {
                    [set addObject:[vCityDic objectForKey:@"id"]];
                }
                [dic setObject:set forKey:cityId];
            }
            eventRightViewController.cityCategoryDic = dic;
        }
        [eventRightViewController.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [eventRightViewController initFrame];
        
        [_contentData setArray:[EventListBean getBeanList:[[Static getRequestData:str] objectForKey:@"events"]]];
        if(_contentData.count >0){
            [_searchBg removeFromSuperview];
        }else{
            [self.view addSubview:_searchBg];
        }
        
        int count = [[[Static getRequestData:str] objectForKey:@"message_count"] intValue];
        [[PublicDao shareInstance].rootViewController.leftViewController setCurrentCount:count];
        
        
        
        [_tableView reloadData];
        if([GB_ToolUtils isNotBlank:_contentData])
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
        __weak HomeEventViewController *safeSelf = self;
        [_tableView setPullToRefreshHandler:^{
            [safeSelf reloadData];
        }];
        [_tableView setPullToRefreshViewBackgroundColor:self.view.backgroundColor];
        [_tableView setPullToRefreshViewPullingText:@"下拉即可刷新"];
        [_tableView setPullToRefreshViewReleaseText:@"松开即可刷新"];
        [_tableView setPullToRefreshViewLoadingText:@"加载中"];
        [_tableView setPullToRefreshViewActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    }
    else{
    //        if (tag == 3){
    //
    //            right1ViewController1 = self.drawer.leftViewController.right1ViewController;
    //            [right1ViewController1.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //            //获得大厅右侧城市列表
    //            //Right1ViewController *right = (Right1ViewController *)[PublicDao shareInstance].rootViewController.rightViewController;
    //            NSDictionary *responseDic = [Static getRequestData:str];
    //            NSMutableArray *cityArr = [NSMutableArray array];
    //            [cityArr addObject:@{@"id":[NSNumber numberWithInt:0],@"name":@"全部"}];
    //            NSDictionary *dic1 = [GB_JsonUtils getDictionaryByJsonString:str];
    //            NSArray *cityArr1 = [dic1 objectForKey:@"citys"];
    //            [cityArr addObjectsFromArray:cityArr1];
    //            right1ViewController1.hallCityArr = cityArr;
    //            right1ViewController1.hallCity =  [[responseDic objectForKey:@"city_id"] intValue];
    //            [right1ViewController1 initFrame1];
    //            [self removeAllObjects];
    //            NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
    //            NSArray *expertArr = [dic objectForKey:@"recommended_user"];
    //            NSArray *inviteArr = [dic objectForKey:@"invite_person"];
    //            NSArray *treadsArr = [dic objectForKey:@"treads"];
    //            pageNum = [dic objectForKey:@"total_page"];
    //
    //        //今日达人
    //        for(NSDictionary *dic1 in expertArr){
    //            ExpertInfo *expert = [[ExpertInfo alloc]initWithDictionary:dic1 error:nil];
    //            [_expertInfoArr addObject:expert];
    //        }
    //        //最近邀约
    //        for(NSDictionary *dic2 in inviteArr){
    //            InviteInfo *invite = [[InviteInfo alloc]initWithDictionary:dic2 error:nil];
    //            [_inviteInfoArr addObject:invite];
    //        }
    //        //全部
    //        for(NSDictionary *dic3 in treadsArr){
    //            TreadsInfo *treads = [[TreadsInfo alloc]initWithDictionary:dic3 error:nil];
    //            [_rightData addObject:treads];
    //        }
    //
    //
    //        for(int i = 0;i<_expertInfoArr.count;i++){
    //            ExpertView *expert = [[[NSBundle mainBundle]loadNibNamed:@"ExpertView" owner:self options:nil]lastObject];
    //            expert.frame = CGRectMake((60 + 3)*i, 0, 60, 60);
    //            expert.userInteractionEnabled = YES;
    //            expert.tag = 200 + i;
    //            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick:)];
    //            [expert addGestureRecognizer:tap];
    //            [_expertImageViewArr addObject:expert];
    //            [personhall.expertScrollView addSubview:expert];
    //        }
    //            personhall.expertScrollView.contentSize = CGSizeMake(63*_expertInfoArr.count, 60);
    //        for (int i = 0; i<_expertInfoArr.count; i++) {
    //            ExpertView *iv = [_expertImageViewArr objectAtIndex:i];
    //            ExpertInfo *expert = [_expertInfoArr objectAtIndex:i];
    //            [GB_NetWorkUtils loadImage:expert.tu_portrait container:iv.bigImg type:GB_ImageCacheTypeAll];
    //        }
    //
    //        for(int j = 0; j<_inviteInfoArr.count;j++){
    //            UIImageView *invite = [[UIImageView alloc]initWithFrame:CGRectMake((60+3)*j, 0, 60, 60)];
    //            invite.userInteractionEnabled = YES;
    //            invite.tag = 300 +j;
    //            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(inviteClick:)];
    //            [invite addGestureRecognizer:tap];
    //            [_inviteImageViewArr addObject:invite];
    //            [personhall.inviteScrollView addSubview:invite];
    //        }
    //        personhall.inviteScrollView.contentSize = CGSizeMake(63*_inviteInfoArr.count, 60);
    //        for (int j = 0; j<_inviteInfoArr.count; j++) {
    //            UIImageView *iv = [_inviteImageViewArr objectAtIndex:j];
    //            InviteInfo *invite = [_inviteInfoArr objectAtIndex:j];
    //            [GB_NetWorkUtils loadImage:invite.tbi_portrait container:iv type:GB_ImageCacheTypeAll];
    //        }
    //
    //            [_rightTableView reloadData];
    //    }
    //        else if (tag == 4){
    //
    //        NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
    //        NSArray *treadsArr = [dic objectForKey:@"treads"];
    //
    //            //LZLog(@"%@",dic);
    //
    //        for(NSDictionary *dic in treadsArr){
    //            TreadsInfo *treads = [[TreadsInfo alloc]initWithDictionary:dic error:nil];
    //            [_rightData addObject:treads];
    //            }
    //            [_rightTableView reloadData];
    //        }
    
    //        else if (tag ==5){
    //                right1ViewController1 = self.drawer.leftViewController.right1ViewController;
    //                NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:str];
    //
    //                NSArray *treadsArr = [dic objectForKey:@"treads"];
    //
    //            // _tu_gender = [treadsArr firstObject][@"tu_gender"];
    //
    //            //LZLog(@"%@",_tu_gender);
    //
    //                [_rightData removeAllObjects];
    //                for(NSDictionary *dic3 in treadsArr){
    //                    TreadsInfo *treads = [[TreadsInfo alloc]initWithDictionary:dic3 error:nil];
    //                    [_rightData addObject:treads];
    //                }
    //                [_rightTableView reloadData];
    //            }
    //        }
    }
}

//#pragma mark - 点击聊天按钮
////聊天事件
//- (void)chatButtonClick:(NSInteger)row
//{
////    TreadsInfo *info = [_rightData objectAtIndex:row];
////    MsgDetailViewController *message = [[MsgDetailViewController alloc]init];
////    message.name = info.tu_nickname;
////    message.fromId = info.tut_uid;
////    [self.navigationController pushViewController:message animated:YES];
////     LZLog(@"%@",info.tut_uid);
//
//    TreadsInfo *info = [_rightData objectAtIndex:row];
//    EMBuddy *buddy = [EMBuddy buddyWithUsername:info.tut_uid];
//    //LZLog(@"%@",buddy.username);
//    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:buddy.username isGroup:NO];
//
//    //chatVC.title = info.tu_nickname;
//    chatVC.fromId = buddy.username;
//    chatVC.tu_portrait = info.tu_portrait;
//    [self.navigationController pushViewController:chatVC animated:YES];
//}

//
//- (void)portraitClick:(NSInteger)row
//{
//    UserInfoViewController *user = [[UserInfoViewController alloc]init];
//    TreadsInfo *info = [_rightData objectAtIndex:row];
//    user.uid =  info.tut_uid;
//    [self.navigationController pushViewController:user animated:YES];
//}

-(void)setCurrentCount:(int)count{
    
}


@end

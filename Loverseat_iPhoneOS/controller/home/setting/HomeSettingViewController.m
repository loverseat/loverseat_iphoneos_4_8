//
//  SettingViewController.m
//  7DianBanPocket_iPhoneOS
//
//  Created by GaoHang on 14-9-29.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "HomeSettingViewController.h"
#import "EditPwdViewController.h"
#import "FeedBackViewController.h"
#import "WebUrlViewController.h"
#import "SettingCell.h"
#import "LogoutCell.h"
#import "MobClick.h"

@interface HomeSettingViewController ()
<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,GB_NetWorkDelegate>
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation HomeSettingViewController

-(id)init{
    self = [super init];
    if(self){
        self.type = 3;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initFrame];
}

-(void)initFrame{
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [NavUtils getNavHeight], [GB_DeviceUtils getScreenWidth], [GB_DeviceUtils getScreenHeight]-[NavUtils getNavHeight]) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundView = nil;
    tableView.separatorColor = GB_UIColorFromRGB(208, 208, 208);
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    [NavUtils addNavBgView:self];
    [NavUtils addNavTitleView:self text:@"设定"];
    [NavUtils addLeftButton:self iconN:[UIImage imageNamed:@"btn_drawer_rose"] iconH:nil sel:@selector(c_drawer)];
}

#pragma mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 2;
    }
    else if(section == 1){
        return 4;
    }
    else if(section == 2){
        return 1;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0){
        if(indexPath.row == 1){
            EditPwdViewController *edit = [[EditPwdViewController alloc]init];
            [self.navigationController pushViewController:edit animated:YES];
        }
    }
    if(indexPath.section == 1){
        if(indexPath.row == 0){
            WebUrlViewController *web = [[WebUrlViewController alloc]init];
            web.title = @"帮助说明";
            web.url = [Url getHelpInfoUrl];
            [self.navigationController pushViewController:web animated:YES];
        }
        if(indexPath.row == 1){
            FeedBackViewController *feedback = [[FeedBackViewController alloc]init];
            [self.navigationController pushViewController:feedback animated:YES];
        }
        if(indexPath.row == 2){
            WebUrlViewController *web = [[WebUrlViewController alloc]init];
            web.title = @"版本说明";
            web.url = [Url getVerionInfoUrl];
            [self.navigationController pushViewController:web animated:YES];
        }
        else if(indexPath.row == 3){
            if([GB_DeviceUtils isIOS7]){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id897465131"]];
                
            }
            else{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=897465131&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software"]];
            }
        }
    }
    if(indexPath.section == 2){
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"注销" message:@"确定注销当前账号吗？" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        alertView.tag = 2;
        [alertView show];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 2){
        static NSString *identifier = @"LogoutCellIdentifier";
        LogoutCell *cell = [tableView dequeueReusableCellWithIdentifier:
                            identifier];
        if (cell == nil) {
            cell = [[LogoutCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:identifier];
        }
        return cell;
    }
    static NSString *identifier = @"SettingCellIdentifier";
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:
                         identifier];
    if (cell == nil) {
        cell = [[SettingCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    cell.state.text = nil;
    [cell setStateArrowFrame];
    BOOL hasArrow  = NO;
    BOOL hasSwitch = NO;
    if(indexPath.section == 0){
        switch (indexPath.row) {
            case 0:
                cell.title.text = @"关联手机";
                cell.state.text = [GB_SecurityUtils getMaskString:[User getUserInfo].tu_mobile median:4 point:GB_AlignmentCenter];
                [cell setStateNormalFrame];
                break;
//            case 1:
//                cell.title.text = @"关联邮箱";
//                if([GB_StringUtils isNotBlank:[User getUserEmail]]){
//                    cell.state.text = [GB_SecurityUtils getMaskString:[User getUserEmail] median:4 point:GB_AlignmentCenter];
//                }
//                else{
//                    cell.state.text = @"未绑定";
//                }
//                [cell setStateNormalFrame];
//                break;
            case 1:
                cell.title.text = @"修改密码";
                hasArrow = YES;
                break;
            default:
                break;
        }
    }
    if(indexPath.section == 1){
        switch (indexPath.row) {
            case 0:
                cell.title.text = @"帮助说明";
                hasArrow = YES;
                break;
            case 1:
                cell.title.text = @"意见反馈";
                hasArrow = YES;
                break;
            case 2:
                cell.title.text = @"版本介绍";
                hasArrow = YES;
                break;
            case 3:
                cell.title.text = @"给双人座评分";
                hasArrow = YES;
                break;
            default:
                break;
        }
    }
    
    if(hasArrow){
        cell.arrow.hidden = NO;
        UIImage *arrow = [UIImage imageNamed:@"icon_arrow_rose"];
        cell.arrow.image = arrow;
        cell.arrow.frame = CGRectMake([GB_DeviceUtils getScreenWidth]-15-7.5-arrow.size.width*0.5, 27.5f-arrow.size.height*0.5, arrow.size.width, arrow.size.height);
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    else{
        cell.arrow.hidden = YES;
        cell.arrow.image = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if(hasSwitch){
        cell.switchView.hidden = NO;
    }
    else{
        cell.switchView.hidden = YES;
    }
    
    return cell;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 2){
        if(buttonIndex != alertView.cancelButtonIndex){
            if([GB_NetWorkUtils checkNetWork:self.navigationController.view]){
                [Static add:self.navigationController.view msg:@"正在注销"];
                [GB_NetWorkUtils startGetAsyncRequest:[Url getLogoutUrl] delegate:self tag:1];
            }
        }
    }
    if(alertView.tag == 1 && alertView.tag == 0){
        if(alertView.tag == buttonIndex){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/xian-lu-ke-dao-you/id897465131?ls=1&mt=8"]];
        }
    }
}

-(void)GB_requestDidFailed:(int)tag{
    [Static remove:self.navigationController.view];
}

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag{
    [Static remove:self.navigationController.view];
    
    if(tag == 1){
        if([Error verify:str view:self.navigationController.view]){
            [User logout];
            [MobClick event:@"4"];
            [GB_ToolUtils postNotificationName:@"GB_ReStart" object:nil];
        }
    }
}

@end

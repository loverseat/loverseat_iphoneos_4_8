//
//  ResponseUtils.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "ResponseUtils.h"

@implementation ResponseUtils

+(NSString *)getCurrentStr:(NSString *)res{
    if([res rangeOfString:@"{"].location != NSNotFound){
        res = [res substringFromIndex:[res rangeOfString:@"{"].location];
    }
    return res;
}

+(BOOL)check:(NSString *)res view:(UIView *)view{
    res = [ResponseUtils getCurrentStr:res];
    if(![Error isRequestSuccess:res]){
        [AlertUtils remove:view];
        if([Error isRequestErrType:res str:@"invalid_code"]){
            [Static alert:view msg:@"密码错误"];
        }
        else if([Error isRequestErrType:res str:@"failure"]){
            
        }
        else if([Error isRequestErrType:res str:@"wallet_balance_not_enough"]){
            [Static alert:view msg:@"余额不足"];
        }
        else if([Error isRequestErrType:res str:@"invalid_page"]){
            [Static alert:view msg:@"最后一页啦!"];
        }
        else if([Error isRequestErrType:res str:@"empty_mobile"]){
            [Static alert:view msg:@"请填写手机号"];
        }
        
        else if([Error isRequestErrType:res str:@"mobile_invalid"]){
            [Static alert:view msg:@"请输入正确的手机格式"];
        }
        
        else if([Error isRequestErrType:res str:@"empty_code"]){
            [Static alert:view msg:@"请输入验证码"];
        }
        
        else if([Error isRequestErrType:res str:@"empty_password"]){
            [Static alert:view msg:@"请输入密码"];
        }
        
        else if([Error isRequestErrType:res str:@"invalid_code"]){
            [Static alert:view msg:@"验证码错误"];
        }
        
        else if([Error isRequestErrType:res str:@"invalid_pwd"]){
            [Static alert:view msg:@"密码错误"];
        }
        else if([Error isRequestErrType:res str:@"no_discovery"]){
            
        }
        else if([Error isRequestErrType:res str:@"no_orders"]){
            
        }
        else if([Error isRequestErrType:res str:@"null"]){
            
        }
        else{
            [AlertUtils alert:view msg:[Static getRequestData:res]];
        }
        return NO;
    }
    return YES;
}

+(BOOL)isRequestSuccess:(NSString *)res{
    NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:res];
    return [GB_ToolUtils isNotBlank:dic]&&(([GB_ToolUtils isNotBlank:[dic objectForKey:@"success"]]&&[[dic objectForKey:@"success"] isEqualToString:@"yes"])||([GB_ToolUtils isNotBlank:[dic objectForKey:@"succes"]]&&[[dic objectForKey:@"succes"] isEqualToString:@"yes"]));
}

+(BOOL)isRequestErrType:(NSString *)data res:(NSString *)res{
    id result = [Static getRequestData:data];
    if([result isKindOfClass:[NSString class]]){
        return [result isEqualToString:res];
    }
    return NO;
}

+(id)getRequestData:(NSString *)res{
    res = [ResponseUtils getCurrentStr:res];
    NSDictionary *dic = [GB_JsonUtils getDictionaryByJsonString:res];
    return [dic objectForKey:@"msg"];
}

@end

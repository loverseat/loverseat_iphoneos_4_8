//
//  AlertUtils.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

@interface AlertUtils : NSObject

/**
 *  添加提示框
 *
 *  @param view 添加的视图
 *  @param msg 显示的文字
**/
+(void)add:(UIView *)view msg:(NSString *)msg;
/**
 * 隐藏提示框
 */
+(void)remove:(UIView *)view;
/**
 *  提示文字
 */
+(void)alert:(UIView *)view msg:(NSString *)msg;

@end

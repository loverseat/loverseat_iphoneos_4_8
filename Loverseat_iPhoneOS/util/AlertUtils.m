//
//  AlertUtils.m
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import "AlertUtils.h"
#import "AlertView.h"

@implementation AlertUtils

#define GB_AlertViewTag 99*99*99*99

+(void)add:(UIView *)view msg:(NSString *)msg{
    if([GB_DeviceUtils isIOS7]){
        if([view viewWithTag:GB_AlertViewTag] == nil){
            CGRect rect = CGRectMake(view.bounds.size.width*.5-80,view.bounds.size.height*.5-70, 160, 140);
            AlertView *v = [[AlertView  alloc]initWithFrame:rect isKind:YES];
            [v setText:msg];
            v.tag = GB_AlertViewTag;
            [view addSubview:v];
        }
    }
    else{
        [GB_AlertUtils add:view msg:msg];
    }
}

+(void)remove:(UIView *)view{
    [GB_AlertUtils remove:view];
}

+(void)alert:(UIView *)view msg:(NSString *)msg{
    if([GB_StringUtils isBlank:msg])return;
    if([GB_DeviceUtils isIOS7]){
        if([view viewWithTag:GB_AlertViewTag] == nil){
            CGRect rect = CGRectMake(view.bounds.size.width*.5-80,view.bounds.size.height*.5-70, 160, 140);
            AlertView *v = [[AlertView alloc]initWithFrame:rect isKind:NO];
            [v setText:msg];
            v.tag = GB_AlertViewTag;
            [self performSelector:@selector(remove:) withObject:v afterDelay:1.5f];
            [view addSubview:v];
        }
    }
    else{
        [GB_AlertUtils alert:view msg:msg];
    }
}


@end

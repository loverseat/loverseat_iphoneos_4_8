//
//  ResponseUtils.h
//  Loverseat_iPhoneOS
//
//  Created by GaoHang on 15/4/8.
//  Copyright (c) 2015年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>

#warning 最后找服务器修改统一返回格式
#warning 最后找服务器修改统一提示内容，客户端不做转换

@interface ResponseUtils : NSObject

+(BOOL)check:(NSString *)res view:(UIView *)view;

+(BOOL)isRequestSuccess:(NSString *)res;

+(BOOL)isRequestErrType:(NSString *)data res:(NSString *)res;

+(NSString *)getCurrentStr:(NSString *)res;

+(id)getRequestData:(NSString *)res;

@end

//
//  TextAlertView.h
//  7dianban
//
//  Created by GaoHang on 14-2-27.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

@interface AlertView : UIView
@property (strong, nonatomic) UILabel *titleLabel;
@property BOOL isKind;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) UIColor *blurTintColor;
-(id)initWithFrame:(CGRect)frame isKind:(BOOL)isKind;
-(void)setText:(NSString *)text;
@end

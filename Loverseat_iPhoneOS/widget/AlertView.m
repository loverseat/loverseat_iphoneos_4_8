//
//  TextAlertView.m
//  7dianban
//
//  Created by GaoHang on 14-2-27.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import "AlertView.h"
#import <QuartzCore/QuartzCore.h>

@interface AlertView ()

@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) CALayer *blurLayer;

@end
@implementation AlertView

-(id)initWithFrame:(CGRect)frame isKind:(BOOL)isKind{
    self = [super initWithFrame:frame];
    if(self){
        [self setToolbar:[[UIToolbar alloc] initWithFrame:[self bounds]]];
        [self setBlurTintColor:[UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.0f]];
        [self setBlurLayer:[[self toolbar] layer]];
        
        UIView *blurView = [UIView new];
        blurView.layer.masksToBounds = YES;
        blurView.layer.cornerRadius = 10;
        [blurView setUserInteractionEnabled:NO];
        [blurView.layer addSublayer:[self blurLayer]];
        [blurView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [blurView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        [self insertSubview:blurView atIndex:0];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[blurView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(blurView)]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(1)-[blurView]-(1)-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(blurView)]];
        
        self.opaque = NO;
        self.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        UILabel *titleLabel =[[UILabel alloc]init];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        titleLabel.numberOfLines = 0;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.autoresizingMask =
        UIViewAutoresizingFlexibleLeftMargin |
        UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin |
        UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:titleLabel];
        self.titleLabel = titleLabel;
        
        if(isKind){
            UIActivityIndicatorView *activityIndicatorView =[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicatorView.color = [UIColor blackColor];
            activityIndicatorView.autoresizingMask =
            UIViewAutoresizingFlexibleLeftMargin |
            UIViewAutoresizingFlexibleRightMargin|
            UIViewAutoresizingFlexibleTopMargin|
            UIViewAutoresizingFlexibleBottomMargin;
            [self addSubview:activityIndicatorView];
            self.activityIndicatorView = activityIndicatorView;
        }
    }
    return self;
}

- (void)removeView
{
    UIView *aSuperview = [self superview];
    [super removeFromSuperview];
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [[aSuperview layer] addAnimation:animation forKey:@"GH_layerAnimation"];
}

-(void)setText:(NSString *)text{
    const CGFloat DEFAULT_LABEL_WIDTH = 140;
    const CGFloat DEFAULT_LABEL_HEIGHT = 60;
    CGRect labelFrame = CGRectMake(0, 0, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT);
    CGSize _size = [GB_ToolUtils getTextSize:text font:[UIFont boldSystemFontOfSize:[UIFont labelFontSize]] size:CGSizeMake(DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT)];
    
    
    labelFrame.origin.x = 0.5 * (self.frame.size.width - DEFAULT_LABEL_WIDTH);
    labelFrame.origin.y = 0.5 * (self.frame.size.height -_size.height);
    labelFrame.size.height = _size.height;
    
    
    if(self.activityIndicatorView!=nil){
        labelFrame.origin.y = 0.5 * (self.frame.size.height -_size.height-50);
        CGRect activityIndicatorRect = self.activityIndicatorView.frame;
        activityIndicatorRect.origin.x =
        0.5 * (self.frame.size.width - activityIndicatorRect.size.width);
        activityIndicatorRect.origin.y =
        labelFrame.origin.y + labelFrame.size.height+5;
        self.activityIndicatorView.frame = activityIndicatorRect;
        [self.activityIndicatorView startAnimating];
    }
    self.titleLabel.frame = labelFrame;
    self.titleLabel.text = text;
    
}

- (void) setBlurTintColor:(UIColor *)blurTintColor {
    [self.toolbar setBarTintColor:blurTintColor];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self.blurLayer setFrame:[self bounds]];
}

@end


//
//  AppDelegate.m
//  7DianBanPocket_iPhoneOS
//
//  Created by gaohang on 14-4-16.
//  Copyright (c) 2014年 gaohang. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+umeng.h"
#import "AppDelegate+home.h"







#import "EventViewController.h"

#import "DateAllCenterViewController.h"
#import "DateAllLeftViewController.h"
#import "DateAllSwitchViewController.h"


//#import "AppDelegate+EaseMob.h"
#import "LoginViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [self umengApplication:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [self umengApplication:application handleOpenURLU:url];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [self umengApplication:application didFinishLaunchingWithOptions:launchOptions];
    [self homeApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    
    
    [self registerRemoteNotification];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)registerRemoteNotification
{
#if !TARGET_IPHONE_SIMULATOR
    UIApplication *application = [UIApplication sharedApplication];
    application.applicationIconBadgeNumber = 0;
    
    //iOS8 注册APNS
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)])
    {
        [application registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else
    {
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeSound |
        UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
    }
    
#endif
}



#pragma mark - 将得到的deviceToken传给SDK
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    if([GB_ToolUtils isNotBlank:deviceToken]){
        NSString *token = [NSString stringWithFormat:@"%@",deviceToken];
        token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Url getParmsArr]];
        [arr addObject:[[[GB_KeyValue alloc]initWithKeyValue:@"token" value:token] autorelease]];
        [arr addObject:[[[GB_KeyValue alloc]initWithKeyValue:@"token_device" value:[GB_DeviceUtils getModel]] autorelease]];
        [arr addObject:[[[GB_KeyValue alloc]initWithKeyValue:@"token_type" value:@"1"] autorelease]];
        [arr addObject:[[[GB_KeyValue alloc]initWithKeyValue:@"token_os" value:[GB_DeviceUtils getOperatingSystem]] autorelease]];
        [User setUserToken:token];
        [GB_NetWorkUtils startSinglePostAsyncRequest:[Url getTokenUrl] list:arr delegate:nil tag:-1];
    }
    
    NSLog(@"token:%@",deviceToken);
}

// 注册deviceToken失败
- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Register Remote Notifications error:{%@}",[error localizedDescription]);
}

//处理收到的消息推送
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    self.userInfo = [NSDictionary dictionaryWithDictionary:userInfo];
    int type = [[userInfo objectForKey:@"type"]intValue];
    if (application.applicationState == UIApplicationStateActive) {
        if(application.applicationIconBadgeNumber!=0){
            application.applicationIconBadgeNumber = 0;
        }
        if(type == 2){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"推送通知" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"忽略" otherButtonTitles:@"查看", nil];
            [alert show];
        }
    } else {
        if(application.applicationIconBadgeNumber!=0){
            application.applicationIconBadgeNumber = 0;
        }
        if(type == 2){
            //跳转到剧目详情
            DateAllCenterViewController *center = [[DateAllCenterViewController alloc]init];
            center.event_id = [userInfo objectForKey:@"tad_id"];
            DateAllLeftViewController *left = [[DateAllLeftViewController alloc]init];
            DateAllSwitchViewController *con = [[DateAllSwitchViewController alloc]initWithLeftViewController:left centerViewController:center];
            [_navController pushViewController:con animated:YES];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    int type = [[_userInfo objectForKey:@"type"]intValue];
    if(buttonIndex!= alertView.cancelButtonIndex){
        if(type == 2){
            //跳转到剧目详情
            EventViewController *event = [[EventViewController alloc]init];
            event.event_id = [_userInfo objectForKey:@"tad_id"];
            [_navController pushViewController:event animated:YES];
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [MobClick event:@"5"];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [MobClick event:@"3"];
    if(application.applicationIconBadgeNumber!=0){
        application.applicationIconBadgeNumber = 0;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
//    [[SDWebImageManager sharedManager] cancelAll];
//    [[SDWebImageManager sharedManager].imageCache clearMemory];
//    [[SDImageCache sharedImageCache]clearDisk];
}

@end

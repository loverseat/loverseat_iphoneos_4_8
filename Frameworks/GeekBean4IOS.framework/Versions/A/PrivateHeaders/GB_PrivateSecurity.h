//
//  GB_PrivateSecurity.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-6-5.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GB_PrivateSecurity : NSObject

+(BOOL)isLegal;

@end

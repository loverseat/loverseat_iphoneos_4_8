//
//  GB_AlertView.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GB_AlertView : UIView

@property (strong, nonatomic) UILabel *titleLabel;
@property BOOL isKind;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;

-(id)initWithFrame:(CGRect)frame isKind:(BOOL)isKind;
-(void)setText:(NSString *)text;

@end

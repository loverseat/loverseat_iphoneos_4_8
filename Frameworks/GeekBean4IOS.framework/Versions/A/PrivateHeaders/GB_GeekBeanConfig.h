//
//  GB_GeekBeanConfig.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-1-30.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#define GB_Name @"GeekBean"
#define GB_Header [GB_Name stringByAppendingString:@"_"]
#define GB_Domain @"http://www.geekbean.com"
#define GB_Identifier @"com.geekbean.ios"
#define GB_PrivateKey @"GB_G_E_E_K_B_E_A_N"

//默认提示内容
#define GB_AlertLoadingText @"正在加载"
//无网络连接提示内容
#define GB_AlertNoNetWorkText @"网络异常"



#define kGB_PrivateSinaKey @"kGB_PrivateSinaKey"
#define kGB_PrivateKaixinKey @"kGB_PrivateKaixinKey"
#define kGB_PrivateDoubanKey @"kGB_PrivateDoubanKey"
#define kGB_PrivateNeteaseKey @"kGB_PrivateNeteaseKey"
#define kGB_PrivateRenrenKey @"kGB_PrivateRenrenKey"


#define kGB_PrivateCookiesKey @"kGB_PrivateCookiesKey"




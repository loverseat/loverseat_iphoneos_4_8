//
//  ASIAuthenticationDialog.h
//  Part of ASIHTTPRequest -> http://allseeing-i.com/ASIHTTPRequest
//
//  Created by Ben Copsey on 21/08/2009.
//  Copyright 2009 All-Seeing Interactive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class GB_ASIHTTPRequest;

typedef enum _GB_ASIAuthenticationType {
	GB_ASIStandardAuthenticationType = 0,
    GB_ASIProxyAuthenticationType = 1
} GB_ASIAuthenticationType;

@interface GB_ASIAutorotatingViewController : UIViewController
@end

@interface GB_ASIAuthenticationDialog : GB_ASIAutorotatingViewController <UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource> {
	GB_ASIHTTPRequest *request;
    GB_ASIAuthenticationType type;
	UITableView *tableView;
	UIViewController *presentingController;
	BOOL didEnableRotationNotifications;
}
+ (void)presentAuthenticationDialogForRequest:(GB_ASIHTTPRequest *)request;
+ (void)dismiss;

@property (retain) GB_ASIHTTPRequest *request;
@property (assign) GB_ASIAuthenticationType type;
@property (assign) BOOL didEnableRotationNotifications;
@property (retain, nonatomic) UIViewController *presentingController;
@end

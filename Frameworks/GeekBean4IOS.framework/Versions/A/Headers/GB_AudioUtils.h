//
//  GB_AudioUtils.h
//  GeekBean4IOS
//
//  Created by gaohang on 14-5-9.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GB_AudioUtils : NSObject

+(void)shock;
+(void)sound:(NSString *)wavPath;

@end

//
//  GB_AlertUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <UIKit/UIKit.h>

@class GB_AlertView;
@interface GB_AlertUtils : NSObject

+(GB_AlertView *)add:(UIView *)view msg:(NSString *)msg;
+(GB_AlertView *)add:(UIView *)view msg:(NSString *)msg hasActivityIndicator:(BOOL)hasActivityIndicator;
+(void)remove:(UIView *)view;
+(void)alert:(UIView *)view msg:(NSString *)msg;
+(void)alert:(UIView *)view msg:(NSString *)msg time:(CGFloat)time;
+(BOOL)hasAlert:(UIView *)view;

@end
//
//  GB_InputAlertView.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-12-7.
//  Copyright (c) 2012年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GB_InputAlertView : UIAlertView

@property(nonatomic, strong) UITextField* inputField;

-(id)initWithTitle:(NSString *)title message:(NSString *)message placeholder:(NSString *)placeholder delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle confirmButtonTitle:(NSString *)confirmButtonTitle keyboardType:(UIKeyboardType)keyboardType;

@end

//
//  GB_ImagePickerUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-27.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GB_ImagePickerType.h"
#import "GB_ImagePickerDelegate.h"

@interface GB_ImagePickerDelegate : NSObject<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIViewController<GB_ImagePickerDelegate> *viewController;

@end

@interface GB_ImagePickerUtils : NSObject

+(BOOL)canUseCamera;

+(BOOL)canUsePhotoLibrary;

+(BOOL)canUseSavedPhotosAlbum;

+(void)pickerImageWithType:(enum GB_ImagePickerType)type viewController:(UIViewController<GB_ImagePickerDelegate> *) viewController;

+(void)pickerImageWithType:(enum GB_ImagePickerType)type allowsEditing:(BOOL)allowsEditing viewController:(UIViewController<GB_ImagePickerDelegate> *) viewController;


@end

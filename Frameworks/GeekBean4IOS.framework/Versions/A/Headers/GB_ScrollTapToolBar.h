//
//  GB_ScrollTapToolBar.h
//  360CaiPiao_iPhoneOS
//
//  Created by gaohang on 14-3-25.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GB_ScrollTapToolBarDelegate <NSObject>

@required
-(void)GB_scrollTapToolBarDidSelect:(NSInteger)index;
@end

@interface GB_ScrollTapToolBar : UIView{
    UIView  *_currentView;
    NSMutableArray *_btnArr;
}

//按钮数量
@property NSInteger numbers;
//按钮标题
@property (strong, nonatomic) NSArray *titleArr;
//两边外边距
@property NSInteger margin;
//文字颜色
@property (strong, nonatomic) UIColor *titleColor;
//选中颜色
@property (strong, nonatomic) UIColor *currentColor;
//选中位置
@property NSInteger currentIndex;
//字体
@property (strong, nonatomic) UIFont *font;

@property (strong, nonatomic) UIImage *bg;

@property (assign, nonatomic) id<GB_ScrollTapToolBarDelegate> delegate;

@property (assign, nonatomic) UIScrollView *scrollView;

-(void)updateCurrentView:(double)arg0;


@end

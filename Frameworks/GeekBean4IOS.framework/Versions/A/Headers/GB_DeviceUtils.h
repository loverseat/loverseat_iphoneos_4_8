//
//  GB_DeviceUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>

@interface GB_DeviceUtils : NSObject

//判断是否是iPhone
+(BOOL)isIPhone;

//判断是否是iPod
+(BOOL)isIPod;

//判断是否是iPad
+(BOOL)isIPad;

//获取操作版本
+(NSString *)getOperatingSystem;

//获取设备型号
+(NSString *)getModel;

//获取屏幕宽度
+(float)getScreenWidth;

//获取屏幕高度
+(float)getScreenHeight;

//获取屏幕缩放比
+(float)getScreenScale;

//获取BundleName;
+(NSString *)getBundleName;

//获取BundleVersion
+(NSString *)getBundleVersion;

//获取BundleIdentifier
+(NSString *)getBundleIdentifier;

//获取IP地址
+(NSString *)getIPAddress;

//获取MAC地址
+(NSString *)getMACaddress;

//判断是否是IOS7以后版本
+(BOOL)isIOS7;

//获取操作系统版本
+(float)getOSVersion;

//获取状态栏增量
+(int)getStatusBarHeightIncremental;

//获取状态栏高度
+(int)getStatusBarHeight;

+(NSString *)getDeviceId;

@end

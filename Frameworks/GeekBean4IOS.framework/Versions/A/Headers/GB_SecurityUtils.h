//
//  GB_SecurityUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>
#import "GB_Alignment.h"

@interface GB_SecurityUtils : NSObject

//MD5加密
+(NSString *)MD5:(NSString *)str;
//base64解密
+ (NSData *)base64decode:(NSString *)string;
//base64加密
+ (NSString *)base64encode:(NSData *)data;
//获取随即数
+(NSString *)getRandomNumber:(int)median;
//获取掩码 用*号遮挡
+(NSString *)getMaskString:(NSString *)str median:(int)median point:(enum GB_Alignment)point;
//获取掩码 用character遮挡
+(NSString *)getMaskString:(NSString *)str median:(int)median point:(enum GB_Alignment)point character:(char *)character;

//与GeekBean4Android中同名方法兼容的DES加密方法
+(NSString *)DESEncodeUniversalAndroidIOSReturnBase64:(NSString *)source key:(NSString *)key;

@end

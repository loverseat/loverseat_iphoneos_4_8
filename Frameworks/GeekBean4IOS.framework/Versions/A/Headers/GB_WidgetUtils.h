//
//  GB_WidgetUtils.h
//  WisdomTree
//
//  Created by GaoHang on 13-5-16.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GB_WidgetUtils : NSObject

+(UIImageView *)getImageView:(CGRect)rect image:(UIImage *)image;
+(UIButton *)getButton:(CGRect)rect image:(UIImage *)image imageH:(UIImage *)imageH id:(id)id sel:(SEL) sel;
+(UIScrollView *)getScrollView:(CGRect)rect color:(UIColor *)color;
+(UILabel *)getLabel:(CGRect)rect title:(NSString *)title font:(UIFont *)font color:(UIColor *)color;
+(UILabel *)getTitleLabel:(NSString *)title color:(UIColor *)color;

@end

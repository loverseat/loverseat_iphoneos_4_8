//
//  GB_BaiduMapViewController.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-5-10.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BaseSelectViewController;
@interface GB_BaiduMapViewController : UIViewController
<UIWebViewDelegate>



-(void)addMarker:(float)lng lat:(float)lat draggable:(BOOL)draggable tag:(int)tag;
-(void)addMarker:(float)lng lat:(float)lat draggable:(BOOL)draggable icon:(NSString *)icon tag:(int)tag;

@property (strong, nonatomic) NSMutableArray *markerData;
@property (strong, nonatomic) UIWebView *webView;
//默认14
@property int zoom;
@property float centerLng;
@property float centerLat;
@property (strong, nonatomic) NSString *centerCity;
@property (strong, nonatomic) NSString *key;

-(NSString *)getPointByCity:(NSString *)city;

-(void)GB_mapDidSelect:(NSDictionary *)info;
-(void)GB_mapDidDragend:(NSDictionary *)info;

@end

//
//  GB_PreviewViewController.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-5-29.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <QuickLook/QuickLook.h>
#import "GB_DownLoadDelegate.h"

@interface GB_PreviewViewController : UIViewController
<QLPreviewControllerDataSource, QLPreviewControllerDelegate, UINavigationControllerDelegate, GB_DownLoadDelegate>

@property (nonatomic, strong) UIView *preView;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *type;

@end

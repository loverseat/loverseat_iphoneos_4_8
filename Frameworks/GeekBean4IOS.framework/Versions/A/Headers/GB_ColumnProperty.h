//
//  GB_ColumnProperty.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//


enum GB_ColumnProperty{
    GB_ColumnPropertyPrimarkey,//主键
    GB_ColumnPropertyAutoIncrement//自增
};

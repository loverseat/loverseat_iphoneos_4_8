//
//  GB_KeyboardDelegate.h
//  GeekBean4IOS
//
//  Created by GaoHang on 14-2-16.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import "GB_BaseDelegate.h"

@protocol GB_KeyboardDelegate<GB_BaseDelegate,UIGestureRecognizerDelegate>

@required

-(void)GB_keyboardWillShow:(NSNotification *)note;

-(void)GB_keyboardWillHide:(NSNotification *)note;

@optional

-(void)GB_singleTap;

-(void)GB_keyboardDidShow:(NSNotification *)note;

-(void)GB_keyboardDidHide:(NSNotification *)note;
@end

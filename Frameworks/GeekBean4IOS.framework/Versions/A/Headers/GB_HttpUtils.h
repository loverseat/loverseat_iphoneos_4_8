//
//  GB_HttpUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-19.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GB_HttpUtils : NSObject

//是否有网络
+(BOOL)hasNetWork;

//是否是wifi
+(BOOL)isWifi;

//是否是3G
+(BOOL)is3G;

+(UIImage *)getImageFromUrl:(NSString *)url;

+(NSData *)getDataFromUrl:(NSString *)url;

@end

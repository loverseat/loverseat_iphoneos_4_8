//
//  GB_CustomIndicatorPageControl.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <UIKit/UIKit.h>

@interface GB_CustomIndicatorPageControl : UIPageControl

@property (nonatomic, retain) UIImage *normalImage;
@property (nonatomic, retain) UIImage *highlightedImage;

-(void)setCurrentPage:(NSInteger)currentPage;
-(void)setImagePageStateNormal:(UIImage*)image;
-(void)setImagePageStateHighlighted:(UIImage *)image;
-(void)updateDots;

@end

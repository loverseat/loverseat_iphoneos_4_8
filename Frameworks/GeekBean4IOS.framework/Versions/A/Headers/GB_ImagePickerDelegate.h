//
//  GB_ImagePickerDelegate.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-27.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import "GB_BaseDelegate.h"
#import <UIKit/UIKit.h>

@protocol GB_ImagePickerDelegate<GB_BaseDelegate>

@required

-(void)GB_imagePickerDidFinish:(UIImage *)image;

-(void)GB_imagePickerDismissFinish:(UIImage *)image;

@optional

-(void)GB_imagePickerDidCancel;

@end
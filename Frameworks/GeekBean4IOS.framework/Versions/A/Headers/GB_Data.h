//
//  GB_Data.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GB_Data : NSObject

@property (nonatomic, strong) NSData *data;

-(id)initWithData:(NSData *)data;

@end

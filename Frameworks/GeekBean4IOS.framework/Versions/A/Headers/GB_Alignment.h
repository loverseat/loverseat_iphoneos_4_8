//
//  GB_Alignment.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

enum GB_Alignment{
    GB_AlignmentLeft,
    GB_AlignmentCenter,
    GB_AlignmentRight
};

//
//  GB_BackgroundCell.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <UIKit/UIKit.h>

@interface GB_BackgroundCell : UITableViewCell

@property (strong, nonatomic) UIImageView *bg_h;
@property (strong, nonatomic) UIImageView *bg_n;

-(void)setBackgroundImageNormal:(UIImage *)normal select:(UIImage *)select;

@end

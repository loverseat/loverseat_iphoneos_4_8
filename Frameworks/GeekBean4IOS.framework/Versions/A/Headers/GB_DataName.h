//
//  GB_DataName.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GB_Data.h"

@interface GB_DataName : GB_Data

@property (nonatomic, strong) NSString *name;

-(id)initWithDataName:(NSData *)data name:(NSString *)name;

@end

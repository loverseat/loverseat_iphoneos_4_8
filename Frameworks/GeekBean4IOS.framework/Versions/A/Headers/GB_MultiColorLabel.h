//
//  GB_MultiColorLabel.h
//  360CaiPiao_iPhoneOS
//
//  Created by gaohang on 14-3-25.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#define GB_MultiColorLabelText(R,G,B,A,text) [NSString stringWithFormat:@"<font color=\"%d,%d,%d,%d\">%@",R,G,B,A,text]


@interface GB_MultiColorLabel : UILabel
{
    float frameXOffset;
    float frameYOffset;
    
    NSAttributedString *attString;
    
    UIFont *font;
    UIColor  *color;
    UIColor  *strokeColor;
    float    strokeWidth;
    
    NSMutableArray *images;
}
@property (retain,nonatomic) UIFont *font;
@property (retain,nonatomic) UIColor  *color;
@property (retain,nonatomic) UIColor  *strokeColor;
@property (assign,nonatomic) float    strokeWidth;

@property (retain,nonatomic) NSAttributedString *attString;

-(NSAttributedString *)attrStringFromMarkup:(NSString *)markup;
@end

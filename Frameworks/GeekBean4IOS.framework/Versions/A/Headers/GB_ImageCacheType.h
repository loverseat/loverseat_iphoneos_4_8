//
//  GB_ImageCacheType.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

enum GB_ImageCacheType{
    GB_ImageCacheTypeNone,//不使用缓存
    GB_ImageCacheTypeSave,//保存 IMAGEPATH不能为空
    GB_ImageCacheTypeLoad,//读取 IMAGEPATH不能为空
    GB_ImageCacheTypeAll//如果有 从缓存读取 如果没有 存入 IMAGEPATH不能为空、IMAGEURL不能为空
};

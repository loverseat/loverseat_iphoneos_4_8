//
//  GB_PictureViewController.h
//  XianLvKe_iPhoneOS
//
//  Created by gaohang on 14-5-19.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GB_LoadImageDelegate.h"
@interface GB_PictureViewController : UIViewController
<UIScrollViewDelegate, GB_LoadImageDelegate>


//初始化  arr中可以是UIImage 或者 是图片URL
-(id)initWithSourceArr:(NSArray *)arr index:(NSInteger)index;

-(id)initWithSourceArr:(NSArray *)arr index:(NSInteger)index scrollViewFrame:(CGRect)rect;

@end

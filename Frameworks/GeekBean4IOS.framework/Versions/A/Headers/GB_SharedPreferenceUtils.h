//
//  GB_SharedPreferenceUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-29.
//
//

#import <Foundation/Foundation.h>

@interface GB_SharedPreferenceUtils : NSObject

+(BOOL)hasObject:(NSString *)key;
+(void)setObject:(id)obj forKey:(NSString *)key;
+(id)getObjectForKey:(NSString *)key;
+(void)removeObjectForKey:(NSString *)key;
+(void)setTimeMillisForKey:(NSString *)key;
+(double)getTimeMillisForKey:(NSString *)key;
+(BOOL)hasTimeOut:(double)time forKey:(NSString *)key;


@end

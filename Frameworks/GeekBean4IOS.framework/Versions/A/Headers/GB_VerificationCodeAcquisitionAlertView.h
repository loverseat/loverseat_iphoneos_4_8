//
//  GB_VerificationCodeAcquisitionAlertView.h
//  EManager
//
//  Created by GaoHang on 13-5-13.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GB_VerificationCodeAcquisitionAlertView : UIAlertView

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UITextField *inputField;

-(id)initWithTitle:(NSString *)title image:(UIImage *)image delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

@end
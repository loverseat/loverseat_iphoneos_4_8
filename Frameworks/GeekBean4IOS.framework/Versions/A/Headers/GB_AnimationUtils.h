//
//  GB_RotateTypeAnimationUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-12-5.
//  Copyright (c) 2012年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

enum GB_RotateType{
    GB_RotateTypeClockWiseHalfWithX,//绕X轴顺时针转动半周
    GB_RotateTypeCounterClockWiseHalfWithX,//绕X轴逆时针转动半周
    GB_RotateTypeClockWiseHalfWithY,//绕Y轴顺时针转动半周
    GB_RotateTypeCounterClockWiseHalfWithY,//绕Y轴逆时针转动半周
    GB_RotateTypeClockWiseHalfWithZ,//绕Z轴顺时针转动半周
    GB_RotateTypeCounterClockWiseHalfWithZ,//绕Z轴逆时针转动半周
    GB_RotateTypeClockWiseWithX,//绕X轴顺时针转动一周
    GB_RotateTypeCounterClockWiseWithX,//绕X轴逆时针转动一周
    GB_RotateTypeClockWiseWithY,//绕Y轴顺时针转动一周
    GB_RotateTypeCounterClockWiseWithY,//绕Y轴逆时针转动一周
    GB_RotateTypeClockWiseWithZ,//绕Z轴顺时针转动一周
    GB_RotateTypeCounterClockWiseWithZ,//绕Z轴逆时针转动一周
};

@interface GB_AnimationUtils : NSObject

+(void)rotate:(UIView *)view duration:(float)duration rotateType:(enum GB_RotateType)type isHalfStart:(BOOL)isHalfStart;

@end

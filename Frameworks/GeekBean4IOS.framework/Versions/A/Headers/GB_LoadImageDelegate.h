//
//  GB_LoadImageDelegate.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//
#import "GB_BaseDelegate.h"

@protocol GB_LoadImageDelegate<GB_BaseDelegate>

@required

-(void)GB_loadImageDidFinish:(UIImage *)image container:(id)container tag:(int) tag;

@end
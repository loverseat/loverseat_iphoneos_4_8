//
//  GB_KeyValue.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-20.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GB_KeyValue : NSObject

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) id value;

-(id)initWithKeyValue:(NSString *)key value:(id)value;
+(NSArray *)getKeyValueList:(NSDictionary *)dic;

@end

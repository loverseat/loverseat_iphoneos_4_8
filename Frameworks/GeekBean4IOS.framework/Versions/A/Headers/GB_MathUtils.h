//
//  GB_MathUtils.h
//  GeekBean4IOS
//
//  Created by gaohang on 14-3-17.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GB_MathUtils : NSObject
+(NSString *)toHexString:(int)int10;
@end

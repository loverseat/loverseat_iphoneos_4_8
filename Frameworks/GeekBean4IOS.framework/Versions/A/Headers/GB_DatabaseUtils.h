//
//  GB_DatabaseUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import "GB_ColumnProperty.h"
#import "GB_ColumnType.h"
#import "GB_ColumnOrder.h"

@class GB_DatabaseInfo;
@class GB_DatabaseInfoList;
@interface GB_DatabaseUtils : NSObject

+(NSString *)getDatabasePath:(NSString *)name;

+(long)getLastInsertRowId;

+(BOOL)openDataBase:(NSString *)name;
+(BOOL)executeSQLite:(NSString *)str;
+(BOOL)executeSQLiteArr:(NSArray *)arr;


#pragma mark - execute sqlite string method
//是否有返回结果
+(BOOL)hasResultByExecuteSQLite:(NSString *)sql;
//是否Replace成功 只支持NSString 和 NSNumber类型
+(BOOL)executeReplaceInfo:(GB_DatabaseInfo *)info table:(NSString *)name;
//是否Replace成功 包含事务 插入效率高 只支持NSString 和 NSNumber类型
+(BOOL)executeReplaceInfoList:(GB_DatabaseInfoList *)infoList table:(NSString *)name;

//返回查询结果 按照数据库字段名称存储
+(NSArray *)executeSelectSQLite:(NSString *)sqliteString;

//是否存在某个表
+(BOOL)isExistTable:(NSString *)tabName;

#pragma mark - create sqlite string method
//创建表SQL语句
+(NSString *)getCreateSQLiteString:(GB_DatabaseInfo *)info table:(NSString *)name;
//Replace SQL语句
+(NSString *)getReplaceSQLiteString:(GB_DatabaseInfo *)info table:(NSString *)name;
//Select整条SQL语句
+(NSString *)getSelectSQLiteString:(GB_DatabaseInfo *)info table:(NSString *)name;
//Select整表SQL语句
+(NSString *)getSelectSQLiteString:(NSString *)name;
//Delete整条记录  SQL语句
+(NSString *)getDeleteSQLiteString:(GB_DatabaseInfo *)info table:(NSString *)name;
//Update任意条件记录  SQL语句
+(NSString *)getUpdateSQLiteString:(GB_DatabaseInfo *)info table:(NSString *)name;
@end

@interface GB_DatabaseInfo : NSObject

@property (assign, nonatomic) SEL selector;
@property (strong, nonatomic) NSMutableDictionary *info;

-(void)addColumnProperty:(NSString *)column Type:(enum GB_ColumnProperty)property;
//增加值 VALUES
-(void)setValue:(id)value forColumn:(NSString *)column;
//获取值 VALUES
-(id)getValueForColumn:(NSString *)column;
//增加规则 WHERE
-(void)setRule:(NSString *)value forColumn:(NSString *)column;
//获取规则 WHERE
-(id)getRuleForColumn:(NSString *)column;
//增加排序 ORDER
-(void)setOrder:(enum GB_ColumnOrder)order forColumn:(NSString *)column;
//获取规则 ORDER
-(NSString *)getOrderForColumn:(NSString *)column;
//清除info
-(void)clear;

//设置类型 TEXT INTEGER
-(void)setType:(enum GB_ColumnType)type forColumn:(NSString *)column;
//获取类型
-(id)getTypeForColumn:(NSString *)column;

-(id)getObjectForKey:(NSString *)key;


-(NSArray *)allKeys;
-(NSInteger)getCount;
-(BOOL)isBlank;
-(BOOL)isNotBlank;

@end

@interface GB_DatabaseInfoList : NSObject

@property (strong, nonatomic) NSMutableArray *infoList;

-(void)addInfo:(GB_DatabaseInfo *)info;
-(GB_DatabaseInfo *)getInfo:(int)index;
-(NSArray *)allInfo;

@end

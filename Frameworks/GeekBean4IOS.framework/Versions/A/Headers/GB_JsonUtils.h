//
//  GB_JsonUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>

@interface GB_JsonUtils : NSObject

//解析列表Json字符串
+(NSArray *)getArrayByJsonString:(NSString *)str;

//解析字典Json字符串
+(NSDictionary *)getDictionaryByJsonString:(NSString *)str;

//列表生成Json字符串
+(NSString *)getJsonStringByArray:(NSArray *)array;

//字典生成Json字符串
+(NSString *)getJsonStringByDictionary:(NSDictionary *)dictionary;

+(NSString *)getJsonStringByArray:(NSArray *)array encoding:(NSStringEncoding)encoding;

+(NSArray *)getArrayByJsonString:(NSString *)str encoding:(NSStringEncoding)encoding;

+(NSDictionary *)getDictionaryByJsonString:(NSString *)str encoding:(NSStringEncoding)encoding;

+(NSString *)getJsonStringByDictionary:(NSDictionary *)dictionary encoding:(NSStringEncoding)encoding;

+(NSData *)getJSONDataFromString:(NSString *)str encoding:(NSStringEncoding)encoding;

+(NSData *)getJSONDataFromObject:(id)obj;

@end

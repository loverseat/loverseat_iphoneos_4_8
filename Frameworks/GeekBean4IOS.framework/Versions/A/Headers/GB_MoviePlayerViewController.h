//
//  GB_MoviePlayerViewController.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-12-11.
//  Copyright (c) 2012年 GaoHang. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>

@interface GB_MoviePlayerViewController : UIViewController

@property (strong , nonatomic) MPMoviePlayerController *moviePlayerViewController;

-(id)initWithUrl:(NSString *)url;
-(void)GB_moviePlaybackStatePlaying;
-(void)GB_moviePlaybackStatePaused;
-(void)GB_moviePlaybackStateStopped;
-(void)GB_moviePlaybackStateSeekingBackward;
-(void)GB_moviePlaybackStateSeekingForward;
-(void)GB_moviePlaybackStateInterrupted;
-(void)GB_movieFinishReasonUserExited;
-(void)GB_movieFinishReasonPlaybackError;
-(void)GB_movieFinishReasonPlaybackEnded;

@end

//
//  GB_ImageUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 14-10-29.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GB_ImageUtils : NSObject
//填充效果
+(UIImage *)addFillingEffect:(UIImage *)image w:(int)w h:(int)h;

//模糊效果
+(UIImage *)addBlurryEffect:(UIImage *)image level:(CGFloat)level;
@end

//
//  GB_DateUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>

@interface GB_DateUtils : NSObject

//格式化毫秒串
+(NSString *)getFormatStringBy10Median:(NSString *)format timeMillis:(double)timeMillis;

+(NSString *)getFormatStringByNow:(NSString *)format;

+(NSString *)getDescriptionTimeBy10Median:(double)timeMillis isTime:(BOOL)isTime;

+(int)get10MedianByFormatString:(NSString *)formatStr format:(NSString *)format;

//获取date是本月第几周，从0开始
+(NSInteger)getWeekIndexInMonth:(NSDate *)date;

//获取date是本年第几周，从0开始
+(NSInteger)getWeekIndexInYear:(NSDate *)date;

//获取date是星期几，星期一：0 星期二：1  ....
+(NSInteger)getWeekDayIndex:(NSDate *)date;

@end

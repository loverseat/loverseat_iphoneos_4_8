//
//  GB_VerificationUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>

@interface GB_VerificationUtils : NSObject

//是否是手机号码
+(BOOL)isEmailAddress:(NSString *)str;

//是否是邮箱地址
+(BOOL)isMobilePhoneNumber:(NSString *)str;

//是否是URL
+(BOOL)isHttpUrl:(NSString *)str;

//是否是纯字母
+(BOOL)isPlurABC:(NSString *)str;

//是否是纯数字
+(BOOL)isPlurInt:(NSString *)str;

//是否是纯数字或者字母
+(BOOL)isPlurIntABC:(NSString *)str;

//是否是身份证
+(BOOL)isIdentityCard:(NSString *)str;

@end

//
//  GB_MacroUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 14-3-7.
//  Copyright (c) 2014年 GaoHang. All rights reserved.
//

#define GB_UIColorFromRGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define GB_NSStringFromInt(intValue) [NSString stringWithFormat:@"%d",intValue]
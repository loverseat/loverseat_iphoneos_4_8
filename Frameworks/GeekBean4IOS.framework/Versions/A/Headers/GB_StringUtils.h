//
//  GB_StringUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>

@interface GB_StringUtils : NSObject

//是否为空
+(BOOL)isEmpty:(NSString *)str;

//是否为空白
+(BOOL)isBlank:(NSString *)str;

//是否不为空
+(BOOL)isNotEmpty:(NSString *)str;

//是否不为空白
+(BOOL)isNotBlank:(NSString *)str;

//是否为数字
+(BOOL)isNumeric:(NSString *)str;

//分割字符串
+(NSArray *)split:(NSString *)src separator:(NSString *)separator;

//是否为子字符串
+(BOOL)isSubString:(NSString *)str key:(NSString *)substr;

//url编码
+(NSString *)urlEncode: (NSString *) input;

//url解码
+(NSString *)urlDecode: (NSString *) input;

+(BOOL)isString:(NSString *)str;

//获取字符串第一个字的首字母
+(NSString *)getPinYinFirstLetter:(NSString *)str;

@end

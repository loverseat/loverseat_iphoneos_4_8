//
//  GB_ColumnType.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

enum GB_ColumnType{
    GB_ColumnTypeText,
    GB_ColumnTypeString,
    GB_ColumnTypeInteger
};

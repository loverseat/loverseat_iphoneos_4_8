//
//  GB_NetWorkDelegate.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//
#import "GB_BaseDelegate.h"

@protocol GB_NetWorkDelegate<GB_BaseDelegate> 

@required

-(void)GB_requestDidFailed:(int)tag;

-(void)GB_requestDidSuccess:(NSString *)str tag:(int)tag;

@optional

-(void)GB_requestDidCancel:(int)tag;


@end

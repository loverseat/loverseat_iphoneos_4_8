//
//  GB_ToolUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GB_KeyboardDelegate.h"


@interface GB_ToolUtils : NSObject

//是否为空白对象
+(BOOL)isBlank:(id)obj;

//是否不为空白对象
+(BOOL)isNotBlank:(id)obj;

//是否为空对象
+(BOOL)isNull:(id)obj;

//是否不为空对象
+(BOOL)isNotNull:(id)obj;

//移除背景
+(void)removeBackground:(UISearchBar *)searchBar;

//获取字体高度
+(float)getFontHeight:(UIFont *)font;

//获取多行字体高度
+(float)getFontHeight:(UIFont *)font numberOfLines:(int)number;

//获取文字size
+(CGSize)getTextSize:(NSString *)text font:(UIFont *)font size:(CGSize)size;

//获取文字宽度
+(float)getTextWidth:(NSString *)text font:(UIFont *)font size:(CGSize)size;

//获取文字高度
+(float)getTextHeight:(NSString *)text font:(UIFont *)font size:(CGSize)size;

//根据文字和最大宽度获取最大字号
+(CGFloat)getTextFontSizeWithTextWidth:(NSString *)str width:(CGFloat)width;

//post通知
+(void)postNotificationName:(NSString *)name object:(id)object;

//修改UISearchBar取消按钮的title，UISearchBar的showsCancelButton必须为true
+(void)setSearchBarCancelBtnTitle:(UISearchBar *)searchBar title:(NSString *)title;

+(void)addKeyboardNotification:(id<GB_KeyboardDelegate>)delegate;

+(void)addTapGestureRecognizer:(UIView *)view delegate:(id<GB_KeyboardDelegate>)delegate;

+(void)keyboardWillHide:(UIScrollView *)scrollView;

+(void)keyboardWillShow:(NSNotification *)note scrollView:(UIScrollView *)scrollView fieldArray:(NSArray *)array rootViewframe:(CGRect)rect;

//输出所有字体名称
+(void)printAllFontName;

//获取纯色图片
+(UIImage *)getPureImg:(UIColor *)color size:(CGSize)size;

+(void)showActivityIndicator:(BOOL)isTrue;

//添加iCould不备份Path
+(BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)filePath;

@end

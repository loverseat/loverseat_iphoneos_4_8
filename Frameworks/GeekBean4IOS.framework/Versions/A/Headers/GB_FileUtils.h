//
//  GB_FileUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 12-11-28.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GB_FileUtils : NSObject

//获取应用程序根目录
+(NSString *)getAppPath;

//获取缓存根目录
+(NSString *)getCachesPath;

//获取缓存目录
+(NSString *)getCachesPath:(NSString *)path;

//是否存在这个目录
+(BOOL)hasPath:(NSString *)path;

//创建目录
+(void)createPath:(NSString *)path;

//打印目录
+(void)printFileListFromDirectory:(NSString *)path;

+(NSArray *)getFileListFromDirectory:(NSString *)path;

//清楚缓存
+(void)clearGeekBeanCache;

+(UIImage *)getImageFromPath:(NSString *)path;

+(NSData *)getDataFromPath:(NSString *)path;

+(NSString *)getCompletePath:(NSString *)directory name:(NSString *)name;

+(void)writeDataToPath:(NSData *)data path:(NSString *)path;

+(NSString *)getGeekBeanPath;

+(NSString *)getGeekBeanTempPath;

+(NSString *)getGeekBeanCachePath;

@end

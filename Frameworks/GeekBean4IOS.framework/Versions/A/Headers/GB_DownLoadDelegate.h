//
//  GB_DownLoadDelegate.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-21.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//
#import "GB_BaseDelegate.h"

@protocol GB_DownLoadDelegate<GB_BaseDelegate>

@required

-(void)GB_downLoadDidFinished:(NSData *)data path:(NSString *)path time:(NSInteger)time;

-(void)GB_downLoadDidFailed:(NSInteger)fileSize time:(NSInteger)time;

-(void)GB_downLoadSizeDidChanage:(NSInteger)totalSize fileSize:(NSInteger)fileSize;

@end

//
//  GB_NetWorkUtils.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-19.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GB_LoadImageDelegate.h"
#import "GB_NetWorkDelegate.h"
#import "GB_DownLoadDelegate.h"
#import "GB_ImageCacheType.h"

@class GB_ASIHTTPRequest;
@class GB_ASIFormDataRequest;

@interface GB_NetWorkUtils : NSObject


+(void)cancelRequest;
+(BOOL)checkNetWork:(UIView *)view;
+(void)useImage:(UIImage *)image container:(id)container;


+(BOOL)loadImage:(NSString *)url container:(id)container;

+(BOOL)loadImage:(NSString *)url container:(id)container delegate:(id<GB_LoadImageDelegate>)delegate;

+(BOOL)loadImage:(NSString *)url container:(id)container type:(enum GB_ImageCacheType)type;

+(BOOL)loadImage:(NSString *)url container:(id)container type:(enum GB_ImageCacheType)type delegate:(id<GB_LoadImageDelegate>)delegate;

+(BOOL)loadImage:(NSString *)url container:(id)container type:(enum GB_ImageCacheType)type delegate:(id<GB_LoadImageDelegate>)delegate tag:(int)tag;

+(BOOL)loadImage:(NSString *)url container:(id)container type:(enum GB_ImageCacheType)type directory:(NSString *)directory tag:(int)tag delegate:(id<GB_LoadImageDelegate>)delegate;

+(void)startGetAsyncRequest:(NSString *)url delegate:(id<GB_NetWorkDelegate>)delegate tag:(int)tag;

+(void)startPostAsyncRequest:(NSString *)url list:(NSArray *)list delegate:(id<GB_NetWorkDelegate>)delegate tag:(int)tag;

+(void)startSingleGetAsyncRequest:(NSString *)url delegate:(id<GB_NetWorkDelegate>)delegate tag:(int)tag;

+(void)startSinglePostAsyncRequest:(NSString *)url list:(NSArray *)list delegate:(id<GB_NetWorkDelegate>)delegate tag:(int)tag;


+(void)downLoadFile:(NSString *)url type:(NSString *)type directory:(NSString *)directory delegate:(id<GB_DownLoadDelegate>)delegate tag:(int)tag;

+(void)downLoadFile:(NSString *)url type:(NSString *)type delegate:(id<GB_DownLoadDelegate>)delegate;

+(void)downLoadFile:(NSString *)url type:(NSString *)type;

+(NSString*)nameValString: (NSDictionary*)dic;

+(void)startMultipartPostAsyncRequest:(NSString *)url params:(NSArray *)params imageParams:(NSArray *)imageParams delegate:(id<GB_NetWorkDelegate>)delegate tag:(int)tag;

+(void)addRequestHeader:(NSDictionary *)headerDic;

+(NSString*)serialization:(id)parms;

//清除SessionCookies
+(void)cleanSessionCookies;



+(void)changeNetWorkingState:(int)privateTag;

+(BOOL)isPublicNetWorking;

+(BOOL)isPrivateNetWorking;

+(BOOL)isNormalNetWorking;

@end

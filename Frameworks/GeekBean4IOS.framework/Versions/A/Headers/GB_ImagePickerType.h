//
//  GB_ImagePickerType.h
//  GeekBean4IOS
//
//  Created by GaoHang on 13-12-27.
//  Copyright (c) 2013年 GaoHang. All rights reserved.
//

enum GB_ImagePickerType{
    GB_ImagePickerTypeNone,
    GB_ImagePickerTypeCamera,//从摄像头选取
    GB_ImagePickerTypePhotoLibrary,//从相册选取
    GB_ImagePickerTypeSavedPhotosAlbum//相机胶卷
};

